import type { Schema, Attribute } from '@strapi/strapi';

export interface CommonAchievementList extends Schema.Component {
  collectionName: 'components_common_achievement_lists';
  info: {
    displayName: 'AchievementList';
    icon: 'star';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    achievements: Attribute.Component<'common.achievement', true>;
  };
}

export interface CommonAchievement extends Schema.Component {
  collectionName: 'components_common_achievements';
  info: {
    displayName: 'Achievement';
    icon: 'star';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    years: Attribute.String;
    image: Attribute.Media<'images'> & Attribute.Required;
    isWinner: Attribute.Boolean & Attribute.Required & Attribute.DefaultTo<true>;
  };
}

export interface CommonApiGameList extends Schema.Component {
  collectionName: 'components_common_api_game_lists';
  info: {
    displayName: 'ApiGameList';
    icon: 'bulletList';
  };
  attributes: {
    gameList: Attribute.String & Attribute.CustomField<'plugin::api-game-list.api-game-list'>;
  };
}

export interface CommonApiGame extends Schema.Component {
  collectionName: 'components_common_api_games';
  info: {
    displayName: 'ApiGame';
    icon: 'hockey-puck';
  };
  attributes: {
    game: Attribute.String & Attribute.CustomField<'plugin::api-game.api-game'>;
  };
}

export interface CommonEmbedVideo extends Schema.Component {
  collectionName: 'components_common_embed_videos';
  info: {
    displayName: 'EmbedVideo';
    description: '';
  };
  attributes: {
    type: Attribute.Enumeration<['youtube', 'facebook', 'ceskatelevize', 'o2tvsport', 'other']> &
      Attribute.Required &
      Attribute.DefaultTo<'youtube'>;
    url: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 3;
      }>;
  };
}

export interface CommonGallery extends Schema.Component {
  collectionName: 'components_common_galleries';
  info: {
    displayName: 'Gallery';
    icon: 'images';
    description: '';
  };
  attributes: {
    gallery: Attribute.Relation<'common.gallery', 'oneToOne', 'api::gallery.gallery'>;
    text: Attribute.RichText &
      Attribute.CustomField<
        'plugin::ckeditor5.CKEditor',
        {
          preset: 'custom';
        }
      >;
  };
}

export interface CommonOfficialList extends Schema.Component {
  collectionName: 'components_common_official_lists';
  info: {
    displayName: 'OfficialList';
    icon: 'bulletList';
    description: '';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    officials: Attribute.Component<'common.official', true> & Attribute.Required;
    showContactInformation: Attribute.Boolean & Attribute.Required & Attribute.DefaultTo<true>;
  };
}

export interface CommonOfficial extends Schema.Component {
  collectionName: 'components_common_officials';
  info: {
    displayName: 'Official';
    icon: 'user';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    person: Attribute.Relation<'common.official', 'oneToOne', 'api::person.person'>;
  };
}

export interface CommonPartnerList extends Schema.Component {
  collectionName: 'components_common_partner_lists';
  info: {
    displayName: 'PartnerList';
    icon: 'th-list';
  };
  attributes: {
    name: Attribute.String & Attribute.Required;
    slug: Attribute.String & Attribute.Required;
    partners: Attribute.Component<'common.partner', true> & Attribute.Required;
  };
}

export interface CommonPartner extends Schema.Component {
  collectionName: 'components_common_partners';
  info: {
    displayName: 'Partner';
    icon: 'id-card-alt';
  };
  attributes: {
    name: Attribute.String & Attribute.Required;
    url: Attribute.String;
    image: Attribute.Media<'images'> & Attribute.Required;
  };
}

export interface CommonRichText extends Schema.Component {
  collectionName: 'components_common_rich_texts';
  info: {
    displayName: 'RichText';
    icon: 'align-justify';
    description: '';
  };
  attributes: {
    text: Attribute.RichText &
      Attribute.CustomField<
        'plugin::ckeditor5.CKEditor',
        {
          preset: 'custom';
        }
      >;
  };
}

export interface PlayerTeamPlayer extends Schema.Component {
  collectionName: 'components_player_team_players';
  info: {
    displayName: 'TeamPlayer';
    icon: 'user';
    description: '';
  };
  attributes: {
    team: Attribute.Relation<'player.team-player', 'oneToOne', 'api::category.category'>;
    number: Attribute.Integer;
    post: Attribute.Enumeration<['Brank\u00E1\u0159', 'Obr\u00E1nce', '\u00DAto\u010Dn\u00EDk']>;
    stick: Attribute.Enumeration<['Lev\u00E1', 'Prav\u00E1']>;
    teamName: Attribute.String & Attribute.Private;
  };
}

declare module '@strapi/types' {
  export module Shared {
    export interface Components {
      'common.achievement-list': CommonAchievementList;
      'common.achievement': CommonAchievement;
      'common.api-game-list': CommonApiGameList;
      'common.api-game': CommonApiGame;
      'common.embed-video': CommonEmbedVideo;
      'common.gallery': CommonGallery;
      'common.official-list': CommonOfficialList;
      'common.official': CommonOfficial;
      'common.partner-list': CommonPartnerList;
      'common.partner': CommonPartner;
      'common.rich-text': CommonRichText;
      'player.team-player': PlayerTeamPlayer;
    }
  }
}
