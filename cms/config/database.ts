export default ({ env }) => ({
  connection: {
    client: 'postgres',
    connection: {
      host: env('POSTGRES_HOST', 'localhost'),
      port: env.int('POSTGRES_PORT', 5432),
      database: env('POSTGRES_DATABASE', 'kert_park_web'),
      user: env('POSTGRES_USER', 'kert_park_web'),
      password: env('POSTGRES_PASSWORD', 'kert_park_web'),
    },
  },
});
