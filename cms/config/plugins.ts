module.exports = ({ env }) => {
  const webPath = env('WEB_PATH');

  return {
    'api-game': {
      enabled: true,
      resolve: './src/plugins/api-game', // path to plugin folder
    },
    'api-game-list': {
      enabled: true,
      resolve: './src/plugins/api-game-list', // path to plugin folder
    },
    graphql: {
      config: {
        endpoint: '/graphql',
        shadowCRUD: true,
        playgroundAlways: false,
        depthLimit: 20,
        amountLimit: 1000,
        apolloServer: {
          tracing: false,
          introspection: true,
        },
      },
    },
    'preview-button': {
      config: {
        contentTypes: [
          {
            uid: 'api::club-page.club-page',
            draft: {
              url: `${webPath}/klub/{slug}/preview`,
            },
            published: {
              url: `${webPath}/klub/{slug}`,
            },
          },
          {
            uid: 'api::post.post',
            draft: {
              url: `${webPath}/{slug}/preview`,
            },
            published: {
              url: `${webPath}/{slug}`,
            },
          },
        ],
      },
    },
    'social-graphics': {
      enabled: true,
      resolve: './src/plugins/social-graphics', // path to plugin folder
    },
    upload: {
      config: {
        provider: 'strapi-provider-upload-php-file-upload',
        sizeLimit: 5 * 1024 * 1024, // 5mb in bytes
        providerOptions: {
          apiPath: env('PHP_FILE_UPLOAD_APIPATH'),
          filesPath: env('PHP_FILE_UPLOAD_FILESPATH'),
        },
        breakpoints: {
          large: 1200,
          medium: 840,
          small: 540,
        },
      },
    },
  };
};
