import { Strapi } from '@strapi/strapi';
import { playerNexusResolver, playerNexusType, playersNexusResolver } from './api/player/customGraphql';
import { subscribeTeamPlayerLifecycle } from './api/player/customLifecycle';
import {
  creatorNexusType,
  dateRangeInputNexusType,
  postByApiGameNexusResolver,
  postBySlugNexusResolver,
  postCompositeResponseCollectionNexusType,
  postCompositeResponseMetaNexusType,
  postCompositeUnionNexusType,
  postExtendedNexusType,
  postExternalCompositeNexusType,
  postsByApiGamesNexusResolver,
  postsCompositeNexusResolver,
} from './api/post/customGraphql';

export default {
  /**
   * An asynchronous register function that runs before
   * your application is initialized.
   *
   * This gives you an opportunity to extend code.
   */
  register({ strapi }: { strapi: Strapi }) {
    const extensionService = strapi.plugin('graphql').service('extension');
    extensionService.shadowCRUD('api::game-metadata.game-metadata').disable();
    extensionService.shadowCRUD('api::category.category').disableMutations();
    extensionService.shadowCRUD('api::club-page.club-page').disableMutations();
    extensionService.shadowCRUD('api::external-post.external-post').disableMutations();
    extensionService.shadowCRUD('api::gallery.gallery').disableMutations();
    extensionService.shadowCRUD('api::partner.partner').disableMutations();
    extensionService.shadowCRUD('api::person.person').disableMutations();
    extensionService.shadowCRUD('api::post.post').disableMutations();
    extensionService.shadowCRUD('api::status.status').disableMutations();
    extensionService.shadowCRUD('api::player.player').disable();

    const extension = ({ nexus }) => ({
      types: [
        // Player
        playerNexusType(nexus),
        playerNexusResolver({ nexus, strapi }),
        playersNexusResolver({ nexus, strapi }),
        // Post
        creatorNexusType(nexus),
        dateRangeInputNexusType(nexus),
        postExtendedNexusType(nexus),
        postBySlugNexusResolver({ nexus, strapi }),
        postByApiGameNexusResolver({ nexus, strapi }),
        postsByApiGamesNexusResolver({ nexus, strapi }),
        // PostComposite
        postExternalCompositeNexusType(nexus),
        postCompositeUnionNexusType(nexus),
        postCompositeResponseMetaNexusType(nexus),
        postCompositeResponseCollectionNexusType(nexus),
        postsCompositeNexusResolver({ nexus, strapi }),
      ],
      resolversConfig: {
        'Query.postBySlug': {
          auth: {
            scope: ['api::post.post.find'],
          },
        },
        'Query.postByApiGame': {
          auth: {
            scope: ['api::post.post.find'],
          },
        },
        'Query.postsByApiGames': {
          auth: {
            scope: ['api::post.post.find'],
          },
        },
        'Query.postsComposite': {
          auth: {
            scope: ['api::post.post.find'],
          },
        },
        'Query.player': {
          auth: {
            scope: ['api::player.player.find'],
          },
        },
        'Query.players': {
          auth: {
            scope: ['api::player.player.find'],
          },
        },
      },
    });

    extensionService.use(extension);
  },

  /**
   * An asynchronous bootstrap function that runs before
   * your application gets started.
   *
   * This gives you an opportunity to set up your data model,
   * run jobs, or perform some special logic.
   */
  bootstrap({ strapi }: { strapi: Strapi }) {
    subscribeTeamPlayerLifecycle(strapi);
  },
};
