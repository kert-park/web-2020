import PluginIcon from './components/PluginIcon';
import getTrad from './utils/getTrad';

export default {
  register(app) {
    console.log('register');
    app.customFields.register({
      name: 'api-game',
      pluginId: 'api-game',
      type: 'string',
      icon: PluginIcon,
      components: {
        Input: async () => import(/* webpackChunkName: "api-game-input-component" */ './components/ApiGame'),
      },
      intlLabel: {
        id: getTrad('api-game.label'),
        defaultMessage: 'Api Game',
      },
      intlDescription: {
        id: getTrad('api-game.description'),
        defaultMessage: 'Select api game',
      },
    });
  },
  async registerTrads() {
    return Promise.resolve([{ data: {}, locale: 'en' }]);
  },
};
