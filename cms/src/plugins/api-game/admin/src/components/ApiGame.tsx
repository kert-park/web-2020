import { ApiGame as CoreApiGame } from '@plugins/core/admin/components/ApiGame/ApiGame';
import { Field, FieldError } from '@strapi/design-system';
import PropTypes from 'prop-types';
import React, { useMemo } from 'react';

const ApiGame = ({ error, name, onChange, value }) => {
  const localValue = useMemo(() => {
    try {
      const { year = '', teamId = '', id = '' } = JSON.parse(value || '{}');
      return {
        year,
        teamId,
        id,
      };
    } catch (e) {
      console.error(e);
      return {
        year: '',
        teamId: '',
        id: '',
      };
    }
  }, [value]);

  const handleChange = ({ year, teamId, id }) => {
    onChange({ target: { name, value: JSON.stringify({ year, teamId, id }) } });
  };

  return (
    <Field
      name={name}
      id={name}
      // GenericInput calls formatMessage and returns a string for the error
      error={error}
    >
      <CoreApiGame onChange={handleChange} value={localValue}>
        <FieldError />
      </CoreApiGame>
    </Field>
  );
};

ApiGame.propTypes = {
  description: PropTypes.object,
  error: PropTypes.string,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};

export default ApiGame;
