/**
 *
 * PluginIcon
 *
 */

import { Dot } from '@strapi/icons';
import React from 'react';

const PluginIcon: React.FC = () => <Dot />;

export default PluginIcon;
