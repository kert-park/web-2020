import { Strapi } from '@strapi/strapi';

export default ({ strapi }: { strapi: Strapi }) => {
  strapi.customFields.register({
    name: 'api-game',
    plugin: 'api-game',
    type: 'string',
  });
};
