/**
 *
 * PluginIcon
 *
 */

import { List } from '@strapi/icons';
import React from 'react';

const PluginIcon: React.FC = () => <List />;

export default PluginIcon;
