import {
  ApiGameList as CoreApiGameList,
  ApiGameListValue,
} from '@plugins/core/admin/components/ApiGameList/ApiGameList';
import { Field, FieldError } from '@strapi/design-system';
import PropTypes from 'prop-types';
import React, { useMemo } from 'react';

const ApiGameList = ({ error, name, onChange, value }) => {
  const localValue = useMemo<ApiGameListValue>(() => {
    try {
      const { year = '', teamIds = [], from = undefined, to = undefined } = JSON.parse(value || '{}');
      return {
        year,
        teamIds,
        from,
        to,
      };
    } catch (e) {
      console.error(e);
      return {
        year: '',
        teamIds: [],
        from: undefined,
        to: undefined,
      };
    }
  }, [value]);

  const handleChange = ({ year, teamIds, from, to }: ApiGameListValue) => {
    onChange({ target: { name, value: JSON.stringify({ year, teamIds, from, to }) } });
  };

  return (
    <Field
      name={name}
      id={name}
      // GenericInput calls formatMessage and returns a string for the error
      error={error}
    >
      <CoreApiGameList onChange={handleChange} value={localValue}>
        <FieldError />
      </CoreApiGameList>
    </Field>
  );
};

ApiGameList.propTypes = {
  description: PropTypes.object,
  error: PropTypes.string,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};

export default ApiGameList;
