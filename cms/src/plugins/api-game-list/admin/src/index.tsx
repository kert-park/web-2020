import PluginIcon from './components/PluginIcon';
import getTrad from './utils/getTrad';

export default {
  register(app) {
    console.log('register');
    app.customFields.register({
      name: 'api-game-list',
      pluginId: 'api-game-list',
      type: 'string',
      icon: PluginIcon,
      components: {
        Input: async () => import(/* webpackChunkName: "api-game-input-component" */ './components/ApiGameList'),
      },
      intlLabel: {
        id: getTrad('api-game-list.label'),
        defaultMessage: 'Api Game List',
      },
      intlDescription: {
        id: getTrad('api-game-list.description'),
        defaultMessage: 'Select api games',
      },
    });
  },
  async registerTrads() {
    return Promise.resolve([{ data: {}, locale: 'en' }]);
  },
};
