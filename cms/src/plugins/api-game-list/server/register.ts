import { Strapi } from '@strapi/strapi';

export default ({ strapi }: { strapi: Strapi }) => {
  strapi.customFields.register({
    name: 'api-game-list',
    plugin: 'api-game-list',
    type: 'string',
  });
};
