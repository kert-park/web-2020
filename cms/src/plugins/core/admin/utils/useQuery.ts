import request from 'graphql-request';
import { useEffect, useState } from 'react';

const apiPath = 'https://admin.kert-park.cz/api/web-graphql';

const getQuery =
  ({ query, setData, setLoading }) =>
  async (variables: Record<string, unknown> | undefined = undefined) => {
    setLoading(true);
    const res = await request(apiPath, query, variables);
    setData(res);
    setLoading(false);
  };

type Options = {
  variables?: Record<string, unknown>;
  skip?: boolean;
};

export const useQuery = <TResponse>(query: string, options?: Options) => {
  const [data, setData] = useState<TResponse | undefined>();
  const [loading, setLoading] = useState(false);

  const { variables = undefined, skip = false } = options || {};

  useEffect(() => {
    if (!skip) {
      getQuery({ query, setData, setLoading })(variables);
    }
  }, [query, skip, JSON.stringify(variables)]);

  return { data, loading };
};
