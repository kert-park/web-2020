import { Option, Select } from '@strapi/design-system';
import { gql } from 'graphql-request';
import React from 'react';

import { useQuery } from '../../utils/useQuery';

const query = gql`
  {
    teamList {
      systemName
      name
    }
  }
`;

type QueryResponse = {
  teamList?: Array<{ systemName: string; name: string }>;
};

const getOptions = (data: QueryResponse | undefined): Array<{ label: string; value: string }> => {
  const defaultOption = { label: 'Vyberte', value: '' };
  if (data && data.teamList && data.teamList.length > 0) {
    return [defaultOption, ...data.teamList.map(({ name, systemName }) => ({ label: name, value: systemName }))];
  }
  return [defaultOption];
};

export type TeamSelectProps = {
  onChange: (value: string) => void;
  value: string;
};

export const TeamSelect = ({ onChange, value }: TeamSelectProps) => {
  const { data, loading } = useQuery<QueryResponse>(query);
  const options = getOptions(data);

  return (
    <Select disabled={loading} name="team" label="Tým" onChange={onChange} value={value}>
      {options.map(({ label, value: optionValue }) => (
        <Option key={optionValue} value={optionValue}>
          {label}
        </Option>
      ))}
    </Select>
  );
};
