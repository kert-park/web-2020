import { Option, Select } from '@strapi/design-system';
import { format } from 'date-fns';
import { gql } from 'graphql-request';
import React from 'react';

import { useQuery } from '../../utils/useQuery';

const query = gql`
  query gameList($teamId: String, $year: String!) {
    gameList(teamId: $teamId, year: $year) {
      id
      date
      home {
        cmshbId
        name
      }
      away {
        cmshbId
        name
      }
      status
      score {
        home {
          finalScore
        }
        away {
          finalScore
        }
      }
    }
  }
`;

type Team = {
  cmshbId: string;
  name: string;
};

type Score = {
  finalScore: string;
};

type TeamsWrapperType<TValue> = {
  home: TValue;
  away: TValue;
};

type QueryResponse = {
  gameList?: Array<
    { id: string; date: string; status: string; score: TeamsWrapperType<Score> } & TeamsWrapperType<Team>
  >;
};

const getOptions = (data: QueryResponse | undefined): Array<{ label: string; value: string }> => {
  const defaultOption = { label: 'Vyberte', value: '' };
  if (data && data.gameList && data.gameList.length > 0) {
    return [
      defaultOption,
      ...data.gameList.map(({ id, date, home, away, status, score }) => {
        const formattedDate = format(new Date(date), 'dd.MM.yy HH:mm');
        const opponentName = `${home.cmshbId === '3100003' ? away.name : home.name}`;
        const finalScore = status ? ` ${score.home.finalScore}:${score.away.finalScore} ${status}` : '';
        return {
          label: `${formattedDate}, ${opponentName}${finalScore}`,
          value: id,
        };
      }),
    ];
  }
  return [defaultOption];
};

export type GameSelectProps = {
  onChange: (value: string) => void;
  teamId: string;
  value: string;
  year: string;
};

export const GameSelect = ({ onChange, teamId, value, year }: GameSelectProps) => {
  const { data, loading } = useQuery<QueryResponse>(query, { variables: { teamId, year }, skip: !teamId || !year });

  const options = getOptions(data);

  return (
    <Select disabled={loading} name="id" label="Zápas" onChange={onChange} value={value}>
      {options.map(({ label, value: optionValue }) => (
        <Option key={optionValue} value={optionValue}>
          {label}
        </Option>
      ))}
    </Select>
  );
};
