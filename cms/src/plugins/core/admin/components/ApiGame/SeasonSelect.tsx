import { Option, Select } from '@strapi/design-system';
import { gql } from 'graphql-request';
import React from 'react';

import { useQuery } from '../../utils/useQuery';

const query = gql`
  {
    seasonList {
      year
    }
  }
`;

type QueryResponse = {
  seasonList?: Array<{ year: string }>;
};

const getOptions = (data: QueryResponse | undefined): Array<{ label: string; value: string }> => {
  const defaultOption = { label: 'Vyberte', value: '' };
  if (data && data.seasonList && data.seasonList.length > 0) {
    return [defaultOption, ...data.seasonList.map(({ year }) => ({ label: year, value: year }))];
  }
  return [defaultOption];
};

export type SeasonSelectProps = {
  onChange: (value: string) => void;
  value: string;
};

export const SeasonSelect = ({ onChange, value }: SeasonSelectProps) => {
  const { data, loading } = useQuery<QueryResponse>(query);
  const options = getOptions(data);

  return (
    <Select disabled={loading} name="year" label="Sezona" onChange={onChange} value={value}>
      {options.map(({ label, value: optionValue }) => (
        <Option key={optionValue} value={optionValue}>
          {label}
        </Option>
      ))}
    </Select>
  );
};
