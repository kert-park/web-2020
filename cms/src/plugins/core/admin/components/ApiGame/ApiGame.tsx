import { Stack } from '@strapi/design-system';
import React, { useEffect, useState } from 'react';

import { GameSelect } from './GameSelect';
import { SeasonSelect } from './SeasonSelect';
import { TeamSelect } from './TeamSelect';

export type ApiGameValue = {
  year: string;
  teamId: string;
  id: string;
};

export type ApiGameProps = {
  children?: React.ReactNode;
  onChange: (value: ApiGameValue) => void;
  value: ApiGameValue;
};

export const ApiGame = ({ onChange, value, children }: ApiGameProps) => {
  const [year, setYear] = useState(value.year);
  const [teamId, setTeamId] = useState(value.teamId);
  const [id, setId] = useState(value.id);

  useEffect(() => {
    setYear(value.year);
    setTeamId(value.teamId);
    setId(value.id);
  }, [value]);

  // Set new value
  useEffect(() => {
    if (year && teamId && id) {
      onChange({ year, teamId, id });
    }
  }, [year, teamId, id]);

  return (
    <Stack spacing={1}>
      <SeasonSelect onChange={setYear} value={year} />
      <TeamSelect onChange={setTeamId} value={teamId} />
      <GameSelect onChange={setId} value={id} teamId={teamId} year={year} />
      {children}
    </Stack>
  );
};
