import { DatePicker, Stack } from '@strapi/design-system';
import { addHours } from 'date-fns';
import React, { useEffect, useState } from 'react';

import { SeasonSelect } from '../ApiGame/SeasonSelect';
import { TeamsSelect } from './TeamsSelect';

export type ApiGameListValue = {
  year: string;
  teamIds: Array<string>;
  from: Date | undefined;
  to: Date | undefined;
};

export type ApiGameListProps = {
  children?: React.ReactNode;
  onChange: (value: ApiGameListValue) => void;
  value: ApiGameListValue;
};

const dateWorkAround = (fn: (value: Date | undefined) => void) => (date: Date | undefined) => {
  if (date) {
    fn(addHours(date, 6));
  } else {
    fn(date);
  }
};

export const ApiGameList = ({ onChange, value, children }: ApiGameListProps) => {
  const [year, setYear] = useState(value.year);
  const [teamIds, setTeamIds] = useState(value.teamIds);
  const [from, setFrom] = useState(value.from);
  const [to, setTo] = useState(value.to);

  useEffect(() => {
    if (year && teamIds?.length > 0 && from && to) {
      onChange({ year, teamIds, from, to });
    }
  }, [year, teamIds, from, to]);

  return (
    <Stack spacing={1}>
      <SeasonSelect onChange={setYear} value={year} />
      <TeamsSelect onChange={setTeamIds} value={teamIds} />
      <DatePicker locale="cs-CZ" onChange={dateWorkAround(setFrom)} selectedDate={from} label="Od" />
      <DatePicker locale="cs-CZ" onChange={dateWorkAround(setTo)} selectedDate={to} label="Do" />
    </Stack>
  );
};
