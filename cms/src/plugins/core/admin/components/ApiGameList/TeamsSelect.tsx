import { MultiSelectOption, MultiSelect } from '@strapi/design-system';
import { gql } from 'graphql-request';
import React from 'react';

import { useQuery } from '../../utils/useQuery';

const query = gql`
  {
    teamList {
      systemName
      name
    }
  }
`;

type QueryResponse = {
  teamList?: Array<{ systemName: string; name: string }>;
};

const getOptions = (data: QueryResponse | undefined): Array<{ label: string; value: string }> => {
  if (data && data.teamList && data.teamList.length > 0) {
    return [...data.teamList.map(({ name, systemName }) => ({ label: name, value: systemName }))];
  }
  return [];
};

export type TeamSelectProps = {
  onChange: (value: Array<string>) => void;
  value: Array<string>;
};

export const TeamsSelect = ({ onChange, value }: TeamSelectProps) => {
  const { data, loading } = useQuery<QueryResponse>(query);
  const options = getOptions(data);

  return (
    <MultiSelect disabled={loading} name="team" label="Týmy" onChange={onChange} value={value} withTags>
      {options.map(({ label, value: optionValue }) => (
        <MultiSelectOption key={optionValue} value={optionValue}>
          {label}
        </MultiSelectOption>
      ))}
    </MultiSelect>
  );
};
