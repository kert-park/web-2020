import { addHours, getDay, nextSaturday, nextSunday, previousSaturday, previousSunday } from 'date-fns';

export const getActualSeason = () => {
  const now = new Date();
  const month = now.getMonth();
  const year = now.getFullYear();
  return month < 7 ? `${year - 1}-${year}` : `${year}-${year + 1}`;
};

export const getNearestWeekendDates = () => {
  const now = new Date();
  const weekday = getDay(now);

  return {
    from: addHours(weekday === 6 ? now : weekday < 3 ? previousSaturday(now) : nextSaturday(now), 6),
    to: addHours(weekday === 0 ? now : weekday < 3 ? previousSunday(now) : nextSunday(now), 6),
  };
};
