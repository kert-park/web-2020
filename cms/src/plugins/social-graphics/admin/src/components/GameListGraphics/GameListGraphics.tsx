import { ApiGameList, ApiGameListValue } from '@plugins/core/admin/components/ApiGameList/ApiGameList';
import { Box, Flex, LinkButton, Typography } from '@strapi/design-system';
import { format } from 'date-fns';
import React, { useEffect, useMemo, useState } from 'react';
import { getActualSeason, getNearestWeekendDates } from '../../utils';

const getDefaultValues = (): ApiGameListValue => {
  const teamIds = ['dorost', 'starsi-zaci', 'mladsi-zaci', 'pripravka', 'minipripravka'];
  const year = getActualSeason();
  const { from, to } = getNearestWeekendDates();

  return { year, teamIds, from, to };
};

export const GameListGraphics = () => {
  const [value, setValue] = useState<ApiGameListValue>(getDefaultValues());
  const [timestamp, setTimestamp] = useState<number>(new Date().getTime());

  useEffect(() => {
    const interval = setInterval(() => {
      setTimestamp(new Date().getTime());
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  const links = useMemo(() => {
    if (value.year && value.teamIds?.length > 0 && value.from && value.to) {
      const base = 'http://kert-park.cz/api/og-images';
      const fromString = format(value.from, 'yyyy-MM-dd');
      const toString = format(value.to, 'yyyy-MM-dd');
      const query = `?teamIds=${value.teamIds.join(',')}&year=${
        value.year
      }&from=${fromString}&to=${toString}&timestamp=${timestamp}`;
      return [
        {
          label: 'Social Post',
          links: [
            {
              label: 'Games Schedule',
              href: `${base}/social/post/game-list-schedule${query}`,
            },
            {
              label: 'Games Results',
              href: `${base}/social/post/game-list-results${query}`,
            },
          ],
        },
        {
          label: 'Social Story',
          links: [
            {
              label: 'Games Schedule',
              href: `${base}/social/story/game-list-schedule${query}`,
            },
            {
              label: 'Games Results',
              href: `${base}/social/story/game-list-results${query}`,
            },
          ],
        },
      ];
    }
    return [];
  }, [timestamp, value]);

  return (
    <Flex direction="column" shrink={1} gap={8}>
      <Box width="100%">
        <ApiGameList value={value} onChange={setValue} />
      </Box>
      {links.map((group, index) => (
        <Box width="100%" key={index}>
          <Flex direction="column" alignItems="flex-start" shrink={1} gap={4}>
            <Typography variant="beta">{group.label}</Typography>
            <Flex direction="row" shrink={1} wrap="wrap" gap={4}>
              {group.links.map((link, index) => (
                <LinkButton key={index} variant="default" href={link.href}>
                  {link.label}
                </LinkButton>
              ))}
            </Flex>
          </Flex>
        </Box>
      ))}
    </Flex>
  );
};
