import { ApiGame, ApiGameValue } from '@plugins/core/admin/components/ApiGame/ApiGame';
import { Box, Flex, LinkButton, Typography } from '@strapi/design-system';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { getActualSeason } from '../../utils';

const getDefaultValues = (): ApiGameValue => {
  const teamId = 'muzia';
  const year = getActualSeason();

  return { year, teamId, id: '' };
};

export const SingleGameGraphics = () => {
  const [value, setValue] = useState<ApiGameValue>(getDefaultValues());
  const [timestamp, setTimestamp] = useState<number>(new Date().getTime());

  useEffect(() => {
    const interval = setInterval(() => {
      setTimestamp(new Date().getTime());
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  const links = useMemo(() => {
    if (value.year && value.teamId && value.id) {
      const isATeam = value.teamId === 'muzia';
      const base = 'http://kert-park.cz/api/og-images';
      const query = `?id=${value.id}&teamId=${value.teamId}&year=${value.year}&timestamp=${timestamp}`;
      return [
        {
          label: 'Web',
          links: [
            {
              label: 'Game Preview',
              href: `${base}/web/game-preview${query}`,
            },
            {
              label: 'Game Result',
              href: `${base}/web/game-result${query}`,
            },
          ],
        },
        {
          label: 'Social Post',
          links: [
            {
              label: 'Game Day',
              href: `${base}/social/post/game-day${query}`,
            },
            {
              label: 'Game Result',
              href: `${base}/social/post/game-result${query}`,
            },
          ],
        },
        {
          label: 'Social Story',
          links: [
            {
              label: 'Game Day',
              href: `${base}/social/story/game-day${query}`,
            },
            ...(isATeam
              ? [
                  {
                    label: 'Game Lineup',
                    href: `${base}/social/story/game-lineup${query}`,
                  },
                  {
                    label: 'Game Result - After First Period',
                    href: `${base}/social/story/game-result${query}&type=after-first-period`,
                  },

                  {
                    label: 'Game Result - After Second Period',
                    href: `${base}/social/story/game-result${query}&type=after-second-period`,
                  },
                ]
              : []),
            {
              label: 'Game Result - Fulltime',
              href: `${base}/social/story/game-result${query}`,
            },
            {
              label: 'Game Statistics',
              href: `${base}/social/story/game-statistics${query}`,
            },
          ],
        },
      ];
    }
    return [];
  }, [timestamp, value]);

  return (
    <Flex direction="column" shrink={1} gap={8}>
      <Box width="100%">
        <ApiGame value={value} onChange={setValue} />
      </Box>
      {links.map((group, index) => (
        <Box width="100%" key={index}>
          <Flex direction="column" alignItems="flex-start" shrink={1} gap={4}>
            <Typography variant="beta">{group.label}</Typography>
            <Flex direction="row" shrink={1} wrap="wrap" gap={4}>
              {group.links.map((link, index) => (
                <LinkButton key={index} variant="default" href={link.href}>
                  {link.label}
                </LinkButton>
              ))}
            </Flex>
          </Flex>
        </Box>
      ))}
    </Flex>
  );
};
