/*
 *
 * HomePage
 *
 */

import {
  BaseHeaderLayout,
  Box,
  ContentLayout,
  Layout,
  Tab,
  TabGroup,
  TabPanel,
  TabPanels,
  Tabs,
} from '@strapi/design-system';
import React from 'react';
import { GameListGraphics } from '../../components/GameListGraphics/GameListGraphics';
import { SingleGameGraphics } from '../../components/SingleGameGraphics/SingleGameGraphics';

const HomePage = () => {
  return (
    <Layout>
      <BaseHeaderLayout title="Social graphics" />
      <ContentLayout>
        <TabGroup label="Graphics type" id="tabs" variant="simple">
          <Tabs>
            <Tab>Single Game</Tab>
            <Tab>Game List </Tab>
          </Tabs>
          <TabPanels>
            <TabPanel>
              <Box padding={4}>
                <SingleGameGraphics />
              </Box>
            </TabPanel>
            <TabPanel>
              <Box padding={4}>
                <GameListGraphics />
              </Box>
            </TabPanel>
          </TabPanels>
        </TabGroup>
      </ContentLayout>
    </Layout>
  );
};

export default HomePage;
