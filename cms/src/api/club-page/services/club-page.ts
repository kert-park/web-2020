/**
 * club-page service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::club-page.club-page');
