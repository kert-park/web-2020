/**
 * club-page router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::club-page.club-page');
