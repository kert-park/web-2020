/**
 * club-page controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::club-page.club-page');
