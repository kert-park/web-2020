import type { Strapi } from '@strapi/types';
import * as nexusEngine from 'nexus';

export const creatorNexusType = (nexus: typeof nexusEngine) => {
  return nexus.objectType({
    name: 'Creator',
    definition(t) {
      t.int('id');
      t.string('firstname');
      t.string('lastname');
    },
  });
};

export const dateRangeInputNexusType = (nexus: typeof nexusEngine) => {
  return nexus.inputObjectType({
    name: 'DateRangeInput',
    definition(t) {
      t.nonNull.field('from', {
        type: 'DateTime',
      });
      t.nonNull.field('to', {
        type: 'DateTime',
      });
    },
  });
};

export const postExtendedNexusType = (nexus: typeof nexusEngine) => {
  return nexus.extendType({
    type: 'Post',
    definition(t) {
      t.field('createdBy', {
        type: 'Creator',
        async resolve(root) {
          const query = strapi.db.query('api::post.post');
          const post = await query.findOne({
            where: {
              id: root.id,
            },
            populate: ['createdBy'],
          });

          return {
            id: post.createdBy.id,
            firstname: post.createdBy.firstname,
            lastname: post.createdBy.lastname,
          };
        },
      });
      t.string('game');
    },
  });
};

export const postBySlugNexusResolver = ({ nexus, strapi }: { nexus: typeof nexusEngine; strapi: Strapi }) => {
  return nexus.extendType({
    type: 'Query',
    definition(t) {
      t.field('postBySlug', {
        type: 'PostEntityResponse',
        args: { slug: nexus.stringArg() },
        async resolve(root, { slug }) {
          const { toEntityResponse } = strapi.service('plugin::graphql.format').returnTypes;
          const post = await strapi.service('api::post.post').findBySlug(slug);
          return toEntityResponse(post);
        },
      });
    },
  });
};

export const postByApiGameNexusResolver = ({ nexus, strapi }: { nexus: typeof nexusEngine; strapi: Strapi }) => {
  return nexus.extendType({
    type: 'Query',
    definition(t) {
      t.field('postByApiGame', {
        type: 'PostEntityResponse',
        args: { year: nexus.stringArg(), teamId: nexus.stringArg(), id: nexus.stringArg() },
        async resolve(root, { year, teamId, id }) {
          const { toEntityResponse } = strapi.service('plugin::graphql.format').returnTypes;
          const post = await strapi.service('api::post.post').findOneByApiGame(teamId, year, id);
          return toEntityResponse(post);
        },
      });
    },
  });
};

export const postsByApiGamesNexusResolver = ({ nexus, strapi }: { nexus: typeof nexusEngine; strapi: Strapi }) => {
  return nexus.extendType({
    type: 'Query',
    definition(t) {
      t.field('postsByApiGames', {
        type: 'PostEntityResponseCollection',
        args: { year: nexus.stringArg(), teamId: nexus.stringArg() },
        async resolve(root, { year, teamId }) {
          const { toEntityResponseCollection } = strapi.service('plugin::graphql.format').returnTypes;
          const posts = await strapi.service('api::post.post').findManyByApiGames(teamId, year);
          const total = posts.length;

          const meta = {
            pagination: {
              total,
              page: 1,
              pageSize: total,
              pageCount: 1,
            },
          };
          return toEntityResponseCollection(posts, {
            args: { start: 0, limit: 100 },
            resourceUID: 'api::post.post',
          });
        },
      });
    },
  });
};

export const postCompositeUnionNexusType = (nexus: typeof nexusEngine) => {
  return nexus.unionType({
    name: 'PostCompositeUnion',
    definition(t) {
      t.members('PostEntity', 'PostExternalComposite');
    },
    resolveType: (item) => {
      if (item.source) {
        return 'PostExternalComposite';
      }
      return 'PostEntity';
    },
  });
};

export const postCompositeResponseMetaNexusType = (nexus: typeof nexusEngine) => {
  return nexus.objectType({
    name: 'PostCompositeResponseMeta',
    definition(t) {
      t.nonNull.field('pagination', {
        type: 'Pagination',
      });
    },
  });
};

export const postCompositeResponseCollectionNexusType = (nexus: typeof nexusEngine) => {
  return nexus.objectType({
    name: 'PostCompositeResponseCollection',
    definition(t) {
      t.nonNull.list.field('data', {
        type: nexus.nonNull('PostCompositeUnion'),
      });
      t.nonNull.field('meta', {
        type: 'PostCompositeResponseMeta',
      });
    },
  });
};

export const postExternalCompositeNexusType = (nexus: typeof nexusEngine) => {
  return nexus.objectType({
    name: 'PostExternalComposite',
    definition(t) {
      t.id('id');
      t.nonNull.string('url');
      t.nonNull.string('title');
      t.nonNull.string('perex');
      t.string('imageUrl');
      t.nonNull.field('publishAt', {
        type: 'DateTime',
      });
      t.nonNull.list.field('categories', {
        type: nexus.nonNull('CategoryEntity'),
      });
    },
  });
};

export const postsCompositeNexusResolver = ({ nexus, strapi }: { nexus: typeof nexusEngine; strapi: Strapi }) => {
  return nexus.extendType({
    type: 'Query',
    definition(t) {
      t.field('postsComposite', {
        type: 'PostCompositeResponseCollection',
        args: {
          categoryCode: nexus.stringArg(),
          dateRange: nexus.arg({
            type: 'DateRangeInput',
          }),
          pagination: nexus.arg({
            type: 'PaginationArg',
          }),
        },
        async resolve(root, args, ctx) {
          const { posts, pagination } = await strapi.service('api::post.post-composite').find(args);

          return {
            data: posts,
            meta: {
              pagination,
            },
          };
        },
      });
    },
  });
};
