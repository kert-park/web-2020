import { startOfHour } from 'date-fns';
import slugify from 'slugify';

const replaceNonAlphanumeric = (inputString) => {
  return inputString.replace(/[^a-zA-Z0-9]/g, '-').replace(/--+/g, '');
};

module.exports = {
  async beforeCreate(event) {
    if (event.params.data.title && !event.params.data.slug) {
      event.params.data.slug = replaceNonAlphanumeric(slugify(event.params.data.title, { lower: true }));
    }
    if (!event.params.data.publishAt) {
      event.params.data.publishAt = startOfHour(new Date()).toISOString();
    }
  },
  async beforeUpdate(event) {
    if (event.params.data.title && !event.params.data.slug) {
      event.params.data.slug = replaceNonAlphanumeric(slugify(event.params.data.title, { lower: true }));
    }
    if (!event.params.data.publishAt && event.params.data.publishAt === null) {
      event.params.data.publishAt = startOfHour(new Date()).toISOString();
    }
  },
};
