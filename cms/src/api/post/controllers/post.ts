/**
 * post controller
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreController('api::post.post', ({ strapi }) => ({
  async findOneByApiGame(ctx) {
    await this.sanitizeQuery(ctx);
    const { year, teamId, id } = ctx.params;
    const post = await strapi.service('api::post.post').findOneByApiGame(teamId, year, id);
    const sanitizedPost = await this.sanitizeOutput(post, ctx);
    return this.transformResponse(sanitizedPost);
  },
  async findManyByApiGames(ctx) {
    await this.sanitizeQuery(ctx);
    const { year, teamId } = ctx.params;
    const post = await strapi.service('api::post.post').findManyByApiGames(teamId, year);
    const sanitizedPost = await this.sanitizeOutput(post, ctx);
    return this.transformResponse(sanitizedPost);
  },
}));
