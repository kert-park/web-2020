import post from './post';
import postComposite from './post-composite';

export default {
  post,
  'post-composite': postComposite,
};
