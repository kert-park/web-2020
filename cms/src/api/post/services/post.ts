/**
 * post service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::post.post', ({ strapi }) => ({
  async getLatestPostForApiGame(apiGameComponentsIds: Array<string>) {
    const postsComponents = await strapi.db.connection
      .select('*')
      .from('posts_components')
      .where('component_type', 'common.api-game')
      .andWhere((builder) => builder.whereIn('component_id', apiGameComponentsIds));
    if (postsComponents.length) {
      const { results } = await super.find({
        filters: {
          id: postsComponents.map(({ entity_id }) => entity_id),
          publishAt: {
            $lt: new Date().toISOString(),
          },
        },
        sort: { publishAt: 'desc' },
        publicationState: 'live',
      });
      return results?.[0] || null;
    }
    return null;
  },

  async findBySlug(slug: string) {
    const { results } = await super.find({ filters: { slug } });
    return results?.[0] || null;
  },
  async findOneByApiGame(teamId: string, year: string, id: string) {
    const apiGameComponents = await strapi.db.query('common.api-game').findMany({
      where: {
        $and: [
          {
            game: {
              $contains: year,
            },
          },
          {
            game: {
              $contains: teamId,
            },
          },
          {
            game: {
              $contains: id,
            },
          },
        ],
      },
    });
    if (apiGameComponents.length) {
      return this.getLatestPostForApiGame(apiGameComponents.map(({ id }) => id));
    }
    return null;
  },
  async findManyByApiGames(teamId: string, year: string) {
    const apiGameComponents = await strapi.db.query('common.api-game').findMany({
      where: {
        $and: [
          {
            game: {
              $contains: year,
            },
          },
          {
            game: {
              $contains: teamId,
            },
          },
        ],
      },
    });
    const groupedApiGameComponentsIds: Record<string, Array<string>> = apiGameComponents.reduce(
      (acc, apiGameComponent) => {
        return {
          ...acc,
          [apiGameComponent.game]: [...(acc[apiGameComponent.game] || []), apiGameComponent.id],
        };
      },
      {},
    );

    return Object.entries(groupedApiGameComponentsIds).reduce<Promise<Array<any>>>(
      async (acc, [game, apiGameComponents]) => {
        const posts = await acc;
        const post = await this.getLatestPostForApiGame(apiGameComponents);
        if (post) {
          return [...posts, { ...post, game }];
        }
        return posts;
      },
      Promise.resolve([]),
    );
  },
}));
