import { Strapi } from '@strapi/strapi';
import axios from 'axios';
import { compareDesc, min } from 'date-fns';

type DateRange = {
  from: Date;
  to: Date;
};

type Pagination = {
  page: number;
  pageSize: number;
};

type FindProps = {
  categoryCode?: string;
  dateRange?: DateRange;
  pagination?: Pagination;
};

type PartialPost = {
  pinToTop: boolean;
  publishAt: string;
  slug: string;
};

type PartialExternalPost = {
  id: string;
  pinToTop: boolean;
  publishAt: string;
  source: string;
  url: string;
};

type PartialExternalPostData = {
  id: string;
};

const isPartialExternalPost = (post: PartialPost | PartialExternalPost): post is PartialExternalPost => {
  return (post as PartialExternalPost).source !== undefined;
};

const getAllPosts = async ({ strapi, props: { categoryCode, dateRange } }: { strapi: Strapi; props: FindProps }) => {
  const posts = await strapi.db.query('api::post.post').findMany({
    where: {
      publishedAt: {
        $notNull: true,
      },
      ...(dateRange
        ? {
            publishAt: {
              $between: [
                min([dateRange.from, new Date()]).toISOString(),
                min([dateRange.to, new Date()]).toISOString(),
              ],
            },
          }
        : {
            publishAt: {
              $lt: new Date().toISOString(),
            },
          }),
      ...(categoryCode
        ? {
            categories: {
              code: {
                $eq: categoryCode,
              },
            },
          }
        : {}),
    },
    orderBy: { publishAt: 'desc' },
  });
  return posts as Array<PartialPost>;
};

const getAllExternalPosts = async ({
  strapi,
  props: { categoryCode, dateRange },
}: {
  strapi: Strapi;
  props: FindProps;
}) => {
  const posts = await strapi.db.query('api::external-post.external-post').findMany({
    where: {
      ...(dateRange
        ? {
            publishAt: {
              $between: [
                min([dateRange.from, new Date()]).toISOString(),
                min([dateRange.to, new Date()]).toISOString(),
              ],
            },
          }
        : {
            publishAt: {
              $lt: new Date().toISOString(),
            },
          }),
      ...(categoryCode
        ? {
            categories: {
              code: {
                $eq: categoryCode,
              },
            },
          }
        : {}),
    },
    orderBy: { publishAt: 'desc' },
    populate: true,
  });
  return posts as Array<PartialExternalPost>;
};

const fetchExternalPostsData = async () => {
  const response = await axios.get('https://admin.kert-park.cz/api/hokejbal-cz-articles');
  return response?.data || [];
};

const sortPosts = (posts: Array<PartialPost | PartialExternalPost>) => {
  return posts.sort((a, b) => {
    if (a?.pinToTop && !b?.pinToTop) {
      return -1;
    } else if (!a?.pinToTop && b?.pinToTop) {
      return 1;
    } else if (a?.publishAt && b?.publishAt) {
      return compareDesc(new Date(a.publishAt), new Date(b.publishAt));
    }
    return 0;
  });
};

const mapExternalPosts = (
  posts: Array<PartialPost | PartialExternalPost>,
  externalPostsData: Array<PartialExternalPostData>,
) => {
  return posts.reduce<Array<PartialPost | PartialExternalPost>>((acc, post) => {
    if (isPartialExternalPost(post)) {
      const data = externalPostsData.find((data) => data.id === post.url);
      if (data) {
        acc.push({
          ...data,
          ...post,
        });
      }
      return acc;
    } else {
      acc.push(post);
    }

    return acc;
  }, []);
};

const getPaginationInfo = (total: number, pagination?: Pagination) => {
  const page = Math.max(pagination?.page || 1, 1);
  const pageSize = pagination?.pageSize || total;
  const pageCount = Math.ceil(total / pageSize);

  return {
    total,
    page,
    pageSize,
    pageCount,
  };
};

const slicePosts = (posts: Array<PartialPost | PartialExternalPost>, pagination: Pagination) => {
  return posts.slice((pagination.page - 1) * pagination.pageSize, pagination.page * pagination.pageSize);
};

export default ({ strapi }: { strapi: Strapi }) => ({
  async find(props: FindProps) {
    const [posts, externalPosts, externalPostsData] = await Promise.all([
      getAllPosts({ strapi, props }),
      getAllExternalPosts({ strapi, props }),
      fetchExternalPostsData(),
    ]);

    const sortedPosts = sortPosts([...posts, ...externalPosts]);
    const mappedPosts = mapExternalPosts(sortedPosts, externalPostsData);
    const paginationInfo = getPaginationInfo(mappedPosts.length, props.pagination);

    return {
      posts: slicePosts(mappedPosts, paginationInfo),
      pagination: paginationInfo,
    };
  },
});
