export default {
  routes: [
    {
      method: 'GET',
      path: '/posts/byApiGame/:teamId/:year',
      handler: 'api::post.post.findManyByApiGames',
      config: {
        auth: {
          scope: ['api::post.post.find'],
        },
      },
    },
    {
      method: 'GET',
      path: '/posts/byApiGame/:teamId/:year/:id',
      handler: 'api::post.post.findOneByApiGame',
      config: {
        auth: {
          scope: ['api::post.post.find'],
        },
      },
    },
  ],
};
