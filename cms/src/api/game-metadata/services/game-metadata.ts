/**
 * game-metadata service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::game-metadata.game-metadata');
