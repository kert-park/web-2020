import axios from 'axios';
import { format } from 'date-fns';

const getGameCategoryAndName = async (game: string) => {
  const { id, teamId, year } = JSON.parse(game);
  const {
    data: { date, home, away, score, status, team },
  } = await axios.get(`https://admin.kert-park.cz/api/games/${id}?teamId=${teamId}&year=${year}`);
  const formattedDate = format(new Date(date), 'yyyy-MM-dd HH:mm');
  const opponentName = `${home.id === '3100003' ? away.name : home.name}`;
  const finalScore = status ? ` ${score.home.finalScore}:${score.away.finalScore} ${status}` : '';
  return {
    category: team.name,
    nameAndDate: `${formattedDate}, ${opponentName}${finalScore}`,
  };
};

module.exports = {
  async beforeCreate(event) {
    try {
      const { category, nameAndDate } = await getGameCategoryAndName(event.params.data.game);
      event.params.data.category = category;
      event.params.data.nameAndDate = nameAndDate;
    } catch (error) {
      console.error(error);
    }
  },
  async beforeUpdate(event) {
    try {
      const { category, nameAndDate } = await getGameCategoryAndName(event.params.data.game);
      event.params.data.category = category;
      event.params.data.nameAndDate = nameAndDate;
    } catch (error) {
      console.error(error);
    }
  },
};
