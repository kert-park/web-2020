export default {
  routes: [
    // {
    //   method: 'POST',
    //   path: '/players/migrate',
    //   handler: 'api::player.player.migrate',
    //   config: {
    //     auth: false,
    //   },
    // },
    {
      method: 'GET',
      path: '/players/byTeam/:teamId',
      handler: 'api::player.player.findManyByTeamId',
      config: {
        auth: {
          scope: ['api::player.player.find'],
        },
      },
    },
    {
      method: 'GET',
      path: '/players/byTeam/:teamId/:cmshbId',
      handler: 'api::player.player.findOneByCmshbIdAndTeamId',
      config: {
        auth: {
          scope: ['api::player.player.find'],
        },
      },
    },
  ],
};
