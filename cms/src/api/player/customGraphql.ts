import type { Strapi } from '@strapi/types';
import * as nexusEngine from 'nexus';

export const playerNexusType = (nexus: typeof nexusEngine) => {
  return nexus.objectType({
    name: 'Player',
    definition(t) {
      t.nonNull.id('id');
      t.id('cmshbId');
      t.nonNull.string('name');
      t.field('birth', {
        type: 'Date',
      });
      t.int('height');
      t.int('weight');
      t.int('number');
      t.field('post', {
        type: 'ENUM_COMPONENTPLAYERTEAMPLAYER_POST',
      });
      t.field('stick', {
        type: 'ENUM_COMPONENTPLAYERTEAMPLAYER_STICK',
      });
      t.string('smallImage');
      t.string('largeImage');
      t.list.field('achievements', {
        type: 'ComponentCommonAchievementList',
      });
    },
  });
};

export const playerNexusResolver = ({ nexus, strapi }: { nexus: typeof nexusEngine; strapi: Strapi }) => {
  return nexus.extendType({
    type: 'Query',
    definition(t) {
      t.field('player', {
        type: 'Player',
        args: {
          id: nexus.nonNull(nexus.idArg()),
          teamId: nexus.nonNull(nexus.idArg()),
        },
        async resolve(root, { id, teamId }, ctx) {
          return await strapi.service('api::player.player').findOneByIdAndTeamId(id, teamId);
        },
      });
    },
  });
};

export const playersNexusResolver = ({ nexus, strapi }: { nexus: typeof nexusEngine; strapi: Strapi }) => {
  return nexus.extendType({
    type: 'Query',
    definition(t) {
      t.nonNull.list.field('players', {
        type: nexus.nonNull('Player'),
        args: {
          teamId: nexus.nonNull(nexus.idArg()),
        },
        async resolve(root, { teamId }, ctx) {
          return await strapi.service('api::player.player').findByTeamId(teamId);
        },
      });
    },
  });
};
