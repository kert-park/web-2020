const getTeamNames = async (data: Array<{ id: string }>): Promise<string | null> => {
  const teamPlayers = await Promise.all(
    data.map(async ({ id }) => {
      return strapi.db.query('player.team-player').findOne({
        where: {
          id,
        },
        populate: {
          team: true,
        },
      });
    }),
  );

  return (
    teamPlayers
      .map((teamPlayer) => {
        return teamPlayer.team.name;
      })
      .join(', ') || null
  );
};

const processTeams = async (event): Promise<void> => {
  if (Array.isArray(event.params.data.teamPlayers)) {
    event.params.data.teams = await getTeamNames(event.params.data.teamPlayers);
  }
};

module.exports = {
  async beforeCreate(event) {
    try {
      await processTeams(event);
    } catch (error) {
      console.error(error);
    }
  },
  async beforeUpdate(event) {
    try {
      await processTeams(event);
    } catch (error) {
      console.error(error);
    }
  },
};
