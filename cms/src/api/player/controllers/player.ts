/**
 * player controller
 */

import { factories } from '@strapi/strapi';
// import axios from 'axios';

export default factories.createCoreController('api::player.player', ({ strapi }) => ({
  async findManyByTeamId(ctx) {
    await this.sanitizeQuery(ctx);
    const { teamId } = ctx.params;
    return await strapi.service('api::player.player').findByTeamId(teamId);
  },
  async findOneByCmshbIdAndTeamId(ctx) {
    await this.sanitizeQuery(ctx);
    const { cmshbId, teamId } = ctx.params;
    return await strapi.service('api::player.player').findOneByCmshbIdAndTeamId(cmshbId, teamId);
  },

  // migrate: async (ctx) => {
  //   const body = ctx.request.body;
  //   if (!body || !body.token || body.token !== process.env.MIGRATE_PLAYERS_TOKEN) {
  //     return ctx.badRequest('Invalid token');
  //   }
  //
  //   const { data } = await axios.get(`https://admin.kert-park.cz/api/players/all`);
  //
  //   if (!data?.length) {
  //     return ctx.internalServerError('Failed to fetch players to migrate');
  //   }
  //
  //   await strapi.service('api::player.player').deleteAll();
  //   await strapi.service('api::player.player').createPlayersFromAdminApi(data);
  //
  //   ctx.status = 204;
  // },
}));
