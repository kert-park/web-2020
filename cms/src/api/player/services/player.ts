/**
 * player service
 */

import { factories } from '@strapi/strapi';

type AdminApiPlayer = {
  id: number;
  cmshbId: string | null;
  name: string;
  birth: Date | null;
  height: number | null;
  weight: number | null;
  teamPlayers: Array<{
    number: number | null;
    post: string | null;
    stick: string | null;
    teamSystemName: string;
  }>;
};

const mapPlayer = (player, teamId) => {
  if (player) {
    const teamPlayer = player.teamPlayers.find((teamPlayer) => teamPlayer.team.code === teamId);

    if (teamPlayer) {
      return {
        id: player.id,
        cmshbId: player.cmshbId,
        name: player.name,
        birth: player.birth,
        height: player.height,
        weight: player.weight,
        number: teamPlayer.number,
        post: teamPlayer.post,
        stick: teamPlayer.stick,
        smallImage: player.smallImage?.url,
        largeImage: player.largeImage?.url,
        achievements: player.achievements,
      };
    }
  }

  return null;
};

export default factories.createCoreService('api::player.player', ({ strapi }) => ({
  async findOneByIdAndTeamId(id: string, teamId: string) {
    const player = await strapi.db.query('api::player.player').findOne({
      where: {
        id,
      },
      populate: {
        achievements: true,
        largeImage: true,
        smallImage: true,
        teamPlayers: {
          populate: {
            team: true,
          },
        },
      },
    });

    return mapPlayer(player, teamId);
  },
  async findOneByCmshbIdAndTeamId(cmshbId: string, teamId: string) {
    const player = await strapi.db.query('api::player.player').findOne({
      where: {
        cmshbId,
      },
      populate: {
        achievements: true,
        largeImage: true,
        smallImage: true,
        teamPlayers: {
          populate: {
            team: true,
          },
        },
      },
    });

    return mapPlayer(player, teamId);
  },
  async findByTeamId(teamId: string) {
    const players = await strapi.db.query('api::player.player').findMany({
      populate: {
        achievements: true,
        largeImage: true,
        smallImage: true,
        teamPlayers: {
          populate: {
            team: true,
          },
        },
      },
    });

    return players.map((player) => mapPlayer(player, teamId)).filter(Boolean);
  },
  // async deleteAll() {
  //   await strapi.db.query('player.team-player').deleteMany();
  //   await strapi.db.query('api::player.player').deleteMany();
  // },
  // async createPlayersFromAdminApi(players: AdminApiPlayer[]) {
  //   const categories = await strapi.db.query('api::category.category').findMany();
  //   const smallImages = await strapi.db.query('plugin::upload.file').findMany({
  //     where: {
  //       folder_path: '/119/120',
  //     },
  //   });
  //   const largeImages = await strapi.db.query('plugin::upload.file').findMany({
  //     where: {
  //       folder_path: '/119/121',
  //     },
  //   });
  //
  //   await Promise.all(
  //     players.map(async (player) => {
  //       const smallImageId = smallImages.find((image) => image.name === `${player.cmshbId}.jpg`)?.id;
  //       const largeImageId = largeImages.find((image) => image.name === `${player.cmshbId}.jpg`)?.id;
  //       const playerEntity = await strapi.db.query('api::player.player').create({
  //         data: {
  //           cmshbId: player.cmshbId || null,
  //           name: player.name,
  //           birth: player.birth || null,
  //           height: player.height || null,
  //           weight: player.weight || null,
  //           smallImage: smallImageId || null,
  //           largeImage: largeImageId || null,
  //         },
  //       });
  //       const teamPlayerEntities = await Promise.all(
  //         player.teamPlayers.map(async (teamPlayer) => {
  //           const category = categories.find((category) => category.code === teamPlayer.teamSystemName);
  //
  //           if (!category?.id) {
  //             console.error(
  //               `Category not found for team ${teamPlayer.teamSystemName} and player ${player.name} ${player.cmshbId}`,
  //             );
  //             return null;
  //           }
  //
  //           const teamPlayerEntity = await strapi.db.query('player.team-player').create({
  //             data: {
  //               number: teamPlayer.number || null,
  //               post: teamPlayer.post || null,
  //               stick: teamPlayer.stick || null,
  //               player: {
  //                 connect: [playerEntity.id],
  //               },
  //               team: {
  //                 connect: [category.id],
  //               },
  //             },
  //           });
  //           return [teamPlayerEntity, category];
  //         }),
  //       );
  //
  //       const createdTeamPlayerEntities = teamPlayerEntities.filter(Boolean);
  //       if (createdTeamPlayerEntities.length) {
  //         await strapi.db.query('api::player.player').update({
  //           where: {
  //             id: playerEntity.id,
  //           },
  //           data: {
  //             teamPlayers: {
  //               connect: createdTeamPlayerEntities.map(([teamPlayer]) => teamPlayer.id),
  //             },
  //             teams: createdTeamPlayerEntities.map(([, category]) => category.name).join(', '),
  //           },
  //         });
  //       }
  //     }),
  //   );
  //
  //   // Update component type
  //   await strapi.db.connection
  //     .select('*')
  //     .from('players_components')
  //     .where('field', 'teamPlayers')
  //     .update('component_type', 'player.team-player');
  // },
}));
