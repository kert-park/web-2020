import { Strapi } from '@strapi/strapi';

const processTeamName = async (event) => {
  if (event.params.data.team.connect?.length) {
    const connect = event.params.data.team.connect[0];
    const categoryId = typeof connect === 'object' ? connect?.id : connect;
    if (categoryId) {
      const category = await strapi.db.query('api::category.category').findOne({
        where: {
          id: categoryId,
        },
      });
      if (category) {
        event.params.data.teamName = category.name;
        return;
      }
    }
  }
  if (event.params.data.team.disconnect?.length) {
    event.params.data.teamName = null;
    return;
  }
};

export const subscribeTeamPlayerLifecycle = (strapi: Strapi) => {
  strapi.db.lifecycles.subscribe({
    models: ['player.team-player'],
    async beforeCreate(event) {
      try {
        await processTeamName(event);
      } catch (error) {
        console.error(error);
      }
    },
    async beforeUpdate(event) {
      try {
        await processTeamName(event);
      } catch (error) {
        console.error(error);
      }
    },
  });
};
