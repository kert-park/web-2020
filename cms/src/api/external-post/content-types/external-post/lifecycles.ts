import axios from 'axios';
import { startOfHour } from 'date-fns';

const scrapeExternalPost = async ({ source, url }: { source: string; url: string }) => {
  if (source === 'hokejbal_cz') {
    try {
      await axios.post('https://admin.kert-park.cz/api/hokejbal-cz-articles/scrape', {
        url,
        token: process.env.ADMIN_WEBHOOK_API_KEY,
      });
      console.log(`Scraped hokejbal.cz article ${url}`);
    } catch (error) {
      console.error(`Failed to scrape hokejbal.cz article ${url}`, error);
    }
  }
};

module.exports = {
  async beforeCreate(event) {
    if (!event.params.data.publishAt) {
      event.params.data.publishAt = startOfHour(new Date()).toISOString();
    }
    await scrapeExternalPost({ source: event.params.data.source, url: event.params.data.url });
  },
  async beforeUpdate(event) {
    if (!event.params.data.publishAt && event.params.data.publishAt === null) {
      event.params.data.publishAt = startOfHour(new Date()).toISOString();
    }
    await scrapeExternalPost({ source: event.params.data.source, url: event.params.data.url });
  },
};
