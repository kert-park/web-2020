/**
 * external-post controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::external-post.external-post');
