/**
 * external-post router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::external-post.external-post');
