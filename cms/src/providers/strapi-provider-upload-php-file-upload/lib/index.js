const fs = require('fs');
const path = require('path');
const fetch = require('node-fetch');
const FormData = require('form-data');
const jwt = require('jsonwebtoken');

const getToken = (filename) => {
  const keyPath = path.join(__dirname, '../../../provider-jwt.key');
  const key = fs.readFileSync(keyPath);
  return jwt.sign({ filename }, key, { algorithm: 'RS256', expiresIn: '1h', issuer: 'Kert Park' });
};

module.exports = {
  init(providerOptions) {
    // init your provider if necessary
    const { apiPath, filesPath } = providerOptions;

    return {
      upload(file) {
        return new Promise((resolve, reject) => {
          const filename = `${file.hash}${file.ext}`;
          const formData = new FormData();
          formData.append('file', file.buffer, { filename });
          const token = getToken(filename);
          fetch(apiPath, {
            method: 'POST',
            body: formData,
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
            .then((response) => {
              if (response.ok) {
                file.url = `${filesPath}/${filename}`;
                resolve(file);
              } else {
                reject(response);
              }
            })
            .catch((err) => {
              console.error(err);
              reject(err);
            });
        });
      },
      delete(file) {
        return new Promise((resolve, reject) => {
          const filename = `${file.hash}${file.ext}`;
          const token = getToken(filename);
          fetch(apiPath, {
            method: 'DELETE',
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
            .then((response) => {
              if (response.ok) {
                resolve(response);
              } else {
                reject(response);
              }
            })
            .catch((err) => {
              console.error(err);
              reject(err);
            });
        });
      },
    };
  },
};
