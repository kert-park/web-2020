import { HttpLink } from 'apollo-link-http';
import { ApolloServer } from 'apollo-server-express';
import { introspectSchema, makeRemoteExecutableSchema, mergeSchemas } from 'graphql-tools';
import fetch from 'isomorphic-unfetch';

const GRAPHQL_CMS_PATH = process.env.GRAPHQL_CMS_PATH;
const GRAPHQL_WEB_PATH = process.env.GRAPHQL_WEB_PATH;

const cmsSchema = async () => {
  const link = new HttpLink({ uri: GRAPHQL_CMS_PATH, fetch });
  const schema = await introspectSchema(link);

  return makeRemoteExecutableSchema({
    schema,
    link,
  });
};

const webSchema = async () => {
  const link = new HttpLink({ uri: GRAPHQL_WEB_PATH, fetch });
  const schema = await introspectSchema(link);

  return makeRemoteExecutableSchema({
    schema,
    link,
  });
};

export const createApolloServer = async (isDev: boolean) => {
  const schema = mergeSchemas({ schemas: [await webSchema(), await cmsSchema()] });
  return new ApolloServer({ schema, introspection: isDev });
};
