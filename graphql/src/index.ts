import express, { Application } from 'express';

import { createApolloServer } from './graphql';

const app: Application = express();
const PORT = process.env.PORT || 3009;
const nodeEnv = process.env.NODE_ENV || 'development';
const isDev = nodeEnv === 'development';

(async () => {
  const apolloServer = await createApolloServer(isDev);
  apolloServer.applyMiddleware({ app });

  app.listen(PORT, () => {
    console.log(`🟢🚀 Graphql server running on port ${PORT} - env ${nodeEnv}`);
  });
})();
