import fetch from 'node-fetch';

const REVALIDATE_WEB_PATH_TOKEN = process.env.REVALIDATE_WEB_PATH_TOKEN;
const WEB_PATH = process.env.WEB_PATH;

const logPrefix = () => {
  return `[${new Date().toISOString()}]`;
};

const revalidateWebPaths = async () => {
  try {
    console.log(`\n${logPrefix()} 🔵 Revalidate started`);
    const response = await fetch(`${WEB_PATH}/api/revalidate?token=${REVALIDATE_WEB_PATH_TOKEN}`);
    const data = await response.json();
    if (response.ok) {
      console.log(`${logPrefix()} 🟢 Revalidate done. Data:`, data);
    } else {
      console.log(`${logPrefix()} 🔴 Revalidate failed. Status: ${response.status}. Data:`, data);
    }
  } catch (e) {
    console.log(`${logPrefix()} 🔴 Revalidate failed. Error:`, e);
  }
  process.exit();
}

revalidateWebPaths();
