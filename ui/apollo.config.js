require('dotenv').config();

module.exports = {
  client: {
    includes: ['./pages/**/*.tsx', './queries/**/*.ts', './components/**/*.tsx', './generated/**/*'],
    tagName: 'gql',
    addTypename: true,
    service: {
      name: 'graphql-gateway',
      url: process.env.GRAPHQL_PATH,
    },
  },
};
