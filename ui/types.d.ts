import React, { PropsWithChildren } from 'react';

type Without<T, U> = { [P in Exclude<keyof T, keyof U>]?: never } & U;

declare global {
  // eslint-disable-next-line @typescript-eslint/ban-types
  type ReactFCWithChildren<P = {}> = React.FC<PropsWithChildren<P>>;
  type XOR<T, U> = Without<T, U> | Without<U, T>;
  type GQL_Date = string;
  type GQL_DateTime = string;
  type GQL_JSON = any;
}

export {};
