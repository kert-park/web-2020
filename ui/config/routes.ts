import { IconProps } from '@kertPark/components/Icon/Icon';
import { IconArticles } from '@kertPark/components/Icon/IconArticles';
import { IconGames } from '@kertPark/components/Icon/IconGames';
import { IconOverview } from '@kertPark/components/Icon/IconOverview';
import { IconRoster } from '@kertPark/components/Icon/IconRoster';
import { IconStatistics } from '@kertPark/components/Icon/IconStatistics';
import { IconTable } from '@kertPark/components/Icon/IconTable';
import { activeTeams } from '@kertPark/config/teams';
import { LinkUrl } from '@kertPark/config/types';
import { getTeamById } from '@kertPark/lib/team';
import React from 'react';

export enum QueryParams {
  category = 'kategorie',
  date = 'datum',
  group = 'skupina',
  page = 'stranka',
  place = 'misto',
  schedule = 'rozpis',
  view = 'zobrazeni',
}

type GetRouteWithQuery = {
  pathname: string;
  pathnameAs: string;
  query: Partial<Record<QueryParams, string | number | undefined>>;
};

export const getRouteObjectWithQuery = ({
  pathname,
  pathnameAs,
  query: queryObject,
}: GetRouteWithQuery): { link: LinkUrl; linkAs: LinkUrl } => {
  const query = Object.entries(queryObject).reduce((acc, [key, value]) => {
    if (Boolean(value)) {
      return {
        ...acc,
        [key]: value,
      };
    }
    return acc;
  }, {});
  return {
    link: {
      pathname,
      query,
    },
    linkAs: {
      pathname: pathnameAs,
      query,
    },
  };
};

export const routes = {
  index: '/',
  club: '/klub/[slug]',
  clubAs: (slug: string) => `/klub/${slug}`,
  clubAllGames: '/klub/multirozpis',
  clubAllGamesAs: '/klub/multirozpis',
  officialList: '/klub/realizacni-tymy',
  officialListAs: '/klub/realizacni-tymy',
  oldWebRedirect: '/[slug]/presmerovani',
  oldWebRedirectAs: (slug: string) => `/${slug}/presmerovani`,
  partners: '/klub/partneri',
  partnersAs: '/klub/partneri',
  posts: '/clanky',
  postsAs: '/clanky',
  post: '/[slug]',
  postAs: (slug: string) => `/${slug}`,
  team: {
    index: '/tym/[teamId]',
    indexAs: (teamId: string) => `/tym/${teamId}`,
    gameDetail: '/tym/[teamId]/zapasy/[year]/[id]',
    gamesDetailAs: (teamId: string, year: string, id: string) => `/tym/${teamId}/zapasy/${year}/${id}`,
    games: '/tym/[teamId]/zapasy',
    gamesAs: (teamId: string) => `/tym/${teamId}/zapasy`,
    player: '/tym/[teamId]/soupiska/[playerId]',
    playerAs: (teamId: string, playerId: string) => `/tym/${teamId}/soupiska/${playerId}`,
    roster: '/tym/[teamId]/soupiska',
    rosterAs: (teamId: string) => `/tym/${teamId}/soupiska`,
    statistics: '/tym/[teamId]/statistiky',
    statisticsAs: (teamId: string) => `/tym/${teamId}/statistiky`,
    table: '/tym/[teamId]/tabulka',
    tableAs: (teamId: string) => `/tym/${teamId}/tabulka`,
  },
};

type MainMenuType = Array<MenuItemType>;

export type MenuItemType = MenuLinkType & { items?: Array<MenuItemType> };

export type MenuLinkType = {
  name: string;
  link?: LinkUrl;
  linkAs?: LinkUrl;
  icon?: {
    component: React.FunctionComponent<IconProps>;
    props?: Record<string, any>;
  };
};

export const getTeamMenu = (teamId: string): Array<MenuLinkType> => {
  const team = getTeamById({ teamId });

  return [
    {
      name: 'Přehled',
      link: routes.team.index,
      linkAs: routes.team.indexAs(teamId),
      icon: { component: IconOverview },
    },
    {
      name: 'Články',
      link: { pathname: routes.posts, query: { [QueryParams.category]: teamId } },
      linkAs: { pathname: routes.postsAs, query: { [QueryParams.category]: teamId } },
      icon: { component: IconArticles },
    },
    {
      name: 'Soupiska',
      link: routes.team.roster,
      linkAs: routes.team.rosterAs(teamId),
      icon: { component: IconRoster },
    },
    ...(team?.hasGames
      ? [
          {
            name: 'Statistiky',
            link: routes.team.statistics,
            linkAs: routes.team.statisticsAs(teamId),
            icon: { component: IconStatistics },
          },
          {
            name: 'Tabulka',
            link: routes.team.table,
            linkAs: routes.team.tableAs(teamId),
            icon: { component: IconTable },
          },
          {
            name: 'Zápasy',
            link: routes.team.games,
            linkAs: routes.team.gamesAs(teamId),
            icon: { component: IconGames },
          },
        ]
      : []),
  ];
};

export const mainMenu: MainMenuType = [
  {
    name: 'Klub',
    items: [
      {
        name: 'Vedení',
        link: routes.club,
        linkAs: routes.clubAs('vedeni'),
      },
      {
        name: 'Kontakt',
        link: routes.club,
        linkAs: routes.clubAs('kontakt'),
      },
      {
        name: 'Hřiště',
        link: routes.club,
        linkAs: routes.clubAs('hriste'),
      },
      {
        name: 'Realizační týmy',
        link: routes.officialList,
        linkAs: routes.officialListAs,
      },
      {
        name: 'Rozpis tréninků',
        link: routes.club,
        linkAs: routes.clubAs('rozpis-treninku'),
      },
      {
        name: 'Multirozpis',
        link: routes.clubAllGames,
        linkAs: routes.clubAllGamesAs,
      },
      {
        name: 'Historie',
        link: routes.club,
        linkAs: routes.clubAs('historie'),
      },
      {
        name: 'HCM',
        link: routes.club,
        linkAs: routes.clubAs('hokejbalove-centrum-mladeze'),
      },
    ],
  },
  {
    name: 'Muži A',
    items: getTeamMenu('muzia'),
  },
  {
    name: 'Mládež',
    items: activeTeams
      .filter((team) => !['muzia'].includes(team.id))
      .map((team) => ({ name: team.name, link: routes.team.index, linkAs: routes.team.indexAs(team.id) })),
  },
  {
    name: 'Články',
    link: routes.posts,
    linkAs: routes.postsAs,
  },
  {
    name: 'Partneři',
    link: routes.partners,
    linkAs: routes.partnersAs,
  },
  {
    name: 'Nábor',
    link: routes.club,
    linkAs: routes.clubAs('nabor'),
  },
];

export const getQueryString = (query: string | Array<string> | undefined): string | undefined => {
  if (!query) {
    return undefined;
  }
  return typeof query === 'string' ? query : query[0] || '';
};
