import { teamsObject as teamsObjectJs } from './teamsObject';

// @ts-ignore
const TEAMS: Array<string> = process.env.TEAMS || [];

export type Team = {
  id: string;
  name: string;
  nameGenitiv: string;
  initials: string;
  hasGames: boolean;
  order: number;
};

export const teamsObject = teamsObjectJs;

export const teamsArray: Array<Team> = Object.values(teamsObject);

export const activeTeams: Array<Team> = teamsArray.filter((team) => TEAMS.includes(team.id));
