const APP_PATH = process.env.APP_PATH;

type PostThumb = Record<ImageSize, string>;

export enum ImageSize {
  large = 'large',
  medium = 'medium',
  small = 'small',
  thumbnail = 'thumbnail',
}

const postThumb: PostThumb = {
  [ImageSize.large]: '/post-thumb-large.jpg',
  [ImageSize.medium]: '/post-thumb-medium.jpg',
  [ImageSize.small]: '/post-thumb-small.jpg',
  [ImageSize.thumbnail]: '/post-thumb-thumbnail.jpg',
};

type GetPostImageUrlProps = {
  formats?: Record<ImageSize, { url: string }> | Record<string, any>;
  size: ImageSize;
};

export const getThumbImageUrl = ({ size }: Pick<GetPostImageUrlProps, 'size'>): string => {
  return `${APP_PATH}${postThumb[size]}`;
};

export const getPostImageUrl = ({ formats, size }: GetPostImageUrlProps): string => {
  if (formats?.[size]?.url) {
    return formats[size].url;
  }
  return getThumbImageUrl({ size });
};
