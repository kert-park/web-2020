import { LinkProps } from 'next/link';

type PropType<TObj, TProp extends keyof TObj> = TObj[TProp];
export type LinkUrl = NonNullable<PropType<LinkProps, 'as'>>;
