const kertColors = {
  bluePrimary: '#292f6d',
  redPrimary: '#c64154',
  yellowPrimary: '#F8EF4A',
  blueSecondary: '#2A5298',
  redSecondary: '#ab051c',
  yellowSecondary: '#FFFAA7',
};

export const breakpoints = {
  md: '48em', // Eq. 768px
  lg: '62em', // Eq. 992px
  xl: '75em', // Eq. 1200px
  xxl: '90em', // Eq. 1440px
};

export const mediaBreakpoints = {
  md: `min-width: ${breakpoints.md}`, // Eq. 768px
  lg: `min-width: ${breakpoints.lg}`, // Eq. 992px
  xl: `min-width: ${breakpoints.xl}`, // Eq. 1200px
  xxl: `min-width: ${breakpoints.xxl}`, // Eq. 1440px
};

export const spaces = ['0', '0.25rem', '0.5rem', '1rem', '2rem', '4rem', '8rem', '16rem'];

export const typography = {
  h1: {
    fontSize: {
      xs: '1.8rem',
      lg: '2.5rem',
    },
  },
  h2: {
    fontSize: {
      xs: '1.6rem',
      lg: '2.075rem',
    },
  },
  h3: {
    fontSize: {
      xs: '1.425rem',
      lg: '1.725rem',
    },
  },
  h4: {
    fontSize: {
      xs: '1.25rem',
      lg: '1.45rem',
    },
  },
  h5: {
    fontSize: {
      xs: '1.125rem',
      lg: '1.2rem',
    },
  },
  overline: {
    fontSize: {
      small: '0.75rem',
      normal: '0.875rem',
    },
  },
  p: {
    fontSize: {
      xs: '1rem',
      lg: '1.125rem',
    },
  },
  pSmall: {
    fontSize: {
      xs: '0.875rem',
      lg: '0.875rem',
    },
  },
};

export const zIndex = {
  desktopHeader: 2,
  desktopMenuDropdown: 2,
  desktopMenuDropdownItem: 3,
  layoutContent: 1,
  mobileMenuSidebar: 2,
  mobileMenuHeader: 3,
  mobileDropdown: 2,
  cookieBar: 4,
  modal: 100,
};

export const theme = {
  colors: {
    textPrimary: '#333333',
    textSecondary: '#999999',
    textTertiary: '#666666',
    textLight: '#bbbbbb',
    textInverted: '#fafafa',
    backgroundPrimary: '#f6f6f6',
    backgroundSecondary: '#fafafa',
    primary: kertColors.bluePrimary,
    secondary: kertColors.redPrimary,
    lightGrey: '#e5e5e5',
    midGrey: '#bbbbbb',
    grey: '#999999',
    green: '#2e7d32',
    black: '#000000',
    white: '#ffffff',
    kert: kertColors,
    boxShadow: {
      normal: 'rgba(0, 0, 0, 0.125)',
      hover: 'rgba(0, 0, 0, 0.25)',
      cookieBar: 'rgba(0, 0, 0, 0.5)',
      hoverRed: 'rgba(198, 65, 84, 0.5)',
      normalRed: 'rgba(198, 65, 84, 0.3)',
    },
    loader: {
      background: '#e5e5e5',
      foreground: '#f0f0f0',
    },
    warning: {
      background: '#fff4e5',
      text: '#663c00',
    },
  },
  gradients: {
    blueRed: `linear-gradient(to bottom right, ${kertColors.bluePrimary} 0%, ${kertColors.redSecondary} 100%)`,
    blue: `linear-gradient(to right, ${kertColors.bluePrimary} 0%, ${kertColors.blueSecondary} 100%)`,
    red: `linear-gradient(to right, ${kertColors.redPrimary} 0%, ${kertColors.redSecondary} 100%)`,
  },
  fonts: {
    header: 'Roboto Condensed, sans-serif',
    text: 'Roboto Condensed, sans-serif',
  },
  space: [0, 4, 8, 16, 32, 64, 128, 256],
  breakpoints: [breakpoints.md, breakpoints.lg, breakpoints.xl, breakpoints.xxl],
  radius: {
    card: '1em',
  },
  spacing: {
    card: spaces[3],
    paragraphs: '1.15rem',
  },
  layout: {
    textContentMaxWidth: '48em',
    videoMaxSize: {
      width: 768,
      height: 432,
    },
  },
  transitionSpeed: '0.2s ease-in-out',
  transitionSpeedMenu: '0.5s cubic-bezier(0.645, 0.045, 0.355, 1)',
};

export const darkTheme = {
  ...theme,
  colors: {
    textPrimary: '#e5e5e5',
    textSecondary: '#bbbbbb',
    textLight: '#999999',
    textInverted: '#333333',
    backgroundPrimary: '#0e0c24',
    backgroundSecondary: '#1b1b32',
    primary: kertColors.yellowPrimary,
    secondary: kertColors.redPrimary,
    lightGrey: '#16213e',
    grey: '#999999',
    green: '#2e7d32',
    kert: kertColors,
    boxShadow: {
      activeRadio: 'rgba(198, 65, 84, 0.5)',
      normal: 'rgba(0, 0, 0, 0.125)',
      hover: 'rgba(0, 0, 0, 0.25)',
    },
  },
};
