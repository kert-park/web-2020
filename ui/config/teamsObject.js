const teamsObject = {
  muzia: {
    id: 'muzia',
    name: 'Muži A',
    nameGenitiv: 'Mužů A',
    initials: 'A',
    hasGames: true,
    order: 1,
  },
  juniori: {
    id: 'juniori',
    name: 'Junioři',
    nameGenitiv: 'Juniorů',
    initials: 'J',
    hasGames: true,
    order: 2,
  },
  dorost: {
    id: 'dorost',
    name: 'Dorost',
    nameGenitiv: 'Dorostu',
    initials: 'D',
    hasGames: true,
    order: 3,
  },
  'starsi-zaci': {
    id: 'starsi-zaci',
    name: 'Starší žáci',
    nameGenitiv: 'Starších žáků',
    initials: 'SŽ',
    hasGames: true,
    order: 4,
  },
  'mladsi-zaci': {
    id: 'mladsi-zaci',
    name: 'Mladší žáci',
    nameGenitiv: 'Mladších žáků',
    initials: 'MŽ',
    hasGames: true,
    order: 5,
  },
  pripravka: {
    id: 'pripravka',
    name: 'Přípravka',
    nameGenitiv: 'Přípravky',
    initials: 'P',
    hasGames: false,
    order: 6,
  },
  minipripravka: {
    id: 'minipripravka',
    name: 'Minipřípravka',
    nameGenitiv: 'Minipřípravky',
    initials: 'MP',
    hasGames: false,
    order: 7,
  },
};

module.exports = {
  teamsObject,
};
