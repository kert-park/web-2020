import { HiddenDesktopBox, HiddenMobileBox } from '@kertPark/components/Atomic/Box';
import { LinkButton } from '@kertPark/components/Atomic/Button';
import { Card } from '@kertPark/components/Atomic/Card';
import { HiddenDesktopFlex, HiddenMobileFlex } from '@kertPark/components/Atomic/Flex';
import { H2 } from '@kertPark/components/Atomic/Typography';
import { NearestGames } from '@kertPark/components/Game/NearestGames';
import { MultirozpisYouthGamesCards } from '@kertPark/components/Game/SmallCard/MultirozpisYouthGamesCards';
import { NearestHomeGamesCards } from '@kertPark/components/Game/SmallCard/NearestHomeGamesCards';
import { Layout } from '@kertPark/components/Layout/Layout';
import { PlayOffSmallOverview } from '@kertPark/components/PlayOff/PlayOffSmallOverview';
import { CardWithThumbPost } from '@kertPark/components/PostList/CardWithThumbPost';
import { SimplePost } from '@kertPark/components/PostList/SimplePost';
import { RecruitmentBannerDesktop } from '@kertPark/components/Recruitment/RecruitmentBannerDesktop';
import { RecruitmentBannerMobile } from '@kertPark/components/Recruitment/RecruitmentBannerMobile';
import { routes } from '@kertPark/config/routes';
import { spaces, theme } from '@kertPark/config/theme';
import {
  GameFilterPlaceEnum,
  GameFilterScheduleEnum,
  GameListDocument,
  GameListQuery,
  GameListQueryVariables,
  PlayOffListDocument,
  PlayOffListQuery,
  PlayOffListQueryVariables,
  PostsCompositeDocument,
  PostsCompositeQuery,
  PostsCompositeQueryVariables,
  TournamentListQuery,
} from '@kertPark/graphql/generated';
import { PlayOffRoundType, PostsCompositeQueryPostsType } from '@kertPark/graphql/generated/helpers';
import { REST_POST_ID, useHomePagePosts } from '@kertPark/hooks/useHomePagePosts';
import { getApolloStateProp, initializeApollo } from '@kertPark/lib/apollo/apolloClient';
import { getDateObject } from '@kertPark/lib/date';
import { getMultirozpisData } from '@kertPark/lib/multirozpis';
import { getCurrentPlayOffRound } from '@kertPark/lib/playOff';
import { Box, Flex } from '@rebass/grid';
import { GetStaticProps, NextPage } from 'next';
import React from 'react';

type Props = {
  mainTeamPlayOffRound?: PlayOffRoundType;
  multirozpisDateString: string;
  multirozpisGames: GameListQuery['gameList'];
  multirozpisTournaments: TournamentListQuery['tournamentList'];
  nearestHomeGames: GameListQuery['gameList'];
  nearestMainTeamGames: GameListQuery['gameList'];
  posts: PostsCompositeQueryPostsType;
};

const Home: NextPage<Props> = ({
  mainTeamPlayOffRound,
  multirozpisDateString,
  multirozpisGames,
  multirozpisTournaments,
  nearestHomeGames,
  nearestMainTeamGames,
  posts = [],
}) => {
  const { mainContentRef, mobileRestPosts, restPosts, sidebarRef, thumbPosts } = useHomePagePosts({
    posts,
  });

  return (
    <Layout>
      <Flex alignItems="flex-start" flexWrap="wrap" m={-2} marginTop={1}>
        <Flex ref={mainContentRef} alignItems="stretch" flexWrap="wrap" width={[1, 2 / 3]}>
          {thumbPosts.map((post) => (
            <Box key={`${post.__typename}_${post.id}`} width={[1, 1 / 2]} p={2}>
              <CardWithThumbPost post={post} />
            </Box>
          ))}
          <HiddenMobileBox width={1} p={2}>
            <RecruitmentBannerDesktop />
          </HiddenMobileBox>
          <HiddenMobileFlex id={REST_POST_ID} flexDirection="column">
            {restPosts.map((post) => (
              <Box width={1} p={2} key={`${post.__typename}_${post.id}`}>
                <SimplePost post={post} />
              </Box>
            ))}
          </HiddenMobileFlex>
          <HiddenMobileFlex justifyContent="center" width={1} p={2}>
            <LinkButton href={routes.posts} hrefAs={routes.postsAs}>
              Další články
            </LinkButton>
          </HiddenMobileFlex>
        </Flex>
        <Flex ref={sidebarRef} flexWrap="wrap" width={[1, 1 / 3]}>
          {mainTeamPlayOffRound ? (
            <Box width={1} p={2}>
              <Card>
                <PlayOffSmallOverview playOffRound={mainTeamPlayOffRound} showGames />
              </Card>
            </Box>
          ) : (
            <NearestGames games={nearestMainTeamGames} />
          )}
          <HiddenDesktopBox width={1} p={2}>
            <RecruitmentBannerMobile />
          </HiddenDesktopBox>
          <HiddenDesktopBox width={1}>
            <H2 marginBottom="0" marginLeft={`calc(${spaces[2]} + ${theme.spacing.card})`}>
              Další články
            </H2>
          </HiddenDesktopBox>
          {mobileRestPosts.map((post) => (
            <HiddenDesktopBox width={1} p={2} key={`${post.__typename}_${post.id}`}>
              <SimplePost post={post} />
            </HiddenDesktopBox>
          ))}
          <HiddenDesktopFlex justifyContent="center" width={1}>
            <LinkButton href={routes.posts} hrefAs={routes.postsAs}>
              Další články
            </LinkButton>
          </HiddenDesktopFlex>
          <NearestHomeGamesCards games={nearestHomeGames} />
          <MultirozpisYouthGamesCards
            date={getDateObject(multirozpisDateString)}
            games={multirozpisGames}
            tournaments={multirozpisTournaments}
          />
          <Flex justifyContent="center" width={1}>
            <LinkButton href={routes.clubAllGames} hrefAs={routes.clubAllGamesAs}>
              Multirozpis
            </LinkButton>
          </Flex>
        </Flex>
      </Flex>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps<Props> = async () => {
  const client = initializeApollo();
  const { data: dataPosts, error: errorPosts } = await client.query<PostsCompositeQuery, PostsCompositeQueryVariables>({
    query: PostsCompositeDocument,
    variables: {
      pagination: {
        page: 1,
        pageSize: 10,
      },
    },
  });

  const { data: dataNearestMainTeamGames, error: errorNearestMainTeamGames } = await client.query<
    GameListQuery,
    GameListQueryVariables
  >({
    query: GameListDocument,
    variables: {
      teamId: 'muzia',
      filter: {
        place: GameFilterPlaceEnum.ALL,
        schedule: GameFilterScheduleEnum.ALL,
        nearest: true,
        limit: 2,
      },
    },
  });

  const { data: dataMainTeamPlayOffs, error: errorMainTeamPlayOffs } = await client.query<
    PlayOffListQuery,
    PlayOffListQueryVariables
  >({
    query: PlayOffListDocument,
    variables: {
      teamId: 'muzia',
    },
  });

  const { data: dataNearestHomeGames, error: errorNearestHomeGames } = await client.query<
    GameListQuery,
    GameListQueryVariables
  >({
    query: GameListDocument,
    variables: {
      filter: {
        place: GameFilterPlaceEnum.HOME_GROUND,
        schedule: GameFilterScheduleEnum.UPCOMING,
        limit: 6,
      },
    },
  });

  const {
    defaultDate: multirozpisDate,
    games: multirozpisGames,
    tournaments: multirozpisTournaments,
  } = await getMultirozpisData(client);
  const mainTeamPlayOffRound = errorMainTeamPlayOffs
    ? undefined
    : getCurrentPlayOffRound(dataMainTeamPlayOffs.playOffList);

  return {
    props: {
      ...getApolloStateProp(client),
      posts: errorPosts ? [] : dataPosts.postsComposite?.data || [],
      ...(mainTeamPlayOffRound ? { mainTeamPlayOffRound } : {}),
      multirozpisDateString: multirozpisDate.toISOString(),
      multirozpisGames,
      multirozpisTournaments,
      nearestHomeGames: errorNearestHomeGames ? [] : dataNearestHomeGames.gameList,
      nearestMainTeamGames: errorNearestMainTeamGames ? [] : dataNearestMainTeamGames.gameList,
    },
    revalidate: 30,
  };
};

export default Home;
