import { PostPage, PostPageProps } from '@kertPark/components/Post/PostPage';
import { PostDocument, PostQuery, PostQueryVariables, PublicationState } from '@kertPark/graphql/generated';
import { initializeApollo } from '@kertPark/lib/apollo/apolloClient';
import { GetServerSideProps } from 'next';

export const getServerSideProps: GetServerSideProps<PostPageProps> = async ({ params }) => {
  const client = initializeApollo();

  const slug = params?.slug as string;

  if (!slug) {
    return {
      notFound: true,
      revalidate: 1,
    };
  }

  const { data } = await client.query<PostQuery, PostQueryVariables>({
    query: PostDocument,
    variables: {
      publicationState: PublicationState.PREVIEW,
      slug,
    },
  });

  const post = data.posts?.data[0];

  if (!post) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      post,
      preview: true,
    },
  };
};

export default PostPage;
