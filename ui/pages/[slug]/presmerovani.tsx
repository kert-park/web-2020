import { ImageAspectRatio } from '@kertPark/components/Atomic/ImageAspectRatio';
import { LinkAnchor } from '@kertPark/components/Atomic/LinkAnchor';
import { H1, P } from '@kertPark/components/Atomic/Typography';
import { Layout } from '@kertPark/components/Layout/Layout';
import { spaces } from '@kertPark/config/theme';
import { getExistingSlugOnOldWeb } from '@kertPark/lib/oldWebRedirect';
import { Flex } from '@rebass/grid';
import { GetServerSideProps, NextPage } from 'next';
import React from 'react';
import styled from 'styled-components';

const ImageWrapper = styled.div`
  margin-top: ${spaces[3]};
  width: 100%;
  max-width: 37.5em;
`;

type Props = {
  url: string;
};

const RedirectPage: NextPage<Props> = ({ url = 'http://kert-park.cz' }) => {
  const title = 'Odkaz vede na starý web';
  return (
    <Layout noRobots>
      <Flex width={1} flexDirection="column" alignItems="center">
        <H1>{title}</H1>
        <P>Tento odkaz na současném webu neexistuje. Našli jsme jej ale na starém webu.</P>
        <LinkAnchor href={url} target="_blank" isText>
          Otevřít stránku na starém webu
        </LinkAnchor>
        <ImageWrapper>
          <ImageAspectRatio src="/redirect.jpg" title={title} />
        </ImageWrapper>
      </Flex>
    </Layout>
  );
};

export const getServerSideProps: GetServerSideProps<Props> = async ({ params }) => {
  const slug = params?.slug;

  if (slug && typeof slug === 'string') {
    const url = await getExistingSlugOnOldWeb(slug);
    if (url) {
      return {
        props: {
          url,
        },
      };
    }
  }

  return {
    notFound: true,
  };
};
export default RedirectPage;
