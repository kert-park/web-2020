import { PostPage, PostPageProps } from '@kertPark/components/Post/PostPage';
import { routes } from '@kertPark/config/routes';
import {
  GameDocument,
  GameQuery,
  GameQueryVariables,
  PostDocument,
  PostQuery,
  PostQueryVariables,
  PostsCompositeDocument,
  PostsCompositeQuery,
  PostsCompositeQueryVariables,
  PublicationState,
} from '@kertPark/graphql/generated';
import { getApolloStateProp, initializeApollo } from '@kertPark/lib/apollo/apolloClient';
import { getExistingSlugOnOldWeb } from '@kertPark/lib/oldWebRedirect';
import { getPostApiGameFromContent } from '@kertPark/lib/post';
import { GetStaticPaths, GetStaticProps } from 'next';
import { GetStaticPathsResult } from 'next/types';

export const getStaticPaths: GetStaticPaths = async () => {
  const client = initializeApollo();
  const { data: dataPosts } = await client.query<PostsCompositeQuery, PostsCompositeQueryVariables>({
    query: PostsCompositeDocument,
    variables: { pagination: { page: 1, pageSize: 20 } },
  });
  const posts = dataPosts.postsComposite?.data || [];
  const paths = posts.reduce<GetStaticPathsResult['paths']>((acc, post) => {
    if (post.__typename === 'PostEntity' && post.attributes?.slug) {
      acc.push({ params: { slug: post.attributes.slug } });
    }
    return acc;
  }, []);

  return { paths, fallback: true };
};

export const getStaticProps: GetStaticProps<PostPageProps> = async ({ params }) => {
  const client = initializeApollo();

  const slug = params?.slug as string;

  if (!slug) {
    return {
      notFound: true,
      revalidate: 1,
    };
  }

  const { data } = await client.query<PostQuery, PostQueryVariables>({
    query: PostDocument,
    variables: {
      publicationState: PublicationState.LIVE,
      slug,
    },
  });

  const post = data.posts?.data[0];

  if (!post) {
    if (Boolean(await getExistingSlugOnOldWeb(slug))) {
      return {
        redirect: {
          destination: routes.oldWebRedirectAs(slug),
          permanent: false,
        },
      };
    }
    return {
      notFound: true,
      revalidate: 1,
    };
  }

  const postGames: Array<GameQueryVariables> =
    post.attributes?.content.reduce((games, content) => {
      const game = getPostApiGameFromContent(content);

      if (game) {
        return [...games, game];
      }

      return games;
    }, []) || [];

  for (const postGame of postGames) {
    await client.query<GameQuery, GameQueryVariables>({
      query: GameDocument,
      variables: postGame,
    });
  }

  return {
    props: {
      ...getApolloStateProp(client),
      post,
      preview: false,
    },
    revalidate: 30,
  };
};

export default PostPage;
