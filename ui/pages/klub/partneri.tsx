import { Card } from '@kertPark/components/Atomic/Card';
import { ContentLoader, Rect } from '@kertPark/components/Atomic/ContentLoader';
import { LinkAnchor } from '@kertPark/components/Atomic/LinkAnchor';
import { H1, H2, P } from '@kertPark/components/Atomic/Typography';
import { Layout } from '@kertPark/components/Layout/Layout';
import { mediaBreakpoints, spaces, theme } from '@kertPark/config/theme';
import { usePartnersQuery } from '@kertPark/graphql/generated';
import { PartnersQueryPartnersListType, PartnersQueryPartnerType } from '@kertPark/graphql/generated/helpers';
import { Flex } from '@rebass/grid';
import { NextPage } from 'next';
import React from 'react';
import styled from 'styled-components';

enum Sizes {
  medium = 'medium',
  small = 'small',
}

const sizes = {
  [Sizes.small]: {
    maxWidth: '240px',
    maxHeight: '80px',
  },
  [Sizes.medium]: {
    maxWidth: '360px',
    maxHeight: '120px',
  },
};

const StyledCard = styled(Card)`
  flex: 0 0 auto;
  margin: ${spaces[2]};
  width: auto;
  height: auto;
`;

const Name = styled(P)`
  margin-bottom: 0;
`;

const Image = styled.img<{ size: Sizes }>`
  max-width: 240px;
  max-height: 80px;
  object-fit: contain;

  @media (${mediaBreakpoints.md}) {
    max-width: ${({ size }) => sizes[size].maxWidth};
    max-height: ${({ size }) => sizes[size].maxHeight};
  }
`;

const ImageWrapper = styled.div<{ size: Sizes }>`
  display: flex;
  align-items: center;
  height: ${sizes[Sizes.small].maxHeight};
  @media (${mediaBreakpoints.md}) {
    height: ${({ size }) => sizes[size].maxHeight};
  }
`;

const PartnerLoader = () => {
  return (
    <StyledCard flexDirection="row" noPadding>
      <ContentLoader viewBox="0 0 240 160" width="240" height="160">
        <Rect rx={theme.radius.card} ry={theme.radius.card} x="0" y="0" width="240" height="120" />
        <Rect x="60" y="134" width="120" height="10" />
      </ContentLoader>
    </StyledCard>
  );
};

const Partner: React.FC<{
  partner: PartnersQueryPartnerType;
  size?: Sizes;
}> = ({ partner, size = Sizes.medium }) => {
  const { name, image, url } = partner || {};
  const imageSrc = image?.data?.attributes?.formats?.small?.url || image?.data?.attributes?.url;

  if (!imageSrc) {
    return null;
  }

  const content = (
    <StyledCard isLink={!!url} alignItems="center">
      <ImageWrapper size={size}>
        <Image alt={name} src={imageSrc} size={size} />
      </ImageWrapper>
      <Name>{name}</Name>
    </StyledCard>
  );

  if (!!url) {
    return (
      <LinkAnchor href={url} target="_blank">
        {content}
      </LinkAnchor>
    );
  }

  return content;
};

const PartnerList: React.FC<{
  partnerList: PartnersQueryPartnersListType;
  withHeader: boolean;
}> = ({ partnerList, withHeader }) => {
  return (
    <>
      {withHeader && <H2>{partnerList?.name}</H2>}
      <Flex
        width={1}
        flexDirection="row"
        alignItems="stretch"
        justifyContent={['center', 'flex-start']}
        flexWrap="wrap"
        m={-2}
      >
        {partnerList?.partners.map((partner) => {
          return <Partner key={partner?.id} partner={partner} size={Sizes.medium} />;
        })}
      </Flex>
    </>
  );
};

const Partneri: NextPage = () => {
  const { data, loading } = usePartnersQuery();

  const partnerLists = data?.partner?.data?.attributes?.partnerLists || [];

  return (
    <Layout title={['Partneři', 'Klub']} withPartners={false}>
      <Flex width={1} flexDirection="column">
        <H1 marginTop="0">Partneři</H1>
        {loading ? (
          <Flex
            width={1}
            flexDirection="row"
            alignItems="stretch"
            justifyContent={['center', 'flex-start']}
            flexWrap="wrap"
            m={-2}
          >
            <PartnerLoader />
            <PartnerLoader />
            <PartnerLoader />
            <PartnerLoader />
            <PartnerLoader />
          </Flex>
        ) : (
          partnerLists.map((partnerList) => (
            <PartnerList key={partnerList?.slug} partnerList={partnerList} withHeader={partnerLists.length > 1} />
          ))
        )}
      </Flex>
    </Layout>
  );
};

export default Partneri;
