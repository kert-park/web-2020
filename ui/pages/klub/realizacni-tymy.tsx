import { H1 } from '@kertPark/components/Atomic/Typography';
import { Layout } from '@kertPark/components/Layout/Layout';
import { OfficialList } from '@kertPark/components/OfficialList/OfficialList';
import { ClubSidebar } from '@kertPark/components/Sidebar/ClubSidebar';
import { mediaBreakpoints, spaces, theme } from '@kertPark/config/theme';
import {
  CategoriesDocument,
  CategoriesQuery,
  CategoriesQueryVariables,
  CommonOfficialListFragment,
} from '@kertPark/graphql/generated';
import { initializeApollo } from '@kertPark/lib/apollo/apolloClient';
import { getOfficialListFromCategories } from '@kertPark/lib/officials';
import { Flex } from '@rebass/grid';
import { GetStaticProps, NextPage } from 'next';
import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  width: 100%;
  @media (${mediaBreakpoints.xxl}) {
    max-width: ${theme.layout.textContentMaxWidth};
  }
`;

type Props = {
  officialList: Array<CommonOfficialListFragment>;
};

const OfficialsList: NextPage<Props> = ({ officialList }) => {
  const title = 'Realizační týmy';
  return (
    <Layout title={[title, 'Klub']} description={title}>
      <Flex alignItems="flex-start" flexWrap="wrap" m={-2}>
        <Flex alignItems="flex-start" flexWrap="wrap" width={[1, 2 / 3]}>
          <Flex width={1} p={2} flexDirection="column" alignItems="center">
            <Wrapper>
              <H1 marginTop={spaces[3]}>{title}</H1>
              {officialList.map((item) => (
                <OfficialList key={item.id} item={item} />
              ))}
            </Wrapper>
          </Flex>
        </Flex>
        <Flex flexWrap="wrap" width={[1, 1 / 3]}>
          <ClubSidebar />
        </Flex>
      </Flex>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps<Props> = async () => {
  const client = initializeApollo();

  const { data, error } = await client.query<CategoriesQuery, CategoriesQueryVariables>({
    query: CategoriesDocument,
  });

  const categories = data.categories?.data;

  if (error || !categories) {
    return {
      notFound: true,
      revalidate: 1,
    };
  }

  const officialList = getOfficialListFromCategories(categories);

  return {
    props: {
      officialList,
    },
    revalidate: 30,
  };
};

export default OfficialsList;
