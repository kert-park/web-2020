import { Card } from '@kertPark/components/Atomic/Card';
import { RadioButtonList } from '@kertPark/components/Atomic/RadioButtonList';
import { H2, Overline } from '@kertPark/components/Atomic/Typography';
import { AddToCalendar } from '@kertPark/components/Game/AddToCalendar';
import { GameListDateFilter } from '@kertPark/components/Game/GameListDateFilter';
import { GameListItem, GameListItemLoader } from '@kertPark/components/Game/GameListItem/GameListItem';
import { Layout } from '@kertPark/components/Layout/Layout';
import { TeamSidebar } from '@kertPark/components/Sidebar/TeamSidebar';
import { TournamentListItem } from '@kertPark/components/Tournament/TournamentListItem';
import { getQueryString, getRouteObjectWithQuery, QueryParams, routes } from '@kertPark/config/routes';
import {
  GameFilterPlaceEnum,
  GameFilterScheduleEnum,
  GameListQuery,
  TournamentListQuery,
  useGameListQuery,
  useTournamentListQuery,
} from '@kertPark/graphql/generated';
import { getApolloStateProp, initializeApollo } from '@kertPark/lib/apollo/apolloClient';
import { getDateObject } from '@kertPark/lib/date';
import { filterAndSortGames } from '@kertPark/lib/game';
import {
  filterPlaceOptions,
  getDateRange,
  getGameFilterPlace,
  getTextFilterPlace,
  groupGamesAndTournamentsByLabel,
  mergeGamesAndTournamentsAndSort,
} from '@kertPark/lib/gamePage';
import { getMultirozpisData } from '@kertPark/lib/multirozpis';
import { getTeamById } from '@kertPark/lib/team';
import { filterTournaments } from '@kertPark/lib/tournament';
import { Box, Flex } from '@rebass/grid';
import { format, parse } from 'date-fns';
import { GetStaticProps, NextPage } from 'next';
import { useRouter } from 'next/router';
import React, { useCallback, useMemo } from 'react';

type Props = {
  defaultDateString: string;
  games: GameListQuery['gameList'];
  tournaments: TournamentListQuery['tournamentList'];
};

const getNoGameText = ({ filterPlace }: { filterPlace: GameFilterPlaceEnum }): string => {
  switch (filterPlace) {
    case GameFilterPlaceEnum.AWAY:
      return 'Žádná venkovní utkání v daném týdnu';
    case GameFilterPlaceEnum.HOME:
      return 'Žádná domácí utkání v daném týdnu';
    case GameFilterPlaceEnum.HOME_GROUND:
      return 'Žádná utkání na Lužinách v daném týdnu';
    case GameFilterPlaceEnum.ALL:
    default:
      return 'Žádná utkání v daném týdnu';
  }
};

const getDate = (defaultDateString: string, dateString: string | undefined): Date => {
  if (dateString) {
    try {
      return parse(dateString, 'yyyy-MM-dd', new Date());
    } catch {}
  }
  return getDateObject(defaultDateString);
};

const aTeam = getTeamById({ teamId: 'muzia' });

const GamesPage: NextPage<Props> = ({ defaultDateString, games = [], tournaments = [] }) => {
  const { query, replace } = useRouter();
  const date = getDate(defaultDateString, getQueryString(query[QueryParams.date]));
  const filterPlace = getGameFilterPlace(getQueryString(query[QueryParams.place]));

  const handleChangeRoute = useCallback(
    (place: GameFilterPlaceEnum, date: Date) => {
      const { link, linkAs } = getRouteObjectWithQuery({
        pathname: routes.clubAllGames,
        pathnameAs: routes.clubAllGamesAs,
        query: {
          [QueryParams.place]: getTextFilterPlace(place),
          [QueryParams.date]: format(date, 'yyyy-MM-dd'),
        },
      });
      replace(link, linkAs);
    },
    [replace],
  );

  const { from, to } = useMemo(() => getDateRange(date), [date]);

  const { data, loading: loadingGames } = useGameListQuery({
    variables: {
      filter: {
        from,
        to,
      },
    },
  });
  const { data: dataTournaments, loading: loadingTournaments } = useTournamentListQuery({
    variables: {
      from,
      to,
    },
  });

  const allItems = useMemo(() => {
    return groupGamesAndTournamentsByLabel(
      mergeGamesAndTournamentsAndSort(
        filterAndSortGames({ filterPlace, filterSchedule: GameFilterScheduleEnum.ALL, games: data?.gameList || games }),
        filterTournaments({ filterPlace, tournaments: dataTournaments?.tournamentList || tournaments }),
      ),
      true,
    );
  }, [data?.gameList, dataTournaments?.tournamentList, games, tournaments]);

  const loading = loadingGames || loadingTournaments;

  return (
    <Layout title={['Multirozpis zápasů']}>
      <Flex alignItems="flex-start" flexWrap="wrap" m={-2}>
        <Flex alignItems="flex-start" flexWrap="wrap" width={[1, 2 / 3]}>
          <Box width={1} px={2} pb={2}>
            <Card noBackground>
              <Flex flex={1} flexDirection="row" flexWrap="wrap" alignItems="center" justifyContent="space-between">
                <H2 margin="0">Multirozpis zápasů</H2>
                <AddToCalendar team={aTeam} />
              </Flex>
              <Flex flex={1} flexDirection="row" flexWrap="wrap" alignItems="flex-start" m={-1} pt={2}>
                <Box width={[1, 1 / 2]} p={1}>
                  <GameListDateFilter date={date} filterPlace={filterPlace} />
                </Box>
                <Box width={[1, 1 / 2]} p={1}>
                  <RadioButtonList<GameFilterPlaceEnum>
                    header="Domací/Hosté"
                    options={filterPlaceOptions}
                    onChange={(selected) => handleChangeRoute(selected, date)}
                    selected={filterPlace}
                  />
                </Box>
              </Flex>
            </Card>
          </Box>
          {loading &&
            [0, 1, 2].map((index) => (
              <Box key={index} width={1} p={2}>
                <Card>
                  <GameListItemLoader />
                </Card>
              </Box>
            ))}
          {!loading &&
            allItems.length > 0 &&
            allItems.map((group, index) => (
              <Box key={`${group.label}_${index}`} width={1} p={2}>
                <Card>
                  <Overline>{group.label}</Overline>
                  {group.items.map((item) =>
                    item.__typename === 'Tournament' ? (
                      <TournamentListItem key={`tournament_${item.id}`} tournament={item} />
                    ) : (
                      <GameListItem
                        key={`game_${item.id}`}
                        game={item}
                        hasLargeMetadata={item.team.systemName === 'muzia'}
                      />
                    ),
                  )}
                </Card>
              </Box>
            ))}
          {!loading && allItems.length === 0 && <Card noBackground>{getNoGameText({ filterPlace })}</Card>}
        </Flex>
        <Flex flexWrap="wrap" width={[1, 1 / 3]}>
          <TeamSidebar team={aTeam} />
        </Flex>
      </Flex>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps<Props> = async () => {
  const client = initializeApollo();

  const { defaultDate, games, tournaments } = await getMultirozpisData(client);

  return {
    props: {
      ...getApolloStateProp(client),
      defaultDateString: defaultDate.toISOString(),
      games,
      tournaments,
    },
    revalidate: 30,
  };
};

export default GamesPage;
