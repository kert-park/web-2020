import { ClubPage, ClubPageProps } from '@kertPark/components/Club/ClubPage';
import {
  ClubPageDocument,
  ClubPageQuery,
  ClubPageQueryVariables,
  ClubPagesDocument,
  ClubPagesQuery,
  PublicationState,
} from '@kertPark/graphql/generated';
import { initializeApollo } from '@kertPark/lib/apollo/apolloClient';
import { GetStaticPaths, GetStaticProps } from 'next';

export const getStaticPaths: GetStaticPaths = async () => {
  const client = initializeApollo();
  const { data } = await client.query<ClubPagesQuery>({
    query: ClubPagesDocument,
  });
  const pages = data.clubPages?.data || [];
  const paths = pages.map((page) => ({
    params: { slug: page.attributes?.slug },
  }));

  return { paths, fallback: true };
};

export const getStaticProps: GetStaticProps<ClubPageProps> = async ({ params }) => {
  const client = initializeApollo();
  const slug = params?.slug;

  if (!slug || typeof slug !== 'string') {
    return {
      notFound: true,
      revalidate: 1,
    };
  }

  const { data } = await client.query<ClubPageQuery, ClubPageQueryVariables>({
    query: ClubPageDocument,
    variables: { slug, publicationState: PublicationState.LIVE },
  });

  const page = data.clubPages?.data[0];

  if (!page) {
    return {
      notFound: true,
      revalidate: 1,
    };
  }

  return {
    props: {
      page,
      preview: false,
    },
    revalidate: 30,
  };
};

export default ClubPage;
