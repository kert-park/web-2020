import { ClubPage, ClubPageProps } from '@kertPark/components/Club/ClubPage';
import { ClubPageDocument, ClubPageQuery, ClubPageQueryVariables, PublicationState } from '@kertPark/graphql/generated';
import { initializeApollo } from '@kertPark/lib/apollo/apolloClient';
import { GetServerSideProps } from 'next';

export const getServerSideProps: GetServerSideProps<ClubPageProps> = async ({ params }) => {
  const client = initializeApollo();
  const slug = params?.slug;

  if (!slug || typeof slug !== 'string') {
    return {
      notFound: true,
      revalidate: 1,
    };
  }

  const { data } = await client.query<ClubPageQuery, ClubPageQueryVariables>({
    query: ClubPageDocument,
    variables: { slug, publicationState: PublicationState.PREVIEW },
  });

  const page = data.clubPages?.data[0];

  if (!page) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      page,
      preview: true,
    },
  };
};

export default ClubPage;
