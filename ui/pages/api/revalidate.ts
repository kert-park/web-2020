import { activeTeams } from '@kertPark/config/teams';
import { NextApiHandler } from 'next';

const getTeamPathsToRevalidate = () => {
  return activeTeams.reduce<Array<string>>((paths, team) => {
    const newPaths: Array<string> = [
      `/tym/${team.id}`,
      `/tym/${team.id}/soupiska`,
      ...(team.hasGames ? [`/tym/${team.id}/statistiky`, `/tym/${team.id}/tabulka`, `/tym/${team.id}/zapasy`] : []),
    ];
    return [...paths, ...newPaths];
  }, []);
};

const handler: NextApiHandler = async (req, res) => {
  // Check for secret to confirm this is a valid request
  if (req.query.token !== process.env.REVALIDATE_TOKEN) {
    return res.status(401).json({ message: 'Invalid token' });
  }

  const pathsToRevalidate = ['/', '/klub/multirozpis', ...getTeamPathsToRevalidate()];
  const resultMessages: Array<string> = [];

  try {
    for (const path of pathsToRevalidate) {
      try {
        await res.revalidate(path);
        resultMessages.push(`🟢 Revalidated '${path}'`);
      } catch (e) {
        resultMessages.push(`🔴 Error revalidating '${path}': ${e.message}`);
      }
    }
    return res.json({ revalidated: true, resultMessages });
  } catch (err) {
    // If there was an error, Next.js will continue
    // to show the last successfully generated page
    return res.status(500).send('🔴 Error revalidating');
  }
};

export default handler;
