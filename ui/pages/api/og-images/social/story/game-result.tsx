import { GameResult as GameResultWhite } from '@kertPark/components/OgImage/WhiteTheme/Social/Story/GameResult';
import { fontsImageResponse } from '@kertPark/lib/og-images/config';
import { GameError } from '@kertPark/lib/og-images/error';
import { getGame } from '@kertPark/lib/og-images/game';
import { ImageResponse } from '@vercel/og';
import { NextRequest } from 'next/server';
import React from 'react';

export const config = {
  runtime: 'experimental-edge',
  unstable_allowDynamic: [
    '/node_modules/lodash/**', // use a glob to allow anything in the function-bind 3rd party module
  ],
};

export default async function handler(request: NextRequest) {
  try {
    const game = await getGame(request);
    const isATeam = game.team.systemName === 'muzia';
    const type = request.nextUrl.searchParams.get('type');

    return new ImageResponse(<GameResultWhite game={game} isATeam={isATeam} type={type || undefined} />, {
      width: 1080,
      height: 1920,
      fonts: await fontsImageResponse,
    });
  } catch (error: any) {
    console.log(`${error.message}`);
    if (error instanceof GameError) {
      return new Response(error.message, {
        status: error.status,
      });
    }
    return new Response('Failed to generate the image', {
      status: 500,
    });
  }
}
