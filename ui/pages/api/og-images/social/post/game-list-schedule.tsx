import { GameListSchedule } from '@kertPark/components/OgImage/WhiteTheme/Social/Post/GameListSchedule';
import { fontsImageResponse } from '@kertPark/lib/og-images/config';
import { GameError } from '@kertPark/lib/og-images/error';
import { getGames } from '@kertPark/lib/og-images/game';
import { getTournaments } from '@kertPark/lib/og-images/tournament';
import { ImageResponse } from '@vercel/og';
import { NextRequest } from 'next/server';
import React from 'react';

export const config = {
  runtime: 'experimental-edge',
  unstable_allowDynamic: [
    '/node_modules/lodash/**', // use a glob to allow anything in the function-bind 3rd party module
  ],
};

export default async function handler(request: NextRequest) {
  try {
    const games = await getGames(request);
    const tournaments = await getTournaments(request);

    if (games.length === 0 && tournaments.length === 0) {
      throw new GameError('Games or tournaments not found', 404);
    }

    const hasATeam = games.some((game) => game.team.systemName === 'muzia');

    return new ImageResponse(<GameListSchedule games={games} hasATeam={hasATeam} tournaments={tournaments} />, {
      width: 1080,
      height: 1080,
      fonts: await fontsImageResponse,
    });
  } catch (error: any) {
    console.log(`${error.message}`);
    if (error instanceof GameError) {
      return new Response(error.message, {
        status: error.status,
      });
    }
    return new Response('Failed to generate the image', {
      status: 500,
    });
  }
}
