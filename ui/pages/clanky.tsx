import { LinkButton } from '@kertPark/components/Atomic/Button';
import { Card } from '@kertPark/components/Atomic/Card';
import { H2 } from '@kertPark/components/Atomic/Typography';
import { IconInverseLetter } from '@kertPark/components/Icon/IconInversedLetter';
import { Layout } from '@kertPark/components/Layout/Layout';
import { ComplexPost, ComplexPostLoader } from '@kertPark/components/PostList/ComplexPost';
import { SidebarMenuLink } from '@kertPark/components/Sidebar/SidebarMenuLink';
import { getQueryString, getRouteObjectWithQuery, QueryParams, routes } from '@kertPark/config/routes';
import { teamsObject } from '@kertPark/config/teams';
import { spaces, theme } from '@kertPark/config/theme';
import {
  CategoriesDocument,
  CategoriesQuery,
  PostsCompositeDocument,
  PostsCompositeQuery,
  PostsCompositeQueryVariables,
  usePostsCompositeQuery,
} from '@kertPark/graphql/generated';
import {
  CategoriesQueryCategoriesType,
  PostsCompositeQueryMetaType,
  PostsCompositeQueryPostsType,
} from '@kertPark/graphql/generated/helpers';
import { getApolloStateProp, initializeApollo } from '@kertPark/lib/apollo/apolloClient';
import { Box, Flex } from '@rebass/grid';
import { GetStaticProps, NextPage } from 'next';
import { useRouter } from 'next/router';
import React, { useEffect, useMemo } from 'react';

const PAGE_SIZE = 10;

const getPage = (query?: string): number => {
  const page = parseInt(query || '1', 10);
  return page > 0 ? page : 1;
};

const getRouteWithQuery = ({ category, page }: { category?: string; page?: number }) => {
  const query = {
    [QueryParams.category]: category,
    [QueryParams.page]: page,
  };

  return getRouteObjectWithQuery({
    pathname: routes.posts,
    pathnameAs: routes.postsAs,
    query,
  });
};

type PrevNextButtonsProps = {
  categoryCode?: string;
  page: number;
  pageCount: number;
};

const PrevNextButtons = ({ categoryCode, page, pageCount }: PrevNextButtonsProps) => {
  const prev = getRouteWithQuery({ category: categoryCode, page: page - 1 });
  const next = getRouteWithQuery({ category: categoryCode, page: page + 1 });
  return (
    <>
      <LinkButton href={prev.link} hrefAs={prev.linkAs} disabled={page <= 1}>
        &larr; Novejší
      </LinkButton>
      <LinkButton href={next.link} hrefAs={next.linkAs} disabled={page >= pageCount}>
        Starší &rarr;
      </LinkButton>
    </>
  );
};

type Props = {
  categories: CategoriesQueryCategoriesType;
  posts: PostsCompositeQueryPostsType;
  postsMeta: PostsCompositeQueryMetaType;
};

const ArticlesPage: NextPage<Props> = ({ categories = [], posts = [], postsMeta }) => {
  const { query, replace } = useRouter();
  const categoryCode = getQueryString(query[QueryParams.category]);
  const page = getPage(getQueryString(query[QueryParams.page]));

  const { data: dataPosts, loading: loadingPosts } = usePostsCompositeQuery({
    variables: {
      ...(categoryCode ? { categoryCode: categoryCode } : {}),
      pagination: {
        page,
        pageSize: PAGE_SIZE,
      },
    },
  });

  const allPosts = dataPosts?.postsComposite?.data || posts;
  const pageCount = dataPosts?.postsComposite?.meta.pagination.pageCount ?? postsMeta?.pagination.pageCount ?? 0;

  useEffect(() => {
    if (pageCount < page) {
      const newPage = 1;
      const { link, linkAs } = getRouteWithQuery({ category: categoryCode, page: newPage });
      replace(link, linkAs);
    }
  }, [page, pageCount]);

  const loadingPostsPlaceholders = useMemo(() => {
    const placeholders: Array<React.ReactNode> = [];
    for (let i = 0; i < 3; i++) {
      placeholders.push(<ComplexPostLoader key={i} />);
    }
    return placeholders;
  }, []);

  const category = categories.find((cat) => cat.attributes?.code === categoryCode);

  return (
    <Layout title={['Články', ...(category?.attributes?.name ? [category.attributes.name] : [])]}>
      <Flex alignItems="flex-start" flexWrap="wrap" m={-2}>
        <Flex alignItems="flex-start" flexWrap="wrap" width={[1, 2 / 3]}>
          <H2
            margin="0"
            marginTop={`calc(${spaces[2]} + ${theme.spacing.card})`}
            marginLeft={`calc(${spaces[2]} + ${theme.spacing.card})`}
          >
            Archiv článků
          </H2>
          {loadingPosts
            ? loadingPostsPlaceholders.map((placeholder, index) => (
                <Box key={index} width={1} p={2}>
                  {placeholder}
                </Box>
              ))
            : allPosts.map((post) => (
                <Box key={post.id} width={1} p={2}>
                  <ComplexPost post={post} />
                </Box>
              ))}
          <Flex width={1} justifyContent="center">
            <PrevNextButtons categoryCode={categoryCode} page={page} pageCount={pageCount} />
          </Flex>
        </Flex>
        <Flex flexWrap="wrap" width={[1, 1 / 3]}>
          <H2
            margin="0"
            marginTop={`calc(${spaces[2]} + ${theme.spacing.card})`}
            marginLeft={`calc(${spaces[2]} + ${theme.spacing.card})`}
          >
            Kategorie
          </H2>
          <Box width={1} p={2}>
            <Card>
              <SidebarMenuLink
                key="vse"
                name="Vše"
                link={routes.posts}
                linkAs={routes.postsAs}
                icon={{
                  component: IconInverseLetter,
                  props: { letter: 'v' },
                }}
              />
              {categories.map((category) => {
                const categoryCode = category.attributes?.code;
                const { link, linkAs } = getRouteWithQuery({ category: categoryCode });
                return (
                  <SidebarMenuLink
                    key={category.attributes?.code}
                    name={category.attributes?.name || ''}
                    link={link}
                    linkAs={linkAs}
                    icon={{
                      component: IconInverseLetter,
                      props: {
                        // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
                        letter: teamsObject[categoryCode || '']?.initials || category.attributes?.name.slice(0, 1),
                        gradient: !!teamsObject[categoryCode || ''],
                      },
                    }}
                  />
                );
              })}
            </Card>
          </Box>
        </Flex>
      </Flex>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const client = initializeApollo();
  const { data: dataPosts } = await client.query<PostsCompositeQuery, PostsCompositeQueryVariables>({
    query: PostsCompositeDocument,
    variables: {
      pagination: {
        page: 1,
        pageSize: PAGE_SIZE,
      },
    },
  });
  const { data: dataCategories } = await client.query<CategoriesQuery>({ query: CategoriesDocument });

  return {
    props: {
      ...getApolloStateProp(client),
      categories: dataCategories.categories?.data || [],
      posts: dataPosts.postsComposite?.data || [],
      postsMeta: dataPosts.postsComposite?.meta || {},
    },
    revalidate: 30,
  };
};

export default ArticlesPage;
