import { ApolloProvider } from '@apollo/client';
import { CookieBar } from '@kertPark/components/Cookies/CookieBar';
import { CookiesContextProvider } from '@kertPark/components/Cookies/CookiesContext';
import { GlobalStyles } from '@kertPark/components/Layout/GlobalStyles';
import { Scripts } from '@kertPark/components/Scripts/Scripts';
import { theme } from '@kertPark/config/theme';
import { useApollo } from '@kertPark/lib/apollo/apolloClient';
import mediaQuery from 'css-mediaquery';
import { AppProps } from 'next/app';
import Head from 'next/head';
import React from 'react';
import { ThemeProvider } from 'styled-components';
import { Normalize } from 'styled-normalize';

type Props = {
  ssrMediaQuery: (query: string) => { matches: boolean };
};

const Web = ({ Component, pageProps }: AppProps & Props) => {
  const apolloClient = useApollo(pageProps);

  const ssrMediaQuery = (query: string): { matches: boolean } => {
    return {
      matches: mediaQuery.match(query, {
        width: '0px',
      }),
    };
  };

  return (
    <>
      <Head>
        <meta content="width=device-width, initial-scale=1, maximum-scale=5, shrink-to-fit=no" name="viewport" />
        <meta name="copyright" content="HC Kert Park Praha, Michal Pěch" />
      </Head>
      <ThemeProvider theme={{ ...theme, ssrMediaQuery }}>
        <Normalize />
        <GlobalStyles />
        <CookiesContextProvider>
          <CookieBar />
          <Scripts />
          <ApolloProvider client={apolloClient}>
            <Component {...pageProps} />
          </ApolloProvider>
        </CookiesContextProvider>
      </ThemeProvider>
    </>
  );
};

export default Web;
