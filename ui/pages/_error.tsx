import { ImageAspectRatio } from '@kertPark/components/Atomic/ImageAspectRatio';
import { LinkAnchor } from '@kertPark/components/Atomic/LinkAnchor';
import { H1 } from '@kertPark/components/Atomic/Typography';
import { Layout } from '@kertPark/components/Layout/Layout';
import { routes } from '@kertPark/config/routes';
import { spaces } from '@kertPark/config/theme';
import { Flex } from '@rebass/grid';
import { NextPage } from 'next';
import React from 'react';
import styled from 'styled-components';

const APP_PATH = process.env.APP_PATH;

const ImageWrapper = styled.div`
  margin-top: ${spaces[3]};
  width: 100%;
  max-width: 37.5em;
`;

type Props = {
  statusCode: number;
};

const ErrorPage: NextPage<Props> = ({ statusCode }) => {
  const title = `${statusCode} - ${statusCode === 404 ? 'Nenelezeno' : 'Něco se pokazilo'}`;
  return (
    <Layout noRobots>
      <Flex width={1} flexDirection="column" alignItems="center">
        <H1>{title}</H1>
        <LinkAnchor href={routes.index} hrefAs={routes.index} isText>
          Návrat na homepage
        </LinkAnchor>
        <ImageWrapper>
          <ImageAspectRatio src={`${APP_PATH}/not-found.jpg`} title={title} />
        </ImageWrapper>
      </Flex>
    </Layout>
  );
};

ErrorPage.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err?.statusCode || 404;
  return { statusCode };
};

export default ErrorPage;
