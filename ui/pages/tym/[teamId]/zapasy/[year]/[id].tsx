import { P, PSmall } from '@kertPark/components/Atomic/Typography';
import { GameWithDetail } from '@kertPark/components/Game/Detail/GameWithDetail';
import { Layout } from '@kertPark/components/Layout/Layout';
import { PostContentWrapper } from '@kertPark/components/Post/PostContentWrapper';
import { PostHeaderContent, PostHeaderLoader } from '@kertPark/components/Post/PostHeader';
import { PostSidebar } from '@kertPark/components/Sidebar/PostSidebar';
import { GameDocument, GameQuery, GameQueryVariables } from '@kertPark/graphql/generated';
import { GameWithCmshbGameDetailType } from '@kertPark/graphql/generated/helpers';
import { getApolloStateProp, initializeApollo } from '@kertPark/lib/apollo/apolloClient';
import { getGameImage } from '@kertPark/lib/game';
import { getGameTitle } from '@kertPark/lib/gameDetail';
import { getTeamById, TeamProps } from '@kertPark/lib/team';
import { Flex } from '@rebass/grid';
import { GetStaticPaths, GetStaticProps, NextPage } from 'next';
import React from 'react';

type Props = TeamProps & {
  game: GameWithCmshbGameDetailType | undefined;
  year: string;
};

const GameDetailPage: NextPage<Props> = ({ game, team, year }) => {
  if (!game || !team) {
    return (
      <Layout noRobots>
        <Flex alignItems="flex-start" flexWrap="wrap" m={-2}>
          <Flex alignItems="flex-start" flexWrap="wrap" width={[1, 2 / 3]}>
            <Flex width={1} p={2} alignItems="center" flexDirection="column">
              <PostHeaderLoader />
            </Flex>
          </Flex>
        </Flex>
      </Layout>
    );
  }

  const imageSrc = getGameImage({ gameId: game.id, teamId: team.id, year });

  return (
    <Layout metadata={[['og:image', imageSrc]]} title={['Zápas', team.name]} noRobots>
      <Flex alignItems="flex-start" flexWrap="wrap" m={-2}>
        <Flex alignItems="flex-start" flexWrap="wrap" width={[1, 2 / 3]}>
          <Flex width={1} p={2} alignItems="center" flexDirection="column">
            <PostHeaderContent
              categoriesString={team.name}
              imageSrc={imageSrc}
              publishedAt={game.date}
              title={game.post?.title || getGameTitle(game)}
            />
            <PostContentWrapper>
              {game.post?.perex && (
                <>
                  <PSmall textAlign="center">⚠️ Textový report vygeneroval jazykový model GPT-4 ⚠️</PSmall>
                  <P bold>{game.post.perex}</P>
                </>
              )}
              <GameWithDetail defaultOpen game={game} />
              {game.post?.content && <P>{game.post.content}</P>}
            </PostContentWrapper>
          </Flex>
        </Flex>
        <Flex flexWrap="wrap" width={[1, 1 / 3]}>
          <PostSidebar categoryCode={team.id} />
        </Flex>
      </Flex>
    </Layout>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  return { paths: [], fallback: true };
};

export const getStaticProps: GetStaticProps<Props> = async ({ params = {} }) => {
  const team = getTeamById({ hasGames: true, teamId: params.teamId });

  if (!team || !params.year || typeof params.year !== 'string' || !params.id || typeof params.id !== 'string') {
    return {
      notFound: true,
      revalidate: 1,
    };
  }

  const client = initializeApollo();

  const { data } = await client.query<GameQuery, GameQueryVariables>({
    query: GameDocument,
    variables: { id: params.id, year: params.year, teamId: team.id },
  });

  const game = data.game;

  if (!game) {
    return {
      notFound: true,
      revalidate: 1,
    };
  }

  return {
    props: {
      ...getApolloStateProp(client),
      game,
      team,
      year: params.year,
    },
    revalidate: 30,
  };
};

export default GameDetailPage;
