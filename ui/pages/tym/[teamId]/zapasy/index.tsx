import { Card } from '@kertPark/components/Atomic/Card';
import { RadioButtonList } from '@kertPark/components/Atomic/RadioButtonList';
import { H2 } from '@kertPark/components/Atomic/Typography';
import { AddToCalendar } from '@kertPark/components/Game/AddToCalendar';
import { GamesPageList } from '@kertPark/components/Game/GamesPageList';
import { Layout } from '@kertPark/components/Layout/Layout';
import { PlayoffsPageList } from '@kertPark/components/PlayOff/PlayoffsPageList';
import { TeamSidebar } from '@kertPark/components/Sidebar/TeamSidebar';
import { getQueryString, getRouteObjectWithQuery, QueryParams, routes } from '@kertPark/config/routes';
import {
  GameFilterPlaceEnum,
  GameFilterScheduleEnum,
  GameListDocument,
  GameListQuery,
  GameListQueryVariables,
  PlayOffListDocument,
  PlayOffListQuery,
  PlayOffListQueryVariables,
} from '@kertPark/graphql/generated';
import { getApolloStateProp, initializeApollo } from '@kertPark/lib/apollo/apolloClient';
import { isNearestGamePlayed } from '@kertPark/lib/game';
import {
  filterPlaceOptions,
  getGameFilterPlace,
  getTextFilterPlace,
  TextFilterSchedule,
  UiScheduleEnum,
} from '@kertPark/lib/gamePage';
import { getTeamById, getTeamStaticPaths, TeamProps } from '@kertPark/lib/team';
import { Box, Flex } from '@rebass/grid';
import { GetStaticPaths, GetStaticProps, NextPage } from 'next';
import { useRouter } from 'next/router';
import React from 'react';

const getGameFilterSchedule = (
  query: TextFilterSchedule | string | undefined,
  initialFilterSchedule: GameFilterScheduleEnum | UiScheduleEnum,
): GameFilterScheduleEnum | UiScheduleEnum => {
  switch (query) {
    case TextFilterSchedule.ALL:
      return GameFilterScheduleEnum.ALL;
    case TextFilterSchedule.RESULT:
      return GameFilterScheduleEnum.RESULT;
    case TextFilterSchedule.UPCOMING:
      return GameFilterScheduleEnum.UPCOMING;
    case TextFilterSchedule.PLAY_OFF:
      return UiScheduleEnum.PLAY_OFF;
    default:
      return initialFilterSchedule;
  }
};

const getTextFilterSchedule = (gameFilterScheduleEnum: GameFilterScheduleEnum | UiScheduleEnum): TextFilterSchedule => {
  switch (gameFilterScheduleEnum) {
    case GameFilterScheduleEnum.ALL:
      return TextFilterSchedule.ALL;
    case GameFilterScheduleEnum.RESULT:
      return TextFilterSchedule.RESULT;
    case GameFilterScheduleEnum.UPCOMING:
      return TextFilterSchedule.UPCOMING;
    case UiScheduleEnum.PLAY_OFF:
      return TextFilterSchedule.PLAY_OFF;
  }
};

type Props = TeamProps & {
  games: GameListQuery['gameList'];
  initialFilterSchedule?: GameFilterScheduleEnum | UiScheduleEnum;
  playOffs: PlayOffListQuery['playOffList'];
};

const GamesPage: NextPage<Props> = ({
  games = [],
  initialFilterSchedule = GameFilterScheduleEnum.ALL,
  playOffs = [],
  team,
}) => {
  const { query, replace } = useRouter();
  const filterPlace = getGameFilterPlace(getQueryString(query[QueryParams.place]));
  const filterSchedule = getGameFilterSchedule(getQueryString(query[QueryParams.schedule]), initialFilterSchedule);

  const handleChangeRoute = (place: GameFilterPlaceEnum, schedule: GameFilterScheduleEnum | UiScheduleEnum) => {
    const { link, linkAs } = getRouteObjectWithQuery({
      pathname: routes.team.games,
      pathnameAs: routes.team.gamesAs(team?.id || ''),
      query: {
        [QueryParams.schedule]: getTextFilterSchedule(schedule),
        [QueryParams.place]: getTextFilterPlace(place),
      },
    });
    replace(link, linkAs);
  };

  return (
    <Layout title={['Zápasy', ...(team ? [team.name] : [])]}>
      <Flex alignItems="flex-start" flexWrap="wrap" m={-2}>
        <Flex alignItems="flex-start" flexWrap="wrap" width={[1, 2 / 3]}>
          <Box width={1} px={2} pb={2}>
            <Card noBackground>
              <Flex flex={1} flexDirection="row" alignItems="center" justifyContent="space-between">
                <H2 margin="0">Zápasy</H2>
                <AddToCalendar team={team} />
              </Flex>
              <Flex flex={1} flexDirection="row" flexWrap="wrap" alignItems="flex-start" m={-1} pt={2}>
                <Box width={[1, 1 / 2]} p={1}>
                  <RadioButtonList<GameFilterScheduleEnum | UiScheduleEnum>
                    header="Nadcházející/Odehrané"
                    options={[
                      { text: 'vše', value: GameFilterScheduleEnum.ALL },
                      ...(playOffs.length > 0 ? [{ text: 'play-off', value: UiScheduleEnum.PLAY_OFF }] : []),
                      { text: 'nadcházející', value: GameFilterScheduleEnum.UPCOMING },
                      { text: 'odehrané', value: GameFilterScheduleEnum.RESULT },
                    ]}
                    onChange={(selected) => handleChangeRoute(filterPlace, selected)}
                    selected={filterSchedule}
                  />
                </Box>
                <Box width={[1, 1 / 2]} p={1}>
                  <RadioButtonList<GameFilterPlaceEnum>
                    header="Domací/Hosté"
                    isDisabled={filterSchedule === UiScheduleEnum.PLAY_OFF}
                    options={filterPlaceOptions}
                    onChange={(selected) => handleChangeRoute(selected, filterSchedule)}
                    selected={filterPlace}
                  />
                </Box>
              </Flex>
            </Card>
          </Box>
          {filterSchedule === UiScheduleEnum.PLAY_OFF ? (
            <PlayoffsPageList playOffs={playOffs} team={team} />
          ) : (
            <GamesPageList filterPlace={filterPlace} filterSchedule={filterSchedule} games={games} team={team} />
          )}
        </Flex>
        <Flex flexWrap="wrap" width={[1, 1 / 3]}>
          <TeamSidebar team={team} />
        </Flex>
      </Flex>
    </Layout>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  return getTeamStaticPaths({ hasGames: true });
};

export const getStaticProps: GetStaticProps<Props> = async ({ params }) => {
  const team = getTeamById({ hasGames: true, teamId: params?.teamId });

  if (!team) {
    return {
      notFound: true,
      revalidate: 1,
    };
  }

  const client = initializeApollo();

  const { data: dataGames, error: errorGames } = await client.query<GameListQuery, GameListQueryVariables>({
    query: GameListDocument,
    variables: {
      teamId: team.id,
    },
  });

  const { data: dataPlayOffs, error: errorPlayOffs } = await client.query<PlayOffListQuery, PlayOffListQueryVariables>({
    query: PlayOffListDocument,
    variables: {
      teamId: team.id,
    },
  });

  const games = errorGames ? [] : dataGames.gameList;
  const playOffs = errorPlayOffs ? [] : dataPlayOffs.playOffList;
  const initialFilterSchedule =
    playOffs.length > 0
      ? UiScheduleEnum.PLAY_OFF
      : games.length > 0
      ? isNearestGamePlayed(games)
        ? GameFilterScheduleEnum.RESULT
        : GameFilterScheduleEnum.UPCOMING
      : GameFilterScheduleEnum.ALL;

  const playOffsWithReverseRounds = playOffs.map((playOff) => ({
    ...playOff,
    rounds: playOff.rounds.length > 1 ? [...playOff.rounds].reverse() : playOff.rounds,
  }));

  return {
    props: {
      ...getApolloStateProp(client),
      games,
      initialFilterSchedule,
      playOffs: playOffsWithReverseRounds,
      team,
    },
    revalidate: 30,
  };
};

export default GamesPage;
