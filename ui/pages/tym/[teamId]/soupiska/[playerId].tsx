import { Layout } from '@kertPark/components/Layout/Layout';
import { PlayerHeader, PlayerHeaderLoader } from '@kertPark/components/Roster/PlayerHeader';
import { PlayerStatistics } from '@kertPark/components/Roster/PlayerStatistics';
import { TeamSidebar } from '@kertPark/components/Sidebar/TeamSidebar';
import { teamsObject } from '@kertPark/config/teams';
import {
  Player,
  PlayerDocument,
  PlayerQuery,
  PlayerQueryVariables,
  PlayersDocument,
  PlayersQuery,
  PlayersQueryVariables,
} from '@kertPark/graphql/generated';
import { getApolloStateProp, initializeApollo } from '@kertPark/lib/apollo/apolloClient';
import { getTeamById, TeamProps } from '@kertPark/lib/team';
import { Box, Flex } from '@rebass/grid';
import { GetStaticPaths, GetStaticProps, NextPage } from 'next';
import React from 'react';

type Props = TeamProps & {
  player?: Player;
};

const PlayerPage: NextPage<Props> = ({ player, team }) => {
  return (
    <Layout title={[...(player ? [player.name] : []), 'Soupiska', ...(team ? [team.name] : [])]}>
      <Flex alignItems="flex-start" flexWrap="wrap" m={-2}>
        <Flex alignItems="flex-start" flexWrap="wrap" width={[1, 2 / 3]}>
          {player ? (
            <>
              <PlayerHeader player={player} />
              <PlayerStatistics player={player} team={team} />
            </>
          ) : (
            <Box width={1} p={2}>
              <PlayerHeaderLoader />
            </Box>
          )}
        </Flex>
        <Flex flexWrap="wrap" width={[1, 1 / 3]}>
          <TeamSidebar team={team} />
        </Flex>
      </Flex>
    </Layout>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  const teamId = teamsObject.muzia.id;
  const client = initializeApollo();
  const { data, error } = await client.query<PlayersQuery, PlayersQueryVariables>({
    query: PlayersDocument,
    variables: {
      teamId,
    },
  });

  if (error) {
    return { paths: [], fallback: true };
  }

  const paths = data.players.map((player) => ({
    params: { playerId: player.id, teamId },
  }));
  return { paths, fallback: true };
};

export const getStaticProps: GetStaticProps<Props> = async ({ params = {} }) => {
  const team = getTeamById(params);

  if (!team || !params.playerId || typeof params.playerId !== 'string') {
    return {
      notFound: true,
      revalidate: 1,
    };
  }

  const client = initializeApollo();

  const { data } = await client.query<PlayerQuery, PlayerQueryVariables>({
    query: PlayerDocument,
    variables: { id: params.playerId, teamId: team.id },
  });

  const player = data.player;

  if (!player) {
    return {
      notFound: true,
      revalidate: 1,
    };
  }

  return {
    props: {
      ...getApolloStateProp(client),
      player,
      team,
    },
    revalidate: 30,
  };
};

export default PlayerPage;
