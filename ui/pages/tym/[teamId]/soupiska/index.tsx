import { Card } from '@kertPark/components/Atomic/Card';
import { RadioButtonList } from '@kertPark/components/Atomic/RadioButtonList';
import { H2, H3, P } from '@kertPark/components/Atomic/Typography';
import { Layout } from '@kertPark/components/Layout/Layout';
import { Official } from '@kertPark/components/OfficialList/Official';
import { OfficialsContainer } from '@kertPark/components/OfficialList/Styles';
import { RosterCardsLayout } from '@kertPark/components/Roster/RosterCardsLayout';
import { RosterTableLayout } from '@kertPark/components/Roster/RosterTableLayout';
import { TeamSidebar } from '@kertPark/components/Sidebar/TeamSidebar';
import { getQueryString, getRouteObjectWithQuery, QueryParams, routes } from '@kertPark/config/routes';
import { Team } from '@kertPark/config/teams';
import {
  CategoriesDocument,
  CategoriesQuery,
  CategoriesQueryVariables,
  CommonOfficialFragment,
  PlayersDocument,
  PlayersQuery,
  PlayersQueryVariables,
} from '@kertPark/graphql/generated';
import { getApolloStateProp, initializeApollo } from '@kertPark/lib/apollo/apolloClient';
import { groupPlayersByPost, PlayerPostLabels, sortPlayersByName } from '@kertPark/lib/player';
import { getTeamById, getTeamStaticPaths, TeamProps } from '@kertPark/lib/team';
import { Box, Flex } from '@rebass/grid';
import { GetStaticPaths, GetStaticProps, NextPage } from 'next';
import { useRouter } from 'next/router';
import React from 'react';

enum RosterLayout {
  cards = 'karty',
  table = 'tabulka',
}

const getRosterLayout = (query: string | undefined, team: Team | undefined): RosterLayout => {
  if (query) {
    return query === RosterLayout.table ? RosterLayout.table : RosterLayout.cards;
  }
  return team?.hasGames ? RosterLayout.cards : RosterLayout.table;
};

type Props = TeamProps & {
  officials?: Array<CommonOfficialFragment>;
  players?: PlayersQuery['players'];
};

const RosterPage: NextPage<Props> = ({ officials = [], players: allPlayers = [], team }) => {
  const { query, replace } = useRouter();
  const rosterLayout = getRosterLayout(getQueryString(query[QueryParams.view]), team);
  const groupedPlayers = groupPlayersByPost(sortPlayersByName(allPlayers), !team?.hasGames);

  const handleChangeRosterLayout = (layout: RosterLayout) => {
    const { link, linkAs } = getRouteObjectWithQuery({
      pathname: routes.team.roster,
      pathnameAs: routes.team.rosterAs(team?.id || ''),
      query: { [QueryParams.view]: layout },
    });
    replace(link, linkAs);
  };

  return (
    <Layout title={['Soupiska', ...(team ? [team.name] : [])]}>
      <Flex alignItems="flex-start" flexWrap="wrap" m={-2}>
        <Flex alignItems="flex-start" flexWrap="wrap" width={[1, 2 / 3]}>
          <Box width={1} px={2} pb={2}>
            <Card noBackground>
              <H2 margin="0">Soupiska</H2>
              <Box width={1} pt={2} pb={2}>
                <RadioButtonList<RosterLayout>
                  header="Zobrazení"
                  options={[
                    { text: 'karty', value: RosterLayout.cards },
                    { text: 'tabulka', value: RosterLayout.table },
                  ]}
                  onChange={handleChangeRosterLayout}
                  selected={rosterLayout}
                />
              </Box>
              {allPlayers.length > 0 ? (
                Object.keys(groupedPlayers).map((groupKey) => {
                  const players = groupedPlayers[groupKey];
                  return (
                    <React.Fragment key={groupKey}>
                      <H3>{PlayerPostLabels[groupKey]}</H3>
                      <Flex alignItems="flex-start" flexWrap="wrap" m={-2}>
                        {rosterLayout === RosterLayout.cards ? (
                          <RosterCardsLayout players={players} team={team} />
                        ) : (
                          <Card>
                            <RosterTableLayout players={players} team={team} />
                          </Card>
                        )}
                      </Flex>
                    </React.Fragment>
                  );
                })
              ) : (
                <P>Soupiska není k dispozici</P>
              )}
              {officials.length > 0 && (
                <>
                  <H3>Realizační tým</H3>
                  <OfficialsContainer>
                    {officials.filter(Boolean).map((official, itemIndex) => {
                      return (
                        <Official key={official.id} itemIndex={itemIndex} official={official} showContactInformation />
                      );
                    })}
                  </OfficialsContainer>
                </>
              )}
            </Card>
          </Box>
        </Flex>
        <Flex flexWrap="wrap" width={[1, 1 / 3]}>
          <TeamSidebar team={team} />
        </Flex>
      </Flex>
    </Layout>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  return getTeamStaticPaths({});
};

export const getStaticProps: GetStaticProps<Props> = async ({ params = {} }) => {
  const team = getTeamById(params);

  if (!team) {
    return {
      notFound: true,
      revalidate: 1,
    };
  }

  const client = initializeApollo();

  const { data: dataPlayers, error: errorPlayers } = await client.query<PlayersQuery, PlayersQueryVariables>({
    query: PlayersDocument,
    variables: { teamId: team.id },
  });

  const { data: dataCategories, error: errorCategories } = await client.query<
    CategoriesQuery,
    CategoriesQueryVariables
  >({
    query: CategoriesDocument,
  });

  const officials = errorCategories
    ? []
    : ((dataCategories.categories?.data
        .find((category) => category.attributes?.code === team.id)
        ?.attributes?.officials?.filter(Boolean) || []) as Array<CommonOfficialFragment>);

  return {
    props: {
      ...getApolloStateProp(client),
      officials,
      players: errorPlayers ? [] : dataPlayers.players,
      team,
    },
    revalidate: 30,
  };
};

export default RosterPage;
