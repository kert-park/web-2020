import { Card } from '@kertPark/components/Atomic/Card';
import { RadioButtonList } from '@kertPark/components/Atomic/RadioButtonList';
import { SelectBox } from '@kertPark/components/Atomic/SelectBox';
import { H2, P } from '@kertPark/components/Atomic/Typography';
import { Layout } from '@kertPark/components/Layout/Layout';
import { TeamSidebar } from '@kertPark/components/Sidebar/TeamSidebar';
import { StatisticsLeadersLayout } from '@kertPark/components/Statistics/StatisticsLeadersLayout';
import { StatisticsTableLayout } from '@kertPark/components/Statistics/StatisticsTableLayout';
import { getQueryString, getRouteObjectWithQuery, QueryParams, routes } from '@kertPark/config/routes';
import {
  CmshbPlayerStatsList,
  CmshbPlayerStatsListDocument,
  CmshbPlayerStatsListQuery,
  CmshbPlayerStatsListQueryVariables,
} from '@kertPark/graphql/generated';
import { getApolloStateProp, initializeApollo } from '@kertPark/lib/apollo/apolloClient';
import { getTeamById, getTeamStaticPaths, TeamProps } from '@kertPark/lib/team';
import { Box, Flex } from '@rebass/grid';
import { GetStaticPaths, GetStaticProps, NextPage } from 'next';
import { useRouter } from 'next/router';
import React from 'react';

enum StatisticsLayout {
  leaders = 'lídri',
  table = 'tabulka',
}

const getStatisticsLayout = (query: string | undefined): StatisticsLayout => {
  return query === StatisticsLayout.table ? StatisticsLayout.table : StatisticsLayout.leaders;
};

const getLeagueGroup = (query: string | undefined, leagueGroupStatistics: Array<CmshbPlayerStatsList>): string => {
  if (query) {
    if (leagueGroupStatistics.some((item) => item.leagueGroup.id === query)) {
      return query;
    }
  }
  return leagueGroupStatistics[0]?.leagueGroup?.id || '';
};

type Props = TeamProps & {
  leagueGroupStatistics?: Array<CmshbPlayerStatsList>;
};

const StatisticsPage: NextPage<Props> = ({ leagueGroupStatistics = [], team }) => {
  const { query, replace } = useRouter();
  const statisticsLayout = getStatisticsLayout(getQueryString(query[QueryParams.view]));
  const leagueGroup = getLeagueGroup(getQueryString(query[QueryParams.group]), leagueGroupStatistics);

  const handleChangeRoute = (layout: StatisticsLayout, group: string) => {
    const { link, linkAs } = getRouteObjectWithQuery({
      pathname: routes.team.statistics,
      pathnameAs: routes.team.statisticsAs(team?.id || ''),
      query: {
        [QueryParams.view]: layout,
        [QueryParams.group]: group,
      },
    });
    replace(link, linkAs);
  };

  const leagueGroupStatistic = leagueGroupStatistics.find((item) => item.leagueGroup.id === leagueGroup) || undefined;

  return (
    <Layout title={['Statistiky', ...(team ? [team.name] : [])]}>
      <Flex alignItems="flex-start" flexWrap="wrap" m={-2}>
        <Flex alignItems="flex-start" flexWrap="wrap" width={[1, 2 / 3]}>
          <Box width={1} px={2} pb={2}>
            <Card noBackground>
              <H2 margin="0">Statistiky</H2>
              <Flex flex={1} flexDirection="row" flexWrap="wrap" alignItems="flex-start" m={-1} pt={2}>
                <Box width={[1, 1 / 2]} p={1}>
                  <RadioButtonList<StatisticsLayout>
                    header="Zobrazení"
                    options={[
                      { text: 'lídři', value: StatisticsLayout.leaders },
                      { text: 'tabulka', value: StatisticsLayout.table },
                    ]}
                    onChange={(selected) => handleChangeRoute(selected, leagueGroup)}
                    selected={statisticsLayout}
                  />
                </Box>
                <Box width={[1, 1 / 2]} p={1}>
                  <SelectBox
                    header="Fáze soutěže"
                    options={leagueGroupStatistics.map((item) => ({
                      text: item.leagueGroup.name,
                      value: item.leagueGroup.id,
                    }))}
                    onChange={(value) => {
                      handleChangeRoute(statisticsLayout, value);
                    }}
                    value={leagueGroup}
                  />
                </Box>
              </Flex>
            </Card>
          </Box>
          {leagueGroupStatistics.length > 0 && leagueGroupStatistic ? (
            statisticsLayout === StatisticsLayout.leaders ? (
              <StatisticsLeadersLayout leagueGroupStatistic={leagueGroupStatistic} teamId={team?.id || ''} />
            ) : (
              <StatisticsTableLayout leagueGroupStatistic={leagueGroupStatistic} teamId={team?.id || ''} />
            )
          ) : (
            <P>Statistiky nejsou k dispozici</P>
          )}
        </Flex>
        <Flex flexWrap="wrap" width={[1, 1 / 3]}>
          <TeamSidebar team={team} />
        </Flex>
      </Flex>
    </Layout>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  return getTeamStaticPaths({ hasGames: true });
};

export const getStaticProps: GetStaticProps<Props> = async ({ params }) => {
  const team = getTeamById({ hasGames: true, teamId: params?.teamId });

  if (!team) {
    return {
      notFound: true,
      revalidate: 1,
    };
  }

  const client = initializeApollo();

  const { data, error } = await client.query<CmshbPlayerStatsListQuery, CmshbPlayerStatsListQueryVariables>({
    query: CmshbPlayerStatsListDocument,
    variables: { teamId: team.id },
  });

  return {
    props: {
      ...getApolloStateProp(client),
      leagueGroupStatistics: error ? [] : data.cmshbPlayerStatsList,
      team,
    },
    revalidate: 30,
  };
};

export default StatisticsPage;
