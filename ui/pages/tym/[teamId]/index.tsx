import { Achievement } from '@kertPark/components/Achievement/Achievement';
import { HiddenDesktopBox, HiddenMobileBox } from '@kertPark/components/Atomic/Box';
import { Card } from '@kertPark/components/Atomic/Card';
import { ContentLoader, Rect } from '@kertPark/components/Atomic/ContentLoader';
import { AspectRatio, ImageAspectRatio } from '@kertPark/components/Atomic/ImageAspectRatio';
import { H2 } from '@kertPark/components/Atomic/Typography';
import { NearestGames } from '@kertPark/components/Game/NearestGames';
import { Layout } from '@kertPark/components/Layout/Layout';
import { LastPosts } from '@kertPark/components/Sidebar/LastPosts';
import { TeamMenuWithHeader } from '@kertPark/components/Sidebar/TeamMenu';
import { SmallLeagueGroupTable } from '@kertPark/components/Table/SmallLeagueGroupTable';
import { TournamentList } from '@kertPark/components/Tournament/TournamentList';
import { getThumbImageUrl, ImageSize } from '@kertPark/config/images';
import { mediaBreakpoints, spaces, theme } from '@kertPark/config/theme';
import {
  CategoriesDocument,
  CategoriesQuery,
  CmshbTeamTable,
  CmshbTeamTableDocument,
  CmshbTeamTableQuery,
  CmshbTeamTableQueryVariables,
  CommonAchievementFragment,
  GameFilterPlaceEnum,
  GameFilterScheduleEnum,
  GameListDocument,
  GameListQuery,
  GameListQueryVariables,
  TournamentListDocument,
  TournamentListQuery,
  TournamentListQueryVariables,
} from '@kertPark/graphql/generated';
import { GameType, ImageType } from '@kertPark/graphql/generated/helpers';
import { useMediaQuery } from '@kertPark/hooks/useMediaQuery';
import { getApolloStateProp, initializeApollo } from '@kertPark/lib/apollo/apolloClient';
import { getTeamById, getTeamStaticPaths, TeamProps } from '@kertPark/lib/team';
import { Box, Flex } from '@rebass/grid';
import { GetStaticPaths, GetStaticProps, NextPage } from 'next';
import React from 'react';

const getImageSrc = ({ isXxlDesktop, image }: { isXxlDesktop: boolean; image?: ImageType }): string => {
  if (image?.data?.attributes) {
    const attributes = image.data.attributes;
    const formats = attributes.formats;

    if (isXxlDesktop && (formats?.large?.url || attributes.url)) {
      return formats?.large?.url || attributes.url;
    }

    return formats?.medium?.url || attributes.url || formats?.small?.url;
  }

  return getThumbImageUrl({ size: isXxlDesktop ? ImageSize.large : ImageSize.medium });
};

type Props = TeamProps & {
  achievements?: Array<CommonAchievementFragment>;
  games?: Array<GameType>;
  tables?: Array<CmshbTeamTable>;
  teamImage?: ImageType;
  tournaments?: TournamentListQuery['tournamentList'];
};

const TeamPage: NextPage<Props> = ({
  achievements = [],
  games = [],
  tables = [],
  team,
  teamImage,
  tournaments = [],
}) => {
  const isXxlDesktop = useMediaQuery(mediaBreakpoints.xxl);
  const imageSrc = getImageSrc({ isXxlDesktop, image: teamImage });

  if (!team) {
    return (
      <Layout>
        <Flex width={[1, 2 / 3]}>
          <Card noPadding>
            <ContentLoader viewBox="0 0 2 1" style={{ borderRadius: theme.radius.card }}>
              <Rect rx={0} ry={0} x="0" y="0" width="100%" height="100%" />
            </ContentLoader>
          </Card>
        </Flex>
      </Layout>
    );
  }

  return (
    <Layout title={[team.name]}>
      <Flex alignItems="flex-start" flexWrap="wrap" m={-2}>
        <Flex alignItems="flex-start" flexWrap="wrap" width={[1, 2 / 3]}>
          <HiddenDesktopBox width={1}>
            <TeamMenuWithHeader team={team} />
          </HiddenDesktopBox>
          <Box width={1} p={2}>
            <ImageAspectRatio ratio={AspectRatio.twoToOne} src={imageSrc} />
          </Box>
          {team.hasGames ? (
            <Flex width={1} flexDirection={['column', 'column', 'row']}>
              <Box width={[1, 1, 1 / 2]}>
                <H2 marginBottom="0" marginLeft={`calc(${spaces[2]} + ${theme.spacing.card})`}>
                  Zápasy
                </H2>
                <NearestGames games={games} teamId={team.id} />
              </Box>
              <Box width={[1, 1, 1 / 2]}>
                <H2 marginBottom="0" marginLeft={`calc(${spaces[2]} + ${theme.spacing.card})`}>
                  Tabulka
                </H2>
                <SmallLeagueGroupTable maxLength={6} team={team} tables={tables} />
              </Box>
            </Flex>
          ) : (
            <Box width={1} p={2}>
              <H2 marginBottom="0" marginLeft={`calc(${spaces[2]} + ${theme.spacing.card})`}>
                Turnaje
              </H2>
              <Card>
                <TournamentList tournaments={tournaments} />
              </Card>
            </Box>
          )}
          {achievements.length > 0 && (
            <Box width={1} p={2}>
              <H2 marginBottom="0" marginLeft={`calc(${spaces[2]} + ${theme.spacing.card})`}>
                Úspěchy
              </H2>
              <Flex flexWrap="wrap" width={1}>
                {achievements.map((achievement, index) => {
                  return (
                    <Box key={index} width={[1 / 2, 1 / 2, 1 / 4]} p={2}>
                      <Card>
                        <Achievement achievement={achievement} />
                      </Card>
                    </Box>
                  );
                })}
              </Flex>
            </Box>
          )}
        </Flex>
        <Flex flexWrap="wrap" width={[1, 1 / 3]}>
          <HiddenMobileBox width={1}>
            <TeamMenuWithHeader team={team} />
          </HiddenMobileBox>
          <LastPosts team={team} />
        </Flex>
      </Flex>
    </Layout>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  return getTeamStaticPaths({});
};

export const getStaticProps: GetStaticProps<Props> = async ({ params }) => {
  const team = getTeamById({ teamId: params?.teamId });

  if (!team) {
    return {
      notFound: true,
      revalidate: 1,
    };
  }

  const client = initializeApollo();

  let returnProps = {};

  if (team.hasGames) {
    const { data: dataGames, error: errorGames } = await client.query<GameListQuery, GameListQueryVariables>({
      query: GameListDocument,
      variables: {
        teamId: team.id,
        filter: {
          place: GameFilterPlaceEnum.ALL,
          schedule: GameFilterScheduleEnum.ALL,
          nearest: true,
          limit: 2,
        },
      },
    });

    const { data: dataTables, error: errorTables } = await client.query<
      CmshbTeamTableQuery,
      CmshbTeamTableQueryVariables
    >({
      query: CmshbTeamTableDocument,
      variables: {
        teamId: team.id,
      },
    });

    returnProps = {
      games: errorGames ? [] : dataGames.gameList,
      tables: errorTables ? [] : dataTables.cmshbTeamTable,
    };
  } else {
    const { data: dataTournaments, error: errorTournaments } = await client.query<
      TournamentListQuery,
      TournamentListQueryVariables
    >({
      query: TournamentListDocument,
      variables: {
        teamId: team.id,
      },
    });

    returnProps = {
      tournaments: errorTournaments ? [] : dataTournaments.tournamentList,
    };
  }

  const { data: dataCategories } = await client.query<CategoriesQuery>({
    query: CategoriesDocument,
  });

  const category = dataCategories.categories?.data.find((category) => category.attributes?.code === team.id);

  const achievements = (category?.attributes?.achievements || []) as Array<CommonAchievementFragment>;
  const teamImage = category?.attributes?.image || undefined;

  return {
    props: {
      ...getApolloStateProp(client),
      ...returnProps,
      achievements,
      team,
      ...(teamImage ? { teamImage } : {}),
    },
    revalidate: 30,
  };
};

export default TeamPage;
