import { Card } from '@kertPark/components/Atomic/Card';
import { H2, H3, P } from '@kertPark/components/Atomic/Typography';
import { Layout } from '@kertPark/components/Layout/Layout';
import { TeamSidebar } from '@kertPark/components/Sidebar/TeamSidebar';
import { LeagueGroupTable } from '@kertPark/components/Table/LeagueGroupTable';
import { spaces, theme } from '@kertPark/config/theme';
import { CmshbTeamTableDocument, CmshbTeamTableQuery, CmshbTeamTableQueryVariables } from '@kertPark/graphql/generated';
import { getApolloStateProp, initializeApollo } from '@kertPark/lib/apollo/apolloClient';
import { getTeamById, getTeamStaticPaths, TeamProps } from '@kertPark/lib/team';
import { Box, Flex } from '@rebass/grid';
import { GetStaticPaths, GetStaticProps, NextPage } from 'next';
import React from 'react';

type Props = TeamProps & {
  leagueGroupTables?: CmshbTeamTableQuery['cmshbTeamTable'];
};

const TablePage: NextPage<Props> = ({ leagueGroupTables = [], team }) => {
  const withLeagueGroupLabel = leagueGroupTables.length > 1;

  return (
    <Layout title={['Tabulka', ...(team ? [team.name] : [])]}>
      <Flex alignItems="flex-start" flexWrap="wrap" m={-2}>
        <Flex alignItems="flex-start" flexWrap="wrap" width={[1, 2 / 3]}>
          <Box width={1} px={2}>
            <Card noBackground>
              <H2 margin="0">Tabulka</H2>
            </Card>
          </Box>
          {leagueGroupTables.length > 0 ? (
            leagueGroupTables.map((leagueGroupTable) => (
              <React.Fragment key={leagueGroupTable.leagueGroup.id}>
                {withLeagueGroupLabel && (
                  <H3
                    margin="0"
                    marginTop={`calc(${spaces[2]} + ${theme.spacing.card})`}
                    marginLeft={`calc(${spaces[2]} + ${theme.spacing.card})`}
                    marginBottom={spaces[1]}
                  >
                    {leagueGroupTable.leagueGroup.name}
                  </H3>
                )}
                <Box width={1} px={2} pb={2}>
                  <Card>
                    <LeagueGroupTable sortable stats={leagueGroupTable.stats} />
                  </Card>
                </Box>
              </React.Fragment>
            ))
          ) : (
            <P>Tabulka není k dispozici</P>
          )}
        </Flex>
        <Flex flexWrap="wrap" width={[1, 1 / 3]}>
          <TeamSidebar team={team} />
        </Flex>
      </Flex>
    </Layout>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  return getTeamStaticPaths({ hasGames: true });
};

export const getStaticProps: GetStaticProps<Props> = async ({ params }) => {
  const team = getTeamById({ hasGames: true, teamId: params?.teamId });

  if (!team) {
    return {
      notFound: true,
      revalidate: 1,
    };
  }

  const client = initializeApollo();

  const { data, error } = await client.query<CmshbTeamTableQuery, CmshbTeamTableQueryVariables>({
    query: CmshbTeamTableDocument,
    variables: {
      teamId: team.id,
    },
  });

  return {
    props: {
      ...getApolloStateProp(client),
      leagueGroupTables: error ? [] : data.cmshbTeamTable,
      team,
    },
    revalidate: 30,
  };
};

export default TablePage;
