import { LinkAnchor } from '@kertPark/components/Atomic/LinkAnchor';
import { PlayerImage } from '@kertPark/components/Atomic/PlayerImage';
import { Table, TBody, Td, Tr } from '@kertPark/components/Atomic/Table';
import { H4, PSmall } from '@kertPark/components/Atomic/Typography';
import { routes } from '@kertPark/config/routes';
import { mediaBreakpoints, theme, typography } from '@kertPark/config/theme';
import { CmshbPlayerStatsPlayerUnionType } from '@kertPark/graphql/generated/helpers';
import { isPlayer } from '@kertPark/lib/player';
import { Flex } from '@rebass/grid';
import Router from 'next/router';
import React from 'react';
import styled from 'styled-components';

export type StatisticLeaderData = {
  player: CmshbPlayerStatsPlayerUnionType;
  value: number;
};

export type StatisticLeaderProps = {
  title: string;
  measurement: (value: number) => string;
  data: Array<StatisticLeaderData>;
  teamId: string;
};

const StyledLinkAnchor = styled(LinkAnchor)``;

const TopPlayerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const PersonName = styled.div`
  font-family: ${theme.fonts.header};
  font-size: ${typography.h5.fontSize.xs};
  color: ${({ theme }) => theme.colors.textPrimary};
  margin-top: 1rem;

  @media (${mediaBreakpoints.lg}) {
    font-size: ${typography.h5.fontSize.lg};
  }

  ${StyledLinkAnchor}:hover & {
    text-decoration: underline;
  }
`;

const StatValue = styled.div`
  color: ${({ theme }) => theme.colors.textPrimary};
  font-family: ${theme.fonts.header};
  font-size: ${typography.h1.fontSize.xs};
  font-weight: bold;

  @media (${mediaBreakpoints.lg}) {
    font-size: ${typography.h1.fontSize.lg};
  }
`;

const StatMeasure = styled.div`
  color: ${({ theme }) => theme.colors.textPrimary};
  font-family: ${theme.fonts.header};
  font-size: ${typography.h5.fontSize.xs};
  font-weight: bold;
  margin-left: 0.25rem;

  @media (${mediaBreakpoints.lg}) {
    font-size: ${typography.h5.fontSize.lg};
    margin-left: 0.5rem;
  }
`;

const TopPlayer: React.FC<StatisticLeaderData & { measurement: (value: number) => string; teamId: string }> = ({
  measurement,
  player,
  player: { id, name },
  value,
  teamId,
}) => {
  const isPlayerType = isPlayer(player);
  const component = (
    <TopPlayerWrapper>
      <PlayerImage
        number={isPlayerType ? player.number : undefined}
        smallImage={isPlayerType ? player.smallImage : undefined}
        wrapperComponent={TopPlayerWrapper}
      />
      <PersonName>{name}</PersonName>
      <Flex flexDirection="row" alignItems="baseline" marginTop={1}>
        <StatValue>{value}</StatValue>
        <StatMeasure>{measurement(value)}</StatMeasure>
      </Flex>
    </TopPlayerWrapper>
  );

  if (isPlayerType && id) {
    return (
      <StyledLinkAnchor href={routes.team.player} hrefAs={routes.team.playerAs(teamId, id)} passHref>
        {component}
      </StyledLinkAnchor>
    );
  }

  return component;
};

export const StatisticLeader: React.FC<StatisticLeaderProps> = ({ title, measurement, data, teamId }) => {
  const nonNullData = data.filter(({ value }) => value > 0);
  const hasData = nonNullData.length > 0;
  const [first, ...rest] = nonNullData;
  const other = rest.slice(0, 2);
  return (
    <Flex flexDirection="column" justifyContent="center" alignItems="center">
      <H4 marginTop="0">{title}</H4>
      {hasData ? (
        <TopPlayer {...first} measurement={measurement} teamId={teamId} />
      ) : (
        <PSmall>Statistika není dostupná</PSmall>
      )}
      {other.length > 0 && (
        <Table>
          <TBody>
            {other.map(({ player: { cmshbId, id, name }, value }) => {
              const onClick = () => {
                if (id) {
                  Router.push({ pathname: routes.team.player, query: { teamId, playerId: id } });
                }
              };
              return (
                <Tr key={cmshbId} onClick={onClick} withLink={!!id}>
                  <Td align="left" hoverUnderline={!!id}>
                    {name}
                  </Td>
                  <Td align="right">{value}</Td>
                </Tr>
              );
            })}
          </TBody>
        </Table>
      )}
    </Flex>
  );
};
