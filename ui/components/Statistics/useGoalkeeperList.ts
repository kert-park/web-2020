import { CmshbGoalkeeperStats } from '@kertPark/graphql/generated';
import { SortFunctions, sortNumber, useSortData } from '@kertPark/hooks/useSortData';

export enum SortKeys {
  GAMES = 'games',
  GOALS_AGAINST = 'goalsAgainst',
  GOALS_AGAINST_AVERAGE = 'goalsAgainstAverage',
  MINUTES = 'minutes',
  SAVES_AVERAGE = 'savesAverage',
  SHOTS_AGAINST = 'shotsAgainst',
  SHUTOUTS = 'shutouts',
}

const sortFunctions: SortFunctions<CmshbGoalkeeperStats, SortKeys> = {
  [SortKeys.GAMES]: sortNumber('games'),
  [SortKeys.GOALS_AGAINST]: sortNumber('goalsAgainst'),
  [SortKeys.GOALS_AGAINST_AVERAGE]: sortNumber('goalsAgainstAverage'),
  [SortKeys.MINUTES]: sortNumber('minutes'),
  [SortKeys.SAVES_AVERAGE]: sortNumber('savesAverage'),
  [SortKeys.SHOTS_AGAINST]: sortNumber('shotsAgainst'),
  [SortKeys.SHUTOUTS]: sortNumber('shutouts'),
};

type Props = {
  stats: Array<CmshbGoalkeeperStats>;
};

export const useGoalkeeperList = ({ stats }: Props) => {
  return useSortData<CmshbGoalkeeperStats, SortKeys>({
    data: stats.filter((stat) => (stat.games || 0) > 0),
    defaultSortKey: SortKeys.SAVES_AVERAGE,
    sortFunctions,
  });
};
