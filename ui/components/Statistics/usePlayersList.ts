import { CmshbPlayerStats } from '@kertPark/graphql/generated';
import { SortFunction, SortFunctions, sortNumber, useSortData } from '@kertPark/hooks/useSortData';

export enum SortKeys {
  ASSISTS = 'assists',
  GAMES = 'games',
  GOALS = 'goals',
  PENALTIES = 'penalties',
  PENALTY_KILL_GOALS = 'penaltyKillGoals',
  POINTS = 'points',
  POINTS_AVERAGE = 'pointsAverage',
  POWER_PLAY_GOALS = 'powerPlayGoals',
}

const sortPointsAverage: SortFunction<CmshbPlayerStats> = (isDesc) => (a, b) => {
  const aVal = calculatePointsAverage(a);
  const bVal = calculatePointsAverage(b);
  if (isDesc) {
    return bVal - aVal;
  } else {
    return aVal - bVal;
  }
};

const sortFunctions: SortFunctions<CmshbPlayerStats, SortKeys> = {
  [SortKeys.ASSISTS]: sortNumber('assists'),
  [SortKeys.GAMES]: sortNumber('games'),
  [SortKeys.GOALS]: sortNumber('goals'),
  [SortKeys.PENALTIES]: sortNumber('penalties'),
  [SortKeys.PENALTY_KILL_GOALS]: sortNumber('penaltyKillGoals'),
  [SortKeys.POINTS]: sortNumber('points'),
  [SortKeys.POINTS_AVERAGE]: sortPointsAverage,
  [SortKeys.POWER_PLAY_GOALS]: sortNumber('powerPlayGoals'),
};

type Props = {
  stats: Array<CmshbPlayerStats>;
};

export const usePlayersList = ({ stats }: Props) => {
  return useSortData<CmshbPlayerStats, SortKeys>({
    data: stats.filter((stat) => (stat.games || 0) > 0),
    defaultSortKey: SortKeys.POINTS,
    sortFunctions,
  });
};

export const calculatePointsAverage = (stat: Pick<CmshbPlayerStats, 'games' | 'points'>): number => {
  return Math.round(((stat.points || 0) / (stat.games || 1)) * 100) / 100 || 0;
};
