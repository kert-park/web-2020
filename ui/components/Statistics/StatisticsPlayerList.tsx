import { Table, TBody, Td, Th, THead, Tr } from '@kertPark/components/Atomic/Table';
import { calculatePointsAverage, SortKeys, usePlayersList } from '@kertPark/components/Statistics/usePlayersList';
import { routes } from '@kertPark/config/routes';
import { CmshbPlayerStats } from '@kertPark/graphql/generated';
import { isPlayer } from '@kertPark/lib/player';
import Router from 'next/router';
import React from 'react';

type Props = {
  players: Array<CmshbPlayerStats>;
  teamId: string;
};

type TableRowProps = {
  stat: CmshbPlayerStats;
  teamId: string;
};

const TableRow: React.FC<TableRowProps> = ({ stat, teamId }) => {
  const hasLink = isPlayer(stat.player) && !!stat.player.id;
  const onClick = () => {
    if (hasLink) {
      Router.push({ pathname: routes.team.player, query: { teamId, playerId: stat.player.id } });
    }
  };
  return (
    <Tr withLink={hasLink} onClick={onClick}>
      <Td>{isPlayer(stat.player) ? stat.player.number : '-'}</Td>
      <Td align="left" hoverUnderline={hasLink}>
        {stat.player.name}
      </Td>
      <Td>{stat.points || '-'}</Td>
      <Td>{stat.goals || '-'}</Td>
      <Td>{stat.assists || '-'}</Td>
      <Td>{stat.games || '-'}</Td>
      <Td desktopOnly>{calculatePointsAverage(stat) || '-'}</Td>
      <Td desktopOnly>{stat.powerPlayGoals || '-'}</Td>
      <Td desktopOnly>{stat.penaltyKillGoals || '-'}</Td>
      <Td>{stat.penalties || '-'}</Td>
    </Tr>
  );
};

export const StatisticsPlayerList: React.FC<Props> = ({ players, teamId }) => {
  const { handleSortKeyChange, sortedData, sortState } = usePlayersList({ stats: players });

  return (
    <Table>
      <THead>
        <Tr>
          <Th title="Číslo dresu" width="2.5rem">
            #
          </Th>
          <Th align="left">Jméno</Th>
          <Th
            onClick={() => handleSortKeyChange(SortKeys.POINTS)}
            sortIsDesc={sortState.key === SortKeys.POINTS ? sortState.isDesc : undefined}
            title="Kanadské body"
            width="3rem"
          >
            KB
          </Th>
          <Th
            onClick={() => handleSortKeyChange(SortKeys.GOALS)}
            sortIsDesc={sortState.key === SortKeys.GOALS ? sortState.isDesc : undefined}
            title="Góly"
            width="3rem"
          >
            G
          </Th>
          <Th
            onClick={() => handleSortKeyChange(SortKeys.ASSISTS)}
            sortIsDesc={sortState.key === SortKeys.ASSISTS ? sortState.isDesc : undefined}
            title="Asistence"
            width="3rem"
          >
            A
          </Th>
          <Th
            onClick={() => handleSortKeyChange(SortKeys.GAMES)}
            sortIsDesc={sortState.key === SortKeys.GAMES ? sortState.isDesc : undefined}
            title="Zápasy"
            width="3rem"
          >
            Z
          </Th>
          <Th
            desktopOnly
            onClick={() => handleSortKeyChange(SortKeys.POINTS_AVERAGE)}
            sortIsDesc={sortState.key === SortKeys.POINTS_AVERAGE ? sortState.isDesc : undefined}
            title="Průměr kanadských bodů na zápas"
            width="4rem"
          >
            ∅
          </Th>
          <Th
            desktopOnly
            onClick={() => handleSortKeyChange(SortKeys.POWER_PLAY_GOALS)}
            sortIsDesc={sortState.key === SortKeys.POWER_PLAY_GOALS ? sortState.isDesc : undefined}
            title="Góly v přesilovce"
            width="3rem"
          >
            PPG
          </Th>
          <Th
            desktopOnly
            onClick={() => handleSortKeyChange(SortKeys.PENALTY_KILL_GOALS)}
            sortIsDesc={sortState.key === SortKeys.PENALTY_KILL_GOALS ? sortState.isDesc : undefined}
            title="Góly v oslabení"
            width="3rem"
          >
            SHG
          </Th>
          <Th
            onClick={() => handleSortKeyChange(SortKeys.PENALTIES)}
            sortIsDesc={sortState.key === SortKeys.PENALTIES ? sortState.isDesc : undefined}
            title="Trestné minuty"
            width="3rem"
          >
            TM
          </Th>
        </Tr>
      </THead>
      <TBody>
        {sortedData.map((stat) => (
          <TableRow key={stat.player.cmshbId} stat={stat} teamId={teamId} />
        ))}
      </TBody>
    </Table>
  );
};
