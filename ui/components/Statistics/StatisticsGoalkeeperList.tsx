import { Table, TBody, Td, Th, THead, Tr } from '@kertPark/components/Atomic/Table';
import { SortKeys, useGoalkeeperList } from '@kertPark/components/Statistics/useGoalkeeperList';
import { routes } from '@kertPark/config/routes';
import { CmshbGoalkeeperStats } from '@kertPark/graphql/generated';
import { isPlayer } from '@kertPark/lib/player';
import Router from 'next/router';
import React from 'react';

type Props = {
  goalkeepers: Array<CmshbGoalkeeperStats>;
  teamId: string;
};

type TableRowProps = {
  stat: CmshbGoalkeeperStats;
  teamId: string;
};

const TableRow: React.FC<TableRowProps> = ({ stat, teamId }) => {
  const hasLink = isPlayer(stat.player) && !!stat.player.id;

  const onClick = () => {
    if (hasLink) {
      Router.push({ pathname: routes.team.player, query: { teamId, playerId: stat.player.id } });
    }
  };
  return (
    <Tr withLink={hasLink} onClick={onClick}>
      <Td>{isPlayer(stat.player) ? stat.player.number : '-'}</Td>
      <Td align="left" hoverUnderline={hasLink}>
        {stat.player.name}
      </Td>
      <Td>{stat.savesAverage || '-'}</Td>
      <Td>{stat.games || '-'}</Td>
      <Td desktopOnly>{stat.minutes || '-'}</Td>
      <Td>{stat.shotsAgainst || '-'}</Td>
      <Td>{stat.goalsAgainst || '-'}</Td>
      <Td desktopOnly>{stat.shutouts || '-'}</Td>
      <Td desktopOnly>{stat.goalsAgainstAverage || '-'}</Td>
    </Tr>
  );
};

export const StatisticsGoalkeeperList: React.FC<Props> = ({ goalkeepers, teamId }) => {
  const { handleSortKeyChange, sortedData, sortState } = useGoalkeeperList({ stats: goalkeepers });
  return (
    <Table>
      <THead>
        <Tr>
          <Th title="Číslo dresu" width="2.5rem">
            #
          </Th>
          <Th align="left">Jméno</Th>
          <Th
            onClick={() => handleSortKeyChange(SortKeys.SAVES_AVERAGE)}
            sortIsDesc={sortState.key === SortKeys.SAVES_AVERAGE ? sortState.isDesc : undefined}
            title="Procentuální úspěšnost zákroků"
            width="4rem"
          >
            %
          </Th>
          <Th
            onClick={() => handleSortKeyChange(SortKeys.GAMES)}
            sortIsDesc={sortState.key === SortKeys.GAMES ? sortState.isDesc : undefined}
            title="Zápasy"
            width="3rem"
          >
            Z
          </Th>
          <Th
            desktopOnly
            onClick={() => handleSortKeyChange(SortKeys.MINUTES)}
            sortIsDesc={sortState.key === SortKeys.MINUTES ? sortState.isDesc : undefined}
            title="Odehrané minuty"
            width="4rem"
          >
            Min
          </Th>
          <Th
            onClick={() => handleSortKeyChange(SortKeys.SHOTS_AGAINST)}
            sortIsDesc={sortState.key === SortKeys.SHOTS_AGAINST ? sortState.isDesc : undefined}
            title="Střely proti"
            width="4rem"
          >
            Stř.
          </Th>
          <Th
            onClick={() => handleSortKeyChange(SortKeys.GOALS_AGAINST)}
            sortIsDesc={sortState.key === SortKeys.GOALS_AGAINST ? sortState.isDesc : undefined}
            title="Inkasované góly"
            width="3rem"
          >
            G
          </Th>
          <Th
            desktopOnly
            onClick={() => handleSortKeyChange(SortKeys.SHUTOUTS)}
            sortIsDesc={sortState.key === SortKeys.SHUTOUTS ? sortState.isDesc : undefined}
            title="Čistá konta"
            width="3rem"
          >
            Nul
          </Th>
          <Th
            desktopOnly
            onClick={() => handleSortKeyChange(SortKeys.GOALS_AGAINST_AVERAGE)}
            sortIsDesc={sortState.key === SortKeys.GOALS_AGAINST_AVERAGE ? sortState.isDesc : undefined}
            title="Průměr inkasovaných gólů na zápas"
            width="4rem"
          >
            ∅
          </Th>
        </Tr>
      </THead>
      <TBody>
        {sortedData.map((stat) => (
          <TableRow key={stat.player.cmshbId} stat={stat} teamId={teamId} />
        ))}
      </TBody>
    </Table>
  );
};
