import { Card } from '@kertPark/components/Atomic/Card';
import { H3 } from '@kertPark/components/Atomic/Typography';
import {
  StatisticLeader,
  StatisticLeaderData,
  StatisticLeaderProps,
} from '@kertPark/components/Statistics/StatisticLeader';
import { spaces, theme } from '@kertPark/config/theme';
import {
  CmshbGoalkeeperStats,
  CmshbPlayerStats,
  CmshbPlayerStatsList,
  EnumComponentplayerteamplayerPost,
} from '@kertPark/graphql/generated';
import { messages } from '@kertPark/lib/messages';
import { isPlayer, PlayerPostLabels } from '@kertPark/lib/player';
import { sortObjectsArrayByProperties } from '@kertPark/lib/utils';
import { Box, Flex } from '@rebass/grid';
import React from 'react';

type PlayerStatWithPointsAverage = CmshbPlayerStats & { pointsAverage: number };

type Props = {
  leagueGroupStatistic: CmshbPlayerStatsList;
  teamId: string;
};

const mapPlayersData = (
  data: Array<CmshbPlayerStats | CmshbGoalkeeperStats | PlayerStatWithPointsAverage>,
  key: keyof CmshbPlayerStats | keyof CmshbGoalkeeperStats | keyof PlayerStatWithPointsAverage,
): Array<StatisticLeaderData> => {
  return data.map((item) => {
    return {
      player: item.player,
      value: item[key] as unknown as number,
    };
  });
};

type StatsData = Array<Omit<StatisticLeaderProps, 'teamId'>>;
type Stats = Array<{ label: string; stats: StatsData }>;

const getLeadersData = ({ leagueGroupStatistic }: Omit<Props, 'teamId'>): Stats => {
  const playersStats: StatsData = [
    {
      title: 'Kanadské bodování',
      measurement: messages.points,
      data: mapPlayersData(
        sortObjectsArrayByProperties<CmshbPlayerStats>(leagueGroupStatistic.players, ['points', 'goals']),
        'points',
      ),
    },
    {
      title: 'Branky',
      measurement: messages.goals,
      data: mapPlayersData(
        sortObjectsArrayByProperties<CmshbPlayerStats>(leagueGroupStatistic.players, ['goals', 'points']),
        'goals',
      ),
    },
    {
      title: 'Asistence',
      measurement: messages.assists,
      data: mapPlayersData(
        sortObjectsArrayByProperties<CmshbPlayerStats>(leagueGroupStatistic.players, ['assists', 'points']),
        'assists',
      ),
    },
    {
      title: 'Průměr bodů',
      measurement: () => '∅',
      data: mapPlayersData(
        sortObjectsArrayByProperties<PlayerStatWithPointsAverage>(
          leagueGroupStatistic.players.map((stat) => ({
            ...stat,
            // @ts-ignore
            pointsAverage: stat.games > 0 ? Math.round((stat.points / stat.games) * 100) / 100 : 0,
          })),
          ['pointsAverage', 'games'],
        ),
        'pointsAverage',
      ),
    },
  ];

  const defenders = leagueGroupStatistic.players.filter(
    (player) => isPlayer(player.player) && player.player.post === EnumComponentplayerteamplayerPost.Obrance,
  );
  const defendersStats: StatsData = [
    {
      title: 'Kanadské bodování',
      measurement: messages.points,
      data: mapPlayersData(sortObjectsArrayByProperties<CmshbPlayerStats>(defenders, ['points', 'goals']), 'points'),
    },
    {
      title: 'Branky',
      measurement: messages.goals,
      data: mapPlayersData(sortObjectsArrayByProperties<CmshbPlayerStats>(defenders, ['goals', 'points']), 'goals'),
    },
    {
      title: 'Asistence',
      measurement: messages.assists,
      data: mapPlayersData(sortObjectsArrayByProperties<CmshbPlayerStats>(defenders, ['assists', 'points']), 'assists'),
    },
    {
      title: 'Průměr bodů',
      measurement: () => '∅',
      data: mapPlayersData(
        sortObjectsArrayByProperties<PlayerStatWithPointsAverage>(
          defenders.map((stat) => ({
            ...stat,
            // @ts-ignore
            pointsAverage: stat.games > 0 ? Math.round((stat.points / stat.games) * 100) / 100 : 0,
          })),
          ['pointsAverage', 'games'],
        ),
        'pointsAverage',
      ),
    },
  ];

  const otherStats: StatsData = [
    {
      title: 'Zákroky',
      measurement: () => '%',
      data: mapPlayersData(
        sortObjectsArrayByProperties<CmshbGoalkeeperStats>(leagueGroupStatistic.goalkeepers, [
          'savesAverage',
          'minutes',
        ]),
        'savesAverage',
      ),
    },
    {
      title: 'Branky v přesilovce',
      measurement: messages.goals,
      data: mapPlayersData(
        sortObjectsArrayByProperties<CmshbPlayerStats>(leagueGroupStatistic.players, [
          'powerPlayGoals',
          'goals',
          'games',
        ]),
        'powerPlayGoals',
      ),
    },
    {
      title: 'Branky v oslabení',
      measurement: messages.goals,
      data: mapPlayersData(
        sortObjectsArrayByProperties<CmshbPlayerStats>(leagueGroupStatistic.players, [
          'penaltyKillGoals',
          'goals',
          'games',
        ]),
        'penaltyKillGoals',
      ),
    },
    {
      title: 'Trestné minuty',
      measurement: messages.penalties,
      data: mapPlayersData(
        sortObjectsArrayByProperties<CmshbPlayerStats>(leagueGroupStatistic.players, ['penalties', 'games']),
        'penalties',
      ),
    },
  ];

  return [
    { label: PlayerPostLabels.P, stats: playersStats },
    { label: PlayerPostLabels.O, stats: defendersStats },
    { label: 'Další', stats: otherStats },
  ];
};

export const StatisticsLeadersLayout: React.FC<Props> = ({ leagueGroupStatistic, teamId }) => {
  const data = getLeadersData({ leagueGroupStatistic });

  return (
    <Flex flexDirection="column" width={1}>
      {data.map(({ label, stats }) => (
        <React.Fragment key={label}>
          <H3
            margin="0"
            marginTop={`calc(${spaces[2]} + ${theme.spacing.card})`}
            marginLeft={`calc(${spaces[2]} + ${theme.spacing.card})`}
          >
            {label}
          </H3>
          <Flex alignItems="flex-start" flexWrap="wrap">
            {stats.map((boardProps, index) => (
              <Box key={index} p={2} width={[1, 1 / 2, 1 / 2, 1 / 2, 1 / 4]}>
                <Card>
                  <StatisticLeader {...boardProps} teamId={teamId} />
                </Card>
              </Box>
            ))}
          </Flex>
        </React.Fragment>
      ))}
    </Flex>
  );
};
