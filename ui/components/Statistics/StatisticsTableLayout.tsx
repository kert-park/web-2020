import { Card } from '@kertPark/components/Atomic/Card';
import { H3 } from '@kertPark/components/Atomic/Typography';
import { StatisticsGoalkeeperList } from '@kertPark/components/Statistics/StatisticsGoalkeeperList';
import { StatisticsPlayerList } from '@kertPark/components/Statistics/StatisticsPlayerList';
import { spaces, theme } from '@kertPark/config/theme';
import { CmshbPlayerStatsList } from '@kertPark/graphql/generated';
import { Box } from '@rebass/grid';
import React from 'react';

type Props = {
  leagueGroupStatistic: CmshbPlayerStatsList;
  teamId: string;
};

export const StatisticsTableLayout: React.FC<Props> = ({ leagueGroupStatistic, teamId }) => {
  return (
    <>
      <H3
        margin="0"
        marginTop={`calc(${spaces[2]} + ${theme.spacing.card})`}
        marginLeft={`calc(${spaces[2]} + ${theme.spacing.card})`}
      >
        Brankáři
      </H3>
      <Box width={1} p={2}>
        <Card>
          <StatisticsGoalkeeperList goalkeepers={leagueGroupStatistic.goalkeepers} teamId={teamId} />
        </Card>
      </Box>
      <H3
        margin="0"
        marginTop={`calc(${spaces[2]} + ${theme.spacing.card})`}
        marginLeft={`calc(${spaces[2]} + ${theme.spacing.card})`}
      >
        Hráči
      </H3>
      <Box width={1} p={2}>
        <Card>
          <StatisticsPlayerList players={leagueGroupStatistic.players} teamId={teamId} />
        </Card>
      </Box>
    </>
  );
};
