import { Button } from '@kertPark/components/Atomic/Button';
import { Card } from '@kertPark/components/Atomic/Card';
import { Checkbox } from '@kertPark/components/Atomic/Checkbox';
import { H3, P } from '@kertPark/components/Atomic/Typography';
import { useCookiesContext } from '@kertPark/components/Cookies/CookiesContext';
import { theme, zIndex } from '@kertPark/config/theme';
import { CookiesSettingsType } from '@kertPark/lib/cookies';
import { Flex } from '@rebass/grid';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  position: fixed;
  bottom: 1rem;
  right: 1rem;
  margin-left: 1rem;
  max-width: 30rem;
  z-index: ${zIndex.cookieBar};
`;

const PStyled = styled(P)`
  margin: 0.25em 0;
`;

const StyledCard = styled(Card)`
  box-shadow: 0 0.25em 2em 0.25em ${theme.colors.boxShadow.cookieBar};
`;

export const CookieBar = () => {
  const { isOpen, settings, setSettings } = useCookiesContext();
  const [formSettings, setFormSettings] = useState<CookiesSettingsType>(settings);

  const handleConfirmAll = () => {
    setFormSettings({ functional: true, analytics: true, social: true });
    setSettings({ functional: true, analytics: true, social: true });
  };

  const handleConfirmSelected = () => {
    setSettings(formSettings);
  };

  useEffect(() => {
    setFormSettings(settings);
  }, [settings]);

  if (!isOpen) {
    return null;
  }

  return (
    <Wrapper>
      <StyledCard>
        <H3 margin="0">Souhlas s cookies</H3>
        <P>
          Tento web používá cookies, aby zajistil správnou funkčnost stránek a umožnil analýzu návštěvnosti pomocí
          služeb třetích stran, jako je například Google Analytics. Taktéž jsou na našich stránkách používány cookies
          sociálních sítí, které Vám umožňují sdílet obsah našich stránek na sociálních sítích.
        </P>
        <Flex flexDirection="row" alignItems="center" justifyContent="space-between">
          <Flex flex="1" alignItems="center">
            <PStyled bold>Funkční cookies</PStyled>
          </Flex>
          <Flex flex="0 0 auto" alignItems="center">
            <Checkbox id="functional" checked disabled />
          </Flex>
        </Flex>
        <Flex flexDirection="row" alignItems="center" justifyContent="space-between">
          <Flex flex="1" alignItems="center">
            <PStyled bold>Analytické cookies</PStyled>
          </Flex>
          <Flex flex="0 0 auto" alignItems="center">
            <Checkbox
              id="analytics"
              checked={formSettings.analytics}
              onChange={() => setFormSettings((prevState) => ({ ...prevState, analytics: !prevState.analytics }))}
            />
          </Flex>
        </Flex>
        <Flex flexDirection="row" alignItems="center" justifyContent="space-between">
          <Flex flex="1" alignItems="center">
            <PStyled bold>Cookies sociálních médií</PStyled>
          </Flex>
          <Flex flex="0 0 auto" alignItems="center">
            <Checkbox
              id="social"
              checked={formSettings.social}
              onChange={() => setFormSettings((prevState) => ({ ...prevState, social: !prevState.social }))}
            />
          </Flex>
        </Flex>
        <Flex flexDirection="row" alignItems="center" justifyContent="center" flexWrap="wrap">
          <Button onClick={handleConfirmAll}>Souhlasím se všemi</Button>
          <Button onClick={handleConfirmSelected}>Souhlasím s výběrem</Button>
        </Flex>
      </StyledCard>
    </Wrapper>
  );
};
