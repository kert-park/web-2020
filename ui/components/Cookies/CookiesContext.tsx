import {
  CookieKey,
  CookiesSettingsType,
  getCookieSettings,
  initialCookieSettings,
  setCookie,
} from '@kertPark/lib/cookies';
import React, { createContext, useContext, useEffect, useState } from 'react';

type CookiesContextType = {
  isOpen: boolean;
  setOpen: (isOpen: boolean) => void;
  settings: CookiesSettingsType;
  setSettings: (settings: CookiesSettingsType) => void;
};

const CookiesContext = createContext<CookiesContextType>({} as CookiesContextType);

export const CookiesContextProvider: ReactFCWithChildren = ({ children }) => {
  const [isOpen, setOpen] = useState(false);
  const [settings, setSettingsState] = useState<CookiesSettingsType>(initialCookieSettings);

  const setSettings = (newSettings: CookiesSettingsType) => {
    setSettingsState(newSettings);
    setOpen(false);
    setCookie({
      key: CookieKey.cookieSettings,
      value: JSON.stringify(newSettings),
      options: {
        domain: process.env.DOMAIN,
        expires: 365,
      },
    });
  };

  useEffect(() => {
    const cookieSettings = getCookieSettings();
    if (cookieSettings) {
      setSettings(cookieSettings);
    } else {
      setOpen(true);
    }
  }, []);

  return (
    <CookiesContext.Provider value={{ isOpen, setOpen, settings, setSettings }}>{children}</CookiesContext.Provider>
  );
};

export const useCookiesContext = () => {
  const context = useContext(CookiesContext);

  // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
  if (!context) {
    throw new Error('useCookiesContext must be used within a CookiesContextProvider');
  }

  return context;
};
