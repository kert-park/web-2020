import { Card } from '@kertPark/components/Atomic/Card';
import { ContentLoader, Rect } from '@kertPark/components/Atomic/ContentLoader';
import { ImageAspectRatio } from '@kertPark/components/Atomic/ImageAspectRatio';
import { LinkAnchor } from '@kertPark/components/Atomic/LinkAnchor';
import { H3, Overline, PSmall } from '@kertPark/components/Atomic/Typography';
import { ExternalSourceLabel } from '@kertPark/components/PostList/ExternalSourceLabel';
import { usePostData } from '@kertPark/components/PostList/usePostData';
import { ImageSize } from '@kertPark/config/images';
import { mediaBreakpoints, spaces, theme } from '@kertPark/config/theme';
import { PostsCompositeQueryPostType } from '@kertPark/graphql/generated/helpers';
import { Box } from '@rebass/grid';
import React from 'react';
import styled, { css } from 'styled-components';

const Container = styled(Box)`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  padding: ${spaces[3]};
  @media (${mediaBreakpoints.lg}) {
    flex-direction: row;
  }
`;

const TitleAndMeta = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: space-between;
`;

const ImageCss = css`
  flex: 0 0 auto;
  width: 100%;
  @media (${mediaBreakpoints.lg}) {
    width: 14.625em;
    height: 9.75em;
  }
`;

const Image = styled(ImageAspectRatio)`
  ${ImageCss}
`;

const StyledAnchor = styled(LinkAnchor)`
  cursor: pointer;
  text-decoration: none;
  &:not(:first-child) {
    margin-top: ${spaces[3]};
  }
`;

const TitleH3 = styled(H3)`
  ${StyledAnchor}:hover & {
    text-decoration: underline;
  }
`;

const StyledCard = styled(Card)`
  flex-direction: column;
  @media (${mediaBreakpoints.lg}) {
    flex-direction: row;
  }
`;

const MetaContainer = styled.div`
  display: flex;
  flex-direction: row;
  gap: ${spaces[1]};
`;

const Meta = styled(Overline)`
  color: ${({ theme }) => theme.colors.secondary};
`;

const StyledPSmall = styled(PSmall)`
  margin-bottom: 0;
`;

type Props = {
  post: PostsCompositeQueryPostType;
};

export const ComplexPost = ({ post }: Props) => {
  const { categoriesJoined, date, href, hrefAs, imageSrc, perex, target, thumbAuthor, title } = usePostData({
    post,
    imageSize: ImageSize.small,
  });
  return (
    <StyledAnchor href={href} hrefAs={hrefAs} passHref target={target}>
      <StyledCard isLink noPadding>
        <Image src={imageSrc} title={thumbAuthor} />
        <Container>
          <TitleAndMeta>
            <MetaContainer>
              <ExternalSourceLabel type={post.__typename} />
              <Meta>
                {categoriesJoined}, {date}
              </Meta>
            </MetaContainer>
            <TitleH3 margin={`${spaces[2]} 0 0 0`}>{title}</TitleH3>
            <StyledPSmall>{perex}</StyledPSmall>
          </TitleAndMeta>
        </Container>
      </StyledCard>
    </StyledAnchor>
  );
};

const ImageContentLoader = styled(ContentLoader)`
  ${ImageCss};
  border-radius: ${theme.radius.card};
`;

export const ComplexPostLoader = () => {
  const w = 280;
  const h = 120;
  return (
    <StyledCard noPadding>
      <ImageContentLoader viewBox="0 0 48 32">
        <Rect rx={0} ry={0} x="0" y="0" width="100%" height="100%" />
      </ImageContentLoader>
      <Container width={1}>
        <ContentLoader viewBox={`0 0 ${w} ${h}`} style={{ maxWidth: w, width: '100%' }}>
          <Rect x={0} y="0" width="100" height="12" />
          <Rect x={0} y="24" width="280" height="28" />
          <Rect x={0} y="72" width="280" height="10" />
          <Rect x={0} y="90" width="280" height="10" />
          <Rect x={0} y="108" width="200" height="10" />
        </ContentLoader>
      </Container>
    </StyledCard>
  );
};
