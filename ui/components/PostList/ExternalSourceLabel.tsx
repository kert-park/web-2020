import { theme } from '@kertPark/config/theme';
import { PostsCompositeQueryPostType } from '@kertPark/graphql/generated/helpers';
import React from 'react';
import styled from 'styled-components';

type Props = {
  type: PostsCompositeQueryPostType['__typename'];
};

const Container = styled.div`
  color: ${({ theme }) => theme.colors.textInverted};
  font-family: ${theme.fonts.text};
  font-size: 0.625rem;
  padding: 0.125rem 0.25rem;
  background-color: ${({ theme }) => theme.colors.secondary};
  border-radius: 0.25em;
`;

export const ExternalSourceLabel: React.FC<Props> = ({ type }) => {
  switch (type) {
    case 'PostExternalComposite':
      return <Container as="div">Hokejbal.cz</Container>;
    default:
      return null;
  }
};
