import { Card } from '@kertPark/components/Atomic/Card';
import { ContentLoader, Rect } from '@kertPark/components/Atomic/ContentLoader';
import { ImageAspectRatio } from '@kertPark/components/Atomic/ImageAspectRatio';
import { LinkAnchor } from '@kertPark/components/Atomic/LinkAnchor';
import { H4, Overline } from '@kertPark/components/Atomic/Typography';
import { ExternalSourceLabel } from '@kertPark/components/PostList/ExternalSourceLabel';
import { usePostData } from '@kertPark/components/PostList/usePostData';
import { ImageSize } from '@kertPark/config/images';
import { mediaBreakpoints, spaces, theme } from '@kertPark/config/theme';
import { PostsCompositeQueryPostType } from '@kertPark/graphql/generated/helpers';
import React from 'react';
import styled, { css } from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: ${spaces[3]};
`;

const TitleAndMeta = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: space-between;
`;

const ImageCss = css`
  flex: 0 0 auto;
  width: 6em;

  @media (${mediaBreakpoints.md}) {
    display: none;
  }

  @media (${mediaBreakpoints.xl}) {
    display: initial;
  }
`;

const Image = styled(ImageAspectRatio)`
  ${ImageCss}
`;

const StyledAnchor = styled(LinkAnchor)`
  cursor: pointer;
  text-decoration: none;
  &:not(:first-child) {
    margin-top: ${spaces[3]};
  }
`;

const TitleH4 = styled(H4)`
  ${StyledAnchor}:hover & {
    color: ${({ theme }) => theme.colors.secondary};
  }
`;

const MetaContainer = styled.div`
  display: flex;
  flex-direction: row;
  gap: ${spaces[1]};
  align-items: center;
`;

const Meta = styled(Overline)`
  color: ${({ theme }) => theme.colors.secondary};
`;

type Props = {
  post: PostsCompositeQueryPostType;
};

export const SmallPost = ({ post }: Props) => {
  const { categoriesJoined, date, href, hrefAs, imageSrc, target, thumbAuthor, title } = usePostData({
    post,
    imageSize: ImageSize.thumbnail,
  });
  return (
    <StyledAnchor href={href} hrefAs={hrefAs} passHref target={target}>
      <Card flexDirection="row" isLink noPadding>
        <Image src={imageSrc} title={thumbAuthor} />
        <Container>
          <TitleAndMeta>
            <MetaContainer>
              <ExternalSourceLabel type={post.__typename} />
              <Meta small>
                {categoriesJoined}, {date}
              </Meta>
            </MetaContainer>
            <TitleH4 margin={`${spaces[2]} 0 0 0`}>{title}</TitleH4>
          </TitleAndMeta>
        </Container>
      </Card>
    </StyledAnchor>
  );
};

const ImageContentLoader = styled(ContentLoader)`
  ${ImageCss};
`;

export const SmallPostLoader = () => {
  const w = 240;
  const h = 104;
  const imageWidth = 96;
  return (
    <Card flexDirection="row" noPadding>
      <ImageContentLoader viewBox={`0 0 ${imageWidth} ${h}`} style={{ width: imageWidth, height: h }}>
        <Rect rx={theme.radius.card} ry={theme.radius.card} x="0" y="0" width={imageWidth} height={h} />
      </ImageContentLoader>
      <Container>
        <ContentLoader viewBox={`0 0 ${w} ${h - 2 * 16}`} style={{ maxWidth: w, width: '100%' }}>
          <Rect x={0} y="2" width="100" height="12" />
          <Rect x={0} y="22" width="208" height="20" />
          <Rect x={0} y="50" width="160" height="20" />
        </ContentLoader>
      </Container>
    </Card>
  );
};
