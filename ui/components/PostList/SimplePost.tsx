import { Card } from '@kertPark/components/Atomic/Card';
import { ImageAspectRatio } from '@kertPark/components/Atomic/ImageAspectRatio';
import { LinkAnchor } from '@kertPark/components/Atomic/LinkAnchor';
import { H3, Overline } from '@kertPark/components/Atomic/Typography';
import { ExternalSourceLabel } from '@kertPark/components/PostList/ExternalSourceLabel';
import { usePostData } from '@kertPark/components/PostList/usePostData';
import { mediaBreakpoints, spaces } from '@kertPark/config/theme';
import { PostsCompositeQueryPostType } from '@kertPark/graphql/generated/helpers';
import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: ${spaces[3]};
`;

const TitleAndMeta = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: space-between;
`;

const Image = styled(ImageAspectRatio)`
  display: none;
  flex: 0 0 auto;
  width: 6em;
  @media (${mediaBreakpoints.md}) {
    display: block;
    width: 9em;
  }
`;

const StyledAnchor = styled(LinkAnchor)`
  cursor: pointer;
  text-decoration: none;
  &:not(:first-child) {
    margin-top: ${spaces[3]};
  }
`;

const TitleH3 = styled(H3)`
  ${StyledAnchor}:hover & {
    color: ${({ theme }) => theme.colors.secondary};
  }
`;

const MetaContainer = styled.div`
  display: flex;
  flex-direction: row;
  gap: ${spaces[1]};
`;

const Meta = styled(Overline)`
  color: ${({ theme }) => theme.colors.secondary};
`;

type Props = {
  post: PostsCompositeQueryPostType;
};

export const SimplePost = ({ post }: Props) => {
  const { categoriesJoined, date, href, hrefAs, imageSrc, target, thumbAuthor, title } = usePostData({ post });

  return (
    <StyledAnchor href={href} hrefAs={hrefAs} passHref target={target}>
      <Card flexDirection="row" isLink noPadding>
        <Image src={imageSrc} title={thumbAuthor} />
        <Container>
          <TitleAndMeta>
            <MetaContainer>
              <ExternalSourceLabel type={post.__typename} />
              <Meta>
                {categoriesJoined}, {date}
              </Meta>
            </MetaContainer>
            <TitleH3 margin={`${spaces[2]} 0 0 0`}>{title}</TitleH3>
          </TitleAndMeta>
        </Container>
      </Card>
    </StyledAnchor>
  );
};
