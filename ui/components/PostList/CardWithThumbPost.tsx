import { Card } from '@kertPark/components/Atomic/Card';
import { ImageAspectRatio } from '@kertPark/components/Atomic/ImageAspectRatio';
import { LinkAnchor } from '@kertPark/components/Atomic/LinkAnchor';
import { H3, Overline } from '@kertPark/components/Atomic/Typography';
import { ExternalSourceLabel } from '@kertPark/components/PostList/ExternalSourceLabel';
import { usePostData } from '@kertPark/components/PostList/usePostData';
import { spaces } from '@kertPark/config/theme';
import { PostsCompositeQueryPostType } from '@kertPark/graphql/generated/helpers';
import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${spaces[3]};
`;

const StyledAnchor = styled(LinkAnchor)``;

const Title = styled(H3)`
  ${StyledAnchor}:hover & {
    color: ${({ theme }) => theme.colors.secondary};
  }
`;

const Meta = styled(Overline)`
  color: ${({ theme }) => theme.colors.secondary};
`;

const MetaContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  gap: ${spaces[1]};
  margin: 0 0 1rem 0;
`;

type Props = {
  post: PostsCompositeQueryPostType;
};

export const CardWithThumbPost = ({ post }: Props) => {
  const { categoriesJoined, date, href, hrefAs, imageSrc, target, thumbAuthor, title } = usePostData({ post });
  return (
    <StyledAnchor href={href} hrefAs={hrefAs} passHref target={target}>
      <Card isLink noPadding>
        <ImageAspectRatio src={imageSrc} title={thumbAuthor} />
        <Container>
          <MetaContainer>
            <ExternalSourceLabel type={post.__typename} />
            <Meta>
              {categoriesJoined}, {date}
            </Meta>
          </MetaContainer>
          <Title margin="0">{title}</Title>
        </Container>
      </Card>
    </StyledAnchor>
  );
};
