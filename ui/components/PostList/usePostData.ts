import { getPostImageUrl, getThumbImageUrl, ImageSize } from '@kertPark/config/images';
import { routes } from '@kertPark/config/routes';
import { PostsCompositeQueryPostType } from '@kertPark/graphql/generated/helpers';
import { getReadableDate } from '@kertPark/lib/date';
import { getCategoriesNameJoined } from '@kertPark/lib/post';

type Props = {
  post: PostsCompositeQueryPostType;
  imageSize?: ImageSize;
};

type Result = {
  categoriesJoined: string;
  date?: string;
  href: string;
  hrefAs?: string;
  imageSrc: string;
  perex?: string;
  target?: '_blank';
  thumbAuthor?: string;
  title?: string;
};

export const usePostData = ({ post, imageSize = ImageSize.medium }: Props): Result => {
  switch (post.__typename) {
    case 'PostExternalComposite':
      return {
        categoriesJoined: getCategoriesNameJoined({ categories: post.categories }),
        date: getReadableDate(post.publishAt),
        href: post.url,
        imageSrc: post.imageUrl || getThumbImageUrl({ size: ImageSize.medium }),
        perex: post.perex,
        target: '_blank',
        thumbAuthor: 'hokejbal.cz',
        title: post.title,
      };
    case 'PostEntity':
    default:
      return {
        categoriesJoined: getCategoriesNameJoined({ categories: post.attributes?.categories?.data }),
        date: post.attributes?.publishAt ? getReadableDate(post.attributes.publishAt) : undefined,
        href: routes.post,
        hrefAs: routes.postAs(post.attributes?.slug || ''),
        imageSrc: getPostImageUrl({
          formats: post.attributes?.thumb?.data?.attributes?.formats || {},
          size: imageSize,
        }),
        perex: post.attributes?.perex,
        thumbAuthor: post.attributes?.thumbAuthor || '',
        title: post.attributes?.title,
      };
  }
};
