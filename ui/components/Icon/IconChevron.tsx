import { IconProps } from '@kertPark/components/Icon/Icon';
import { theme } from '@kertPark/config/theme';
import React from 'react';
import styled from 'styled-components';

type Props = IconProps & { rotate?: number; withAnimation?: boolean };

const Svg = styled.svg<Pick<Props, 'rotate' | 'withAnimation'>>`
  ${({ withAnimation }) => (withAnimation ? `transition: transform ${theme.transitionSpeedMenu};` : '')}
  transform: rotate(${({ rotate }) => rotate}deg);
`;

export const IconChevron = ({
  className,
  color = theme.colors.kert.bluePrimary,
  rotate = 0,
  size = 64,
  withAnimation = false,
}: Props) => {
  return (
    <Svg
      className={className}
      viewBox="0 0 256 256"
      height={size}
      width={size}
      fill={color}
      rotate={rotate}
      withAnimation={withAnimation}
    >
      <polygon points="79.093,0 48.907,30.187 146.72,128 48.907,225.813 79.093,256 207.093,128" />
    </Svg>
  );
};
