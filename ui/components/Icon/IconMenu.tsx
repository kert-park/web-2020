import { IconProps } from '@kertPark/components/Icon/Icon';
import { theme } from '@kertPark/config/theme';
import React from 'react';

export const IconMenu = ({ color = theme.colors.kert.yellowPrimary, size = 64 }: IconProps) => {
  return (
    <svg viewBox="0 0 341.333 341.333" height={size} width={size} fill={color}>
      <rect y="277.333" width="341.333" height="42.667" />
      <rect y="149.333" width="341.333" height="42.667" />
      <rect y="21.333" width="341.333" height="42.667" />
    </svg>
  );
};
