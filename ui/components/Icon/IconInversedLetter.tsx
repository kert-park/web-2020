import { IconProps } from '@kertPark/components/Icon/Icon';
import { theme } from '@kertPark/config/theme';
import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div<{ gradient?: boolean; size: string }>`
  display: flex;
  justify-content: center;
  align-items: center;
  color: ${({ theme }) => theme.colors.textInverted};
  width: ${({ size }) => size};
  height: ${({ size }) => size};
  font-size: calc(${({ size }) => size} - 0.5rem);
  font-family: ${theme.fonts.header};
  font-weight: bold;
  background: ${({ gradient }) => (gradient ? theme.gradients.blueRed : theme.colors.primary)};
  text-transform: uppercase;
  border-radius: 0.25em;
`;

export const IconInverseLetter = ({ size, gradient, letter }: IconProps & { gradient?: boolean; letter: string }) => {
  return (
    <Wrapper gradient={gradient} size={typeof size === 'string' ? size : `${size}em`}>
      {letter}
    </Wrapper>
  );
};
