import { IconProps } from '@kertPark/components/Icon/Icon';
import { theme } from '@kertPark/config/theme';
import React from 'react';

type Props = {
  orientation: 'horizontal' | 'vertical';
} & IconProps;

export const IconMore = ({ color = theme.colors.kert.yellowPrimary, orientation = 'vertical', size = 64 }: Props) => {
  return (
    <svg
      viewBox="0 0 60 60"
      height={size}
      width={size}
      fill={color}
      transform={`rotate(${orientation === 'horizontal' ? 90 : 0})`}
    >
      <path d="M30,16c4.411,0,8-3.589,8-8s-3.589-8-8-8s-8,3.589-8,8S25.589,16,30,16z" />
      <path d="M30,44c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,44,30,44z" />
      <path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" />
    </svg>
  );
};
