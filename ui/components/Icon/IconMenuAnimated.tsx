import { IconProps } from '@kertPark/components/Icon/Icon';
import { theme } from '@kertPark/config/theme';
import React from 'react';
import styled from 'styled-components';

const SIZE = 12;
const SIZE_MULTIPLIER = 2.5;

const Container = styled.div`
  position: relative;
  width: 50vw;
  height: 50vw;
  max-width: ${SIZE}px;
  max-height: ${SIZE}px;
`;

const Wrapper = styled.div`
  position: absolute;
  top: -20px;
  left: -8px;
`;

const Path = styled.path<Pick<Props, 'color' | 'checked'>>`
  fill: none;
  stroke: ${({ color }) => color};
  stroke-width: 3;
  stroke-linecap: round;
  stroke-linejoin: round;
  transform: translateX(${({ checked }) => (checked ? '30px' : 0)});
  transition: all ${theme.transitionSpeedMenu};
`;

const PrimaryPath = styled(Path)<Pick<Props, 'checked'>>`
  stroke-dasharray: ${({ checked }) => (checked ? 22.627416998 : 24)} 111.22813415527344;
  stroke-dashoffset: ${({ checked }) => (checked ? -16.9705627485 : -50.22813415527344)};
`;

const SecondaryPath = styled(Path)<Pick<Props, 'checked'>>`
  stroke-dasharray: ${({ checked }) => (checked ? 0 : 24)} 99;
  stroke-dashoffset: ${({ checked }) => (checked ? -20 : -38)};
`;

type Props = Pick<IconProps, 'color'> & { checked: boolean };

export const IconMenuAnimated = ({ color = theme.colors.kert.yellowPrimary, checked }: Props) => {
  return (
    <Container>
      <Wrapper>
        <svg
          viewBox="0 0 99 30"
          height={SIZE * SIZE_MULTIPLIER}
          width={SIZE * SIZE_MULTIPLIER}
          xmlns="http://www.w3.org/2000/svg"
        >
          <PrimaryPath color={color} checked={checked} d="M0 70l28-28c2-2 2-2 7-2h64" />
          <SecondaryPath color={color} checked={checked} d="M0 50h99" />
          <PrimaryPath color={color} checked={checked} d="M0 30l28 28c2 2 2 2 7 2h64" />
        </svg>
      </Wrapper>
    </Container>
  );
};
