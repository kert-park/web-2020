import { IconProps } from '@kertPark/components/Icon/Icon';
import { theme } from '@kertPark/config/theme';
import React from 'react';

type Props = IconProps;

export const IconClose = ({ className, color = theme.colors.kert.bluePrimary, size = 64 }: Props) => {
  return (
    <svg className={className} viewBox="0 0 386.667 386.667" height={size} width={size} fill={color}>
      <path d="m386.667 45.564-45.564-45.564-147.77 147.769-147.769-147.769-45.564 45.564 147.769 147.769-147.769 147.77 45.564 45.564 147.769-147.769 147.769 147.769 45.564-45.564-147.768-147.77z" />
    </svg>
  );
};
