import { IconProps } from '@kertPark/components/Icon/Icon';
import { theme } from '@kertPark/config/theme';
import React from 'react';

export const IconArticles = ({ color = theme.colors.kert.bluePrimary, size = 64 }: IconProps) => {
  return (
    <svg viewBox="0 0 512 512" height={size} width={size} fill={color}>
      <path d="M472,24H40A24.028,24.028,0,0,0,16,48V464a24.028,24.028,0,0,0,24,24H472a24.028,24.028,0,0,0,24-24V48A24.028,24.028,0,0,0,472,24ZM40,40H472a8.009,8.009,0,0,1,8,8V88H32V48A8.009,8.009,0,0,1,40,40ZM472,472H40a8.009,8.009,0,0,1-8-8V104H480V464A8.009,8.009,0,0,1,472,472Z" />
      <circle cx="120" cy="64" r="8" />
      <circle cx="88" cy="64" r="8" />
      <circle cx="56" cy="64" r="8" />
      <path d="M72,184H88a8,8,0,0,0,0-16H72a8,8,0,0,0,0,16Z" />
      <path d="M272,176a8,8,0,0,0-8-8H120a8,8,0,0,0,0,16H264A8,8,0,0,0,272,176Z" />
      <path d="M72,216H184a8,8,0,0,0,0-16H72a8,8,0,0,0,0,16Z" />
      <path d="M216,216H360a8,8,0,0,0,0-16H216a8,8,0,0,0,0,16Z" />
      <path d="M72,152H264a8,8,0,0,0,0-16H72a8,8,0,0,0,0,16Z" />
      <path d="M440,168H296a8,8,0,0,0,0,16H440a8,8,0,0,0,0-16Z" />
      <path d="M440,200H392a8,8,0,0,0,0,16h48a8,8,0,0,0,0-16Z" />
      <path d="M72,296H88a8,8,0,0,0,0-16H72a8,8,0,0,0,0,16Z" />
      <path d="M272,288a8,8,0,0,0-8-8H120a8,8,0,0,0,0,16H264A8,8,0,0,0,272,288Z" />
      <path d="M72,328H184a8,8,0,0,0,0-16H72a8,8,0,0,0,0,16Z" />
      <path d="M216,328H360a8,8,0,0,0,0-16H216a8,8,0,0,0,0,16Z" />
      <path d="M72,264H264a8,8,0,0,0,0-16H72a8,8,0,0,0,0,16Z" />
      <path d="M440,280H296a8,8,0,0,0,0,16H440a8,8,0,0,0,0-16Z" />
      <path d="M440,312H392a8,8,0,0,0,0,16h48a8,8,0,0,0,0-16Z" />
      <path d="M72,408H88a8,8,0,0,0,0-16H72a8,8,0,0,0,0,16Z" />
      <path d="M264,392H120a8,8,0,0,0,0,16H264a8,8,0,0,0,0-16Z" />
      <path d="M184,424H72a8,8,0,0,0,0,16H184a8,8,0,0,0,0-16Z" />
      <path d="M360,424H216a8,8,0,0,0,0,16H360a8,8,0,0,0,0-16Z" />
      <path d="M72,376H264a8,8,0,0,0,0-16H72a8,8,0,0,0,0,16Z" />
      <path d="M440,392H296a8,8,0,0,0,0,16H440a8,8,0,0,0,0-16Z" />
      <path d="M440,424H392a8,8,0,0,0,0,16h48a8,8,0,0,0,0-16Z" />
    </svg>
  );
};
