import { IconProps } from '@kertPark/components/Icon/Icon';
import { theme } from '@kertPark/config/theme';
import React from 'react';

export const IconFacebook = ({ color = theme.colors.kert.bluePrimary, size = 64 }: IconProps) => {
  return (
    <svg viewBox="0 0 20 20" height={size} width={size} fill={color}>
      <path d="M20 10.06C20 4.708 15.834.293 10.522.014 5.21-.266.61 3.689.055 9.012-.5 14.335 3.184 19.164 8.438 20v-7.031H5.899V10.06h2.539V7.844c0-2.522 1.493-3.915 3.777-3.915.75.01 1.498.076 2.238.196v2.477h-1.26c-.436-.059-.874.086-1.191.391-.317.306-.479.74-.44 1.18v1.887h2.774l-.443 2.909h-2.33V20c4.86-.774 8.438-4.99 8.437-9.94z" />
    </svg>
  );
};
