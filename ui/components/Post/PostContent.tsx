import { Markdown } from '@kertPark/components/Atomic/Markdown';
import { P } from '@kertPark/components/Atomic/Typography';
import { Gallery } from '@kertPark/components/Gallery/Gallery';
import { PostApiGame } from '@kertPark/components/Post/ApiGame/PostApiGame';
import { PostApiGameList } from '@kertPark/components/Post/ApiGameList/PostApiGameList';
import { PostVideo } from '@kertPark/components/Post/EmbedVideo/PostVideo';
import { PostContentWrapper } from '@kertPark/components/Post/PostContentWrapper';
import { PostQueryPostType } from '@kertPark/graphql/generated/helpers';
import React from 'react';

type Props = {
  post: PostQueryPostType;
};

export const PostContent = ({ post }: Props) => {
  const content = post?.attributes?.content.map((item) => {
    switch (item?.__typename) {
      case 'ComponentCommonGallery':
        return item.gallery?.data ? (
          <Gallery key={`${item.__typename}_${item.id}`} gallery={item.gallery.data} text={item.text || undefined} />
        ) : undefined;
      case 'ComponentCommonApiGame':
        return <PostApiGame key={`${item.__typename}_${item.id}`} game={item.game} />;
      case 'ComponentCommonApiGameList':
        return <PostApiGameList key={`${item.__typename}_${item.id}`} gameList={item.gameList} />;
      case 'ComponentCommonRichText':
        return item.text ? <Markdown key={`${item.__typename}_${item.id}`} text={item.text} /> : undefined;
      case 'ComponentCommonEmbedVideo':
        return <PostVideo key={`${item.__typename}_${item.id}`} type={item.type} url={item.url} />;
      default:
        return null;
    }
  });

  return (
    <PostContentWrapper>
      <P bold>{post?.attributes?.perex}</P>
      {content}
    </PostContentWrapper>
  );
};
