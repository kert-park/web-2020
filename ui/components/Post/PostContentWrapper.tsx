import { mediaBreakpoints, theme } from '@kertPark/config/theme';
import styled from 'styled-components';

export const PostContentWrapper = styled.div`
  width: 100%;
  @media (${mediaBreakpoints.xxl}) {
    max-width: ${theme.layout.textContentMaxWidth};
  }
`;
