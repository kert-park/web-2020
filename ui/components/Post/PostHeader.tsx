import { ContentLoader, Rect } from '@kertPark/components/Atomic/ContentLoader';
import { ImageAspectRatio } from '@kertPark/components/Atomic/ImageAspectRatio';
import { H1, Overline } from '@kertPark/components/Atomic/Typography';
import { mediaBreakpoints, spaces, theme } from '@kertPark/config/theme';
import { PostQueryPostType } from '@kertPark/graphql/generated/helpers';
import { useMediaQuery } from '@kertPark/hooks/useMediaQuery';
import { getReadableDate } from '@kertPark/lib/date';
import { getAuthorName, getCategoriesNameJoined, getImageSrc } from '@kertPark/lib/post';
import { Box, Flex } from '@rebass/grid';
import React, { useContext } from 'react';
import styled, { css, ThemeContext } from 'styled-components';

const ImageCss = css`
  margin: ${spaces[3]} 0;
`;

const Image = styled(ImageAspectRatio)`
  ${ImageCss}
`;

type Props = {
  post: PostQueryPostType;
};

export const PostHeader = ({ post }: Props) => {
  const isXxlDesktop = useMediaQuery(mediaBreakpoints.xxl);
  const imageSrc = getImageSrc({ isXxlDesktop, post });

  return (
    <PostHeaderContent
      author={getAuthorName(post)}
      categoriesString={getCategoriesNameJoined({
        categories: post?.attributes?.categories?.data,
      })}
      imageAuthor={post?.attributes?.thumbAuthor || undefined}
      imageSrc={imageSrc}
      imageTitle={post?.attributes?.thumbAuthor || undefined}
      publishedAt={post?.attributes?.publishAt}
      title={post?.attributes?.title || ''}
    />
  );
};

type PostHeaderContentProps = {
  author?: string;
  categoriesString: string;
  imageAuthor?: string;
  imageSrc?: string;
  imageTitle?: string;
  publishedAt?: Date;
  title: string;
};

export const PostHeaderContent: React.FC<PostHeaderContentProps> = ({
  author,
  categoriesString,
  imageAuthor,
  imageSrc,
  imageTitle = '',
  publishedAt,
  title,
}) => {
  const kertTheme = useContext(ThemeContext);
  return (
    <Flex width={1} flexDirection="column" alignItems="flex-start">
      <H1 marginTop={spaces[3]} color={kertTheme.colors.textPrimary}>
        {title}
      </H1>
      <Overline color={kertTheme.colors.secondary} margin="0 1em 0 0">
        {categoriesString}
        {publishedAt && `, ${getReadableDate(publishedAt)}`}
      </Overline>
      <Box width={1}>{imageSrc && <Image title={imageTitle} src={imageSrc} />}</Box>
      <Flex width={1} flexDirection={['column', 'row']} justifyContent={'flex-start'} flexWrap="wrap">
        <Overline color={kertTheme.colors.textLight}>
          {[...(author ? [`autor: ${author}`] : []), ...(imageAuthor ? [`fotografie: ${imageAuthor}`] : [])].join(', ')}
        </Overline>
      </Flex>
    </Flex>
  );
};

const ImageContentLoader = styled(ContentLoader)`
  ${ImageCss};
  border-radius: ${theme.radius.card};
`;

export const PostHeaderLoader = () => {
  const isLgDesktop = useMediaQuery(mediaBreakpoints.lg);
  const w = isLgDesktop ? 600 : 420;

  return (
    <Flex width={1} flexDirection="column" alignItems="flex-start">
      <ContentLoader viewBox={`0 0 ${w} 90`} style={{ marginTop: spaces[3], maxWidth: w, width: '100%' }}>
        <Rect x="0" y="16" width={isLgDesktop ? 460 : 380} height="42" />
        <Rect x="0" y="76" width="120" height="12" />
      </ContentLoader>
      <ImageContentLoader viewBox="0 0 48 32">
        <Rect rx={0} ry={0} x="0" y="0" width="100%" height="100%" />
      </ImageContentLoader>
    </Flex>
  );
};
