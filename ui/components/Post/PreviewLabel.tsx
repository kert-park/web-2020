import { spaces, theme, typography } from '@kertPark/config/theme';
import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  color: ${({ theme }) => theme.colors.textInverted};
  font-family: ${theme.fonts.header};
  font-size: ${typography.pSmall.fontSize.xs};
  font-weight: bold;
  text-transform: uppercase;
  background-color: ${({ theme }) => theme.colors.primary};
  margin: ${spaces[3]} ${spaces[2]};
  padding: 0.5rem 0.75rem;
  border-radius: 0.25em;
  width: 100%;
  text-align: center;
`;

export const PreviewLabel = () => {
  return <Wrapper>Preview</Wrapper>;
};
