import { Card } from '@kertPark/components/Atomic/Card';
import { Overline } from '@kertPark/components/Atomic/Typography';
import { GameListItem, GameListItemLoader } from '@kertPark/components/Game/GameListItem/GameListItem';
import { usePostApiGameList } from '@kertPark/components/Post/ApiGameList/usePostApiGameList';
import { TournamentListItem } from '@kertPark/components/Tournament/TournamentListItem';
import { ComponentCommonApiGameList, GameFilterScheduleEnum } from '@kertPark/graphql/generated';
import { Box, Flex } from '@rebass/grid';
import React from 'react';

type Props = Pick<ComponentCommonApiGameList, 'gameList'>;

export const PostApiGameList: React.FC<Props> = ({ gameList: gameListStringProps }) => {
  const { allItems, loading } = usePostApiGameList({ gameList: gameListStringProps });

  if (loading) {
    return (
      <Flex alignItems="flex-start" flexWrap="wrap" m={-2}>
        {[0, 1, 2].map((index) => (
          <Box key={index} width={1} p={2}>
            <Card>
              <GameListItemLoader />
            </Card>
          </Box>
        ))}
      </Flex>
    );
  }

  if (allItems.length > 0) {
    return (
      <Flex alignItems="flex-start" flexWrap="wrap" m={-2}>
        {allItems.map((group, index) => (
          <Box key={`${group.label}_${index}`} width={1} p={2}>
            <Card>
              <Overline>{group.label}</Overline>
              {group.items.map((item) =>
                item.__typename === 'Tournament' ? (
                  <TournamentListItem key={`tournament_${item.id}`} tournament={item} />
                ) : (
                  <GameListItem
                    key={`game_${item.id}`}
                    game={item}
                    hasLargeMetadata={item.team.systemName === 'muzia'}
                    scheduleVariant={GameFilterScheduleEnum.UPCOMING}
                  />
                ),
              )}
            </Card>
          </Box>
        ))}
      </Flex>
    );
  }

  return null;
};
