import {
  ComponentCommonApiGameList,
  useGameListLazyQuery,
  useTournamentListLazyQuery,
} from '@kertPark/graphql/generated';
import { groupGamesAndTournamentsByLabel, mergeGamesAndTournamentsAndSort } from '@kertPark/lib/gamePage';
import { endOfDay, isValid, startOfDay } from 'date-fns';
import { useEffect, useMemo, useState } from 'react';

type Props = Pick<ComponentCommonApiGameList, 'gameList'>;

type ApiGameListVariables = {
  from: Date;
  teamIds: Array<string>;
  to: Date;
  year: string;
};

const isApiGameListVariables = (variables: any): variables is ApiGameListVariables => {
  return (
    variables &&
    isValid(variables.from) &&
    Array.isArray(variables.teamIds) &&
    variables.teamIds.every((teamId: any) => typeof teamId === 'string') &&
    isValid(variables.to) &&
    typeof variables.year === 'string'
  );
};

export const usePostApiGameList = ({ gameList: gameListStringProps }: Props) => {
  const [variables, setVariables] = useState<ApiGameListVariables | undefined>(undefined);
  const [getGameList, { data: gamesData, loading: gamesLoading }] = useGameListLazyQuery();
  const [getTournamentList, { data: tournamentsData, loading: tournamentsLoading }] = useTournamentListLazyQuery();

  useEffect(() => {
    try {
      const parsed = JSON.parse(gameListStringProps as string);
      const variables = {
        from: startOfDay(new Date(parsed.from)),
        teamIds: parsed.teamIds,
        to: endOfDay(new Date(parsed.to)),
        year: parsed.year,
      };
      if (isApiGameListVariables(variables)) {
        setVariables(variables);
        getGameList({
          variables: {
            teamIds: variables.teamIds,
            year: variables.year,
            filter: {
              from: variables.from,
              to: variables.to,
            },
          },
        });
        getTournamentList({ variables });
      } else {
        setVariables(undefined);
      }
    } catch (e) {
      console.error(e);
      setVariables(undefined);
    }
  }, [gameListStringProps]);

  const allItems = useMemo(() => {
    const numberOfTeams = variables?.teamIds.length || 0;
    const games = gamesData?.gameList || [];
    const tournaments = tournamentsData?.tournamentList || [];

    return groupGamesAndTournamentsByLabel(mergeGamesAndTournamentsAndSort(games, tournaments), numberOfTeams > 0);
  }, [gamesData, tournamentsData, variables]);

  const loading = gamesLoading || tournamentsLoading;

  return {
    allItems,
    loading,
  };
};
