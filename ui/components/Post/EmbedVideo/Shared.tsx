import { Card } from '@kertPark/components/Atomic/Card';
import { ContentLoader, Rect } from '@kertPark/components/Atomic/ContentLoader';
import { H2, P } from '@kertPark/components/Atomic/Typography';
import { spaces, theme } from '@kertPark/config/theme';
import React from 'react';
import styled from 'styled-components';

const Link = styled.span`
  color: ${({ theme }) => theme.colors.textLight};
  text-decoration: underline;
  cursor: pointer;
  &:hover {
    color: ${({ theme }) => theme.colors.textInverted};
  }
`;

const StyledCard = styled(Card)`
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${theme.gradients.blueRed};
  padding: ${spaces[2]} ${spaces[4]};
`;

export const Loader: React.FC = () => {
  const { width, height } = theme.layout.videoMaxSize;
  return (
    <ContentLoader viewBox={`0 0 ${width} ${height}`} style={{ maxWidth: width }}>
      <Rect x="0" y="0" rx="16" ry="16" width={width} height={height} />
    </ContentLoader>
  );
};

type MissingCookieConsentProps = {
  onOpenCookieSettings: () => void;
};

export const MissingCookieConsent: React.FC<MissingCookieConsentProps> = ({ onOpenCookieSettings }) => {
  return (
    <StyledCard>
      <H2 as="div" color={theme.colors.textInverted}>
        Video nelze načíst
      </H2>
      <P color={theme.colors.textInverted}>
        Nedá se nic dělat, abychom Vám mohli přehrát toto video, musíte povolit cookies sociálních sítí.
      </P>
      <P color={theme.colors.textInverted}>
        <Link onClick={onOpenCookieSettings}>Otevřít nastavení cookies</Link>
      </P>
    </StyledCard>
  );
};

export const UnableToLoadVideo: React.FC = () => {
  return (
    <StyledCard>
      <H2 as="div" color={theme.colors.textInverted}>
        Video nelze načíst
      </H2>
      <P color={theme.colors.textInverted}>
        Omlouváme se, ale video v současné chvíli nelze načíst. Zkuste to prosím později.
      </P>
    </StyledCard>
  );
};
