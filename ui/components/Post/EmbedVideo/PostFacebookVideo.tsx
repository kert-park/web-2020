import { useCookiesContext } from '@kertPark/components/Cookies/CookiesContext';
import { Loader, MissingCookieConsent } from '@kertPark/components/Post/EmbedVideo/Shared';
import { theme } from '@kertPark/config/theme';
import Script from 'next/script';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  padding-bottom: 56.25%;
  position: relative;
  height: 0;

  iframe {
    background-color: ${theme.colors.black};
    border-radius: ${theme.radius.card};
  }
`;

type Props = {
  url: string;
};

export const PostFacebookVideo: React.FC<Props> = ({ url }) => {
  const [initialized, setInitialized] = useState(false);
  const { setOpen, settings } = useCookiesContext();

  useEffect(() => {
    setInitialized(true);
  }, []);

  const handleOnScriptReady = () => {
    // @ts-ignore
    window.FB?.XFBML?.parse?.();
  };

  if (!initialized) {
    return <Loader />;
  }

  if (!settings.social) {
    return <MissingCookieConsent onOpenCookieSettings={() => setOpen(true)} />;
  }

  return (
    <>
      <Script
        async
        defer
        onReady={handleOnScriptReady}
        src="https://connect.facebook.net/cs_CZ/sdk.js#xfbml=1&version=v3.2"
        strategy="lazyOnload"
      />
      <Wrapper
        className="fb-video"
        data-href={url}
        data-allowfullscreen="true"
        data-width={theme.layout.videoMaxSize.width}
        data-height={theme.layout.videoMaxSize.height}
        data-lazy="true"
      />
    </>
  );
};
