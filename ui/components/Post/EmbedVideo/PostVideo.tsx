import { PostFacebookVideo } from '@kertPark/components/Post/EmbedVideo/PostFacebookVideo';
import { PostYouTubeVideo } from '@kertPark/components/Post/EmbedVideo/PostYouTubeVideo';
import { ComponentCommonEmbedVideo, EnumComponentcommonembedvideoType } from '@kertPark/graphql/generated';
import React from 'react';

type Props = Pick<ComponentCommonEmbedVideo, 'type' | 'url'>;

export const PostVideo: React.FC<Props> = ({ type, url }) => {
  switch (type) {
    case EnumComponentcommonembedvideoType.youtube:
      return <PostYouTubeVideo url={url} />;
    case EnumComponentcommonembedvideoType.facebook:
      return <PostFacebookVideo url={url} />;
    default:
      return null;
  }
};
