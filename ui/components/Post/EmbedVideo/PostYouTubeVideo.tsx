import { useCookiesContext } from '@kertPark/components/Cookies/CookiesContext';
import { Loader, MissingCookieConsent, UnableToLoadVideo } from '@kertPark/components/Post/EmbedVideo/Shared';
import { theme } from '@kertPark/config/theme';
import { getYouTubeVideoId } from '@kertPark/lib/url';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  padding-bottom: 56.25%;
  position: relative;
  height: 0;
`;

const Iframe = styled.iframe`
  left: 0;
  top: 0;
  height: 100%;
  width: 100%;
  position: absolute;
  border: 0;
  border-radius: ${theme.radius.card};
`;

type Props = {
  url: string;
};

export const PostYouTubeVideo: React.FC<Props> = ({ url }) => {
  const [initialized, setInitialized] = useState(false);
  const { setOpen, settings } = useCookiesContext();

  useEffect(() => {
    setInitialized(true);
  }, []);

  if (!initialized) {
    return <Loader />;
  }

  if (!settings.social) {
    return <MissingCookieConsent onOpenCookieSettings={() => setOpen(true)} />;
  }

  const embedId = getYouTubeVideoId(url);

  if (!embedId) {
    return <UnableToLoadVideo />;
  }

  return (
    <Wrapper>
      <Iframe
        width={theme.layout.videoMaxSize.width}
        src={`https://www.youtube.com/embed/${embedId}`}
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
        allowFullScreen
      />
    </Wrapper>
  );
};
