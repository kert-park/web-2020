import { Layout } from '@kertPark/components/Layout/Layout';
import { PostContent } from '@kertPark/components/Post/PostContent';
import { PostHeader, PostHeaderLoader } from '@kertPark/components/Post/PostHeader';
import { PreviewLabel } from '@kertPark/components/Post/PreviewLabel';
import { PostSidebar } from '@kertPark/components/Sidebar/PostSidebar';
import { PostQuery } from '@kertPark/graphql/generated';
import { getMostRelevantCategoryCode, getOgTags } from '@kertPark/lib/post';
import { Flex } from '@rebass/grid';
import React from 'react';

export type PostPageProps = {
  post: NonNullable<PostQuery['posts']>['data'][0] | undefined;
  preview: boolean;
};

export const PostPage: React.FC<PostPageProps> = ({ post, preview }) => {
  if (!post) {
    return (
      <Layout noRobots>
        <Flex alignItems="flex-start" flexWrap="wrap" m={-2}>
          <Flex alignItems="flex-start" flexWrap="wrap" width={[1, 2 / 3]}>
            <Flex width={1} p={2} alignItems="center" flexDirection="column">
              <PostHeaderLoader />
            </Flex>
          </Flex>
        </Flex>
      </Layout>
    );
  }

  const categoryCode = getMostRelevantCategoryCode({ categories: post.attributes?.categories?.data });

  return (
    <Layout
      title={[...(preview ? ['Preview'] : []), ...(post.attributes?.title ? [post.attributes.title] : [])]}
      description={post.attributes?.perex}
      metadata={getOgTags(post)}
      noRobots={preview}
    >
      <Flex alignItems="flex-start" flexWrap="wrap" m={-2}>
        <Flex alignItems="flex-start" flexWrap="wrap" width={[1, 2 / 3]}>
          <Flex width={1} p={2} alignItems="center" flexDirection="column">
            {preview && <PreviewLabel />}
            <PostHeader post={post} />
            <PostContent post={post} />
          </Flex>
        </Flex>
        <Flex flexWrap="wrap" width={[1, 1 / 3]}>
          <PostSidebar categoryCode={categoryCode} postSlug={post.attributes?.slug || ''} />
        </Flex>
      </Flex>
    </Layout>
  );
};
