import { GameWithDetail, GameWithDetailLoader } from '@kertPark/components/Game/Detail/GameWithDetail';
import { ComponentCommonApiGame, GameQueryVariables, useGameLazyQuery } from '@kertPark/graphql/generated';
import React, { useEffect } from 'react';

const isGameProps = (game: unknown): game is GameQueryVariables => {
  return (
    !!game &&
    !!(game as GameQueryVariables).id &&
    !!(game as GameQueryVariables).teamId &&
    !!(game as GameQueryVariables).year
  );
};

type Props = Pick<ComponentCommonApiGame, 'game'>;

export const PostApiGame: React.FC<Props> = ({ game: gameStringProps }) => {
  const [getGame, { data, loading }] = useGameLazyQuery();

  useEffect(() => {
    try {
      const gameProps = JSON.parse(gameStringProps as string);
      if (isGameProps(gameProps)) {
        getGame({ variables: gameProps });
      }
    } catch (e) {
      console.error(e);
    }
  }, [gameStringProps]);

  const game = data?.game || undefined;

  if (loading || !game) {
    return <GameWithDetailLoader />;
  }

  return <GameWithDetail game={game} />;
};
