import { Overline } from '@kertPark/components/Atomic/Typography';
import { Flex } from '@rebass/grid';
import React from 'react';
import styled from 'styled-components';

const BarContainer = styled.div<{ isHome?: boolean }>`
  position: relative;
  flex: 1;
  width: 100%;
  height: 0.5em;
  background-color: ${({ theme }) => theme.colors.lightGrey};
  ${({ isHome }) => (isHome ? 'margin-right: 0.125em;' : 'margin-left: 0.125em;')};
`;

const Bar = styled.div<{ isHome?: boolean; width: number }>`
  position: absolute;
  top: 0;
  bottom: 0;
  ${({ isHome }) => (isHome ? 'right: 0;' : 'left: 0;')};
  height: 100%;
  width: ${({ width }) => width}%;
  background: ${({ isHome, theme }) => (isHome ? theme.gradients.blue : theme.gradients.red)};
`;

const CenterLabel = styled(Overline)`
  flex: 1;
  text-align: center;
`;

const StatLabel = styled(Overline)<{ isHome?: boolean }>`
  flex: 0 0 4em;
  text-align: ${({ isHome }) => (isHome ? 'left' : 'right')};
`;

type Props = {
  className?: string;
  label: string;
  home: {
    label?: string;
    value: number;
  };
  away: {
    label?: string;
    value: number;
  };
};

export const TwoSideVerticalBar: React.FC<Props> = ({ className, label, home, away }) => {
  const sum = home.value + away.value;
  const homeValue = sum ? Math.round((home.value / sum) * 100) : 0;
  const awayValue = sum ? Math.round((away.value / sum) * 100) : 0;

  return (
    <Flex className={className} flexDirection="column">
      <Flex justifyContent="space-between">
        <StatLabel as="div" isHome>
          {home.label || home.value}
        </StatLabel>
        <CenterLabel as="div">{label}</CenterLabel>
        <StatLabel as="div">{away.label || away.value}</StatLabel>
      </Flex>
      <Flex flexDirection="row" mt={1}>
        <BarContainer isHome>
          <Bar isHome width={homeValue} />
        </BarContainer>
        <BarContainer>
          <Bar width={awayValue} />
        </BarContainer>
      </Flex>
    </Flex>
  );
};
