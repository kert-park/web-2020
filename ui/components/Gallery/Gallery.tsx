import { Card } from '@kertPark/components/Atomic/Card';
import { AspectRatio, ImageAspectRatio } from '@kertPark/components/Atomic/ImageAspectRatio';
import { Markdown } from '@kertPark/components/Atomic/Markdown';
import { ImageModal } from '@kertPark/components/Gallery/ImageModal';
import { useGallery } from '@kertPark/components/Gallery/useGallery';
import { theme } from '@kertPark/config/theme';
import { GalleryFragment } from '@kertPark/graphql/generated';
import { Box, Flex } from '@rebass/grid';
import React, { useMemo } from 'react';
import styled from 'styled-components';

type Props = {
  gallery: GalleryFragment;
  text?: string;
};

const ImageWrapper = styled(Box)`
  width: 9.5rem;
`;

const Image = styled(ImageAspectRatio)`
  cursor: pointer;
  &:hover {
    opacity: 0.9;
    transition: opacity ${theme.transitionSpeed};
  }
`;

export const Gallery: React.FC<Props> = ({ gallery, text }) => {
  const images = useMemo(() => {
    return (gallery.attributes?.images.data || [])
      .filter((image) => Boolean(image.attributes?.name))
      .sort((a, b) => {
        return a.attributes!.name.localeCompare(b.attributes!.name);
      });
  }, [gallery.attributes?.images.data]);
  const { handleBack, handleClose, handleNext, handleOpen, url } = useGallery({ images });

  return (
    <>
      {text ? <Markdown text={text} /> : null}
      <Flex flexDirection="row" flexWrap="wrap" alignItems="center" justifyContent="center" m={-2}>
        {images.map((image) => {
          return (
            <ImageWrapper key={image.id} p={2} onClick={() => handleOpen(image)}>
              <Card isLink noPadding>
                <Image ratio={AspectRatio.oneToOne} src={image.attributes?.formats?.small.url} />
              </Card>
            </ImageWrapper>
          );
        })}
      </Flex>
      <ImageModal handleBack={handleBack} handleClose={handleClose} handleNext={handleNext} url={url} />
    </>
  );
};
