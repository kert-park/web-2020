import { UseGallery } from '@kertPark/components/Gallery/useGallery';
import { IconChevron } from '@kertPark/components/Icon/IconChevron';
import { IconClose } from '@kertPark/components/Icon/IconClose';
import { spaces, theme, zIndex as themeZIndex } from '@kertPark/config/theme';
import React from 'react';
import Modal, { Styles } from 'react-modal';
import styled from 'styled-components';

const zIndex = {
  close: themeZIndex.modal + 4,
  content: themeZIndex.modal + 1,
  controls: themeZIndex.modal + 3,
  image: themeZIndex.modal + 2,
  overlay: themeZIndex.modal,
};

const modalStyles: Styles = {
  overlay: {
    zIndex: zIndex.overlay,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    zIndex: zIndex.content,
    position: 'relative',
    padding: '1rem',
    inset: 'unset',
    border: 0,
    background: 'none',
  },
};

type Props = Omit<UseGallery, 'handleOpen'>;

const Img = styled.img`
  border-radius: ${theme.radius.card};
  max-width: 100%;
  max-height: calc(100vh - 2rem);
  z-index: ${zIndex.image};
`;

const ImageWrapper = styled.div`
  max-width: 100%;
  max-height: calc(100vh - 2rem);
`;

const ControlsWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

const Control = styled.div`
  position: absolute;
  bottom: 0;
  height: 100%;
  width: 30%;
  z-index: ${zIndex.controls};
  display: flex;
  align-items: center;
  cursor: pointer;
  opacity: 0;
  transition: opacity ${theme.transitionSpeed};

  &:hover {
    opacity: 1;
  }
`;

const ControlNext = styled(Control)`
  justify-content: flex-end;
  padding-right: ${spaces[4]};
  right: 0;
`;

const ControlBack = styled(Control)`
  padding-left: ${spaces[4]};
  left: 0;
`;

const ControlClose = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  z-index: ${zIndex.close};
  padding: ${spaces[4]};
  cursor: pointer;
  opacity: 0.75;
  transition: opacity ${theme.transitionSpeed};

  &:hover {
    opacity: 1;
  }
`;

const StyledIconClose = styled(IconClose)`
  filter: drop-shadow(0 0 0.25em ${theme.colors.boxShadow.hover});
`;

const StyledIconChevron = styled(IconChevron)`
  filter: drop-shadow(0 0 0.25em ${theme.colors.boxShadow.hover});
`;

Modal.setAppElement('#__next');

export const ImageModal: React.FC<Props> = ({ handleBack, handleClose, handleNext, url }) => {
  return (
    <Modal isOpen={!!url} onRequestClose={handleClose} style={modalStyles}>
      <ImageWrapper>
        {!!url ? (
          <>
            <ControlsWrapper>
              <ControlClose onClick={handleClose}>
                <StyledIconClose color={theme.colors.backgroundPrimary} size={16} />
              </ControlClose>
              <ControlBack onClick={handleBack}>
                <StyledIconChevron color={theme.colors.backgroundPrimary} rotate={180} size={32} />
              </ControlBack>
              <ControlNext onClick={handleNext}>
                <StyledIconChevron color={theme.colors.backgroundPrimary} size={32} />
              </ControlNext>
            </ControlsWrapper>
            <Img src={url} />
          </>
        ) : null}
      </ImageWrapper>
    </Modal>
  );
};
