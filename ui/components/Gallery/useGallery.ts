import { ImageFragment } from '@kertPark/graphql/generated';
import { useCallback, useEffect, useState } from 'react';

const getImageSrc = ({ attributes }: ImageFragment): string => {
  return (
    attributes?.formats?.large?.url ||
    attributes?.url ||
    attributes?.formats?.medium?.url ||
    attributes?.formats?.small?.url
  );
};

type Props = {
  images: Array<ImageFragment>;
};

export type UseGallery = {
  handleBack: () => void;
  handleClose: () => void;
  handleNext: () => void;
  handleOpen: (image: ImageFragment) => void;
  url?: string;
};

type HandleKeyboard = (event: KeyboardEvent) => void;

export const useGallery = ({ images }: Props): UseGallery => {
  const [image, setImage] = useState<ImageFragment | undefined>();

  const handleClose: UseGallery['handleClose'] = () => {
    setImage(undefined);
  };

  const handleOpen: UseGallery['handleOpen'] = (image) => {
    setImage(image);
  };

  const handleBack = () => {
    if (image) {
      const index = images.findIndex((item) => item.id === image.id);
      if (index > 0) {
        setImage(images[index - 1]);
      }
    }
  };

  const handleNext = () => {
    if (image) {
      const index = images.findIndex((item) => item.id === image.id);
      if (images.length > index + 1) {
        setImage(images[index + 1]);
      }
    }
  };

  const handleKeyboard: HandleKeyboard = useCallback(
    (event) => {
      switch (event.key) {
        case 'ArrowLeft': {
          handleBack();
          break;
        }
        case 'ArrowRight': {
          handleNext();
          break;
        }
      }
    },
    [handleBack, handleNext],
  );

  useEffect(() => {
    window.addEventListener('keydown', handleKeyboard);
    return () => window.removeEventListener('keydown', handleKeyboard);
  }, [handleKeyboard]);

  const url = image ? getImageSrc(image) : undefined;

  return {
    handleBack,
    handleClose,
    handleNext,
    handleOpen,
    url,
  };
};
