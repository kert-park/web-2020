import { Result } from '@kertPark/components/OgImage/Common/Result';
import { TeamLogo } from '@kertPark/components/OgImage/Common/TeamLogo';
import { GameProps } from '@kertPark/components/OgImage/types';
import { Container, Header, ResultContainer } from '@kertPark/components/OgImage/Web/Styles';
import React from 'react';

export const GameResult: React.FC<GameProps> = ({ game }) => {
  return (
    <Container>
      <Header>
        Výsledek
        {'\n'}
        zápasu
      </Header>
      <ResultContainer>
        <TeamLogo id={game.home.cmshbId!} />
        <Result
          awayScore={game.score.away.finalScore}
          homeScore={game.score.home.finalScore}
          status={game.status}
          themeType="red"
        />
        <TeamLogo id={game.away.cmshbId!} />
      </ResultContainer>
    </Container>
  );
};
