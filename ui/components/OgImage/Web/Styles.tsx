import { Flex, yellow, yellowTextShadow } from '@kertPark/components/OgImage/Common/Styles';
import React from 'react';

const APP_PATH = process.env.APP_PATH;

export const Container: ReactFCWithChildren = ({ children }) => {
  return (
    <Flex
      style={{
        height: '100%',
        width: '100%',
        flexDirection: 'column',
        flexWrap: 'nowrap',
        fontFamily: 'Plus Jakarta Sans',
        gap: '48px',
        backgroundImage: `url(${APP_PATH}/thumb-background.jpg)`,
        color: yellow,
      }}
    >
      {children}
    </Flex>
  );
};

export const Header: ReactFCWithChildren = ({ children }) => {
  return (
    <Flex
      style={{
        fontSize: '120px',
        fontStyle: 'italic',
        textShadow: `${yellowTextShadow}, 0px 4px 4px #000000`,
        textTransform: 'uppercase',
        transform: 'rotate(-5deg)',
        lineHeight: 1.1,
      }}
    >
      {children}
    </Flex>
  );
};
export const ResultContainer: ReactFCWithChildren = ({ children }) => {
  return (
    <Flex
      style={{
        flexDirection: 'row',
        gap: '32px',
      }}
    >
      {children}
    </Flex>
  );
};
