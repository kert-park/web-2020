import { TeamsWithDateAndPlace } from '@kertPark/components/OgImage/Common/TeamsWithDateAndPlace';
import { GameProps } from '@kertPark/components/OgImage/types';
import { Container, Header } from '@kertPark/components/OgImage/Web/Styles';
import React from 'react';

export const GamePreview: React.FC<GameProps> = ({ game }) => {
  return (
    <Container>
      <Header>Preview</Header>
      <TeamsWithDateAndPlace game={game} style={{ marginTop: '40px' }} themeType="red" />
    </Container>
  );
};
