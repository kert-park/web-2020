import { GameWithCmshbGameDetailType, TournamentType } from '@kertPark/graphql/generated/helpers';

export type GameProps = {
  game: GameWithCmshbGameDetailType;
};

export type GamesProps = {
  games: Array<GameWithCmshbGameDetailType>;
  hasATeam: boolean;
};

export type TeamProps = {
  team: GameWithCmshbGameDetailType['team'];
};

export type TournamentProps = {
  tournament: TournamentType;
};

export type TournamentsProps = {
  tournaments: Array<TournamentType>;
};

export type ThemeProps = {
  themeType: ThemeType;
};

export type GameResultType = 'after-first-period' | 'after-second-period' | string;

export type ThemeType = 'red' | 'white';

export type WhiteThemeGameProps = {
  game: GameWithCmshbGameDetailType;
  isATeam: boolean;
};

export type LineUpPlayer = {
  cmshbId?: string | null;
  name: string;
  number: number;
  smallImage?: string | null;
};

export type LineUpProps = {
  defenders: Array<LineUpPlayer>;
  forwards: Array<LineUpPlayer>;
  goalkeepers: Array<LineUpPlayer>;
  rowsSum: number;
};
