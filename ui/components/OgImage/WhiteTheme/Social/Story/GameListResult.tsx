import { Flex } from '@kertPark/components/OgImage/Common/Styles';
import { GamesProps, TournamentsProps } from '@kertPark/components/OgImage/types';
import { GameResult } from '@kertPark/components/OgImage/WhiteTheme/Social/GameList/GameResult';
import { Tournament } from '@kertPark/components/OgImage/WhiteTheme/Social/GameList/Tournament';
import { ContainerStory, Header } from '@kertPark/components/OgImage/WhiteTheme/Social/Styles';
import { mergeGamesAndTournamentsAndSort } from '@kertPark/lib/og-images/helpers';
import React from 'react';

export const GameListResult: React.FC<GamesProps & TournamentsProps> = ({ games, hasATeam, tournaments }) => {
  const gamesAndTournaments = mergeGamesAndTournamentsAndSort(games, tournaments);

  return (
    <ContainerStory isATeam={hasATeam}>
      <Header subTitle={hasATeam ? undefined : 'Mládeže'} size={140}>
        Výsledky
      </Header>
      <Flex
        style={{
          flexDirection: 'column',
          gap: '40px',
          marginTop: '120px',
          width: '720px',
        }}
      >
        {gamesAndTournaments.map((gameOrTournament) => {
          if (gameOrTournament.__typename === 'Tournament') {
            const tournament = gameOrTournament;
            return <Tournament key={tournament.id} tournament={tournament} />;
          } else {
            const game = gameOrTournament;
            return <GameResult key={game.id} game={game} />;
          }
        })}
      </Flex>
    </ContainerStory>
  );
};
