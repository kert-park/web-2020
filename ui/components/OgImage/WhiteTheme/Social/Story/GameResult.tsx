import { TeamGoals } from '@kertPark/components/OgImage/Common/Story/GameResult/TeamGoals';
import { TeamsAndResult } from '@kertPark/components/OgImage/Common/Story/GameResult/TeamsAndResult';
import { Flex } from '@kertPark/components/OgImage/Common/Styles';
import { GameResultType, WhiteThemeGameProps } from '@kertPark/components/OgImage/types';
import { ContainerStory, Header } from '@kertPark/components/OgImage/WhiteTheme/Social/Styles';
import { teamsObject } from '@kertPark/config/teams';
import React from 'react';

type Props = {
  type?: GameResultType;
} & WhiteThemeGameProps;

export const GameResult: React.FC<Props> = ({ game, isATeam, type }) => {
  const titleATeam = (() => {
    switch (type) {
      case 'after-first-period':
        return ['Stav po', '1. třetině'];
      case 'after-second-period':
        return ['Stav po', '2. třetině'];
      default:
        return ['Výsledek', 'zápasu'];
    }
  })();

  return (
    <ContainerStory isATeam={isATeam}>
      {isATeam ? (
        <Header size={160} hasRotate lineHeight={1.1}>
          {titleATeam.map((text, index) => (
            <span key={index}>{text}</span>
          ))}
        </Header>
      ) : (
        <Header
          size={140}
          style={{ marginTop: '140px' }}
          subTitle={teamsObject[game.team.systemName]?.nameGenitiv || ''}
        >
          Výsledek
        </Header>
      )}
      <Flex
        style={{
          width: '720px',
        }}
      >
        <TeamsAndResult game={game} type={type} style={{ marginTop: '60px' }} themeType="white" />
      </Flex>
      <Flex
        style={{
          flexDirection: 'row',
          alignItems: 'flex-start',
          width: '720px',
          marginTop: '80px',
        }}
      >
        <TeamGoals goals={game.goals?.home || []} isHome themeType="white" type={type} />
        <TeamGoals goals={game.goals?.away || []} themeType="white" type={type} />
      </Flex>
    </ContainerStory>
  );
};
