import { Flex } from '@kertPark/components/OgImage/Common/Styles';
import { LineUpPlayer } from '@kertPark/components/OgImage/types';
import { Player } from '@kertPark/components/OgImage/WhiteTheme/Social/Story/GameLineUp/Player';
import React from 'react';

type LineUpPostProps = {
  post: string;
  players: Array<LineUpPlayer>;
};

export const LineUpPost: React.FC<LineUpPostProps> = ({ post, players }) => {
  return (
    <Flex style={{ width: '100%', flexDirection: 'row', gap: '24px' }}>
      <Flex
        style={{
          width: '30px',
          fontSize: '20px',
          textTransform: 'uppercase',
          transform: 'rotate(-90deg)',
        }}
      >
        {post}
      </Flex>
      <Flex
        style={{
          width: '100%',
          justifyContent: 'flex-start',
          flexDirection: 'row',
          flexWrap: 'wrap',
          gap: '16px',
        }}
      >
        {players
          .sort((a, b) => a.name.localeCompare(b.name, 'cs-CZ'))
          .map((player, index) => (
            <Player key={index} {...player} />
          ))}
      </Flex>
    </Flex>
  );
};
