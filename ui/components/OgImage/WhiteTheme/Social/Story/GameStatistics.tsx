import { TeamsAndResult } from '@kertPark/components/OgImage/Common/Story/GameResult/TeamsAndResult';
import { Statistics } from '@kertPark/components/OgImage/Common/Story/GameStatistics/Statistics';
import { WhiteThemeGameProps } from '@kertPark/components/OgImage/types';
import { ContainerStory, Header } from '@kertPark/components/OgImage/WhiteTheme/Social/Styles';
import { teamsObject } from '@kertPark/config/teams';
import React from 'react';

export const GameStatistics: React.FC<WhiteThemeGameProps> = ({ game, isATeam }) => {
  return (
    <ContainerStory isATeam={isATeam}>
      {isATeam ? (
        <Header size={160} hasRotate lineHeight={1.1}>
          Statistiky
        </Header>
      ) : (
        <Header size={140} subTitle={teamsObject[game.team.systemName]?.nameGenitiv || ''}>
          Statistiky
        </Header>
      )}
      <TeamsAndResult game={game} style={{ width: '720px', marginTop: '60px' }} themeType="white" />
      <Statistics
        game={game}
        style={{
          width: '720px',
          marginTop: isATeam ? '80px' : '40px',
        }}
        themeType="white"
      />
    </ContainerStory>
  );
};
