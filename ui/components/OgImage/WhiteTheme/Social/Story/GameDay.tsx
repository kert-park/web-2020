import { DateAndPlace } from '@kertPark/components/OgImage/Common/DateAndPlace';
import { Teams } from '@kertPark/components/OgImage/Common/Teams';
import { WhiteThemeGameProps } from '@kertPark/components/OgImage/types';
import { ContainerStory, Header } from '@kertPark/components/OgImage/WhiteTheme/Social/Styles';
import { teamsObject } from '@kertPark/config/teams';
import React from 'react';

export const GameDay: React.FC<WhiteThemeGameProps> = ({ game, isATeam }) => {
  return (
    <ContainerStory isATeam={isATeam}>
      {isATeam ? (
        <Header size={180} hasRotate style={{ marginTop: '140px' }}>
          <span>Game</span>
          <span>Day</span>
        </Header>
      ) : (
        <Header
          size={140}
          style={{ marginTop: '140px' }}
          subTitle={teamsObject[game.team.systemName]?.nameGenitiv || ''}
        >
          Game Day
        </Header>
      )}
      <Teams game={game} style={{ marginTop: isATeam ? '180px' : '220px' }} />
      <DateAndPlace game={game} style={{ marginTop: isATeam ? '180px' : '80px' }} themeType="white" />
    </ContainerStory>
  );
};
