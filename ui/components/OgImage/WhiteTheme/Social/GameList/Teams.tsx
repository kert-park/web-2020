import { Flex } from '@kertPark/components/OgImage/Common/Styles';
import { TeamLogo } from '@kertPark/components/OgImage/Common/TeamLogo';
import { GameProps } from '@kertPark/components/OgImage/types';
import React from 'react';

export const Teams: React.FC<GameProps> = ({ game }) => {
  return (
    <Flex
      style={{
        flexDirection: 'row',
        gap: '10px',
      }}
    >
      <TeamLogo id={game.home.cmshbId!} size={80} />
      <TeamLogo id={game.away.cmshbId!} size={80} />
    </Flex>
  );
};
