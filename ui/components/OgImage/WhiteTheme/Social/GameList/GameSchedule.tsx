import { Flex } from '@kertPark/components/OgImage/Common/Styles';
import { GameProps } from '@kertPark/components/OgImage/types';
import { DayAndTime } from '@kertPark/components/OgImage/WhiteTheme/Social/GameList/DayAndTime';
import { GameOrTournamentInfo } from '@kertPark/components/OgImage/WhiteTheme/Social/GameList/GameOrTournamentInfo';
import { Teams } from '@kertPark/components/OgImage/WhiteTheme/Social/GameList/Teams';
import React from 'react';

export const GameSchedule: React.FC<GameProps> = ({ game }) => {
  const isKertHomeTeam = game.home.cmshbId === '3100003';

  return (
    <Flex
      style={{
        flexDirection: 'row',
        justifyContent: 'flex-start',
        gap: '20px',
        width: '720px',
      }}
    >
      <Teams game={game} />
      <GameOrTournamentInfo
        opponentOrTournamentName={isKertHomeTeam ? game.away.shortName : game.home.shortName}
        place={game.place || ''}
        teamName={game.team.name}
      />
      <DayAndTime date={game.date} withTime />
    </Flex>
  );
};
