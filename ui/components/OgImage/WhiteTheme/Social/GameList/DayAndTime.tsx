import { blue, blueTextShadow, Flex } from '@kertPark/components/OgImage/Common/Styles';
import { getDateObject, getReadableTime } from '@kertPark/lib/date';
// eslint-disable-next-line import/no-duplicates
import { format } from 'date-fns';
// eslint-disable-next-line import/no-duplicates
import { cs } from 'date-fns/locale';
import React from 'react';

type Props = {
  date: Date;
  withTime?: boolean;
};

export const DayAndTime: React.FC<Props> = ({ date, withTime = false }) => {
  const formattedDate = format(getDateObject(date), 'iiii', {
    locale: cs,
  });
  const day = `${formattedDate[0].toUpperCase()}${formattedDate.slice(1)}`;
  return (
    <Flex
      style={{
        color: blue,
        lineHeight: 1,
        flexDirection: 'column',
        width: '160px',
        justifyContent: 'center',
      }}
    >
      <Flex
        style={{
          fontSize: '28px',
          whiteSpace: 'nowrap',
          overflow: 'visible',
        }}
      >
        {day}
      </Flex>
      {withTime && (
        <Flex
          style={{
            fontSize: '48px',
            fontStyle: 'italic',
            whiteSpace: 'nowrap',
            overflow: 'visible',
            textShadow: `${blueTextShadow}`,
          }}
        >
          {getReadableTime(date)}
        </Flex>
      )}
    </Flex>
  );
};
