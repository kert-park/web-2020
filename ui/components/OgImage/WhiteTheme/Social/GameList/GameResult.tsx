import { Flex } from '@kertPark/components/OgImage/Common/Styles';
import { GameProps } from '@kertPark/components/OgImage/types';
import { GameOrTournamentInfo } from '@kertPark/components/OgImage/WhiteTheme/Social/GameList/GameOrTournamentInfo';
import { Result } from '@kertPark/components/OgImage/WhiteTheme/Social/GameList/Result';
import { Teams } from '@kertPark/components/OgImage/WhiteTheme/Social/GameList/Teams';
import React from 'react';

export const GameResult: React.FC<GameProps> = ({ game }) => {
  const isKertHomeTeam = game.home.cmshbId === '3100003';

  return (
    <Flex
      style={{
        flexDirection: 'row',
        justifyContent: 'flex-start',
        gap: '20px',
        width: '720px',
      }}
    >
      <Teams game={game} />
      <GameOrTournamentInfo
        opponentOrTournamentName={isKertHomeTeam ? game.away.shortName : game.home.shortName}
        place={game.place || ''}
        teamName={game.team.name}
      />
      <Result awayScore={game.score.away.finalScore} homeScore={game.score.home.finalScore} status={game.status} />
    </Flex>
  );
};
