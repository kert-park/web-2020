import { Flex } from '@kertPark/components/OgImage/Common/Styles';
import { TournamentProps } from '@kertPark/components/OgImage/types';
import { DayAndTime } from '@kertPark/components/OgImage/WhiteTheme/Social/GameList/DayAndTime';
import { GameOrTournamentInfo } from '@kertPark/components/OgImage/WhiteTheme/Social/GameList/GameOrTournamentInfo';
import { PlaceLogo } from '@kertPark/components/OgImage/WhiteTheme/Social/GameList/PlaceLogo';
import React from 'react';

type Props = TournamentProps & {
  withDayAndTime?: boolean;
};

export const Tournament: React.FC<Props> = ({ tournament, withDayAndTime = false }) => {
  return (
    <Flex
      style={{
        flexDirection: 'row',
        justifyContent: 'flex-start',
        gap: '20px',
        width: '720px',
      }}
    >
      <PlaceLogo tournament={tournament} />
      <GameOrTournamentInfo
        opponentOrTournamentName={tournament.place?.name || ''}
        place="Turnaj"
        teamName={tournament.team.name}
      />
      {withDayAndTime && <DayAndTime date={tournament.date} />}
    </Flex>
  );
};
