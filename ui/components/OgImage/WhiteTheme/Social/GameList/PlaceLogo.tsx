import { Flex } from '@kertPark/components/OgImage/Common/Styles';
import { TournamentProps } from '@kertPark/components/OgImage/types';
import { getPlaceSrc } from '@kertPark/lib/static';
import React from 'react';

export const PlaceLogo: React.FC<TournamentProps> = ({ tournament }) => {
  if (tournament.place) {
    const src = getPlaceSrc(tournament.place.systemName);
    return (
      <Flex
        style={{
          flexDirection: 'row',
          width: '170px',
          justifyContent: 'flex-end',
        }}
      >
        <img src={src} width={80} height={80} />
      </Flex>
    );
  }

  return null;
};
