import { blue, blueTextShadow, Flex } from '@kertPark/components/OgImage/Common/Styles';
import { GameProps } from '@kertPark/components/OgImage/types';
import { GameStatus } from '@kertPark/lib/game';
import React from 'react';

type Props = {
  awayScore: GameProps['game']['score']['away']['finalScore'];
  homeScore: GameProps['game']['score']['home']['finalScore'];
} & Pick<GameProps['game'], 'status'>;
export const Result: React.FC<Props> = ({ awayScore, homeScore, status: propsStatus }) => {
  const score = `${homeScore}:${awayScore}`;
  const status = [GameStatus.p, GameStatus.pp, GameStatus.ss].includes(propsStatus as GameStatus)
    ? propsStatus
    : undefined;

  return (
    <Flex
      style={{
        flexDirection: 'row',
        color: blue,
        fontSize: '60px',
        fontStyle: 'italic',
        lineHeight: 1,
        textShadow: blueTextShadow,
        alignItems: 'flex-end',
        gap: '4px',
        whiteSpace: 'nowrap',
        overflow: 'visible',
        width: '160px',
      }}
    >
      {score}
      {status && (
        <Flex
          style={{
            color: blue,
            fontSize: '24px',
            textTransform: 'uppercase',
            textShadow: 'none',
            marginBottom: '6px',
          }}
        >
          {status}
        </Flex>
      )}
    </Flex>
  );
};
