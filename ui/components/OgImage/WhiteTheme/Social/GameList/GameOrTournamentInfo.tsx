import { blue, Flex } from '@kertPark/components/OgImage/Common/Styles';
import { theme } from '@kertPark/config/theme';
import React from 'react';

type Props = {
  opponentOrTournamentName: string;
  place: string;
  teamName: string;
};

export const GameOrTournamentInfo: React.FC<Props> = ({ opponentOrTournamentName, place, teamName }) => {
  return (
    <Flex
      style={{
        color: blue,
        lineHeight: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        width: '350px',
      }}
    >
      <Flex
        style={{
          flexDirection: 'row',
          gap: '8px',
        }}
      >
        <Flex
          style={{
            color: theme.colors.white,
            fontSize: '20px',
            backgroundColor: blue,
            padding: '8px',
            borderRadius: '8px',
            textTransform: 'uppercase',
          }}
        >
          {teamName}
        </Flex>
        <Flex
          style={{
            fontSize: '20px',
          }}
        >
          {place}
        </Flex>
      </Flex>
      <Flex
        style={{
          fontSize: '40px',
          fontStyle: 'italic',
          whiteSpace: 'nowrap',
          overflow: 'visible',
        }}
      >
        {opponentOrTournamentName}
      </Flex>
    </Flex>
  );
};
