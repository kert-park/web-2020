import { Footer } from '@kertPark/components/OgImage/Common/Footer';
import { blue, blueTextShadow, Flex, FlexProps } from '@kertPark/components/OgImage/Common/Styles';
import { theme } from '@kertPark/config/theme';
import React from 'react';

const APP_PATH = process.env.APP_PATH;

type Props = {
  isATeam: boolean;
};

export const ContainerPost: ReactFCWithChildren<Props> = ({ children, isATeam }) => {
  return (
    <Flex
      style={{
        height: '100%',
        width: '100%',
        flexDirection: 'column',
        flexWrap: 'nowrap',
        fontFamily: 'Plus Jakarta Sans',
        backgroundImage: `url(${APP_PATH}/social/${isATeam ? 'background-white.png' : 'background-white-2.png'})`,
        backgroundPosition: '0 -580px',
        color: blue,
      }}
    >
      <Flex style={{ flex: 1, width: '100%', padding: '80px', flexDirection: 'column', justifyContent: 'flex-start' }}>
        {children}
      </Flex>
      <Flex style={{ flex: '0 0 100px', padding: '20px 40px 40px', justifyContent: 'flex-start' }}>
        <Footer isATeam={isATeam} themeType="white" />
      </Flex>
    </Flex>
  );
};

export const ContainerStory: ReactFCWithChildren<Props> = ({ children, isATeam }) => {
  return (
    <Flex
      style={{
        height: '100%',
        width: '100%',
        flexDirection: 'column',
        flexWrap: 'nowrap',
        fontFamily: 'Plus Jakarta Sans',
        backgroundImage: `url(${APP_PATH}/social/${isATeam ? 'background-white.png' : 'background-white-2.png'})`,
        color: blue,
      }}
    >
      <Flex
        style={{
          flex: 1,
          width: '100%',
          padding: '240px 80px 80px',
          flexDirection: 'column',
          justifyContent: 'flex-start',
        }}
      >
        {children}
      </Flex>
      <Flex style={{ flex: '0 0 240px', padding: '20px 40px 40px', justifyContent: 'flex-start' }}>
        <Footer isATeam={isATeam} themeType="white" />
      </Flex>
    </Flex>
  );
};

type HeaderProps = {
  hasRotate?: boolean;
  lineHeight?: 1 | 1.1;
  size: 128 | 140 | 160 | 180;
  subTitle?: string;
};

export const Header: ReactFCWithChildren<HeaderProps & FlexProps> = ({
  children,
  hasRotate,
  lineHeight = 1,
  size,
  style = {},
  subTitle,
}) => {
  const header = (
    <Flex
      style={{
        color: blue,
        fontSize: `${size}px`,
        fontStyle: 'italic',
        textShadow: `${blueTextShadow}`,
        textTransform: 'uppercase',
        lineHeight,
        ...(hasRotate ? { transform: 'rotate(-5deg)' } : {}),
        ...style,
      }}
    >
      {children}
    </Flex>
  );

  if (!subTitle) {
    return header;
  }

  return (
    <Flex
      style={{
        flexDirection: 'column',
        gap: '-10px',
      }}
    >
      {header}
      <Flex
        style={{
          color: theme.colors.white,
          fontSize: '48px',
          textTransform: 'uppercase',
          backgroundColor: theme.colors.kert.redSecondary,
          padding: '12px 16px 4px',
          borderRadius: '8px',
        }}
      >
        {subTitle}
      </Flex>
    </Flex>
  );
};
