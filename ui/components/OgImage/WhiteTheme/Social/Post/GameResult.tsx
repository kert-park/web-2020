import { Result } from '@kertPark/components/OgImage/Common/Result';
import { Teams } from '@kertPark/components/OgImage/Common/Teams';
import { WhiteThemeGameProps } from '@kertPark/components/OgImage/types';
import { ContainerPost, Header } from '@kertPark/components/OgImage/WhiteTheme/Social/Styles';
import { teamsObject } from '@kertPark/config/teams';
import React from 'react';

export const GameResult: React.FC<WhiteThemeGameProps> = ({ game, isATeam }) => {
  return (
    <ContainerPost isATeam={isATeam}>
      {isATeam ? (
        <Header size={128} hasRotate lineHeight={1.1}>
          <span>Výsledek</span>
          <span>zápasu</span>
        </Header>
      ) : (
        <Header size={140} subTitle={teamsObject[game.team.systemName]?.nameGenitiv || ''}>
          Výsledek
        </Header>
      )}
      <Teams game={game} style={{ marginTop: isATeam ? '100px' : '120px' }} />
      <Result
        awayScore={game.score.away.finalScore}
        homeScore={game.score.home.finalScore}
        status={game.status}
        style={{ marginTop: '60px' }}
        themeType="white"
      />
    </ContainerPost>
  );
};
