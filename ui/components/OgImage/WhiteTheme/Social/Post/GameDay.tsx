import { DateAndPlace } from '@kertPark/components/OgImage/Common/DateAndPlace';
import { Teams } from '@kertPark/components/OgImage/Common/Teams';
import { WhiteThemeGameProps } from '@kertPark/components/OgImage/types';
import { ContainerPost, Header } from '@kertPark/components/OgImage/WhiteTheme/Social/Styles';
import { teamsObject } from '@kertPark/config/teams';
import React from 'react';

export const GameDay: React.FC<WhiteThemeGameProps> = ({ game, isATeam }) => {
  return (
    <ContainerPost isATeam={isATeam}>
      {isATeam ? (
        <Header hasRotate size={160}>
          <span>Game</span>
          <span>Day</span>
        </Header>
      ) : (
        <Header size={140} subTitle={teamsObject[game.team.systemName]?.nameGenitiv || ''}>
          Game Day
        </Header>
      )}
      <Teams game={game} style={{ marginTop: isATeam ? '90px' : '120px' }} />
      <DateAndPlace game={game} style={{ marginTop: '80px' }} themeType="white" />
    </ContainerPost>
  );
};
