import { Flex, FlexProps } from '@kertPark/components/OgImage/Common/Styles';
import { Team } from '@kertPark/components/OgImage/Common/Team';
import { GameProps } from '@kertPark/components/OgImage/types';
import React from 'react';

export const Teams: React.FC<GameProps & FlexProps> = ({ game, style = {} }) => {
  return (
    <Flex style={{ flexDirection: 'row', gap: '32px', ...style }}>
      <Team team={game.home} withName />
      <Flex style={{ fontSize: '32px', fontStyle: 'italic' }}>vs</Flex>
      <Team team={game.away} withName />
    </Flex>
  );
};
