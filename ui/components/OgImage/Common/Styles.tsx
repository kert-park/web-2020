import { theme } from '@kertPark/config/theme';
import React, { CSSProperties } from 'react';

export const yellow = theme.colors.kert.yellowPrimary;
export const red = theme.colors.kert.redPrimary;
export const blue = theme.colors.kert.bluePrimary;
export const yellowTextShadow = `1px 1px 0px ${yellow}, -1px 1px 0px ${yellow}, 1px -1px 0px ${yellow}, -1px -1px 0px ${yellow}, 0px 1px 0px ${yellow}, 1px 0px 0px ${yellow}, 0px -1px 0px ${yellow}, 0px -1px 0px ${yellow}`;
export const redTextShadow = `1px 1px 0px ${red}, -1px 1px 0px ${red}, 1px -1px 0px ${red}, -1px -1px 0px ${red}, 0px 1px 0px ${red}, 1px 0px 0px ${red}, 0px -1px 0px ${red}, 0px -1px 0px ${red}`;
export const blueTextShadow = `1px 1px 0px ${blue}, -1px 1px 0px ${blue}, 1px -1px 0px ${blue}, -1px -1px 0px ${blue}, 0px 1px 0px ${blue}, 1px 0px 0px ${blue}, 0px -1px 0px ${blue}, 0px -1px 0px ${blue}`;

export type FlexProps = {
  style?: CSSProperties | undefined;
};

export const Flex: ReactFCWithChildren<FlexProps> = ({ children, style = {} }) => {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        ...style,
      }}
    >
      {children}
    </div>
  );
};
