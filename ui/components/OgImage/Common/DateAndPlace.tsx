import { blueTextShadow, Flex, FlexProps, yellowTextShadow } from '@kertPark/components/OgImage/Common/Styles';
import { GameProps, ThemeProps } from '@kertPark/components/OgImage/types';
import { getDateObject, getReadableTime } from '@kertPark/lib/date';
// eslint-disable-next-line import/no-duplicates
import { format } from 'date-fns';
// eslint-disable-next-line import/no-duplicates
import { cs } from 'date-fns/locale';
import React from 'react';

export const DateAndPlace: React.FC<GameProps & FlexProps & ThemeProps> = ({ game, style = {}, themeType }) => {
  const formattedDate = format(getDateObject(game.date), 'iiii d. MMMM', {
    locale: cs,
  });
  const date = `${formattedDate[0].toUpperCase()}${formattedDate.slice(1)}`;
  return (
    <Flex style={{ lineHeight: 1, ...style }}>
      <Flex
        style={{
          fontSize: '28px',
        }}
      >
        {date}
      </Flex>
      <Flex
        style={{
          fontSize: '80px',
          fontStyle: 'italic',
          textTransform: 'uppercase',
          textShadow: themeType === 'red' ? `${yellowTextShadow}, 0px 2px 10px rgba(0, 0, 0, 0.5)` : blueTextShadow,
        }}
      >
        {getReadableTime(game.date)}
      </Flex>
      <Flex
        style={{
          fontSize: '32px',
        }}
      >
        {game.place}
      </Flex>
    </Flex>
  );
};
