import { blueTextShadow, Flex, FlexProps, yellowTextShadow } from '@kertPark/components/OgImage/Common/Styles';
import { GameProps, ThemeProps } from '@kertPark/components/OgImage/types';
import React from 'react';

const getStatus = (status: string | null | undefined) => {
  switch (status) {
    case 'pp':
      return 'Po prodloužení';
    case 'ss':
      return 'Po sam. střílení';
    default:
      return undefined;
  }
};

type Props = {
  awayScore: GameProps['game']['score']['away']['finalScore'];
  homeScore: GameProps['game']['score']['home']['finalScore'];
  size?: 96 | 120;
} & Pick<GameProps['game'], 'status'> &
  FlexProps &
  ThemeProps;

export const Result: React.FC<Props> = ({
  awayScore,
  homeScore,
  size = 96,
  status: propsStatus,
  style = {},
  themeType,
}) => {
  const score = `${homeScore}:${awayScore}`;
  const status = getStatus(propsStatus);

  return (
    <Flex style={style}>
      <Flex
        style={{
          fontSize: `${size}px`,
          fontStyle: 'italic',
          alignItems: 'baseline',
          textShadow: themeType === 'red' ? `${yellowTextShadow}, 0px 2px 10px rgba(0, 0, 0, 0.5)` : blueTextShadow,
        }}
      >
        {score}
      </Flex>
      {status && (
        <Flex
          style={{
            fontSize: '24px',
            fontStyle: 'italic',
            textTransform: 'uppercase',
          }}
        >
          {status}
        </Flex>
      )}
    </Flex>
  );
};
