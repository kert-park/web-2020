import { Flex } from '@kertPark/components/OgImage/Common/Styles';
import { ThemeProps } from '@kertPark/components/OgImage/types';
import { theme } from '@kertPark/config/theme';
import { getTeamLogoSrc } from '@kertPark/lib/static';
import React from 'react';

const APP_PATH = process.env.APP_PATH;

type Item = {
  color: string;
  text: Array<string>;
};

const Item: ReactFCWithChildren<Item> = ({ children, color, text }) => {
  return (
    <Flex style={{ flexDirection: 'row', gap: '8px' }}>
      {children}
      <Flex
        style={{
          color,
          fontSize: '16px',
          textTransform: 'uppercase',
          flexDirection: 'column',
          alignItems: 'flex-start',
          lineHeight: 0.9,
        }}
      >
        {text.map((value) => (
          <span key={value}>{value}</span>
        ))}
      </Flex>
    </Flex>
  );
};

type FooterProps = {
  isATeam: boolean;
};

export const Footer: React.FC<FooterProps & ThemeProps> = ({ isATeam, themeType }) => {
  const { primaryColor, secondaryColor } = (() => {
    switch (themeType) {
      case 'red':
        return {
          primaryColor: theme.colors.kert.yellowPrimary,
          secondaryColor: theme.colors.white,
        };
      default: {
        return {
          primaryColor: theme.colors.kert.bluePrimary,
          secondaryColor: theme.colors.kert.bluePrimary,
        };
      }
    }
  })();

  return (
    <Flex style={{ flexDirection: 'row', gap: '30px' }}>
      {isATeam && (
        <Item color={secondaryColor} text={['Crossdock', 'extraliga']}>
          <img src={`${APP_PATH}/extraliga.png`} width={40} height={40} />
        </Item>
      )}
      <Item color={primaryColor} text={['HC Kert', 'Park Praha']}>
        <img src={getTeamLogoSrc(3100003)} width={40} height={40} />
      </Item>
      {isATeam && (
        <Item color={secondaryColor} text={['Mistr extraligy', '2017 - 2024']}>
          <img src={`${APP_PATH}/extraliga-pohar.png`} width={18} height={40} />
        </Item>
      )}
    </Flex>
  );
};
