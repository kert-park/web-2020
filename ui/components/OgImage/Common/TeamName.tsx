import { Flex } from '@kertPark/components/OgImage/Common/Styles';
import React from 'react';

export const TeamName: React.FC<{ name: string }> = ({ name }) => {
  return (
    <Flex
      style={{
        fontSize: '20px',
        textTransform: 'uppercase',
        whiteSpace: 'nowrap',
        overflow: 'visible',
      }}
    >
      {name}
    </Flex>
  );
};
