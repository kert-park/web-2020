import { Flex } from '@kertPark/components/OgImage/Common/Styles';
import { TeamLogo } from '@kertPark/components/OgImage/Common/TeamLogo';
import { TeamName } from '@kertPark/components/OgImage/Common/TeamName';
import { GameWithCmshbGameDetailType } from '@kertPark/graphql/generated/helpers';
import React from 'react';

export type TeamProps = {
  team: GameWithCmshbGameDetailType['home'];
  withName?: boolean;
};

export const Team: React.FC<TeamProps> = ({ team, withName = false }) => {
  if (withName) {
    return (
      <Flex style={{ flexDirection: 'column', gap: '10px', width: '180px' }}>
        <TeamLogo id={team.cmshbId!} />
        <TeamName name={team.shortName} />
      </Flex>
    );
  }
  return <TeamLogo id={team.cmshbId!} />;
};
