import { Flex } from '@kertPark/components/OgImage/Common/Styles';
import { GameProps, GameResultType, ThemeProps } from '@kertPark/components/OgImage/types';
import { theme } from '@kertPark/config/theme';
import { getReadableMinuteFromTimeInSeconds } from '@kertPark/lib/date';
import { getShortName } from '@kertPark/lib/player';
import React from 'react';

type GoalTypeProps = {
  goalType: string | null | undefined;
};
const GoalType: React.FC<GoalTypeProps & ThemeProps> = ({ goalType, themeType }) => {
  if (goalType && goalType !== 'EQ') {
    const { color, backgroundColor } = (() => {
      switch (themeType) {
        case 'red':
          return {
            color: theme.colors.kert.bluePrimary,
            backgroundColor: theme.colors.kert.yellowPrimary,
          };
        default: {
          return {
            color: theme.colors.white,
            backgroundColor: theme.colors.kert.bluePrimary,
          };
        }
      }
    })();

    return (
      <Flex
        style={{
          color,
          backgroundColor,
          borderRadius: '4px',
          padding: '2px 4px',
          fontSize: '22px',
        }}
      >
        {goalType}
      </Flex>
    );
  }

  return null;
};

type Props = {
  goals: NonNullable<GameProps['game']['goals']>['home'];
  isHome?: boolean;
  type?: GameResultType;
} & ThemeProps;

export const TeamGoals: React.FC<Props> = ({ goals, isHome = false, themeType, type }) => {
  const filteredGoals = goals.filter(({ timeInSeconds }) => {
    switch (type) {
      case 'after-first-period':
        return timeInSeconds <= 15 * 60;
      case 'after-second-period':
        return timeInSeconds <= 30 * 60;
      default:
        return true;
    }
  });

  return (
    <Flex
      style={{
        flex: 1,
        flexDirection: 'column',
        alignItems: isHome ? 'flex-start' : 'flex-end',
        gap: '16px',
      }}
    >
      {filteredGoals.map(({ timeInSeconds, goal, type: goalType }, index) => {
        const time = getReadableMinuteFromTimeInSeconds(timeInSeconds);
        const name = getShortName(goal.name);
        return (
          <Flex
            key={index}
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              fontSize: '30px',
              gap: '8px',
            }}
          >
            {time} {name}
            <GoalType goalType={goalType} themeType={themeType} />
          </Flex>
        );
      })}
    </Flex>
  );
};
