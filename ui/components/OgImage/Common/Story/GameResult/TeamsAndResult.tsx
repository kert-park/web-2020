import { Periods } from '@kertPark/components/OgImage/Common/Periods';
import { Result } from '@kertPark/components/OgImage/Common/Result';
import { Flex, FlexProps } from '@kertPark/components/OgImage/Common/Styles';
import { Team } from '@kertPark/components/OgImage/Common/Team';
import { GameProps, GameResultType, ThemeProps } from '@kertPark/components/OgImage/types';
import React from 'react';

type Props = {
  type?: GameResultType;
} & GameProps &
  FlexProps &
  ThemeProps;

export const TeamsAndResult: React.FC<Props> = ({ game, style = {}, themeType, type }) => {
  const { awayScore, homeScore, status } = (() => {
    switch (type) {
      case 'after-first-period':
        return {
          awayScore: game.score.away.firstPeriod,
          homeScore: game.score.home.firstPeriod,
          status: undefined,
        };
      case 'after-second-period':
        return {
          awayScore: (game.score.away.firstPeriod || 0) + (game.score.away.secondPeriod || 0),
          homeScore: (game.score.home.firstPeriod || 0) + (game.score.home.secondPeriod || 0),
          status: undefined,
        };
      default: {
        return {
          awayScore: game.score.away.finalScore,
          homeScore: game.score.home.finalScore,
          status: game.status,
        };
      }
    }
  })();
  return (
    <Flex
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        width: '100%',
        ...style,
      }}
    >
      <Team team={game.home} withName />
      <Flex
        style={{
          flexDirection: 'column',
        }}
      >
        <Result awayScore={awayScore} homeScore={homeScore} size={120} status={status} themeType={themeType} />
        <Periods game={game} type={type} />
      </Flex>
      <Team team={game.away} withName />
    </Flex>
  );
};
