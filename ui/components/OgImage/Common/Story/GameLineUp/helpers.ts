import { GameProps, LineUpProps } from '@kertPark/components/OgImage/types';
import { isPlayer } from '@kertPark/lib/player';

const getRowSum = (length: number) => {
  return Math.ceil(length / 4);
};

export const getLineUp = ({ game }: GameProps): LineUpProps => {
  const isHomeTeam = game.home.cmshbId === '3100003';
  const goalkeepers = game.goalkeepers?.[isHomeTeam ? 'home' : 'away'];
  const players = game.players?.[isHomeTeam ? 'home' : 'away'];

  if (goalkeepers && players) {
    const { defenders, forwards } = players.reduce(
      (acc, player) => {
        const isDefender = player.post === 'O';
        return {
          defenders: isDefender ? [...acc.defenders, player] : acc.defenders,
          forwards: !isDefender ? [...acc.forwards, player] : acc.forwards,
        };
      },
      { defenders: [], forwards: [] },
    );
    return {
      defenders: defenders.map((player) => ({
        cmshbId: player.player.cmshbId,
        name: player.player.name,
        number: player.number!,
        smallImage: isPlayer(player.player) ? player.player.smallImage : undefined,
      })),
      forwards: forwards.map((player) => ({
        cmshbId: player.player.cmshbId,
        name: player.player.name,
        number: player.number!,
        smallImage: isPlayer(player.player) ? player.player.smallImage : undefined,
      })),
      goalkeepers: goalkeepers.map((goalkeeper) => ({
        cmshbId: goalkeeper.player.cmshbId,
        name: goalkeeper.player.name,
        number: goalkeeper.number!,
        smallImage: isPlayer(goalkeeper.player) ? goalkeeper.player.smallImage : undefined,
      })),
      rowsSum: getRowSum(defenders.length) + getRowSum(forwards.length) + getRowSum(goalkeepers.length),
    };
  }

  return { defenders: [], forwards: [], goalkeepers: [], rowsSum: 0 };
};
