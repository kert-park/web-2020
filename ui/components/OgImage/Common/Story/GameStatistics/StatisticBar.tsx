import { Flex } from '@kertPark/components/OgImage/Common/Styles';
import { ThemeProps } from '@kertPark/components/OgImage/types';
import { theme } from '@kertPark/config/theme';
import React from 'react';

const StatLabel = ({ label, isHome = false }: { label: string | number; isHome?: boolean }) => {
  return (
    <Flex
      style={{
        flex: '0 0 80px',
        alignItems: isHome ? 'flex-start' : 'flex-end',
      }}
    >
      {label}
    </Flex>
  );
};

const BarContainer: ReactFCWithChildren<ThemeProps> = ({ children, themeType }) => {
  return (
    <Flex
      style={{
        flex: 1,
        width: '100%',
        height: '12px',
        backgroundColor: themeType === 'red' ? theme.colors.boxShadow.hover : theme.colors.boxShadow.normal,
        borderRadius: '4px',
      }}
    >
      {children}
    </Flex>
  );
};

const Bar = ({
  isHome = false,
  isKertHomeTeam = false,
  themeType,
  width,
}: {
  width: number;
  isHome?: boolean;
  isKertHomeTeam?: boolean;
} & ThemeProps) => {
  const { backgroundColor } = (() => {
    switch (themeType) {
      case 'red':
        return {
          backgroundColor: isKertHomeTeam ? theme.colors.kert.yellowPrimary : theme.colors.kert.redSecondary,
        };
      default: {
        return {
          backgroundColor: isKertHomeTeam ? theme.colors.kert.bluePrimary : theme.colors.kert.redSecondary,
        };
      }
    }
  })();
  return (
    <div
      style={{
        position: 'absolute',
        top: 0,
        bottom: 0,
        ...(isHome ? { right: 0 } : { left: 0 }),
        width: `${width}%`,
        height: '100%',
        backgroundColor,
        borderRadius: '4px',
      }}
    />
  );
};

type Props = {
  label: string;
  home: {
    label?: string;
    value: number;
  };
  away: {
    label?: string;
    value: number;
  };
  isKertHomeTeam?: boolean;
} & ThemeProps;

export const StatisticBar: React.FC<Props> = ({ label, home, away, isKertHomeTeam, themeType }) => {
  const sum = home.value + away.value;
  const homeValue = sum ? Math.round((home.value / sum) * 100) : 0;
  const awayValue = sum ? Math.round((away.value / sum) * 100) : 0;

  return (
    <Flex
      style={{
        flexDirection: 'column',
        alignItems: 'flex-start',
        width: '100%',
        fontSize: '24px',
        textTransform: 'uppercase',
        gap: '8px',
      }}
    >
      <Flex
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          width: '100%',
        }}
      >
        <StatLabel label={home.label || home.value} isHome />
        <Flex style={{ flex: 1 }}>{label}</Flex>
        <StatLabel label={away.label || away.value} />
      </Flex>
      <Flex
        style={{
          flexDirection: 'row',
          width: '100%',
          gap: '8px',
        }}
      >
        <BarContainer themeType={themeType}>
          <Bar isHome isKertHomeTeam={isKertHomeTeam} width={homeValue} themeType={themeType} />
        </BarContainer>
        <BarContainer themeType={themeType}>
          <Bar isKertHomeTeam={!isKertHomeTeam} width={awayValue} themeType={themeType} />
        </BarContainer>
      </Flex>
    </Flex>
  );
};
