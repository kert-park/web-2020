import { StatisticBar } from '@kertPark/components/OgImage/Common/Story/GameStatistics/StatisticBar';
import { Flex, FlexProps } from '@kertPark/components/OgImage/Common/Styles';
import { GameProps, ThemeProps } from '@kertPark/components/OgImage/types';
import { getGameStatistics } from '@kertPark/lib/gameDetail';
import React from 'react';

export const Statistics: React.FC<GameProps & FlexProps & ThemeProps> = ({ game, style = {}, themeType }) => {
  const {
    homeGoalsEn,
    awayGoalsEn,
    homeShots,
    awayShots,
    homeShotsPercentage,
    awayShotsPercentage,
    homeSaves,
    awaySaves,
    homeSavesPercentage,
    awaySavesPercentage,
    homePowerPlays,
    awayPowerPlays,
    homePowerPlayGoals,
    awayPowerPlayGoals,
    homePowerPlaysPercentage,
    awayPowerPlaysPercentage,
    homePenaltyKillsPercentage,
    awayPenaltyKillsPercentage,
  } = getGameStatistics(game);
  const isHomeTeam = game.home.cmshbId === '3100003';

  return (
    <Flex
      style={{
        flexDirection: 'column',
        gap: '32px',
        ...style,
      }}
    >
      <StatisticBar
        label="Střely"
        home={{ value: homeShots }}
        away={{ value: awayShots }}
        isKertHomeTeam={isHomeTeam}
        themeType={themeType}
      />
      <StatisticBar
        label="Úspěšnost střel"
        home={{ value: homeShotsPercentage, label: `${homeShotsPercentage}%` }}
        away={{ value: awayShotsPercentage, label: `${awayShotsPercentage}%` }}
        isKertHomeTeam={isHomeTeam}
        themeType={themeType}
      />
      <StatisticBar
        label="Zákroky brankařů"
        home={{ value: homeSaves }}
        away={{ value: awaySaves }}
        isKertHomeTeam={isHomeTeam}
        themeType={themeType}
      />
      <StatisticBar
        label="Úspěšnost zákroků"
        home={{ value: homeSavesPercentage, label: `${homeSavesPercentage}%` }}
        away={{ value: awaySavesPercentage, label: `${awaySavesPercentage}%` }}
        isKertHomeTeam={isHomeTeam}
        themeType={themeType}
      />
      <StatisticBar
        label="Vyloučení"
        home={{ value: game.statistics?.home.penalties || 0 }}
        away={{ value: game.statistics?.away.penalties || 0 }}
        isKertHomeTeam={isHomeTeam}
        themeType={themeType}
      />
      <StatisticBar
        label="Přesilovky"
        home={{ value: homePowerPlays }}
        away={{ value: awayPowerPlays }}
        isKertHomeTeam={isHomeTeam}
        themeType={themeType}
      />
      <StatisticBar
        label="Góly v přesilovce"
        home={{ value: homePowerPlayGoals }}
        away={{ value: awayPowerPlayGoals }}
        isKertHomeTeam={isHomeTeam}
        themeType={themeType}
      />
      <StatisticBar
        label="Góly v oslabení"
        home={{ value: game.statistics?.home.penaltyKillGoals || 0 }}
        away={{ value: game.statistics?.away.penaltyKillGoals || 0 }}
        isKertHomeTeam={isHomeTeam}
        themeType={themeType}
      />
      <StatisticBar
        label="Využití přesilovek"
        home={{ value: homePowerPlaysPercentage, label: `${homePowerPlaysPercentage}%` }}
        away={{ value: awayPowerPlaysPercentage, label: `${awayPowerPlaysPercentage}%` }}
        isKertHomeTeam={isHomeTeam}
        themeType={themeType}
      />
      <StatisticBar
        label="Úspěšnost oslabení"
        home={{ value: homePenaltyKillsPercentage, label: `${homePenaltyKillsPercentage}%` }}
        away={{ value: awayPenaltyKillsPercentage, label: `${awayPenaltyKillsPercentage}%` }}
        isKertHomeTeam={isHomeTeam}
        themeType={themeType}
      />
      <StatisticBar
        label="Góly do prázdné"
        home={{ value: homeGoalsEn }}
        away={{ value: awayGoalsEn }}
        isKertHomeTeam={isHomeTeam}
        themeType={themeType}
      />
    </Flex>
  );
};
