import { getTeamLogoSrc } from '@kertPark/lib/static';
import React from 'react';

type TeamLogoProps = {
  id: string;
  size?: 80 | 150;
};

export const TeamLogo: React.FC<TeamLogoProps> = ({ id, size = 150 }) => {
  const src = getTeamLogoSrc(id);
  return <img src={src} width={size} height={size} />;
};
