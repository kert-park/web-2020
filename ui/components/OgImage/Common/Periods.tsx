import { Flex } from '@kertPark/components/OgImage/Common/Styles';
import { GameProps, GameResultType } from '@kertPark/components/OgImage/types';
import { GameStatus } from '@kertPark/lib/game';
import React from 'react';

type Props = {
  type?: GameResultType;
} & GameProps;

export const Periods: React.FC<Props> = ({ game, type }) => {
  const periods = (() => {
    const { away, home } = game.score;
    const firstPeriod = `${home.firstPeriod}:${away.firstPeriod}`;
    const secondPeriod = `${home.secondPeriod}:${away.secondPeriod}`;
    const thirdPeriod = `${home.thirdPeriod}:${away.thirdPeriod}`;
    const overtime = `${home.overtime}:${away.overtime}`;
    const emptyPeriod = '-:-';

    switch (type) {
      case 'after-first-period':
        return [firstPeriod, emptyPeriod, emptyPeriod].join(',');
      case 'after-second-period':
        return [firstPeriod, secondPeriod, emptyPeriod].join(',');
      default:
        const allPeriods = [firstPeriod, secondPeriod, thirdPeriod].join(',');
        const hasOvertime = [GameStatus.p, GameStatus.pp, GameStatus.ss].includes(game.status as GameStatus);
        return `${allPeriods}${hasOvertime ? ` - ${overtime}` : ''}`;
    }
  })();

  return (
    <Flex
      style={{
        fontSize: '24px',
        fontStyle: 'italic',
        textTransform: 'uppercase',
      }}
    >
      ({periods})
    </Flex>
  );
};
