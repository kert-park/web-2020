import { DateAndPlace } from '@kertPark/components/OgImage/Common/DateAndPlace';
import { Flex, FlexProps } from '@kertPark/components/OgImage/Common/Styles';
import { Team } from '@kertPark/components/OgImage/Common/Team';
import { GameProps, ThemeProps } from '@kertPark/components/OgImage/types';
import React from 'react';

export const TeamsWithDateAndPlace: React.FC<GameProps & FlexProps & ThemeProps> = ({
  game,
  style = {},
  themeType,
}) => {
  return (
    <Flex style={{ flexDirection: 'row', alignItems: 'flex-end', gap: '32px', ...style }}>
      <Team team={game.home} withName />
      <DateAndPlace game={game} themeType={themeType} />
      <Team team={game.away} withName />
    </Flex>
  );
};
