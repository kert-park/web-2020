import { DateAndPlace } from '@kertPark/components/OgImage/Common/DateAndPlace';
import { Teams } from '@kertPark/components/OgImage/Common/Teams';
import { ContainerStory, Header } from '@kertPark/components/OgImage/RedTheme/Social/Styles';
import { GameProps } from '@kertPark/components/OgImage/types';
import React from 'react';

export const GameDay: React.FC<GameProps> = ({ game }) => {
  return (
    <ContainerStory>
      <Header size={180} lineHeight={1} style={{ marginTop: '140px' }}>
        <span>Game</span>
        <span>Day</span>
      </Header>
      <Teams game={game} style={{ marginTop: '180px' }} />
      <DateAndPlace game={game} style={{ marginTop: '180px' }} themeType="red" />
    </ContainerStory>
  );
};
