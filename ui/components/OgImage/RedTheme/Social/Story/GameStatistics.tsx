import { TeamsAndResult } from '@kertPark/components/OgImage/Common/Story/GameResult/TeamsAndResult';
import { Statistics } from '@kertPark/components/OgImage/Common/Story/GameStatistics/Statistics';
import { ContainerStory, Header } from '@kertPark/components/OgImage/RedTheme/Social/Styles';
import { GameProps } from '@kertPark/components/OgImage/types';
import React from 'react';

export const GameStatistics: React.FC<GameProps> = ({ game }) => {
  return (
    <ContainerStory>
      <Header size={160} lineHeight={1.1}>
        Statistiky
      </Header>
      <TeamsAndResult game={game} style={{ width: '720px', marginTop: '60px' }} themeType="red" />
      <Statistics
        game={game}
        style={{
          width: '720px',
          marginTop: '80px',
        }}
        themeType="red"
      />
    </ContainerStory>
  );
};
