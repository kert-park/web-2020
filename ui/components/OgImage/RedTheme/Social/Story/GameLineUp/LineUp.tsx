import { Flex, FlexProps } from '@kertPark/components/OgImage/Common/Styles';
import { LineUpPost } from '@kertPark/components/OgImage/RedTheme/Social/Story/GameLineUp/LineUpPost';
import { LineUpProps } from '@kertPark/components/OgImage/types';
import React from 'react';

export const LineUp: React.FC<LineUpProps & FlexProps> = ({
  defenders,
  forwards,
  goalkeepers,
  rowsSum,
  style = {},
}) => {
  if (rowsSum > 0) {
    return (
      <Flex
        style={{
          flexDirection: 'column',
          gap: '36px',
          width: '720px',
          height: '1000px',
          justifyContent: 'flex-start',
          alignItems: 'flex-start',
          ...style,
        }}
      >
        <LineUpPost post="Brankaři" players={goalkeepers} />
        <LineUpPost post="Obránci" players={defenders} />
        <LineUpPost post="Útočníci" players={forwards} />
      </Flex>
    );
  }

  return null;
};
