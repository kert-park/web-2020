import { getLineUp } from '@kertPark/components/OgImage/Common/Story/GameLineUp/helpers';
import { TeamsWithDateAndPlace } from '@kertPark/components/OgImage/Common/TeamsWithDateAndPlace';
import { LineUp } from '@kertPark/components/OgImage/RedTheme/Social/Story/GameLineUp/LineUp';
import { ContainerStory, Header } from '@kertPark/components/OgImage/RedTheme/Social/Styles';
import { GameProps } from '@kertPark/components/OgImage/types';
import React from 'react';

export const GameLineUp: React.FC<GameProps> = ({ game }) => {
  const { defenders, forwards, goalkeepers, rowsSum } = getLineUp({ game });
  return (
    <ContainerStory>
      <Header size={180} lineHeight={1}>
        <span>Sestava</span>
      </Header>
      {rowsSum < 7 && <TeamsWithDateAndPlace game={game} style={{ marginTop: '40px' }} themeType="red" />}
      <LineUp
        defenders={defenders}
        forwards={forwards}
        goalkeepers={goalkeepers}
        rowsSum={rowsSum}
        style={{ marginTop: '40px' }}
      />
    </ContainerStory>
  );
};
