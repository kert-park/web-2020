import { Flex } from '@kertPark/components/OgImage/Common/Styles';
import { LineUpPlayer } from '@kertPark/components/OgImage/types';
import { theme } from '@kertPark/config/theme';
import { getShortName } from '@kertPark/lib/player';
import React from 'react';

export const Player: React.FC<LineUpPlayer> = ({ name, number, smallImage }) => {
  const shortName = getShortName(name);

  return (
    <Flex
      style={{
        width: '160px',
        height: '140px',
        borderRadius: '16px',
        backgroundColor: theme.colors.boxShadow.hover,
        ...(smallImage ? { backgroundImage: `url(${smallImage})` } : {}),
        backgroundPosition: '5px -10px',
        backgroundSize: '150px 150px',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
      }}
    >
      <Flex
        style={{
          padding: '2px 4px',
          color: theme.colors.kert.yellowPrimary,
          fontSize: '32px',
          textShadow: `0 4px 4px ${theme.colors.boxShadow.cookieBar}`,
        }}
      >
        {number}
      </Flex>
      <Flex
        style={{
          width: '100%',
          backgroundColor: theme.colors.kert.yellowPrimary,
          padding: '0 4px',
          borderRadius: '0 0 16px 16px',
          color: theme.colors.kert.bluePrimary,
          fontSize: '20px',
        }}
      >
        {shortName}
      </Flex>
    </Flex>
  );
};
