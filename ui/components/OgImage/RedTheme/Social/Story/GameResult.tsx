import { TeamGoals } from '@kertPark/components/OgImage/Common/Story/GameResult/TeamGoals';
import { TeamsAndResult } from '@kertPark/components/OgImage/Common/Story/GameResult/TeamsAndResult';
import { Flex } from '@kertPark/components/OgImage/Common/Styles';
import { ContainerStory, Header } from '@kertPark/components/OgImage/RedTheme/Social/Styles';
import { GameProps, GameResultType } from '@kertPark/components/OgImage/types';
import React from 'react';

type Props = {
  type?: GameResultType;
} & GameProps;

export const GameResult: React.FC<Props> = ({ game, type }) => {
  const title = (() => {
    switch (type) {
      case 'after-first-period':
        return ['Stav po', '1. třetině'];
      case 'after-second-period':
        return ['Stav po', '2. třetině'];
      default:
        return ['Výsledek', 'zápasu'];
    }
  })();
  return (
    <ContainerStory>
      <Header size={160} lineHeight={1.1}>
        {title.map((text, index) => (
          <span key={index}>{text}</span>
        ))}
      </Header>
      <Flex
        style={{
          width: '720px',
        }}
      >
        <TeamsAndResult game={game} type={type} style={{ marginTop: '60px' }} themeType="red" />
      </Flex>
      <Flex
        style={{
          flexDirection: 'row',
          alignItems: 'flex-start',
          width: '720px',
          marginTop: '80px',
        }}
      >
        <TeamGoals goals={game.goals?.home || []} isHome themeType="red" type={type} />
        <TeamGoals goals={game.goals?.away || []} themeType="red" type={type} />
      </Flex>
    </ContainerStory>
  );
};
