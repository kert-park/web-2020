import { Result } from '@kertPark/components/OgImage/Common/Result';
import { Teams } from '@kertPark/components/OgImage/Common/Teams';
import { ContainerPost, Header } from '@kertPark/components/OgImage/RedTheme/Social/Styles';
import { GameProps } from '@kertPark/components/OgImage/types';
import React from 'react';

export const GameResult: React.FC<GameProps> = ({ game }) => {
  return (
    <ContainerPost>
      <Header size={128} lineHeight={1.1}>
        <span>Výsledek</span>
        <span>zápasu</span>
      </Header>
      <Teams game={game} style={{ marginTop: '100px' }} />
      <Result
        awayScore={game.score.away.finalScore}
        homeScore={game.score.home.finalScore}
        status={game.status}
        style={{ marginTop: '60px' }}
        themeType="red"
      />
    </ContainerPost>
  );
};
