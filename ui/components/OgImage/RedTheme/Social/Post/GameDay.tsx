import { DateAndPlace } from '@kertPark/components/OgImage/Common/DateAndPlace';
import { Teams } from '@kertPark/components/OgImage/Common/Teams';
import { ContainerPost, Header } from '@kertPark/components/OgImage/RedTheme/Social/Styles';
import { GameProps } from '@kertPark/components/OgImage/types';
import React from 'react';

export const GameDay: React.FC<GameProps> = ({ game }) => {
  return (
    <ContainerPost>
      <Header size={160} lineHeight={1}>
        <span>Game</span>
        <span>Day</span>
      </Header>
      <Teams game={game} style={{ marginTop: '90px' }} />
      <DateAndPlace game={game} style={{ marginTop: '80px' }} themeType="red" />
    </ContainerPost>
  );
};
