import { Footer } from '@kertPark/components/OgImage/Common/Footer';
import { Flex, FlexProps, yellow, yellowTextShadow } from '@kertPark/components/OgImage/Common/Styles';
import React from 'react';

const APP_PATH = process.env.APP_PATH;

export const ContainerPost: ReactFCWithChildren = ({ children }) => {
  return (
    <Flex
      style={{
        height: '100%',
        width: '100%',
        flexDirection: 'column',
        flexWrap: 'nowrap',
        fontFamily: 'Plus Jakarta Sans',
        backgroundImage: `url(${APP_PATH}/social/background-red-2.png)`,
        backgroundPosition: '0 -620px',
        color: yellow,
      }}
    >
      <Flex style={{ flex: 1, padding: '80px', flexDirection: 'column', justifyContent: 'flex-start' }}>
        {children}
      </Flex>
      <Flex style={{ flex: '0 0 100px', padding: '20px 40px 40px', justifyContent: 'flex-start' }}>
        <Footer isATeam themeType="red" />
      </Flex>
    </Flex>
  );
};

export const ContainerStory: ReactFCWithChildren = ({ children }) => {
  return (
    <Flex
      style={{
        height: '100%',
        width: '100%',
        flexDirection: 'column',
        flexWrap: 'nowrap',
        fontFamily: 'Plus Jakarta Sans',
        backgroundImage: `url(${APP_PATH}/social/background-red.png)`,
        color: yellow,
      }}
    >
      <Flex style={{ flex: 1, padding: '240px 80px 80px', flexDirection: 'column', justifyContent: 'flex-start' }}>
        {children}
      </Flex>
      <Flex style={{ flex: '0 0 240px', padding: '20px 40px 40px', justifyContent: 'flex-start' }}>
        <Footer isATeam themeType="red" />
      </Flex>
    </Flex>
  );
};

type HeaderProps = {
  lineHeight: 1 | 1.1;
  size: 128 | 160 | 180;
};

export const Header: ReactFCWithChildren<HeaderProps & FlexProps> = ({ children, lineHeight, size, style = {} }) => {
  return (
    <Flex
      style={{
        fontSize: `${size}px`,
        fontStyle: 'italic',
        textShadow: `${yellowTextShadow}, 0px 4px 4px #000000`,
        textTransform: 'uppercase',
        transform: 'rotate(-5deg)',
        lineHeight,
        ...style,
      }}
    >
      {children}
    </Flex>
  );
};
