import { Table, TBody, Td, Th, THead, Tr } from '@kertPark/components/Atomic/Table';
import { routes } from '@kertPark/config/routes';
import { Team } from '@kertPark/config/teams';
import { Player } from '@kertPark/graphql/generated';
import { getPlayerDate, PlayerStickLabelMap } from '@kertPark/lib/player';
import Router from 'next/router';
import React from 'react';

type Props = {
  players: Array<Player>;
  team?: Team;
};

type TableRowProps = {
  player: Player;
  team?: Team;
};

const TableRow: React.FC<TableRowProps> = ({ player, team }) => {
  const { birth, year } = getPlayerDate({ player });
  const hasLink = !!team?.hasGames && !!team.id;

  const onClick = () => {
    if (hasLink) {
      void Router.push({ pathname: routes.team.player, query: { teamId: team!.id, playerId: player.id } });
    }
  };

  return (
    <Tr withLink={hasLink} onClick={onClick}>
      <Td>{player.number || '-'}</Td>
      <Td align="left" hoverUnderline={hasLink}>
        {player.name}
      </Td>
      <Td desktopOnly>{birth || '-'}</Td>
      <Td>{year || '-'}</Td>
      <Td>{player.height || '-'}</Td>
      <Td>{player.weight || '-'}</Td>
      <Td desktopOnly>{player.stick ? PlayerStickLabelMap[player.stick] : '-'}</Td>
    </Tr>
  );
};

export const RosterTableLayout: React.FC<Props> = ({ players, team }) => {
  return (
    <Table>
      <THead>
        <Tr>
          <Th title="Číslo dresu" width="2.5rem">
            #
          </Th>
          <Th align="left">Jméno</Th>
          <Th desktopOnly>Narozen</Th>
          <Th>Věk</Th>
          <Th>Výška</Th>
          <Th>Váha</Th>
          <Th desktopOnly>Hůl</Th>
        </Tr>
      </THead>
      <TBody>
        {players.map((player) => (
          <TableRow key={player.id} player={player} team={team} />
        ))}
      </TBody>
    </Table>
  );
};
