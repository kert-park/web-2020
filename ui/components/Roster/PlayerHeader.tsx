import { Attribute, AttributeRow, AttributeType } from '@kertPark/components/Atomic/Attribute';
import { ContentLoader, Rect } from '@kertPark/components/Atomic/ContentLoader';
import { ImageAspectRatio } from '@kertPark/components/Atomic/ImageAspectRatio';
import { H1, Overline } from '@kertPark/components/Atomic/Typography';
import { getThumbImageUrl, ImageSize } from '@kertPark/config/images';
import { mediaBreakpoints, theme } from '@kertPark/config/theme';
import { PlayerQuery } from '@kertPark/graphql/generated';
import { getPlayerDate, PlayerPostLabelMap, PlayerStickLabelMap } from '@kertPark/lib/player';
import { Flex } from '@rebass/grid';
import React from 'react';
import styled from 'styled-components';

const Image = styled(ImageAspectRatio)`
  flex: 0 0 auto;
  width: 100%;

  @media (${mediaBreakpoints.lg}) {
    width: 300px;
    height: 400px;
  }

  @media (${mediaBreakpoints.xl}) {
    width: 400px;
    height: 400px;
  }

  @media (${mediaBreakpoints.xxl}) {
    width: 500px;
    height: 400px;
  }
`;

const Number = styled.div`
  color: ${({ theme }) => theme.colors.textPrimary};
  font-family: ${theme.fonts.header};
  font-weight: bold;
  font-size: 3.5rem;

  @media (${mediaBreakpoints.md}) {
    font-size: 4.5rem;
  }
`;

const Post = styled(Overline)`
  font-size: 1rem;

  @media (${mediaBreakpoints.md}) {
    font-size: 1.25rem;
  }
`;

const getAttr = (title: string, data: string | number | null | undefined, measure?: string): Array<AttributeType> => {
  if (!data) {
    return [];
  }
  return [{ title, data: `${data}${measure ? ` ${measure}` : ''}` }];
};

type Props = {
  player: NonNullable<PlayerQuery['player']>;
};

export const PlayerHeader = ({ player }: Props) => {
  const { birth, year } = getPlayerDate({ player });

  const attributes: Array<AttributeType> = [
    ...getAttr('Narozen', birth),
    ...getAttr('Věk', year, 'let'),
    ...getAttr('Výška', player.height, 'cm'),
    ...getAttr('Váha', player.weight, 'kg'),
    ...getAttr('Hůl', player.stick ? PlayerStickLabelMap[player.stick] : undefined),
  ];
  const attributesData = attributes.reduce<Array<Array<AttributeType>>>((acc, _, index) => {
    if (index % 2 === 0) {
      acc.push(attributes.slice(index, index + 2));
    }
    return acc;
  }, []);

  return (
    <Flex width={1} flexDirection={['column', 'column', 'row']}>
      <Flex flex="0 0 auto" p={2}>
        <Image src={player.largeImage || getThumbImageUrl({ size: ImageSize.medium })} />
      </Flex>
      <Flex width={1} p={2} flexDirection="column" justifyContent="space-between">
        <Flex
          width={1}
          flexDirection={['row', 'row', 'column']}
          justifyContent={['center', 'center', 'flex-start']}
          alignItems={['center', 'center', 'flex-start']}
        >
          <Flex marginRight={1}>
            <Number>{player.number}</Number>
          </Flex>
          <Flex flexDirection="column">
            <H1 margin="0">{player.name}</H1>
            <Post>{player.post ? PlayerPostLabelMap[player.post] : null}</Post>
          </Flex>
        </Flex>
        <Flex
          flexDirection={['row', 'row', 'column']}
          alignItems={['center', 'center', 'flex-start']}
          justifyContent={['center', 'center', 'flex-start']}
          flexWrap="wrap"
        >
          {attributesData.map((data, index) => (
            <AttributeRow key={index}>
              {data.map(({ title, data }, attrIndex) => (
                <Attribute key={attrIndex} data={data} title={title} />
              ))}
            </AttributeRow>
          ))}
        </Flex>
      </Flex>
    </Flex>
  );
};

export const PlayerHeaderLoader = () => {
  return (
    <ContentLoader viewBox="0 0 240 320" style={{ maxWidth: 240 }}>
      <Rect x="0" y="10" width="72" height="60" />
      <Rect x="0" y="80" width="240" height="40" />
      <Rect x="0" y="130" width="120" height="20" />
      <Rect x="0" y="180" width="60" height="12" />
      <Rect x="0" y="200" width="120" height="20" />
      <Rect x="130" y="180" width="60" height="12" />
      <Rect x="130" y="200" width="120" height="20" />
      <Rect x="0" y="230" width="60" height="12" />
      <Rect x="0" y="250" width="120" height="20" />
      <Rect x="130" y="230" width="60" height="12" />
      <Rect x="130" y="250" width="120" height="20" />
      <Rect x="0" y="280" width="60" height="12" />
      <Rect x="0" y="300" width="120" height="20" />
    </ContentLoader>
  );
};
