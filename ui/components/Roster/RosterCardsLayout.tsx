import { RosterListItem } from '@kertPark/components/Roster/RosterListItem';
import { Team } from '@kertPark/config/teams';
import { Player } from '@kertPark/graphql/generated';
import { Box } from '@rebass/grid';
import React from 'react';

type Props = {
  players: Array<Player>;
  team?: Team;
};

export const RosterCardsLayout: React.FC<Props> = ({ players, team }) => {
  return (
    <>
      {players.map((player) => (
        <Box key={player.id} width={[1, 1 / 2, 1 / 2, 1 / 3]} p={2}>
          <RosterListItem player={player} team={team} />
        </Box>
      ))}
    </>
  );
};
