import { GoalkeeperSeasonStatistics } from '@kertPark/components/Roster/PlayerSeasonStatistics/GoalkeeperSeasonStatistics';
import { PlayerSeasonStatistics } from '@kertPark/components/Roster/PlayerSeasonStatistics/PlayerSeasonStatistics';
import { EnumComponentplayerteamplayerPost, Player, useCmshbPlayerStatsListQuery } from '@kertPark/graphql/generated';
import { TeamProps } from '@kertPark/lib/team';
import React from 'react';

type Props = TeamProps & {
  player: Player;
};

export const PlayerStatistics: React.FC<Props> = ({ player, team }) => {
  const isGoalkeeper = player.post === EnumComponentplayerteamplayerPost.Brankar;
  const { data, loading } = useCmshbPlayerStatsListQuery({
    variables: { teamId: team?.id || '' },
  });

  return (
    <>
      {isGoalkeeper ? (
        <GoalkeeperSeasonStatistics data={data?.cmshbPlayerStatsList || []} loading={loading} playerId={player.id} />
      ) : (
        <PlayerSeasonStatistics data={data?.cmshbPlayerStatsList || []} loading={loading} playerId={player.id} />
      )}
    </>
  );
};
