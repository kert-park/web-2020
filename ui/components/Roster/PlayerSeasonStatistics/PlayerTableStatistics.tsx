import { Table, TBody, Td, Th, THead, Tr } from '@kertPark/components/Atomic/Table';
import { PlayerStatsType } from '@kertPark/components/Roster/PlayerSeasonStatistics/types';
import { calculatePointsAverage } from '@kertPark/components/Statistics/usePlayersList';
import React from 'react';

type Props = {
  stats: PlayerStatsType;
};

export const PlayerTableStatistics: React.FC<Props> = ({ stats }) => {
  return (
    <Table>
      <THead>
        <Tr>
          <Th align="left">Soutěž</Th>
          <Th title="Kanadské body" width="3rem">
            KB
          </Th>
          <Th title="Góly" width="3rem">
            G
          </Th>
          <Th title="Asistence" width="3rem">
            A
          </Th>
          <Th title="Zápasy" width="3rem">
            Z
          </Th>
          <Th desktopOnly title="Průměr kanadských bodů na zápas" width="4rem">
            ∅
          </Th>
          <Th desktopOnly title="Góly v přesilovce" width="3rem">
            PPG
          </Th>
          <Th desktopOnly title="Góly v oslabení" width="3rem">
            SHG
          </Th>
          <Th title="Trestné minuty" width="3rem">
            TM
          </Th>
        </Tr>
      </THead>
      <TBody>
        {stats.map(({ leagueGroup, stat }) => (
          <Tr key={leagueGroup.id}>
            <Td align="left">{leagueGroup.name}</Td>
            <Td>{stat.points || '-'}</Td>
            <Td>{stat.goals || '-'}</Td>
            <Td>{stat.assists || '-'}</Td>
            <Td>{stat.games || '-'}</Td>
            <Td desktopOnly>{calculatePointsAverage(stat) || '-'}</Td>
            <Td desktopOnly>{stat.powerPlayGoals || '-'}</Td>
            <Td desktopOnly>{stat.penaltyKillGoals || '-'}</Td>
            <Td>{stat.penalties || '-'}</Td>
          </Tr>
        ))}
      </TBody>
    </Table>
  );
};
