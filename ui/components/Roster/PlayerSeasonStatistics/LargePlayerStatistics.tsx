import { Card } from '@kertPark/components/Atomic/Card';
import { Overline } from '@kertPark/components/Atomic/Typography';
import { AttributeStyled } from '@kertPark/components/Roster/PlayerSeasonStatistics/Styles';
import { PlayerStatType } from '@kertPark/components/Roster/PlayerSeasonStatistics/types';
import { calculatePointsAverage } from '@kertPark/components/Statistics/usePlayersList';
import { Flex } from '@rebass/grid';
import React, { useContext } from 'react';
import { ThemeContext } from 'styled-components';

type Props = PlayerStatType;

export const LargePlayerStatistics: React.FC<Props> = ({ leagueGroup, stat }) => {
  const kertTheme = useContext(ThemeContext);

  return (
    <Card>
      <Overline color={kertTheme.colors.secondary}>{leagueGroup.name}</Overline>
      <Flex flexDirection="row" flexWrap="wrap" justifyContent="space-between">
        <AttributeStyled title="KB" label="Kanadské body" data={stat.points || '-'} />
        <AttributeStyled title="G" label="Góly" data={stat.goals || '-'} />
        <AttributeStyled title="A" label="Asistence" data={stat.assists || '-'} />
        <AttributeStyled title="Z" label="Zápasy" data={stat.games || '-'} />
        <AttributeStyled title="∅" label="Průměr kanadských bodů na zápas" data={calculatePointsAverage(stat) || '-'} />
        <AttributeStyled title="PPG" label="Góly v přesilovce" data={stat.powerPlayGoals || '-'} />
        <AttributeStyled title="SHG" label="Góly v oslabení" data={stat.penaltyKillGoals || '-'} />
        <AttributeStyled title="TM" label="Trestné minuty" data={stat.penalties || '-'} />
      </Flex>
    </Card>
  );
};
