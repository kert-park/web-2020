import { Card } from '@kertPark/components/Atomic/Card';
import { H3 } from '@kertPark/components/Atomic/Typography';
import { LargePlayerStatistics } from '@kertPark/components/Roster/PlayerSeasonStatistics/LargePlayerStatistics';
import { PlayerTableStatistics } from '@kertPark/components/Roster/PlayerSeasonStatistics/PlayerTableStatistics';
import { LargeStatisticsLoader } from '@kertPark/components/Roster/PlayerSeasonStatistics/Styles';
import { StatisticsProps } from '@kertPark/components/Roster/PlayerSeasonStatistics/types';
import { getPlayersData } from '@kertPark/components/Roster/PlayerSeasonStatistics/utils';
import { spaces, theme } from '@kertPark/config/theme';
import { Box, Flex } from '@rebass/grid';
import React from 'react';

export const PlayerSeasonStatistics: React.FC<StatisticsProps> = ({ data, loading, playerId }) => {
  const { stats, total } = getPlayersData({ data, playerId });

  if (loading) {
    return (
      <Box width={1} p={2}>
        <Card>
          <LargeStatisticsLoader />
        </Card>
      </Box>
    );
  }

  if (stats.length > 0) {
    const mainStat = stats[0];
    const hasMoreStats = stats.length > 1;

    return (
      <>
        <H3
          margin="0"
          marginTop={`calc(${spaces[2]} + ${theme.spacing.card})`}
          marginLeft={`calc(${spaces[2]} + ${theme.spacing.card})`}
        >
          Statistiky v sezoně
        </H3>
        <Flex width={1} flexDirection="row" flexWrap="wrap" justifyContent="space-between" alignItems="center">
          <Box width={[1, 1, 1, 1 / 2]} p={2}>
            <LargePlayerStatistics leagueGroup={mainStat.leagueGroup} stat={mainStat.stat} />
          </Box>
          {hasMoreStats && (
            <Box width={[1, 1, 1, 1 / 2]} p={2}>
              <LargePlayerStatistics leagueGroup={total.leagueGroup} stat={total.stat} />
            </Box>
          )}
        </Flex>

        {hasMoreStats ? (
          <Box width={1} p={2}>
            <Card>
              <PlayerTableStatistics stats={stats} />
            </Card>
          </Box>
        ) : null}
      </>
    );
  }

  return null;
};
