import { Attribute } from '@kertPark/components/Atomic/Attribute';
import { ContentLoader, Rect } from '@kertPark/components/Atomic/ContentLoader';
import { mediaBreakpoints, spaces } from '@kertPark/config/theme';
import React from 'react';
import styled from 'styled-components';

export const AttributeStyled = styled(Attribute)`
  &&& {
    flex-direction: column-reverse;
    margin-top: ${spaces[2]};

    @media (${mediaBreakpoints.lg}) {
      align-items: center;
    }
  }
`;

export const LargeStatisticsLoader: React.FC = () => {
  const w = 340;
  const h = 70;
  return (
    <ContentLoader viewBox={`0 0 ${w} ${h}`} style={{ maxWidth: w, width: '100%' }}>
      <Rect x={0} y="0" width="120" height="12" />
      <Rect x={22} y="30" width="16" height="26" />
      <Rect x={0} y="60" width="60" height="10" />
      <Rect x={92} y="30" width="16" height="26" />
      <Rect x={70} y="60" width="60" height="10" />
      <Rect x={162} y="30" width="16" height="26" />
      <Rect x={140} y="60" width="60" height="10" />
      <Rect x={232} y="30" width="16" height="26" />
      <Rect x={210} y="60" width="60" height="10" />
      <Rect x={302} y="30" width="16" height="26" />
      <Rect x={280} y="60" width="60" height="10" />
    </ContentLoader>
  );
};
