import {
  GoalkeeperStatsType,
  GoalkeeperStatType,
  PlayerStatsType,
  PlayerStatType,
  StatisticsProps,
} from '@kertPark/components/Roster/PlayerSeasonStatistics/types';

export const getGoalkeeperData = ({
  data,
  playerId,
}: Omit<StatisticsProps, 'loading'>): {
  stats: GoalkeeperStatsType;
  total: GoalkeeperStatType;
} => {
  const stats = data.reduce((acc, curr) => {
    const stat = curr.goalkeepers.find((player) => player.player.id === playerId);
    if (stat && stat.games) {
      return [
        ...acc,
        {
          leagueGroup: curr.leagueGroup,
          stat,
        },
      ];
    }
    return acc;
  }, []);

  const total = stats.reduce<GoalkeeperStatType['stat']>(
    (acc, curr) => ({
      assists: (acc.assists || 0) + (curr.stat.assists || 0),
      games: (acc.games || 0) + (curr.stat.games || 0),
      goalsAgainst: (acc.goalsAgainst || 0) + (curr.stat.goalsAgainst || 0),
      goalsAgainstAverage: 0,
      minutes: (acc.minutes || 0) + (curr.stat.minutes || 0),
      penalties: (acc.penalties || 0) + (curr.stat.penalties || 0),
      saves: (acc.saves || 0) + (curr.stat.saves || 0),
      savesAverage: 0,
      shotsAgainst: (acc.shotsAgainst || 0) + (curr.stat.shotsAgainst || 0),
      shutouts: (acc.shutouts || 0) + (curr.stat.shutouts || 0),
    }),
    {
      assists: 0,
      games: 0,
      goalsAgainst: 0,
      goalsAgainstAverage: 0,
      minutes: 0,
      penalties: 0,
      saves: 0,
      savesAverage: 0,
      shotsAgainst: 0,
      shutouts: 0,
    },
  );

  const totalSavesAverage =
    Math.round((100 - ((total.goalsAgainst || 0) / (total.shotsAgainst || 1)) * 100) * 100) / 100;
  const totalGoalsAgainstAverageV2 =
    Math.round(
      stats.reduce<number>((acc, curr) => {
        return acc + (curr.stat.goalsAgainstAverage || 0) * ((curr.stat.games || 0) / (total.games || 1));
      }, 0) * 100,
    ) / 100;

  return {
    stats,
    total: {
      leagueGroup: {
        id: 'total',
        name: 'Celkem v sezoně',
      },
      stat: { ...total, goalsAgainstAverage: totalGoalsAgainstAverageV2, savesAverage: totalSavesAverage || 0 },
    },
  };
};

export const getPlayersData = ({
  data,
  playerId,
}: Omit<StatisticsProps, 'loading'>): {
  stats: PlayerStatsType;
  total: PlayerStatType;
} => {
  const stats = data.reduce((acc, curr) => {
    const stat = curr.players.find((player) => player.player.id === playerId);
    if (stat && stat.games) {
      return [
        ...acc,
        {
          leagueGroup: curr.leagueGroup,
          stat,
        },
      ];
    }
    return acc;
  }, []);

  const total = stats.reduce<PlayerStatType['stat']>(
    (acc, curr) => ({
      assists: (acc.assists || 0) + (curr.stat.assists || 0),
      games: (acc.games || 0) + (curr.stat.games || 0),
      goals: (acc.goals || 0) + (curr.stat.goals || 0),
      penalties: (acc.penalties || 0) + (curr.stat.penalties || 0),
      points: (acc.points || 0) + (curr.stat.points || 0),
      powerPlayGoals: (acc.powerPlayGoals || 0) + (curr.stat.powerPlayGoals || 0),
      penaltyKillGoals: (acc.penaltyKillGoals || 0) + (curr.stat.penaltyKillGoals || 0),
    }),
    {
      assists: 0,
      games: 0,
      goals: 0,
      penalties: 0,
      points: 0,
      powerPlayGoals: 0,
      penaltyKillGoals: 0,
    },
  );

  return {
    stats,
    total: {
      leagueGroup: {
        id: 'total',
        name: 'Celkem v sezoně',
      },
      stat: total,
    },
  };
};
