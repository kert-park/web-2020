import { Card } from '@kertPark/components/Atomic/Card';
import { Overline } from '@kertPark/components/Atomic/Typography';
import { AttributeStyled } from '@kertPark/components/Roster/PlayerSeasonStatistics/Styles';
import { GoalkeeperStatType } from '@kertPark/components/Roster/PlayerSeasonStatistics/types';
import { Flex } from '@rebass/grid';
import React, { useContext } from 'react';
import { ThemeContext } from 'styled-components';

type Props = GoalkeeperStatType;

export const LargeGoalkeeperStatistics: React.FC<Props> = ({ leagueGroup, stat }) => {
  const kertTheme = useContext(ThemeContext);

  return (
    <Card>
      <Overline color={kertTheme.colors.secondary}>{leagueGroup.name}</Overline>
      <Flex flexDirection="row" flexWrap="wrap" justifyContent="space-between">
        <AttributeStyled title="Úspěšnost" label="Procentuální úspěšnost zákroků" data={`${stat.savesAverage} %`} />
        <AttributeStyled title="Z" label="Zápasy" data={stat.games || '-'} />
        <AttributeStyled title="Min" label="Odehrané minuty" data={stat.minutes || '-'} />
        <AttributeStyled title="Stř." label="Střely proti" data={stat.shotsAgainst || '-'} />
        <AttributeStyled title="G" label="Inkasované góly" data={stat.goalsAgainst || '-'} />
        <AttributeStyled title="Nul" label="Čistá konta" data={stat.shutouts || '-'} />
        <AttributeStyled
          title="Průměr"
          label="Průměr inkasovaných gólů na zápas"
          data={stat.goalsAgainstAverage || '-'}
        />
      </Flex>
    </Card>
  );
};
