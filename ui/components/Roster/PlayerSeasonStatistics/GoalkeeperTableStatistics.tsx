import { Table, TBody, Td, Th, THead, Tr } from '@kertPark/components/Atomic/Table';
import { GoalkeeperStatsType } from '@kertPark/components/Roster/PlayerSeasonStatistics/types';
import React from 'react';

type Props = {
  stats: GoalkeeperStatsType;
};

export const GoalkeeperTableStatistics: React.FC<Props> = ({ stats }) => {
  return (
    <Table>
      <THead>
        <Tr>
          <Th align="left">Soutěž</Th>
          <Th title="Procentuální úspěšnost zákroků" width="4rem">
            %
          </Th>
          <Th title="Zápasy" width="3rem">
            Z
          </Th>
          <Th desktopOnly title="Odehrané minuty" width="4rem">
            Min
          </Th>
          <Th title="Střely proti" width="4rem">
            Stř.
          </Th>
          <Th title="Inkasované góly" width="3rem">
            G
          </Th>
          <Th desktopOnly title="Čistá konta" width="3rem">
            Nul
          </Th>
          <Th desktopOnly title="Průměr inkasovaných gólů na zápas" width="4rem">
            ∅
          </Th>
        </Tr>
      </THead>
      <TBody>
        {stats.map(({ leagueGroup, stat }) => (
          <Tr key={leagueGroup.id}>
            <Td align="left">{leagueGroup.name}</Td>
            <Td>{stat.savesAverage || '-'}</Td>
            <Td>{stat.games || '-'}</Td>
            <Td desktopOnly>{stat.minutes || '-'}</Td>
            <Td>{stat.shotsAgainst || '-'}</Td>
            <Td>{stat.goalsAgainst || '-'}</Td>
            <Td desktopOnly>{stat.shutouts || '-'}</Td>
            <Td desktopOnly>{stat.goalsAgainstAverage || '-'}</Td>
          </Tr>
        ))}
      </TBody>
    </Table>
  );
};
