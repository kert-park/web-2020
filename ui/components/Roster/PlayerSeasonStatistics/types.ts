import {
  CmshbGoalkeeperStats,
  CmshbLeagueGroup,
  CmshbPlayerStats,
  CmshbPlayerStatsList,
} from '@kertPark/graphql/generated';

type LeagueGroupType = Pick<CmshbLeagueGroup, 'id' | 'name'>;

export type StatisticsProps = {
  data: Array<CmshbPlayerStatsList>;
  loading: boolean;
  playerId: string;
};

export type GoalkeeperStatType = {
  leagueGroup: LeagueGroupType;
  stat: Omit<CmshbGoalkeeperStats, 'player' | '__typename'>;
};

export type GoalkeeperStatsType = Array<GoalkeeperStatType>;

export type PlayerStatType = {
  leagueGroup: LeagueGroupType;
  stat: Omit<CmshbPlayerStats, 'player' | '__typename'>;
};

export type PlayerStatsType = Array<PlayerStatType>;
