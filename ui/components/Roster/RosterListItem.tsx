import { Card, CardProps } from '@kertPark/components/Atomic/Card';
import { LinkAnchor } from '@kertPark/components/Atomic/LinkAnchor';
import { PlayerImage } from '@kertPark/components/Atomic/PlayerImage';
import { routes } from '@kertPark/config/routes';
import { Team } from '@kertPark/config/teams';
import { mediaBreakpoints, spaces, theme, typography } from '@kertPark/config/theme';
import { Player } from '@kertPark/graphql/generated';
import { getPlayerDate, PlayerStickLabelMap } from '@kertPark/lib/player';
import React from 'react';
import styled from 'styled-components';

const PlayerWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  flex: 1;
  padding-right: ${spaces[3]};

  @media (${mediaBreakpoints.md}) {
    padding-bottom: ${spaces[3]};
  }
`;

const PlayerInfoWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const PlayerName = styled.div`
  color: ${({ theme }) => theme.colors.textPrimary};
  font-family: ${theme.fonts.header};
  font-weight: bold;
  font-size: 1.25rem;
  margin-bottom: 0.25em;
`;

const PlayerInfo = styled.div`
  color: ${({ theme }) => theme.colors.textSecondary};
  font-family: ${theme.fonts.header};
  font-weight: bold;
  font-size: ${typography.overline.fontSize.small};
`;

const PlayerPhoto = styled(PlayerImage)`
  margin-right: 0.5em;
`;

const CardStyled = styled(Card)<CardProps>`
  cursor: ${({ isLink }) => (isLink ? 'pointer' : 'inherit')};
`;

const getPlayerInfo = ({ player }: { player: Player }): Array<string> => {
  const result: Array<string> = [];

  if (player.height) {
    result.push(`${player.height} cm`);
  }
  if (player.weight) {
    result.push(`${player.weight} kg`);
  }
  if (player.stick) {
    result.push(PlayerStickLabelMap[player.stick]);
  }

  return result;
};

type Props = {
  player: Player;
  team?: Team;
};

export const RosterListItem = ({ player, team }: Props) => {
  const { birth, year } = getPlayerDate({ player });
  const playerInfo = getPlayerInfo({ player });
  const hasGames = !!team?.hasGames;

  const card = (
    <CardStyled flexDirection="row" isLink={hasGames} noPadding>
      <PlayerPhoto number={player.number} smallImage={player.smallImage} wrapperComponent={CardStyled} />
      <PlayerWrapper>
        <PlayerInfoWrapper>
          <PlayerName>{player.name}</PlayerName>
          {birth && (
            <PlayerInfo>
              {year} let / {birth}
            </PlayerInfo>
          )}
          <PlayerInfo>
            {playerInfo.map((info, index) => (
              <React.Fragment key={index}>
                {index !== 0 && ' / '}
                {info}
              </React.Fragment>
            ))}
          </PlayerInfo>
        </PlayerInfoWrapper>
      </PlayerWrapper>
    </CardStyled>
  );

  if (hasGames) {
    return (
      // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
      <LinkAnchor href={routes.team.player} hrefAs={routes.team.playerAs(team?.id || '', player.id)} passHref>
        {card}
      </LinkAnchor>
    );
  }

  return card;
};
