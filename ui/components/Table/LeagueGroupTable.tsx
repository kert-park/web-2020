import { StaticTeamLogo } from '@kertPark/components/Atomic/StaticImage';
import { Table, TBody, Td, Th, THead, Tr } from '@kertPark/components/Atomic/Table';
import { SortKeys, useLeagueGroupTable } from '@kertPark/components/Table/useLeagueGroupTable';
import { theme } from '@kertPark/config/theme';
import { CmshbTeamTableStats } from '@kertPark/graphql/generated';
import React from 'react';
import styled from 'styled-components';

const Logo = styled(StaticTeamLogo)<{ small: boolean }>`
  width: ${({ small }) => (small ? 1.5 : 2)}em;
  height: ${({ small }) => (small ? 1.5 : 2)}em;
`;

type TableRowProps = {
  small: boolean;
  stat: CmshbTeamTableStats;
};

const TableRow: React.FC<TableRowProps> = ({ small, stat }) => {
  const tdProps = stat.team.cmshbId === '3100003' ? { bold: true, color: theme.colors.kert.redPrimary } : {};
  return (
    <Tr key={stat.team.cmshbId}>
      <Td {...tdProps}>{stat.standing}.</Td>
      <Td align="right">
        <Logo id={stat.team.cmshbId || ''} small={small} title={stat.team.name} />
      </Td>
      <Td {...tdProps} align="left">
        {small ? stat.team.shortName : stat.team.name}
      </Td>
      <Td {...tdProps}>{stat.games}</Td>
      {!small && (
        <>
          <Td desktopOnly {...tdProps}>
            {stat.wins}
          </Td>
          <Td desktopOnly {...tdProps}>
            {stat.overtimeWins}
          </Td>
          <Td desktopOnly {...tdProps}>
            {stat.overtimeLosses}
          </Td>
          <Td desktopOnly {...tdProps}>
            {stat.losses}
          </Td>
        </>
      )}
      <Td {...tdProps}>
        {stat.goals}:{stat.goalsAgainst}
      </Td>
      <Td {...tdProps}>{stat.points}</Td>
    </Tr>
  );
};

type Props = {
  small?: boolean;
  sortable?: boolean;
  stats: Array<CmshbTeamTableStats>;
};

export const LeagueGroupTable: React.FC<Props> = ({ small = false, sortable = false, stats }) => {
  const { handleSortKeyChange, sortedData, sortState } = useLeagueGroupTable({ stats });
  const data = sortable ? sortedData : stats;

  return (
    <Table>
      <THead>
        <Tr>
          <Th
            onClick={sortable ? () => handleSortKeyChange(SortKeys.STANDING) : undefined}
            sortIsDesc={sortState.key === SortKeys.STANDING ? !sortState.isDesc : undefined}
            title="Pořadí"
            width={3.5}
          >
            #
          </Th>
          <Th colSpan={2}>Tým</Th>
          <Th
            onClick={sortable ? () => handleSortKeyChange(SortKeys.GAMES) : undefined}
            sortIsDesc={sortState.key === SortKeys.GAMES ? sortState.isDesc : undefined}
            title="Zápasy"
          >
            Z
          </Th>
          {!small && (
            <>
              <Th
                desktopOnly
                onClick={sortable ? () => handleSortKeyChange(SortKeys.WINS) : undefined}
                sortIsDesc={sortState.key === SortKeys.WINS ? sortState.isDesc : undefined}
                title="Výhry"
              >
                V
              </Th>
              <Th
                desktopOnly
                onClick={sortable ? () => handleSortKeyChange(SortKeys.OVERTIME_WINS) : undefined}
                sortIsDesc={sortState.key === SortKeys.OVERTIME_WINS ? sortState.isDesc : undefined}
                title="Výhry v prodloužení"
              >
                VP
              </Th>
              <Th
                desktopOnly
                onClick={sortable ? () => handleSortKeyChange(SortKeys.OVERTIME_LOSSES) : undefined}
                sortIsDesc={sortState.key === SortKeys.OVERTIME_LOSSES ? sortState.isDesc : undefined}
                title="Prohry v prodloužení"
              >
                R
              </Th>
              <Th
                desktopOnly
                onClick={sortable ? () => handleSortKeyChange(SortKeys.LOSSES) : undefined}
                sortIsDesc={sortState.key === SortKeys.LOSSES ? sortState.isDesc : undefined}
                title="Prohry"
              >
                P
              </Th>
            </>
          )}
          <Th
            onClick={sortable ? () => handleSortKeyChange(SortKeys.SCORE) : undefined}
            sortIsDesc={sortState.key === SortKeys.SCORE ? sortState.isDesc : undefined}
            title="Skóre"
          >
            Skóre
          </Th>
          <Th
            onClick={sortable ? () => handleSortKeyChange(SortKeys.POINTS) : undefined}
            sortIsDesc={sortState.key === SortKeys.POINTS ? sortState.isDesc : undefined}
            title="Body"
          >
            Body
          </Th>
        </Tr>
      </THead>
      <TBody>
        {data.map((stat) => (
          <TableRow key={stat.standing} small={small} stat={stat} />
        ))}
      </TBody>
    </Table>
  );
};
