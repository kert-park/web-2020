import { CmshbTeamTableStats } from '@kertPark/graphql/generated';
import { SortFunction, SortFunctions, sortNumber, sortNumberAsc, useSortData } from '@kertPark/hooks/useSortData';

export enum SortKeys {
  GAMES = 'games',
  LOSSES = 'losses',
  OVERTIME_LOSSES = 'overtimeLosses',
  OVERTIME_WINS = 'overtimeWins',
  POINTS = 'points',
  SCORE = 'score',
  STANDING = 'standing',
  WINS = 'wins',
}

const sortScore: SortFunction<CmshbTeamTableStats> = (isDesc) => (a, b) => {
  const aGoals = a.goals || 0;
  const bGoals = b.goals || 0;
  const aGoalsAgainst = a.goalsAgainst || 0;
  const bGoalsAgainst = b.goalsAgainst || 0;
  const goalsDiff = bGoals - aGoals;

  if (goalsDiff !== 0) {
    return isDesc ? goalsDiff : -goalsDiff;
  }

  const goalsAgainstDiff = bGoalsAgainst - aGoalsAgainst;

  return isDesc ? -goalsAgainstDiff : goalsAgainstDiff;
};

const sortFunctions: SortFunctions<CmshbTeamTableStats, SortKeys> = {
  [SortKeys.GAMES]: sortNumber('games'),
  [SortKeys.LOSSES]: sortNumber('losses'),
  [SortKeys.OVERTIME_LOSSES]: sortNumber('overtimeLosses'),
  [SortKeys.OVERTIME_WINS]: sortNumber('overtimeWins'),
  [SortKeys.POINTS]: sortNumber('points'),
  [SortKeys.SCORE]: sortScore,
  [SortKeys.STANDING]: sortNumberAsc('standing'),
  [SortKeys.WINS]: sortNumber('wins'),
};

type Props = {
  stats: Array<CmshbTeamTableStats>;
};

export const useLeagueGroupTable = ({ stats }: Props) => {
  return useSortData<CmshbTeamTableStats, SortKeys>({
    data: stats,
    defaultSortKey: SortKeys.STANDING,
    sortFunctions,
  });
};
