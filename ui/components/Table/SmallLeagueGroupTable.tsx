import { Card } from '@kertPark/components/Atomic/Card';
import { LeagueGroupTable } from '@kertPark/components/Table/LeagueGroupTable';
import { Team } from '@kertPark/config/teams';
import { CmshbTeamTable, CmshbTeamTableStats, useCmshbTeamTableQuery } from '@kertPark/graphql/generated';
import { Box } from '@rebass/grid';
import React from 'react';

const filterTable = ({
  maxLength,
  table,
}: {
  maxLength?: number;
  table: Array<CmshbTeamTableStats>;
}): Array<CmshbTeamTableStats> => {
  const tableLength = table.length;
  const teamStanding = table.find((team) => team.team.cmshbId === '3100003')?.standing;

  if (maxLength && tableLength > maxLength && teamStanding) {
    if (teamStanding < Math.ceil(maxLength / 2) + 1) {
      return table.slice(0, maxLength);
    } else if (teamStanding >= tableLength - Math.floor(maxLength / 2)) {
      return table.slice(tableLength - maxLength - 1, tableLength);
    } else {
      return table.slice(teamStanding - Math.ceil(maxLength / 2), teamStanding + Math.floor(maxLength / 2));
    }
  }

  return table;
};

type Props = {
  maxLength?: number;
  tables?: Array<CmshbTeamTable>;
  team: Team;
};

export const SmallLeagueGroupTable: React.FC<Props> = ({ maxLength, tables: propTables, team }) => {
  const { data } = useCmshbTeamTableQuery({
    variables: {
      teamId: team.id,
    },
  });

  const tables = data?.cmshbTeamTable || propTables || [];
  // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
  const firstTable = tables[0]?.stats || [];
  const table = filterTable({ maxLength, table: [...firstTable] });

  return (
    <Box width={1} p={2}>
      <Card>
        <LeagueGroupTable small stats={table} />
      </Card>
    </Box>
  );
};
