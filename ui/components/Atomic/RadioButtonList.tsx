import { Overline } from '@kertPark/components/Atomic/Typography';
import { theme, typography } from '@kertPark/config/theme';
import { Flex } from '@rebass/grid';
import React from 'react';
import styled, { css } from 'styled-components';

const Radio = styled.div<{ active?: boolean; isDisabled?: boolean }>`
  color: ${({ active, isDisabled, theme }) =>
    isDisabled ? theme.colors.textSecondary : active ? theme.colors.textInverted : theme.colors.textPrimary};
  font-family: ${theme.fonts.header};
  font-size: ${typography.pSmall.fontSize.xs};
  font-weight: bold;
  text-transform: uppercase;
  background-color: ${({ active, isDisabled, theme }) =>
    active && !isDisabled ? theme.colors.secondary : theme.colors.lightGrey};
  margin: 0.5rem 0.5rem 0 0;
  padding: 0.5rem 0.75rem;
  border-radius: 0.25em;
  cursor: ${({ isDisabled }) => (isDisabled ? 'not-allowed' : 'cursor')};

  ${({ active, isDisabled, theme }) =>
    !isDisabled &&
    css`
      box-shadow: 0 0.25em 1em 0 ${active ? theme.colors.boxShadow.hoverRed : theme.colors.boxShadow.normal};
      &:hover {
        box-shadow: 0 0.25em 1em 0 ${active ? theme.colors.boxShadow.hoverRed : theme.colors.boxShadow.hover};
      }
    `};
`;

type Props<T> = {
  header?: string;
  isDisabled?: boolean;
  options: Array<{ text: string; value: T }>;
  onChange: (selected: T) => void;
  selected: T;
};

export const RadioButtonList = <T extends string>({ header, isDisabled, options, onChange, selected }: Props<T>) => {
  return (
    <Flex flex="1" flexDirection="column" flexWrap="wrap">
      {header && <Overline>{header}</Overline>}
      <Flex flex="1" flexDirection="row" flexWrap="wrap">
        {options.map(({ text, value }) => (
          <Radio
            key={value}
            active={(selected as string) === value}
            isDisabled={isDisabled}
            onClick={isDisabled ? undefined : () => onChange(value)}
          >
            {text}
          </Radio>
        ))}
      </Flex>
    </Flex>
  );
};
