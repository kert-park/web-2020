import { Overline } from '@kertPark/components/Atomic/Typography';
import { theme, typography } from '@kertPark/config/theme';
import { Flex } from '@rebass/grid';
import React from 'react';
import styled from 'styled-components';

const Select = styled.select`
  color: ${({ theme }) => theme.colors.textPrimary};
  font-family: ${theme.fonts.header};
  font-size: ${typography.pSmall.fontSize.xs};
  margin: 0.5rem 0 0 0;
  padding: 0 0.5rem;
  border-color: ${({ theme }) => theme.colors.grey};
  border-radius: 0.25em;
  height: 2rem;
`;

type Props<T> = {
  header?: string;
  options: Array<{ text: string; value: T }>;
  onChange: (selected: T) => void;
  value: T;
};

export const SelectBox = <T extends string>({ header, onChange, options, value }: Props<T>) => {
  return (
    <Flex flex="1" flexDirection="column" flexWrap="wrap">
      {header && <Overline>{header}</Overline>}
      <Select onChange={(event) => onChange(event.target.value as T)} value={value}>
        {options.map(({ text, value }) => (
          <option key={value} value={value}>
            {text}
          </option>
        ))}
      </Select>
    </Flex>
  );
};
