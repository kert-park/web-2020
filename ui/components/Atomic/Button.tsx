import { LinkAnchor } from '@kertPark/components/Atomic/LinkAnchor';
import { spaces, theme, typography } from '@kertPark/config/theme';
import styled, { css } from 'styled-components';

type ButtonProps = { disabled?: boolean };

const buttonCss = css<ButtonProps>`
  color: ${({ theme }) => theme.colors.textInverted};
  font-family: ${theme.fonts.header};
  font-size: ${typography.pSmall.fontSize.xs};
  font-weight: bold;
  text-transform: uppercase;
  background-color: ${({ disabled, theme }) => (disabled ? theme.colors.lightGrey : theme.colors.primary)};
  margin: ${spaces[3]} ${spaces[2]};
  padding: 0.5rem 0.75rem;
  border: 0;
  border-radius: 0.25em;
  cursor: pointer;
  text-decoration: none;
  pointer-events: ${({ disabled }) => (disabled ? 'none' : 'inherit')};
  box-shadow: 0 0.25em 1em 0 ${({ theme }) => theme.colors.boxShadow.normal};

  &:hover {
    text-decoration: underline;
    box-shadow: 0 0.25em 1em 0 ${({ theme }) => theme.colors.boxShadow.hover};
  }
  &:focus,
  &:active,
  &:visited {
    color: ${({ theme }) => theme.colors.textInverted};
    outline: none;
    border: 0;
  }
`;

export const LinkButton = styled(LinkAnchor)<ButtonProps>`
  ${buttonCss}
`;

export const Button = styled.button<ButtonProps>`
  ${buttonCss}
`;
