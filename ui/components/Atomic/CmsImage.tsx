import { theme } from '@kertPark/config/theme';
import React from 'react';
import styled from 'styled-components';

export const Image = styled.img`
  width: 100%;
  border-radius: ${theme.radius.card};
`;

export const CmsImage: React.FC = (props) => {
  return <Image {...props} />;
};
