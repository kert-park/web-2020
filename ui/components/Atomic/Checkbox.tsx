import { theme } from '@kertPark/config/theme';
import React from 'react';
import styled from 'styled-components';

const Input = styled.input`
  display: none;
`;

const Label = styled.label<Pick<Props, 'checked' | 'disabled'>>`
  display: block;
  position: relative;
  width: 3em;
  height: 1.6em;
  border-radius: 1em;
  background: ${({ checked, disabled, theme }) =>
    checked && !disabled ? theme.colors.green : theme.colors.backgroundPrimary};
  box-shadow: inset 0 0 0.25em rgba(0, 0, 0, 0.2);
  cursor: ${({ disabled }) => (disabled ? 'not-allowed' : 'pointer')};
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  -webkit-transition: background ${theme.transitionSpeed};
  transition: background ${theme.transitionSpeed};

  &:before {
    content: '';
    display: block;
    position: absolute;
    top: 0.2em;
    left: ${({ checked }) => (checked ? '1.6' : '0.2')}em;
    width: 1.2em;
    height: 1.2em;
    border-radius: 1em;
    background: ${({ theme }) => theme.colors.backgroundSecondary};
    box-shadow: ${({ checked }) => (checked ? '-0.125em' : '0.125em')} 0 0.25em rgba(0, 0, 0, 0.2);
    -webkit-transition: all ${theme.transitionSpeed};
    transition: all ${theme.transitionSpeed};
  }
`;

type Props = {
  id: string;
  disabled?: boolean;
  checked: boolean;
  onChange?: () => void;
};

export const Checkbox: React.FC<Props> = ({ id, disabled = false, checked, onChange }) => {
  return (
    <>
      <Input type="checkbox" id={id} checked={checked} readOnly />
      <Label id={id} onClick={!disabled ? onChange : undefined} checked={checked} disabled={disabled}></Label>
    </>
  );
};
