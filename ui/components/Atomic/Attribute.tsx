import { H4, Overline } from '@kertPark/components/Atomic/Typography';
import { mediaBreakpoints, spaces } from '@kertPark/config/theme';
import React from 'react';
import styled from 'styled-components';

const AttributeWrapper = styled.div`
  display: flex;
  flex: 0 0 auto;
  flex-direction: column;
  align-items: center;

  &:not(:last-child) {
    padding-right: ${spaces[3]};
  }

  @media (${mediaBreakpoints.lg}) {
    align-items: flex-start;
  }
`;

export const AttributeRow = styled(AttributeWrapper)`
  flex-direction: row;
  flex-wrap: wrap;
  margin-top: ${spaces[2]};
`;

export type AttributeType = {
  title: React.ReactNode;
  data: React.ReactNode;
  label?: string;
};

export const Attribute: React.FC<AttributeType & { className?: string }> = ({ className, label, title, data }) => {
  return (
    <AttributeWrapper className={className}>
      <Overline title={label}>{title}</Overline>
      <H4 marginTop="0" marginBottom="0">
        {data}
      </H4>
    </AttributeWrapper>
  );
};
