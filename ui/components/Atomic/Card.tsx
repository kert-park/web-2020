import { H2 } from '@kertPark/components/Atomic/Typography';
import { mediaBreakpoints, spaces, theme } from '@kertPark/config/theme';
import React from 'react';
import styled from 'styled-components';

export type CardProps = {
  className?: string;
  header?: string;
  flexDirection?: 'row' | 'column';
  alignItems?: 'flex-start' | 'center' | 'flex-end';
  justifyContent?: 'flex-start' | 'center' | 'flex-end';
  isLink?: boolean;
  noBackground?: boolean;
  noPadding?: boolean;
};

const Container = styled.div<Omit<CardProps, 'header' | 'className'>>`
  display: flex;
  flex-direction: ${({ flexDirection }) => flexDirection};
  ${({ alignItems }) => (alignItems ? `align-items: ${alignItems};` : '')};
  ${({ justifyContent }) => (justifyContent ? `justify-content: ${justifyContent};` : '')};
  width: 100%;
  height: 100%;
  ${({ noBackground, theme }) =>
    noBackground
      ? ''
      : `
    background-color: ${theme.colors.backgroundSecondary};
    border-radius: ${theme.radius.card};
    box-shadow: 0 1em 1em 0 ${theme.colors.boxShadow.normal};
  `};
  ${({ isLink, theme }) =>
    !isLink
      ? ''
      : `
    &:hover {
      box-shadow: 0 1em 1em 0 ${theme.colors.boxShadow.hover};
    } 
  `};
  transition: box-shadow ${theme.transitionSpeed};
  padding: ${({ noPadding }) => (noPadding ? 0 : theme.spacing.card)};
`;

export const Card: ReactFCWithChildren<CardProps> = ({
  className,
  children,
  header,
  flexDirection = 'column',
  alignItems,
  justifyContent,
  isLink = false,
  noBackground = false,
  noPadding = false,
}) => {
  const CardContent = (
    <Container
      className={header ? '' : className}
      alignItems={alignItems}
      flexDirection={flexDirection}
      justifyContent={justifyContent}
      isLink={isLink}
      noBackground={noBackground}
      noPadding={noPadding}
    >
      {children}
    </Container>
  );

  if (header) {
    return (
      <div className={className}>
        <H2 marginBottom={spaces[2]} marginLeft={theme.spacing.card}>
          {header}
        </H2>
        {CardContent}
      </div>
    );
  }

  return CardContent;
};

export const CardMobile = styled(Card)`
  @media (${mediaBreakpoints.md}) {
    display: none;
  }
`;

export const CardDesktop = styled(Card)`
  display: none;

  @media (${mediaBreakpoints.md}) {
    display: inherit;
  }
`;
