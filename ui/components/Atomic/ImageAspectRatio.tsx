import { theme } from '@kertPark/config/theme';
import React from 'react';
import styled from 'styled-components';

const ImageWrapper = styled.div<Pick<Props, 'ratio'>>`
  position: relative;

  &::after {
    display: block;
    content: '';
    padding-bottom: ${({ ratio }) => ratio}%;
  }
`;

const Image = styled.img`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: ${theme.radius.card};
`;

export enum AspectRatio {
  oneToOne = 100,
  threeToTwo = 66.67,
  twoToOne = 50,
  threeToOne = 33.33,
}

type Props = {
  className?: string;
  ratio?: AspectRatio;
} & Required<Pick<HTMLImageElement, 'src'>> &
  Partial<Omit<HTMLImageElement, 'src'>>;

export const ImageAspectRatio: React.FC<Props> = ({ className, ratio = AspectRatio.threeToTwo, src, title }) => {
  const alt = `Fotografie${title ? `: ${title}` : ''}`;
  return (
    <ImageWrapper className={className} ratio={ratio}>
      <Image alt={alt} src={src} title={alt} />
    </ImageWrapper>
  );
};
