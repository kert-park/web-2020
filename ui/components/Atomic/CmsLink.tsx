import { LinkAnchor, TextAnchor } from '@kertPark/components/Atomic/LinkAnchor';
import React from 'react';

type Props = {
  href?: string;
};

export const CmsLink: ReactFCWithChildren<Props> = ({ children, href, ...props }) => {
  if (href?.startsWith('/')) {
    return (
      <LinkAnchor href={href} hrefAs={href} isText {...props}>
        {children}
      </LinkAnchor>
    );
  }

  return (
    <TextAnchor href={href} {...(href?.startsWith('http') ? { target: '_blank', rel: 'noopener' } : {})} {...props}>
      {children}
    </TextAnchor>
  );
};
