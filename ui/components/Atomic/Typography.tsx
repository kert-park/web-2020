import { mediaBreakpoints, theme, typography } from '@kertPark/config/theme';
import styled, { css } from 'styled-components';

type HeaderProps = {
  color?: string;
  margin?: string;
  marginTop?: string;
  marginLeft?: string;
  marginBottom?: string;
  textTransform?: string;
};

const headerStyle = css<HeaderProps>`
  color: ${({ color, theme }) => color || theme.colors.textPrimary};
  font-family: ${theme.fonts.header};
  font-weight: bold;
  margin: ${({ margin }) => margin || '2rem 0 1rem'};
  text-transform: ${({ textTransform }) => textTransform || 'none'};
  ${({ marginTop }) => (marginTop ? `margin-top: ${marginTop}` : '')};
  ${({ marginLeft }) => (marginLeft ? `margin-left: ${marginLeft}` : '')};
  ${({ marginBottom }) => (marginBottom ? `margin-bottom: ${marginBottom}` : '')};
`;

export const H1 = styled.h1<HeaderProps>`
  ${headerStyle};
  font-size: ${typography.h1.fontSize.xs};

  @media (${mediaBreakpoints.lg}) {
    font-size: ${typography.h1.fontSize.lg};
  }
`;

export const H2 = styled.h2<HeaderProps>`
  ${headerStyle};
  font-size: ${typography.h2.fontSize.xs};

  @media (${mediaBreakpoints.lg}) {
    font-size: ${typography.h2.fontSize.lg};
  }
`;

export const H3 = styled.h3<HeaderProps>`
  ${headerStyle};
  font-size: ${typography.h3.fontSize.xs};

  @media (${mediaBreakpoints.lg}) {
    font-size: ${typography.h3.fontSize.lg};
  }
`;

export const H4 = styled.h4<HeaderProps>`
  ${headerStyle};
  font-size: ${typography.h4.fontSize.xs};

  @media (${mediaBreakpoints.lg}) {
    font-size: ${typography.h4.fontSize.lg};
  }
`;

export const H5 = styled.h5<HeaderProps>`
  ${headerStyle};
  font-size: ${typography.h5.fontSize.xs};

  @media (${mediaBreakpoints.lg}) {
    font-size: ${typography.h5.fontSize.lg};
  }
`;

export const Overline = styled.span<{
  bold?: boolean;
  color?: string;
  margin?: string;
  small?: boolean;
  textAlign?: string;
  textTransform?: string;
}>`
  color: ${({ color, theme }) => color || theme.colors.textSecondary};
  font-family: ${theme.fonts.header};
  font-size: ${({ small }) => (small ? typography.overline.fontSize.small : typography.overline.fontSize.normal)};
  font-weight: ${({ bold = true }) => (bold ? 'bold' : 'normal')};
  text-transform: ${({ textTransform }) => textTransform || 'uppercase'};
  text-align: ${({ textAlign }) => textAlign || 'left'};
  margin: ${({ margin }) => margin || '0'};
`;

export const P = styled.p<{ color?: string; bold?: boolean }>`
  color: ${({ color, theme }) => color || theme.colors.textPrimary};
  font-family: ${theme.fonts.text};
  font-size: ${typography.p.fontSize.xs};
  font-weight: ${({ bold }) => (bold ? 'bold' : 'normal')};
  text-align: justify;
  line-height: 1.5;
  margin-bottom: ${theme.spacing.paragraphs};

  @media (${mediaBreakpoints.lg}) {
    font-size: ${typography.p.fontSize.lg};
  }
`;

export const PSmall = styled.p<{ color?: string; bold?: boolean; textAlign?: string }>`
  color: ${({ color, theme }) => color || theme.colors.textPrimary};
  font-family: ${theme.fonts.text};
  font-size: ${typography.pSmall.fontSize.xs};
  font-weight: ${({ bold }) => (bold ? 'bold' : 'normal')};
  text-align: ${({ textAlign }) => textAlign || 'justify'};
  line-height: 1.5;
  margin-bottom: ${theme.spacing.paragraphs};

  @media (${mediaBreakpoints.lg}) {
    font-size: ${typography.pSmall.fontSize.lg};
  }
`;
