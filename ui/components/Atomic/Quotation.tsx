import { mediaBreakpoints, theme, typography } from '@kertPark/config/theme';
import styled from 'styled-components';

export const Quotation = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  align-items: center;
  text-align: center;
  color: ${({ theme }) => theme.colors.secondary};
  font-family: ${theme.fonts.header};
  font-weight: bold;
  font-style: italic;
  font-size: ${typography.h3.fontSize.xs};

  @media (${mediaBreakpoints.lg}) {
    font-size: ${typography.h3.fontSize.lg};
  }
`;
