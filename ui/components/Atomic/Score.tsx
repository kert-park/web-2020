import { theme } from '@kertPark/config/theme';
import { GameType } from '@kertPark/graphql/generated/helpers';
import { getGameResult } from '@kertPark/lib/game';
import React from 'react';
import styled from 'styled-components';

const StyledScore = styled.div<{ hasWin?: boolean }>`
  color: ${({ hasWin, theme }) => (hasWin ? theme.colors.green : theme.colors.kert.redSecondary)};
  font-family: ${theme.fonts.header};
  font-weight: bold;
  font-size: 1.625rem;
`;

type Props = {
  className?: string;
  game: GameType;
  swap?: boolean;
};

export const Score = ({ className, game, swap }: Props) => {
  const { score, hasWin } = getGameResult(game, swap);
  return (
    <StyledScore className={className} hasWin={hasWin}>
      {score}
    </StyledScore>
  );
};
