import { LinkUrl } from '@kertPark/config/types';
import Link, { LinkProps } from 'next/link';
import React from 'react';
import styled from 'styled-components';

export const Anchor = styled.a`
  cursor: pointer;
  text-decoration: none;
`;

export const TextAnchor = styled.a`
  color: ${({ theme }) => theme.colors.kert.redPrimary};
  &:active,
  &:visited {
    color: ${({ theme }) => theme.colors.kert.redPrimary};
  }
  &:hover {
    color: ${({ theme }) => theme.colors.kert.redSecondary};
  }
`;

export type LinkAnchorProps = Omit<LinkProps, 'as'> & {
  hrefAs?: LinkUrl;
  className?: string;
  children: React.ReactNode;
  isText?: boolean;
} & Pick<React.AnchorHTMLAttributes<HTMLAnchorElement>, 'target'>;

export const LinkAnchor = ({
  className,
  children,
  hrefAs,
  isText = false,
  passHref = true,
  target,
  onClick,
  ...rest
}: LinkAnchorProps) => {
  const AnchorComponent = isText ? TextAnchor : Anchor;
  return (
    <Link {...rest} as={hrefAs} passHref={passHref}>
      <AnchorComponent className={className} onClick={onClick} target={target}>
        {children}
      </AnchorComponent>
    </Link>
  );
};
