import { mediaBreakpoints, theme } from '@kertPark/config/theme';
import React from 'react';
import styled, { css } from 'styled-components';

export const SIZES = {
  xs: {
    size: '5em',
    borderRadius: theme.radius.card,
  },
  lg: {
    size: '6.25em',
    borderRadius: theme.radius.card,
  },
};

export const PersonImageStyle = css<{ noMargin?: boolean }>`
  width: ${SIZES.xs.size};
  height: ${SIZES.xs.size};
  border-radius: ${SIZES.xs.borderRadius};
  object-fit: cover;
  ${({ noMargin }) => (noMargin ? '' : 'margin: 0.25em 0;')};

  @media (${mediaBreakpoints.lg}) {
    width: ${SIZES.lg.size};
    height: ${SIZES.lg.size};
    border-radius: ${SIZES.lg.borderRadius};
  }
`;

export const PersonImage = styled.img<{ noMargin?: boolean }>`
  ${PersonImageStyle};
`;

const PersonImagePlaceholderWrapper = styled.div<{ noMargin?: boolean }>`
  ${PersonImageStyle};
  border: 1px solid ${({ theme }) => theme.colors.lightGrey};
`;

export const PersonImagePlaceholder = ({ noMargin }: { noMargin?: boolean }) => {
  return (
    <PersonImagePlaceholderWrapper noMargin={noMargin}>
      <svg width="100%" viewBox="0 0 1792 1792" fill={theme.colors.lightGrey}>
        <path d="M1600 1405q0 120-73 189.5t-194 69.5h-874q-121 0-194-69.5t-73-189.5q0-53 3.5-103.5t14-109 26.5-108.5 43-97.5 62-81 85.5-53.5 111.5-20q9 0 42 21.5t74.5 48 108 48 133.5 21.5 133.5-21.5 108-48 74.5-48 42-21.5q61 0 111.5 20t85.5 53.5 62 81 43 97.5 26.5 108.5 14 109 3.5 103.5zm-320-893q0 159-112.5 271.5t-271.5 112.5-271.5-112.5-112.5-271.5 112.5-271.5 271.5-112.5 271.5 112.5 112.5 271.5z" />
      </svg>
    </PersonImagePlaceholderWrapper>
  );
};
