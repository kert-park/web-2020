import { Blockquote } from '@kertPark/components/Atomic/Blockquote';
import { CmsImage } from '@kertPark/components/Atomic/CmsImage';
import { CmsLink } from '@kertPark/components/Atomic/CmsLink';
import { Figure } from '@kertPark/components/Atomic/Figure';
import { Quotation } from '@kertPark/components/Atomic/Quotation';
import { Table, TBody, Td, Th, THead, Tr } from '@kertPark/components/Atomic/Table';
import { H2, H3, H4, H5, P } from '@kertPark/components/Atomic/Typography';
import MarkdownToJSX from 'markdown-to-jsx';
import React from 'react';

export const Markdown: React.FC<{ text: string }> = ({ text }) => {
  return (
    <MarkdownToJSX
      options={{
        overrides: {
          a: CmsLink,
          blockquote: Blockquote,
          figure: Figure,
          h2: H2,
          h3: H3,
          h4: H4,
          h5: H5,
          h6: Quotation,
          img: CmsImage,
          p: P,
          quotation: Quotation,
          table: Table,
          tbody: TBody,
          td: Td,
          th: Th,
          thead: THead,
          tr: Tr,
        },
      }}
    >
      {text}
    </MarkdownToJSX>
  );
};
