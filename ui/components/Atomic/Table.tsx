import { mediaBreakpoints, spaces, theme, typography } from '@kertPark/config/theme';
import React from 'react';
import styled, { css } from 'styled-components';

export const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
`;

export const THead = styled.thead`
  th {
    background-color: ${({ theme }) => theme.colors.lightGrey};
    &:first-child {
      border-radius: 0.25em 0 0 0.25em;
    }
    &:last-child {
      border-radius: 0 0.25em 0.25em 0;
    }
  }
`;

export const TBody = styled.tbody`
  tr:first-child {
    td {
      border-top: 0;
    }
  }
`;

export const Tr = styled.tr<{ withLink?: boolean }>`
  ${({ withLink }) => (withLink ? 'cursor: pointer;' : '')}
`;

type TdProps = Pick<
  React.DetailedHTMLProps<React.TdHTMLAttributes<HTMLTableDataCellElement>, HTMLTableDataCellElement>,
  'colSpan' | 'rowSpan' | 'align' | 'valign' | 'color' | 'width' | 'onClick' | 'title'
> & { bold?: boolean; desktopOnly?: boolean; hoverUnderline?: boolean };

type ThProps = TdProps & {
  sortIsDesc?: boolean;
};

const tdDesktopOnly = css`
  display: none;
  @media (${mediaBreakpoints.md}) {
    display: table-cell;
  }
`;

const tdStyles = css<TdProps>`
  font-family: ${theme.fonts.header};
  padding: ${spaces[2]} ${spaces[1]};
  text-align: ${({ align }) => align || 'center'};
  vertical-align: ${({ valign }) => valign || 'middle'};
  @media (${mediaBreakpoints.lg}) {
    ${({ width }) => (width ? `width: ${width};` : '')};
  }
  ${({ desktopOnly }) => (desktopOnly ? tdDesktopOnly : '')};
  &:hover {
    ${({ hoverUnderline }) => (!!hoverUnderline ? 'text-decoration: underline;' : '')};
  }
`;

export const Td = styled.td<TdProps>`
  ${tdStyles};
  color: ${({ color, theme }) => color || theme.colors.textPrimary};
  font-size: ${typography.p.fontSize.xs};
  ${({ bold }) => (bold ? 'font-weight: bold;' : '')};
  border-top: 1px solid ${({ theme }) => theme.colors.grey};
`;

export const ThWrapper = styled.th<ThProps>`
  ${tdStyles};
  color: ${({ color, theme }) => color || theme.colors.textTertiary};
  text-transform: uppercase;

  cursor: ${({ onClick }) => (!!onClick ? 'pointer' : 'default')};
  user-select: ${({ onClick }) => (!!onClick ? 'none' : 'default')};

  &:hover {
    text-decoration: ${({ onClick }) => (!!onClick ? 'underline' : 'none')};
  }
`;

const FontArrow = styled.span`
  font-size: 0.625rem;
`;

export const Th: ReactFCWithChildren<ThProps> = ({ children, color, sortIsDesc, ...props }) => {
  return (
    <ThWrapper color={color} sortIsDesc={sortIsDesc} {...props}>
      {children}
      {typeof sortIsDesc === 'boolean' && (
        <FontArrow>
          {'\u00a0'}
          {sortIsDesc ? '\u25bc' : '\u25b2'}
        </FontArrow>
      )}
    </ThWrapper>
  );
};
