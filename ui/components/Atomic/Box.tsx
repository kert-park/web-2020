import { mediaBreakpoints } from '@kertPark/config/theme';
import { Box, Flex } from '@rebass/grid';
import styled from 'styled-components';

export const HiddenMobileBox = styled(Box)`
  display: none;

  @media (${mediaBreakpoints.md}) {
    display: block;
  }
`;

export const HiddenMobileFlex = styled(Flex)`
  display: none;

  @media (${mediaBreakpoints.md}) {
    display: flex;
  }
`;

export const HiddenDesktopBox = styled(Box)`
  @media (${mediaBreakpoints.md}) {
    display: none;
  }
`;
