import { theme } from '@kertPark/config/theme';
import React, { useContext, useId } from 'react';
import ReactContentLoader, { IContentLoaderProps } from 'react-content-loader';
import { ThemeContext } from 'styled-components';

export const ContentLoader: React.FC<IContentLoaderProps> = ({ backgroundColor, foregroundColor, ...restProps }) => {
  const kertTheme = useContext<typeof theme>(ThemeContext);
  const id = useId();

  return (
    <ReactContentLoader
      uniqueKey={id}
      backgroundColor={backgroundColor || kertTheme.colors.loader.background}
      foregroundColor={foregroundColor || kertTheme.colors.loader.foreground}
      {...restProps}
    />
  );
};

export const Rect: React.FC<React.SVGProps<SVGRectElement>> = ({ rx = 5, ry = 5, ...restProps }) => {
  return <rect rx={rx} ry={ry} {...restProps} />;
};
