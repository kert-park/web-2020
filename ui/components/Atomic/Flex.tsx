import { mediaBreakpoints } from '@kertPark/config/theme';
import { Flex } from '@rebass/grid';
import styled from 'styled-components';

export const HiddenMobileFlex = styled(Flex)`
  display: none;

  @media (${mediaBreakpoints.md}) {
    display: flex;
  }
`;

export const HiddenDesktopFlex = styled(Flex)`
  @media (${mediaBreakpoints.md}) {
    display: none;
  }
`;
