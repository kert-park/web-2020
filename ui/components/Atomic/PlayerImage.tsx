import { PersonImage, SIZES } from '@kertPark/components/Atomic/PersonImage';
import { mediaBreakpoints, theme } from '@kertPark/config/theme';
import { Player } from '@kertPark/graphql/generated';
import React from 'react';
import styled, { StyledComponent } from 'styled-components';

const PhotoWrapper = styled.div`
  position: relative;
  width: ${SIZES.xs.size};
  height: ${SIZES.xs.size};
  flex: 0 0 ${SIZES.xs.size};
  @media (${mediaBreakpoints.lg}) {
    width: ${SIZES.lg.size};
    height: ${SIZES.lg.size};
    flex: 0 0 ${SIZES.lg.size};
  }
`;

const PhotoOverlay = styled.div<{ showAlways: boolean; wrapperComponent?: StyledComponent<any, any> }>`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: ${theme.gradients.blueRed};
  opacity: ${({ showAlways }) => (showAlways ? 1 : 0)};
  transition: opacity ${theme.transitionSpeed};
  border-radius: ${SIZES.xs.borderRadius};

  ${({ wrapperComponent }) => (wrapperComponent ? `${wrapperComponent}:hover & { opacity: 0.75 }` : '')}

  @media (${mediaBreakpoints.lg}) {
    border-radius: ${SIZES.lg.borderRadius};
  }
`;

const PhotoOverlayText = styled.div<{ showAlways: boolean; wrapperComponent?: StyledComponent<any, any> }>`
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  color: ${({ theme }) => theme.colors.kert.yellowPrimary};
  font-family: ${theme.fonts.header};
  font-weight: bold;
  font-size: 2rem;
  opacity: ${({ showAlways }) => (showAlways ? 1 : 0)};
  transition: opacity ${theme.transitionSpeed};

  ${({ wrapperComponent }) => (wrapperComponent ? `${wrapperComponent}:hover & { opacity: 1 }` : '')}
`;

type PlayerImageProps = Pick<Player, 'number' | 'smallImage'> & {
  className?: string;
  wrapperComponent?: StyledComponent<any, any>;
};

export const PlayerImage: React.FC<PlayerImageProps> = ({ className, number, smallImage, wrapperComponent }) => {
  return (
    <PhotoWrapper className={className}>
      <PhotoOverlay showAlways={!smallImage} wrapperComponent={wrapperComponent} />
      <PhotoOverlayText showAlways={!smallImage} wrapperComponent={wrapperComponent}>
        {number}
      </PhotoOverlayText>
      {smallImage ? <PersonImage noMargin src={smallImage} /> : null}
    </PhotoWrapper>
  );
};
