import { ContentLoader } from '@kertPark/components/Atomic/ContentLoader';
import { useImageExists } from '@kertPark/hooks/useImageExists';
import { getPlaceSrc, getTeamLogoSrc } from '@kertPark/lib/static';
import React from 'react';

type StaticImageProps = {
  className?: string;
  alt: string;
  src: string;
};

export const StaticImage: React.FC<StaticImageProps> = ({ className, alt, src }) => {
  const { exists } = useImageExists({ src });

  if (exists) {
    return <img className={className} alt={alt} src={src} />;
  }

  return (
    <ContentLoader className={className} viewBox="0 0 40 40" backgroundOpacity={0} speed={2}>
      <circle cx="20" cy="20" r="20" />
    </ContentLoader>
  );
};

type StaticLogoProps = {
  className?: string;
  id: string;
  title: string;
};

export const StaticTeamLogo: React.FC<StaticLogoProps> = ({ className, id, title }) => {
  return <StaticImage className={className} alt={title} src={getTeamLogoSrc(id)} />;
};

export const StaticPlaceLogo: React.FC<StaticLogoProps> = ({ className, id, title }) => {
  return <StaticImage className={className} alt={title} src={getPlaceSrc(id)} />;
};
