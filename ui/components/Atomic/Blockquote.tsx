import { spaces } from '@kertPark/config/theme';
import styled from 'styled-components';

export const Blockquote = styled.blockquote`
  margin: -${spaces[2]} 0 0;
  quotes: '„' '“' '„' '“';
  p {
    &::before {
      content: open-quote;
    }
    &::after {
      content: close-quote;
    }
  }
`;
