import { Card } from '@kertPark/components/Atomic/Card';
import { Overline } from '@kertPark/components/Atomic/Typography';
import { Container, GameStatus, GameWrapper, TeamLogo, Teams } from '@kertPark/components/Game/NearestGamesStyles';
import { GameListQuery } from '@kertPark/graphql/generated';
import { getDateObject, getReadableDate, getReadableTime } from '@kertPark/lib/date';
import { GameStatus as GameStatusEnum, GameStatusName, groupGamesByDate } from '@kertPark/lib/game';
import { Box } from '@rebass/grid';
import { isAfter, startOfDay } from 'date-fns';
import React from 'react';

export const NearestHomeGames = ({ games }: { games: GameListQuery['gameList'] }) => {
  const today = startOfDay(new Date());
  const allGames = games.filter((game) => isAfter(getDateObject(game.date), today));
  const gamesByDate = groupGamesByDate(allGames);

  if (allGames.length === 0) {
    return null;
  }

  return (
    <Box width={1} p={2}>
      <Card header="Utkání na Lužinách">
        <Container>
          {Object.keys(gamesByDate).map((dateKey) => {
            const dateGames = gamesByDate[dateKey];
            return (
              <React.Fragment key={dateKey}>
                <Overline margin="1em 0 0" small>
                  {getReadableDate(dateKey, false, true)}
                </Overline>
                {dateGames.map((game) => {
                  const isHomeTeam = game.home.cmshbId === '3100003';
                  const opponent = isHomeTeam ? game.away : game.home;
                  return (
                    <GameWrapper key={game.id}>
                      <TeamLogo id={opponent.cmshbId || ''} title={opponent.name} />
                      <Teams textTransform="none">
                        {game.team.name} - {opponent.shortName}
                      </Teams>
                      <GameStatus>
                        {game.status === GameStatusEnum.od ? GameStatusName.od : getReadableTime(game.date)}
                      </GameStatus>
                    </GameWrapper>
                  );
                })}
              </React.Fragment>
            );
          })}
        </Container>
      </Card>
    </Box>
  );
};
