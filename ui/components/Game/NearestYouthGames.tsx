import { Card } from '@kertPark/components/Atomic/Card';
import { Overline } from '@kertPark/components/Atomic/Typography';
import {
  Container,
  GameStatus,
  GameWrapper,
  StyledScore,
  TeamLogo,
  Teams,
} from '@kertPark/components/Game/NearestGamesStyles';
import { GameListQuery } from '@kertPark/graphql/generated';
import { getDateObject, getReadableDate, getReadableTime } from '@kertPark/lib/date';
import {
  GameOtherStatuses,
  GamePlayedStatuses,
  GameStatus as GameStatusType,
  GameStatusName,
  groupGamesByDate,
  sliceGamesGroupedByDate,
} from '@kertPark/lib/game';
import { Box } from '@rebass/grid';
import { compareAsc } from 'date-fns';
import React from 'react';

type Props = {
  games: GameListQuery['gameList'];
};

export const NearestYouthGames: React.FC<Props> = ({ games }) => {
  const allGames = games
    .filter((game) => game.team.systemName !== 'muzia')
    .sort((a, b) => compareAsc(getDateObject(a.date), getDateObject(b.date)));

  const gamesByDate = sliceGamesGroupedByDate(groupGamesByDate(allGames));

  if (allGames.length === 0) {
    return null;
  }

  return (
    <Box width={1} p={2}>
      <Card header="Výsledky mládeže">
        <Container>
          {Object.entries(gamesByDate).map(([dateKey, dateGames]) => {
            return (
              <React.Fragment key={dateKey}>
                <Overline margin="1em 0 0" small>
                  {getReadableDate(dateKey, false, true)}
                </Overline>
                {dateGames.map((game) => {
                  const isHomeTeam = game.home.cmshbId === '3100003';
                  const opponent = isHomeTeam ? game.away : game.home;
                  return (
                    <GameWrapper key={game.id}>
                      <TeamLogo id={opponent.cmshbId || ''} title={opponent.name} />
                      <Teams textTransform="none">
                        {game.team.name} - {opponent.shortName}
                      </Teams>
                      {GamePlayedStatuses.includes(game.status as GameStatusType) ? (
                        <StyledScore game={game} swap={!isHomeTeam} />
                      ) : GameOtherStatuses.includes(game.status as GameStatusType) ? (
                        <GameStatus>{GameStatusName[game.status as GameStatusType]}</GameStatus>
                      ) : (
                        <GameStatus>{getReadableTime(game.date)}</GameStatus>
                      )}
                    </GameWrapper>
                  );
                })}
              </React.Fragment>
            );
          })}
        </Container>
      </Card>
    </Box>
  );
};
