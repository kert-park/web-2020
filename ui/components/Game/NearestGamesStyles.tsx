import { Score } from '@kertPark/components/Atomic/Score';
import { StaticTeamLogo } from '@kertPark/components/Atomic/StaticImage';
import { Overline } from '@kertPark/components/Atomic/Typography';
import { typography } from '@kertPark/config/theme';
import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

export const GameWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-top: 0.5em;
`;

export const TeamLogo = styled(StaticTeamLogo)`
  flex: 0 0 1.875em;
  width: 1.875em;
  height: 1.875em;
  margin-right: 0.5em;
`;

export const Teams = styled(Overline)`
  color: ${({ theme }) => theme.colors.textPrimary};
  flex: 1;
`;

export const GameStatus = styled(Overline)`
  flex: 0 0 auto;
  margin-left: 0.5em;
`;

export const StyledScore = styled(Score)`
  font-size: ${typography.overline.fontSize.normal};
`;
