import { mediaBreakpoints } from '@kertPark/config/theme';
import { GameType } from '@kertPark/graphql/generated/helpers';
import React from 'react';
import styled from 'styled-components';

const getGameBroadcasterLogo = (game: GameType) => {
  if (game.metadata?.liveVideo?.type) {
    switch (game.metadata.liveVideo.type) {
      case 'ceskatelevize':
        return '/media/ctsport.png';
      case 'o2tvsport':
        return '/media/o2tv.png';
      default: {
        return undefined;
      }
    }
  }

  return undefined;
};

const Img = styled.img<Pick<Props, 'size'>>`
  max-height: ${({ size }) => size}em;

  @media (${mediaBreakpoints.md}) {
    max-width: 3.5em;
  }

  @media (${mediaBreakpoints.lg}) {
    max-width: initial;
  }
`;

type Props = {
  game: GameType;
  size: 1 | 0.75;
};

export const LiveVideoBroadcaster = ({ game, size }: Props) => {
  const gameBroadcasterLogo = getGameBroadcasterLogo(game);

  if (gameBroadcasterLogo) {
    return <Img size={size} src={gameBroadcasterLogo} alt="Poskytovatel video přenosu" />;
  }

  return null;
};
