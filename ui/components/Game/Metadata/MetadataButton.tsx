import { LinkButton } from '@kertPark/components/Atomic/Button';
import { mediaBreakpoints, typography } from '@kertPark/config/theme';
import styled, { css } from 'styled-components';

export type MetadataButtonProps = {
  size: 'small' | 'large';
};

export const MetadataButton = styled(LinkButton)<MetadataButtonProps>`
  cursor: pointer;
  color: ${({ theme }) => theme.colors.textPrimary};
  background-color: ${({ theme }) => theme.colors.lightGrey};
  margin: 0;
  padding: 0.25em 0.5em;
  ${({ size }) => {
    if (size === 'small') {
      return css`
        font-size: ${typography.overline.fontSize.small};
        @media (${mediaBreakpoints.md}) {
          font-size: 0.625rem;
        }
        @media (${mediaBreakpoints.lg}) {
          font-size: ${typography.overline.fontSize.small};
        }
      `;
    }
    return css`
      font-size: ${typography.overline.fontSize.normal};
    `;
  }}

  &:focus,
  &:active,
  &:visited {
    color: ${({ theme }) => theme.colors.textPrimary};
  }
`;
