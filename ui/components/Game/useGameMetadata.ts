import { LinkAnchorProps } from '@kertPark/components/Atomic/LinkAnchor';
import { routes } from '@kertPark/config/routes';
import { GameFilterScheduleEnum } from '@kertPark/graphql/generated';
import { GameType } from '@kertPark/graphql/generated/helpers';
import { GamePlayedStatuses, GameStatus } from '@kertPark/lib/game';

const SEASON_YEAR = process.env.SEASON_YEAR;

export type MetadataItem = {
  label: string;
} & Pick<LinkAnchorProps, 'href' | 'hrefAs' | 'target'>;

type Props = {
  game: GameType;
  scheduleVariant?: GameFilterScheduleEnum;
};

export const useGameMetadata = ({ game, scheduleVariant = GameFilterScheduleEnum.ALL }: Props): Array<MetadataItem> => {
  const isGamePostponed = game.status === GameStatus.od;

  if (isGamePostponed) {
    return [];
  }

  const isGamePlayed = GamePlayedStatuses.includes(game.status as GameStatus);
  const onlajnyCz: Array<MetadataItem> = game.metadata?.onlajnyCz
    ? [{ label: 'Onlajny.cz', href: game.metadata.onlajnyCz, target: '_blank' }]
    : [];
  const liveVideo: Array<MetadataItem> = game.metadata?.liveVideo
    ? [{ label: 'Video přenos', href: game.metadata.liveVideo.url, target: '_blank' }]
    : [];

  if (isGamePlayed && [GameFilterScheduleEnum.ALL, GameFilterScheduleEnum.RESULT].includes(scheduleVariant)) {
    return [
      ...(game.metadata?.reportPostSlug
        ? [{ label: 'Reportáž', href: routes.post, hrefAs: routes.postAs(game.metadata.reportPostSlug) }]
        : [
            {
              // TODO: label: game.metadata?.hasAiReport ? 'AI Report' : 'Detail',
              label: 'Detail',
              href: routes.team.gameDetail,
              hrefAs: routes.team.gamesDetailAs(game.team.systemName, SEASON_YEAR || '', game.id),
            },
          ]),
      ...(game.metadata?.highlightsVideo
        ? [{ label: 'Sestřih', href: game.metadata.highlightsVideo.url, target: '_blank' }]
        : []),
      ...liveVideo,
      ...onlajnyCz,
    ];
  }

  return [...liveVideo, ...onlajnyCz];
};
