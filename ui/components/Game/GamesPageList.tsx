import { Card, CardDesktop, CardMobile } from '@kertPark/components/Atomic/Card';
import { Overline } from '@kertPark/components/Atomic/Typography';
import { GameListItem } from '@kertPark/components/Game/GameListItem/GameListItem';
import { GameOverview } from '@kertPark/components/Game/GameOverview';
import { SmallGameOverview } from '@kertPark/components/Game/SmallGameOverview';
import { GameFilterPlaceEnum, GameFilterScheduleEnum, GameListQuery } from '@kertPark/graphql/generated';
import { filterAndSortGames, GameStatus, groupGamesByLabel } from '@kertPark/lib/game';
import { UiScheduleEnum } from '@kertPark/lib/gamePage';
import { TeamProps } from '@kertPark/lib/team';
import { Box } from '@rebass/grid';
import React, { useMemo } from 'react';

const getNoGameText = ({
  filterPlace,
  filterSchedule,
}: {
  filterPlace: GameFilterPlaceEnum;
  filterSchedule: GameFilterScheduleEnum | UiScheduleEnum;
}): string => {
  switch (filterSchedule) {
    case GameFilterScheduleEnum.ALL: {
      switch (filterPlace) {
        case GameFilterPlaceEnum.AWAY:
          return 'Tým momentálně nemá žádná venkovní utkání';
        case GameFilterPlaceEnum.HOME:
          return 'Tým momentálně nemá žádná domácí utkání';
        case GameFilterPlaceEnum.HOME_GROUND:
          return 'Tým momentálně nemá žádná utkání na Lužinách';
        case GameFilterPlaceEnum.ALL:
        default:
          return 'Tým momentálně nemá žádná utkání';
      }
    }
    case GameFilterScheduleEnum.RESULT: {
      switch (filterPlace) {
        case GameFilterPlaceEnum.AWAY:
          return 'Tým neodehrál žádná venkovní utkání';
        case GameFilterPlaceEnum.HOME:
          return 'Tým neodehrál žádná domácí utkání';
        case GameFilterPlaceEnum.HOME_GROUND:
          return 'Tým neodehrál žádná utkání na Lužinách';
        case GameFilterPlaceEnum.ALL:
        default:
          return 'Tým neodehrál žádná utkání';
      }
    }
    case GameFilterScheduleEnum.UPCOMING: {
      switch (filterPlace) {
        case GameFilterPlaceEnum.AWAY:
          return 'Tým momentálně nemá žádná nadcházející venkovní utkání';
        case GameFilterPlaceEnum.HOME:
          return 'Tým momentálně nemá žádná nadcházející domácí utkání';
        case GameFilterPlaceEnum.HOME_GROUND:
          return 'Tým momentálně nemá žádná nadcházející utkání na Lužinách';
        case GameFilterPlaceEnum.ALL:
        default:
          return 'Tým momentálně nemá žádná nadcházející utkání';
      }
    }
  }
  return 'Tým momentálně nemá žádná nadcházející utkání';
};

type Props = TeamProps & {
  filterPlace: GameFilterPlaceEnum;
  filterSchedule: GameFilterScheduleEnum | UiScheduleEnum;
  games: GameListQuery['gameList'];
};

export const GamesPageList: React.FC<Props> = ({ filterPlace, filterSchedule, games, team }) => {
  const { gamesBeforeHighlightedGame, highlightedGame, gamesAfterHighlightedGame } = useMemo(() => {
    const allGames = filterAndSortGames({ filterPlace, filterSchedule, games });
    const highlightedGameIndex =
      [GameFilterScheduleEnum.RESULT, GameFilterScheduleEnum.UPCOMING].includes(
        filterSchedule as GameFilterScheduleEnum,
      ) && allGames.length > 0
        ? allGames.findIndex((game) => game.status !== GameStatus.od)
        : -1;
    const highlightedGame = highlightedGameIndex !== -1 ? allGames[highlightedGameIndex] : undefined;

    return {
      gamesBeforeHighlightedGame: groupGamesByLabel(highlightedGame ? allGames.slice(0, highlightedGameIndex) : []),
      highlightedGame,
      gamesAfterHighlightedGame: groupGamesByLabel(
        highlightedGame ? allGames.slice(highlightedGameIndex + 1) : allGames,
      ),
    };
  }, [filterPlace, filterSchedule, games]);

  return (
    <>
      {gamesBeforeHighlightedGame.length > 0 &&
        gamesBeforeHighlightedGame.map((group, index) => (
          <Box key={`${group.label}_${index}`} width={1} p={2}>
            <Card>
              <Overline>{group.label}</Overline>
              {group.games.map((game) => (
                <GameListItem key={game.id} game={game} hasLargeMetadata={team?.id === 'muzia'} />
              ))}
            </Card>
          </Box>
        ))}
      {highlightedGame && (
        <Box width={1} p={2}>
          <CardDesktop>
            <GameOverview game={highlightedGame} withMetadata />
          </CardDesktop>
          <CardMobile>
            <SmallGameOverview game={highlightedGame} withMetadata />
          </CardMobile>
        </Box>
      )}
      {gamesAfterHighlightedGame.length > 0 &&
        gamesAfterHighlightedGame.map((group, index) => (
          <Box key={`${group.label}_${index}`} width={1} p={2}>
            <Card>
              <Overline>{group.label}</Overline>
              {group.games.map((game) => (
                <GameListItem key={game.id} game={game} hasLargeMetadata={team?.id === 'muzia'} />
              ))}
            </Card>
          </Box>
        ))}
      {gamesBeforeHighlightedGame.length === 0 && !highlightedGame && gamesAfterHighlightedGame.length === 0 && (
        <Card noBackground>{getNoGameText({ filterPlace, filterSchedule })}</Card>
      )}
    </>
  );
};
