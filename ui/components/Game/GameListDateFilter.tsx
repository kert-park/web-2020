import { LinkButton } from '@kertPark/components/Atomic/Button';
import { Overline } from '@kertPark/components/Atomic/Typography';
import { getRouteObjectWithQuery, QueryParams, routes } from '@kertPark/config/routes';
import { GameFilterPlaceEnum } from '@kertPark/graphql/generated';
import { getReadableDate } from '@kertPark/lib/date';
import { getDateRange, getTextFilterPlace } from '@kertPark/lib/gamePage';
import { Flex } from '@rebass/grid';
import { addDays, format, subDays } from 'date-fns';
import React from 'react';
import styled from 'styled-components';

const StyledLinkButton = styled(LinkButton)`
  margin: 0.5rem 0.5rem 0 0;
`;
const getRouteWithQuery = ({ date, filterPlace }: Props) => {
  return getRouteObjectWithQuery({
    pathname: routes.clubAllGames,
    pathnameAs: routes.clubAllGamesAs,
    query: {
      [QueryParams.place]: getTextFilterPlace(filterPlace),
      [QueryParams.date]: format(date, 'yyyy-MM-dd'),
    },
  });
};

type Props = {
  date: Date;
  filterPlace: GameFilterPlaceEnum;
};

export const GameListDateFilter: React.FC<Props> = ({ date, filterPlace }) => {
  const prev = getRouteWithQuery({ date: subDays(date, 7), filterPlace });
  const next = getRouteWithQuery({ date: addDays(date, 7), filterPlace });
  const { from, to } = getDateRange(date);

  return (
    <Flex flex="1" flexDirection="column" flexWrap="wrap">
      <Overline>
        Týden {getReadableDate(from)} - {getReadableDate(to)}
      </Overline>
      <Flex flex="1" flexDirection="row" flexWrap="wrap">
        <StyledLinkButton href={prev.link} hrefAs={prev.linkAs}>
          &larr; Předchozí
        </StyledLinkButton>
        <StyledLinkButton href={next.link} hrefAs={next.linkAs}>
          Další &rarr;
        </StyledLinkButton>
      </Flex>
    </Flex>
  );
};
