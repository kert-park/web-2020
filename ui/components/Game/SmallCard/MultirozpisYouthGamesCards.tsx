import { H2, Overline } from '@kertPark/components/Atomic/Typography';
import { GameCard } from '@kertPark/components/Game/SmallCard/GameCard';
import { Container, Header } from '@kertPark/components/Game/SmallCard/Styles';
import { TournamentCard } from '@kertPark/components/Game/SmallCard/TournamentCard';
import {
  GameFilterPlaceEnum,
  GameFilterScheduleEnum,
  GameListQuery,
  TournamentListQuery,
} from '@kertPark/graphql/generated';
import { getReadableDate } from '@kertPark/lib/date';
import { filterAndSortGames } from '@kertPark/lib/game';
import { getDateRange, mergeGamesAndTournamentsAndSort } from '@kertPark/lib/gamePage';
import { filterTournaments } from '@kertPark/lib/tournament';
import { Box } from '@rebass/grid';
import React, { useMemo } from 'react';

type Props = {
  date: Date;
  games: GameListQuery['gameList'];
  tournaments: TournamentListQuery['tournamentList'];
};

export const MultirozpisYouthGamesCards: React.FC<Props> = ({ date, games, tournaments }) => {
  const { from, to } = getDateRange(date);

  const allItems = useMemo(() => {
    return mergeGamesAndTournamentsAndSort(
      filterAndSortGames({
        filterPlace: GameFilterPlaceEnum.ALL,
        filterSchedule: GameFilterScheduleEnum.ALL,
        games: games.filter((game) => game.team.systemName !== 'muzia'),
      }),
      filterTournaments({ filterPlace: GameFilterPlaceEnum.ALL, tournaments }),
    );
  }, [games, tournaments]);

  if (allItems.length === 0) {
    return null;
  }

  return (
    <Box width={1} p={2}>
      <Header>
        <H2 margin="0">Multirozpis mládeže</H2>
        <Overline>
          Týden {getReadableDate(from)} - {getReadableDate(to)}
        </Overline>
      </Header>
      <Container>
        {allItems.map((item) =>
          item.__typename === 'Tournament' ? (
            <TournamentCard key={item.id} tournament={item} />
          ) : (
            <GameCard key={item.id} game={item} />
          ),
        )}
      </Container>
    </Box>
  );
};
