import { Card } from '@kertPark/components/Atomic/Card';
import { StaticPlaceLogo } from '@kertPark/components/Atomic/StaticImage';
import { Overline } from '@kertPark/components/Atomic/Typography';
import { mediaBreakpoints, spaces, typography } from '@kertPark/config/theme';
import { Tournament } from '@kertPark/graphql/generated';
import { getReadableDate } from '@kertPark/lib/date';
import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components';

const LogoAndNameWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  flex: 1;
  width: 100%;
  margin: ${spaces[2]} 0 ${spaces[1]};
`;

const Logo = styled(StaticPlaceLogo)`
  width: 4.5em;
  height: 4.5em;
  margin-bottom: ${spaces[2]};

  @media (${mediaBreakpoints.md}) {
    width: 3.25em;
    height: 3.25em;
  }

  @media (${mediaBreakpoints.xl}) {
    width: 4.5em;
    height: 4.5em;
  }
`;

const Name = styled(Overline)`
  display: flex;
  flex: 1;
  align-items: center;
  color: ${({ theme }) => theme.colors.textPrimary};
  font-size: ${typography.overline.fontSize.normal};
  text-align: center;

  @media (${mediaBreakpoints.md}) {
    font-size: ${typography.overline.fontSize.small};
  }

  @media (${mediaBreakpoints.xl}) {
    font-size: ${typography.overline.fontSize.normal};
  }
`;

type Props = {
  tournament: Tournament;
};

export const TournamentCard: React.FC<Props> = ({ tournament }) => {
  const kertTheme = useContext(ThemeContext);

  return (
    <Card>
      <Overline color={kertTheme.colors.textPrimary} small textAlign="center">
        {tournament.team.name}
      </Overline>
      <Overline small textAlign="center">
        {getReadableDate(tournament.date, false, true)}
      </Overline>
      <LogoAndNameWrapper>
        {tournament.place?.systemName && <Logo id={tournament.place.systemName} title={tournament.place.name} />}
        <Name as="div" textTransform="none">
          {tournament.place?.name || 'Místo není známo'}
        </Name>
      </LogoAndNameWrapper>
      <Overline small textAlign="center">
        Turnaj
      </Overline>
    </Card>
  );
};
