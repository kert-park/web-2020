import { Card } from '@kertPark/components/Atomic/Card';
import { Score } from '@kertPark/components/Atomic/Score';
import { StaticTeamLogo } from '@kertPark/components/Atomic/StaticImage';
import { Overline } from '@kertPark/components/Atomic/Typography';
import { mediaBreakpoints, spaces, typography } from '@kertPark/config/theme';
import { GameType } from '@kertPark/graphql/generated/helpers';
import { getReadableDate, getReadableTime } from '@kertPark/lib/date';
import {
  GameOtherStatuses,
  GamePlayedStatuses,
  GameStatus as GameStatusType,
  GameStatusName,
} from '@kertPark/lib/game';
import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components';

const TeamWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  flex: 1;
  width: 100%;
  margin: ${spaces[2]} 0 ${spaces[1]};
`;

const Logo = styled(StaticTeamLogo)`
  width: 4.5em;
  height: 4.5em;
  margin-bottom: ${spaces[2]};

  @media (${mediaBreakpoints.md}) {
    width: 3.25em;
    height: 3.25em;
  }

  @media (${mediaBreakpoints.xl}) {
    width: 4.5em;
    height: 4.5em;
  }
`;

const TeamName = styled(Overline)`
  display: flex;
  flex: 1;
  align-items: center;
  color: ${({ theme }) => theme.colors.textPrimary};
  font-size: ${typography.overline.fontSize.normal};
  text-align: center;

  @media (${mediaBreakpoints.md}) {
    font-size: ${typography.overline.fontSize.small};
  }

  @media (${mediaBreakpoints.xl}) {
    font-size: ${typography.overline.fontSize.normal};
  }
`;

export const StyledScore = styled(Score)`
  font-size: ${typography.overline.fontSize.normal};
  text-align: center;
`;

type Props = {
  game: GameType;
  showUpcomingOnly?: boolean;
};

export const GameCard: React.FC<Props> = ({ game, showUpcomingOnly = false }) => {
  const kertTheme = useContext(ThemeContext);

  const isHomeTeam = game.home.cmshbId === '3100003';
  const opponent = isHomeTeam ? game.away : game.home;

  return (
    <Card>
      <Overline color={kertTheme.colors.textPrimary} small textAlign="center">
        {game.team.name}
      </Overline>
      <Overline small textAlign="center">
        {getReadableDate(game.date, false, true)}
      </Overline>
      <TeamWrapper>
        <Logo id={opponent.cmshbId || ''} title={opponent.name} />
        <TeamName as="div" textTransform="none">
          {opponent.shortName}
        </TeamName>
      </TeamWrapper>
      {!showUpcomingOnly && GamePlayedStatuses.includes(game.status as GameStatusType) ? (
        <StyledScore game={game} swap={!isHomeTeam} />
      ) : GameOtherStatuses.includes(game.status as GameStatusType) ? (
        <Overline textAlign="center">{GameStatusName[game.status as GameStatusType]}</Overline>
      ) : (
        <Overline textAlign="center">{getReadableTime(game.date)}</Overline>
      )}
    </Card>
  );
};
