import { H2 } from '@kertPark/components/Atomic/Typography';
import { GameCard } from '@kertPark/components/Game/SmallCard/GameCard';
import { Container, Header } from '@kertPark/components/Game/SmallCard/Styles';
import { GameListQuery } from '@kertPark/graphql/generated';
import { getDateObject } from '@kertPark/lib/date';
import { Box } from '@rebass/grid';
import { compareAsc, isAfter, startOfDay } from 'date-fns';
import React from 'react';

type Props = {
  games: GameListQuery['gameList'];
};

export const NearestHomeGamesCards: React.FC<Props> = ({ games }) => {
  const today = startOfDay(new Date());
  const allGames = games
    .filter((game) => isAfter(getDateObject(game.date), today))
    .sort((a, b) => compareAsc(getDateObject(a.date), getDateObject(b.date)));

  if (allGames.length === 0) {
    return null;
  }

  return (
    <Box width={1} p={2}>
      <Header>
        <H2 margin="0">Utkání na Lužinách</H2>
      </Header>
      <Container>
        {allGames.map((game) => (
          <GameCard key={game.id} game={game} showUpcomingOnly />
        ))}
      </Container>
    </Box>
  );
};
