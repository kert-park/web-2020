import { mediaBreakpoints, spaces, theme } from '@kertPark/config/theme';
import styled from 'styled-components';

export const Container = styled.div`
  display: grid;
  grid-template-columns: repeat(3, minmax(0, 1fr));
  gap: ${spaces[3]};

  @media (${mediaBreakpoints.md}) {
    grid-template-columns: repeat(2, minmax(0, 1fr));
  }

  @media (${mediaBreakpoints.xl}) {
    grid-template-columns: repeat(3, minmax(0, 1fr));
  }
`;

export const Header = styled.div`
  display: flex;
  flex-direction: column;
  margin: ${spaces[4]} 0 ${spaces[2]} ${theme.spacing.card};
`;
