import { ContentLoader, Rect } from '@kertPark/components/Atomic/ContentLoader';
import { Score } from '@kertPark/components/Atomic/Score';
import { StaticTeamLogo } from '@kertPark/components/Atomic/StaticImage';
import { Overline } from '@kertPark/components/Atomic/Typography';
import { GameOverviewMetadata } from '@kertPark/components/Game/GameOverviewMetadata';
import { LiveVideoBroadcaster } from '@kertPark/components/Game/Metadata/LiveVideoBroadcaster';
import { spaces, theme } from '@kertPark/config/theme';
import { GameType } from '@kertPark/graphql/generated/helpers';
import { getReadableDate } from '@kertPark/lib/date';
import { GameOtherStatuses, GamePlayedStatuses, GameStatus, GameStatusName } from '@kertPark/lib/game';
import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const TeamsAndResultWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: stretch;
  justify-content: center;
  flex: 1;
  width: 100%;
  margin-top: ${spaces[3]};
`;

const TeamWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  flex: 1;
  width: 100%;
`;

const Logo = styled(StaticTeamLogo)`
  width: 6.25em;
  height: 6.25em;
  margin-bottom: ${spaces[2]};
`;

const TeamName = styled.div`
  color: ${({ theme }) => theme.colors.textPrimary};
  font-family: ${theme.fonts.header};
  font-weight: bold;
  font-size: 1rem;
  text-align: center;
`;

const ResultWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  flex: 0 0 auto;
  min-width: 4em;

  > * {
    flex: 0 0 auto;
  }

  > *:first-child {
    display: flex;
    flex: 1;
    align-items: center;
    justify-content: center;
  }
`;

const StyledScore = styled(Score)`
  font-size: 2.25rem;
`;

const StyledStatus = styled(Overline)`
  font-size: 1.5rem;
`;

const Versus = styled.div`
  color: ${({ theme }) => theme.colors.textLight};
  font-family: ${theme.fonts.header};
  font-weight: bold;
  font-size: 2.25rem;
`;

type Props = {
  game: GameType;
  withMetadata?: boolean;
};

export const GameOverview = ({ game, withMetadata }: Props) => {
  const kertTheme = useContext(ThemeContext);

  return (
    <Container>
      <Overline color={kertTheme.colors.secondary} textAlign="center">
        {game.label}
      </Overline>
      <Overline small textAlign="center">
        {getReadableDate(game.date, true, true)}, {game.place}
      </Overline>
      <TeamsAndResultWrapper>
        <TeamWrapper>
          <Logo id={game.home.cmshbId || ''} title={game.home.name} />
          <TeamName>{game.home.name}</TeamName>
        </TeamWrapper>
        <ResultWrapper>
          {GamePlayedStatuses.includes(game.status as GameStatus) ? (
            <StyledScore game={game} />
          ) : GameOtherStatuses.includes(game.status as GameStatus) ? (
            <StyledStatus>{GameStatusName[game.status as GameStatus]}</StyledStatus>
          ) : (
            <Versus>vs</Versus>
          )}
          {withMetadata && <LiveVideoBroadcaster game={game} size={1} />}
        </ResultWrapper>
        <TeamWrapper>
          <Logo id={game.away.cmshbId || ''} title={game.away.name} />
          <TeamName>{game.away.name}</TeamName>
        </TeamWrapper>
      </TeamsAndResultWrapper>
      {withMetadata && <GameOverviewMetadata game={game} size="large" />}
    </Container>
  );
};

export const GameOverviewLoader: React.FC = () => {
  const w = 600;
  const h = 170;
  return (
    <ContentLoader viewBox={`0 0 ${w} ${h}`} style={{ maxWidth: w }}>
      <Rect x={(w - 80) / 2} y="2" width="80" height="12" />
      <Rect x={(w - 160) / 2} y="18" width="160" height="10" />
      <Rect x={(w - 34) / 2} y="95" width="32" height="24" />
      <circle cx={86 + 50} cy={44 + 50} r="50" />
      <Rect x={66} y={h - (16 + 2)} width="140" height="16" />
      <circle cx={w - (86 + 50)} cy={44 + 50} r="50" />
      <Rect x={w - 140 - 66} y={h - (16 + 2)} width="140" height="16" />
    </ContentLoader>
  );
};
