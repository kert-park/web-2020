import { Card } from '@kertPark/components/Atomic/Card';
import { SmallGameOverview, SmallGameOverviewLoader } from '@kertPark/components/Game/SmallGameOverview';
import { GameFilterPlaceEnum, GameFilterScheduleEnum, useGameListQuery } from '@kertPark/graphql/generated';
import { GameType } from '@kertPark/graphql/generated/helpers';
import { Box } from '@rebass/grid';
import React from 'react';

type Props = {
  games?: Array<GameType>;
  limit?: 1 | 2;
  teamId?: string;
};

export const NearestGames = ({ games: propGames, limit = 2, teamId = 'muzia' }: Props) => {
  const { data, loading } = useGameListQuery({
    variables: {
      teamId,
      filter: {
        place: GameFilterPlaceEnum.ALL,
        schedule: GameFilterScheduleEnum.ALL,
        nearest: true,
        limit: 2,
      },
    },
  });

  if (loading) {
    const placeholders: Array<React.ReactNode> = [];

    for (let i = 0; i < limit; i++) {
      placeholders.push(
        <Box key={`nearest_game_loader_${i}`} width={1} p={2}>
          <Card alignItems="center">
            <SmallGameOverviewLoader />
          </Card>
        </Box>,
      );
    }

    return <>{placeholders}</>;
  }

  const games = (data?.gameList || propGames || []).slice(0, limit);

  return (
    <>
      {games.map((game) => (
        <Box key={game.id} width={1} p={2}>
          <Card>
            <SmallGameOverview key={game.id} game={game} withMetadata />
          </Card>
        </Box>
      ))}
    </>
  );
};
