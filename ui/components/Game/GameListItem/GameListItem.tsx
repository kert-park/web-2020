import { ContentLoader, Rect } from '@kertPark/components/Atomic/ContentLoader';
import { LinkAnchor } from '@kertPark/components/Atomic/LinkAnchor';
import { Score } from '@kertPark/components/Atomic/Score';
import { DesktopMetadata } from '@kertPark/components/Game/GameListItem/DesktopMetadata';
import { Logos } from '@kertPark/components/Game/GameListItem/Logos';
import { MobileMetadata } from '@kertPark/components/Game/GameListItem/MobileMetadata';
import { TeamNames } from '@kertPark/components/Game/GameListItem/TeamNames';
import { LiveVideoBroadcaster } from '@kertPark/components/Game/Metadata/LiveVideoBroadcaster';
import { useGameMetadata } from '@kertPark/components/Game/useGameMetadata';
import { ItemInfo, ItemInfoWrapper, ItemWrapper } from '@kertPark/components/ListItem/Styles';
import { mediaBreakpoints, theme } from '@kertPark/config/theme';
import { GameFilterScheduleEnum } from '@kertPark/graphql/generated';
import { GameType } from '@kertPark/graphql/generated/helpers';
import { getReadableDate, getReadableTime } from '@kertPark/lib/date';
import {
  GameOtherStatuses,
  GamePlayedStatuses,
  GameStatus as GameStatusType,
  GameStatusName,
} from '@kertPark/lib/game';
import React from 'react';
import styled, { css } from 'styled-components';

const ScoreAndMetadataWrapper = styled.div<Pick<Props, 'hasLargeMetadata'>>`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  flex: 0 0 auto;
  gap: 1rem;

  ${({ hasLargeMetadata }) =>
    hasLargeMetadata &&
    css`
      @media (${mediaBreakpoints.md}) {
        width: 12em;
      }
      @media (${mediaBreakpoints.lg}) {
        width: 14em;
      }
      @media (${mediaBreakpoints.xl}) {
        width: 18em;
      }
      @media (${mediaBreakpoints.xxl}) {
        width: 23em;
      }
    `}
`;

const GameStatus = styled.div`
  flex: 0 0 auto;
  font-family: ${theme.fonts.header};
  color: ${({ theme }) => theme.colors.textSecondary};
  font-size: 1rem;
  font-weight: bold;

  @media (${mediaBreakpoints.md}) {
    font-size: 1.25rem;
  }
`;

const StyledAnchor = styled(LinkAnchor)``;

const StyledScore = styled(Score)`
  flex: 0 0 auto;
  font-size: 1.25rem;

  ${StyledAnchor}:hover & {
    text-decoration: underline;
  }

  @media (${mediaBreakpoints.md}) {
    font-size: 1.625rem;
  }
`;

type Props = {
  game: GameType;
  hasLargeMetadata: boolean;
  scheduleVariant?: GameFilterScheduleEnum;
};

export const GameListItem = ({ game, hasLargeMetadata, scheduleVariant = GameFilterScheduleEnum.ALL }: Props) => {
  const metadataItems = useGameMetadata({ game, scheduleVariant });
  return (
    <ItemWrapper>
      <Logos game={game} />
      <ItemInfoWrapper>
        <ItemInfo>
          {getReadableDate(game.date, false, true)}, {game.place}
          <LiveVideoBroadcaster game={game} size={0.75} />
        </ItemInfo>
        <TeamNames game={game} />
      </ItemInfoWrapper>
      <ScoreAndMetadataWrapper hasLargeMetadata={hasLargeMetadata && metadataItems.length > 1}>
        <DesktopMetadata metadataItems={metadataItems} />
        <MobileMetadata metadataItems={metadataItems} />
        {scheduleVariant === GameFilterScheduleEnum.ALL &&
        GamePlayedStatuses.includes(game.status as GameStatusType) ? (
          <StyledScore game={game} />
        ) : scheduleVariant === GameFilterScheduleEnum.ALL &&
          GameOtherStatuses.includes(game.status as GameStatusType) ? (
          <GameStatus>{GameStatusName[game.status as GameStatusType]}</GameStatus>
        ) : scheduleVariant === GameFilterScheduleEnum.UPCOMING && game.status === GameStatusType.od ? (
          <GameStatus>{GameStatusName.od}</GameStatus>
        ) : (
          <GameStatus>{getReadableTime(game.date)}</GameStatus>
        )}
      </ScoreAndMetadataWrapper>
    </ItemWrapper>
  );
};

export const GameListItemLoader = () => {
  const r = 25;
  const w = 560;
  return (
    <ContentLoader viewBox={`0 0 ${w} 66`} style={{ maxWidth: w }}>
      <circle cx={r} cy={r + 8} r={r} />
      <circle cx={2 * r + 8 + r} cy={r + 8} r={r} />
      <Rect x="116" y="14" width="200" height="12" />
      <Rect x="116" y="33" width="444" height="16" />
    </ContentLoader>
  );
};
