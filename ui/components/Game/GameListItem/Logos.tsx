import { StaticTeamLogo } from '@kertPark/components/Atomic/StaticImage';
import { itemLogoStyle } from '@kertPark/components/ListItem/Styles';
import { mediaBreakpoints } from '@kertPark/config/theme';
import { GameType } from '@kertPark/graphql/generated/helpers';
import React from 'react';
import styled from 'styled-components';

const TeamLogo = styled(StaticTeamLogo)`
  ${itemLogoStyle}
`;

const TeamLogoMobile = styled(TeamLogo)`
  @media (${mediaBreakpoints.md}) {
    display: none;
  }
`;

const TeamLogoDesktop = styled(TeamLogo)`
  display: none;

  @media (${mediaBreakpoints.md}) {
    display: inherit;
  }
`;

type Props = {
  game: GameType;
};

export const Logos: React.FC<Props> = ({ game }) => {
  return (
    <>
      <TeamLogoDesktop id={game.home.cmshbId || ''} title={game.home.name} />
      <TeamLogoDesktop id={game.away.cmshbId || ''} title={game.away.name} />
      {game.home.cmshbId !== '3100003' && <TeamLogoMobile id={game.home.cmshbId || ''} title={game.home.name} />}
      {game.away.cmshbId !== '3100003' && <TeamLogoMobile id={game.away.cmshbId || ''} title={game.away.name} />}
    </>
  );
};
