import { MetadataButton } from '@kertPark/components/Game/Metadata/MetadataButton';
import { MetadataItem } from '@kertPark/components/Game/useGameMetadata';
import { mediaBreakpoints, spaces } from '@kertPark/config/theme';
import React from 'react';
import styled from 'styled-components';

const Container = styled.div<{ justifyContent: 'flex-start' | 'flex-end' }>`
  display: none;
  @media (${mediaBreakpoints.md}) {
    display: flex;
    flex: 1;
    flex-flow: row wrap;
    gap: ${spaces[1]};
    justify-content: ${({ justifyContent }) => justifyContent};
  }
`;

type Props = {
  metadataItems: Array<MetadataItem>;
};

export const DesktopMetadata = ({ metadataItems }: Props) => {
  if (!metadataItems.length) {
    return null;
  }

  return (
    <Container justifyContent={metadataItems.length > 1 ? 'flex-start' : 'flex-end'}>
      {metadataItems.map((item, index) => (
        <MetadataButton key={index} href={item.href} hrefAs={item.hrefAs} target={item.target} isText size="small">
          {item.label}
        </MetadataButton>
      ))}
    </Container>
  );
};
