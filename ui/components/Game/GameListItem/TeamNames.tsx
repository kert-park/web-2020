import { ItemName } from '@kertPark/components/ListItem/Styles';
import { mediaBreakpoints } from '@kertPark/config/theme';
import { GameType } from '@kertPark/graphql/generated/helpers';
import React from 'react';
import styled from 'styled-components';

const DesktopVisible = styled.span`
  display: none;

  @media (${mediaBreakpoints.md}) {
    display: inline;
  }
`;

type Props = {
  game: GameType;
};

export const TeamNames: React.FC<Props> = ({ game }) => {
  return (
    <ItemName>
      {game.home.cmshbId === '3100003' ? (
        <>
          <DesktopVisible>{game.home.name} - </DesktopVisible>
          <strong>{game.away.name}</strong>
        </>
      ) : (
        <>
          <strong>{game.home.name}</strong>
          <DesktopVisible> - {game.away.name}</DesktopVisible>
        </>
      )}
    </ItemName>
  );
};
