import { LinkAnchor } from '@kertPark/components/Atomic/LinkAnchor';
import { MetadataItem } from '@kertPark/components/Game/useGameMetadata';
import { IconMore } from '@kertPark/components/Icon/IconMore';
import { mediaBreakpoints, zIndex } from '@kertPark/config/theme';
import { useClickAway } from '@kertPark/hooks/useClickAway';
import { useKertTheme } from '@kertPark/hooks/useKertTheme';
import React, { useState } from 'react';
import styled from 'styled-components';

const StyledButton = styled.button`
  background-color: ${({ theme }) => theme.colors.lightGrey};
  margin: 0;
  line-height: 1;
  padding: 0.25rem 0.375rem;
  border: 0;
  border-radius: 0.25em;
  box-shadow: 0 0.25em 1em 0 ${({ theme }) => theme.colors.boxShadow.normal};

  &:focus,
  &:active,
  &:visited {
    outline: none;
    border: 0;
  }
`;

const Wrapper = styled.div`
  display: flex;
  position: relative;
  @media (${mediaBreakpoints.md}) {
    display: none;
  }
`;

const ContextMenu = styled.div`
  position: absolute;
  z-index: ${zIndex.mobileDropdown};
  top: 2rem;
  right: 0;
  display: flex;
  flex-flow: column;
  border-radius: 0.5em;
  background-color: ${({ theme }) => theme.colors.lightGrey};
  width: 8em;
  box-shadow: 0 0.25em 1em 0 ${({ theme }) => theme.colors.boxShadow.hover};
`;

const ContextMenuItem = styled(LinkAnchor)`
  color: ${({ theme }) => theme.colors.textPrimary};
  padding: 0.5em;
  line-height: 1;
  font-weight: bold;
  text-transform: uppercase;
  text-decoration: none;
  &:active,
  &:visited {
    color: ${({ theme }) => theme.colors.textPrimary};
  }
  &:hover {
    color: ${({ theme }) => theme.colors.textPrimary};
  }
  &:not(:first-child) {
    border-top: 1px solid ${({ theme }) => theme.colors.midGrey};
  }
`;

type Props = {
  metadataItems: Array<MetadataItem>;
};

export const MobileMetadata = ({ metadataItems }: Props) => {
  const [isExpanded, setExpanded] = useState(false);
  const { colors } = useKertTheme();
  const ref = useClickAway<HTMLDivElement>(() => setExpanded(false));

  if (!metadataItems.length) {
    return null;
  }

  return (
    <Wrapper ref={ref}>
      <StyledButton onClick={() => setExpanded((prevExpanded) => !prevExpanded)}>
        <IconMore color={colors.textPrimary} size={12} orientation={'horizontal'} />
      </StyledButton>
      {isExpanded && (
        <ContextMenu>
          {metadataItems.map((item, index) => (
            <ContextMenuItem
              key={index}
              href={item.href}
              hrefAs={item.hrefAs}
              target={item.target}
              isText
              onClick={() => setExpanded(false)}
            >
              {item.label}
            </ContextMenuItem>
          ))}
        </ContextMenu>
      )}
    </Wrapper>
  );
};
