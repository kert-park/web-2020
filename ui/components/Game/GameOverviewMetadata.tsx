import { MetadataButton, MetadataButtonProps } from '@kertPark/components/Game/Metadata/MetadataButton';
import { useGameMetadata } from '@kertPark/components/Game/useGameMetadata';
import { spaces } from '@kertPark/config/theme';
import { GameType } from '@kertPark/graphql/generated/helpers';
import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-flow: row wrap;
  margin-top: ${spaces[3]};
  gap: ${spaces[1]};
`;

type Props = {
  game: GameType;
} & MetadataButtonProps;

export const GameOverviewMetadata = ({ game, size }: Props) => {
  const items = useGameMetadata({ game });

  if (!items.length) {
    return null;
  }

  return (
    <Container>
      {items.map((item, index) => (
        <MetadataButton key={index} href={item.href} hrefAs={item.hrefAs} target={item.target} isText size={size}>
          {item.label}
        </MetadataButton>
      ))}
    </Container>
  );
};
