import { ContentLoader, Rect } from '@kertPark/components/Atomic/ContentLoader';
import { Score } from '@kertPark/components/Atomic/Score';
import { StaticTeamLogo } from '@kertPark/components/Atomic/StaticImage';
import { Overline } from '@kertPark/components/Atomic/Typography';
import { GameOverviewMetadata } from '@kertPark/components/Game/GameOverviewMetadata';
import { LiveVideoBroadcaster } from '@kertPark/components/Game/Metadata/LiveVideoBroadcaster';
import { mediaBreakpoints, spaces, theme, typography } from '@kertPark/config/theme';
import { GameType } from '@kertPark/graphql/generated/helpers';
import { getReadableDate } from '@kertPark/lib/date';
import { GameOtherStatuses, GamePlayedStatuses, GameStatus, GameStatusName } from '@kertPark/lib/game';
import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const TeamsAndResultWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: stretch;
  justify-content: center;
  flex: 1;
  width: 100%;
  margin-top: ${spaces[3]};
`;

const TeamWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  flex: 1;
  width: 100%;
`;

const Logo = styled(StaticTeamLogo)`
  width: 4.5em;
  height: 4.5em;
  margin-bottom: ${spaces[2]};

  @media (${mediaBreakpoints.md}) {
    width: 3.25em;
    height: 3.25em;
  }

  @media (${mediaBreakpoints.lg}) {
    width: 4.5em;
    height: 4.5em;
  }
`;

const TeamName = styled(Overline)`
  color: ${({ theme }) => theme.colors.textPrimary};
  font-size: ${typography.overline.fontSize.normal};
  text-align: center;

  @media (${mediaBreakpoints.md}) {
    font-size: ${typography.overline.fontSize.small};
  }

  @media (${mediaBreakpoints.lg}) {
    font-size: ${typography.overline.fontSize.normal};
  }
`;

const ResultWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  flex: 0 0 auto;
  min-width: 4em;

  > * {
    flex: 0 0 auto;
  }

  > *:first-child {
    display: flex;
    flex: 1;
    align-items: center;
    justify-content: center;
  }
`;

const StyledScore = styled(Score)`
  @media (${mediaBreakpoints.md}) {
    font-size: 1.25rem;
  }

  @media (${mediaBreakpoints.lg}) {
    font-size: 1.625rem;
  }
`;

const Versus = styled.div`
  color: ${({ theme }) => theme.colors.textLight};
  font-family: ${theme.fonts.header};
  font-weight: bold;
  font-size: 1.75rem;

  @media (${mediaBreakpoints.md}) {
    font-size: 1.25rem;
  }

  @media (${mediaBreakpoints.lg}) {
    font-size: 1.75rem;
  }
`;

type Props = {
  game: GameType;
  withMetadata?: boolean;
};

export const SmallGameOverview = ({ game, withMetadata = false }: Props) => {
  const kertTheme = useContext(ThemeContext);

  return (
    <Container>
      <Overline color={kertTheme.colors.secondary} textAlign="center">
        {game.label}
      </Overline>
      <Overline small textAlign="center">
        {getReadableDate(game.date, true, true)}, {game.place}
      </Overline>
      <TeamsAndResultWrapper>
        <TeamWrapper>
          <Logo id={game.home.cmshbId || ''} title={game.home.name} />
          <TeamName>{game.home.shortName}</TeamName>
        </TeamWrapper>
        <ResultWrapper>
          {GamePlayedStatuses.includes(game.status as GameStatus) ? (
            <StyledScore game={game} />
          ) : GameOtherStatuses.includes(game.status as GameStatus) ? (
            <Overline small>{GameStatusName[game.status as GameStatus]}</Overline>
          ) : (
            <Versus>vs</Versus>
          )}
          {withMetadata && <LiveVideoBroadcaster game={game} size={1} />}
        </ResultWrapper>
        <TeamWrapper>
          <Logo id={game.away.cmshbId || ''} title={game.away.name} />
          <TeamName>{game.away.shortName}</TeamName>
        </TeamWrapper>
      </TeamsAndResultWrapper>
      {withMetadata && <GameOverviewMetadata game={game} size="small" />}
    </Container>
  );
};

export const SmallGameOverviewLoader: React.FC = () => {
  const w = 340;
  const h = 140;
  return (
    <ContentLoader viewBox={`0 0 ${w} ${h}`} style={{ maxWidth: w }}>
      <Rect x={(w - 80) / 2} y="2" width="80" height="12" />
      <Rect x={(w - 160) / 2} y="18" width="160" height="10" />
      <Rect x={(w - 24) / 2} y="84" width="24" height="18" />
      <circle cx={36 + 36} cy={44 + 36} r="36" />
      <Rect x={22} y={h - (12 + 2)} width="100" height="12" />
      <circle cx={340 - (36 + 36)} cy={44 + 36} r="36" />
      <Rect x={w - 100 - 22} y={h - (12 + 2)} width="100" height="12" />
    </ContentLoader>
  );
};
