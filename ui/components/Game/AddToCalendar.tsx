import { Button } from '@kertPark/components/Atomic/Button';
import { Card } from '@kertPark/components/Atomic/Card';
import { H2, Overline, P } from '@kertPark/components/Atomic/Typography';
import { IconClose } from '@kertPark/components/Icon/IconClose';
import { activeTeams, Team } from '@kertPark/config/teams';
import { theme, typography, zIndex as themeZIndex } from '@kertPark/config/theme';
import { Box, Flex } from '@rebass/grid';
import copy from 'copy-to-clipboard';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import Modal, { Styles } from 'react-modal';
import styled from 'styled-components';

const Wrapper = styled(Card)`
  width: 100%;
  max-width: 40em;
`;

const StyledButton = styled(Button)`
  margin: 0;
`;

const StyledP = styled(P)`
  margin: 0;
`;

const ControlClose = styled.div`
  cursor: pointer;
`;

const zIndex = {
  content: themeZIndex.modal + 1,
  overlay: themeZIndex.modal,
};

const Select = styled.select`
  color: ${({ theme }) => theme.colors.textPrimary};
  font-family: ${theme.fonts.header};
  font-size: ${typography.pSmall.fontSize.xs};
  margin: 0.5rem 0 0 0;
  border-color: ${({ theme }) => theme.colors.grey};
  border-radius: 0.25em;
`;

const modalStyles: Styles = {
  overlay: {
    zIndex: zIndex.overlay,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    zIndex: zIndex.content,
    position: 'relative',
    padding: '2rem 1rem',
    inset: 'unset',
    border: 0,
    background: 'none',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
};

type Props = {
  team?: Team;
};

Modal.setAppElement('#__next');

export const AddToCalendar: React.FC<Props> = ({ team }) => {
  const [isCopied, setCopied] = useState(false);
  const [isOpen, setOpen] = useState(false);
  const [selected, setSelected] = useState(team?.id ? [team.id] : []);
  const timeoutId = useRef<NodeJS.Timeout>();

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleOnChange = (event) => {
    const values = [...(event?.target?.selectedOptions || [])].map((option) => option.value);
    setSelected(values);
  };

  const handleClearTimeout = useCallback(() => {
    if (timeoutId.current) {
      clearTimeout(timeoutId.current);
      timeoutId.current = undefined;
    }
  }, [timeoutId]);

  const link = `https://calendar.kert-park.cz/?teamIds=${selected.join(',')}`;

  const handleCopy = useCallback(() => {
    copy(link);
    handleClearTimeout();
    setCopied(true);
    timeoutId.current = setTimeout(() => {
      setCopied(false);
    }, 2000);
  }, [handleClearTimeout, link, setCopied]);

  useEffect(() => {
    return handleClearTimeout;
  }, []);

  return (
    <>
      <StyledButton onClick={handleOpen}>Přidat do kalendáře</StyledButton>
      <Modal isOpen={isOpen} onRequestClose={handleClose} style={modalStyles}>
        <Wrapper>
          <Flex flex={1} flexDirection="row" alignItems="center" justifyContent="space-between" mb={3}>
            <H2 margin="0">Přidat do kalendáře</H2>
            <ControlClose onClick={handleClose}>
              <IconClose color={theme.colors.textPrimary} size={16} />
            </ControlClose>
          </Flex>
          <Overline>Vyberte týmy</Overline>
          <Select multiple onChange={handleOnChange} value={selected}>
            {activeTeams.map(({ id, name }) => (
              <option key={id} value={id}>
                {name}
              </option>
            ))}
          </Select>
          <Box my={3}>
            <Overline>Url</Overline>
            <StyledP>{link}</StyledP>
          </Box>
          <StyledButton disabled={!(selected.length > 0) || isCopied} onClick={handleCopy}>
            {isCopied ? 'Zkopírováno' : 'Zkopírovat odkaz'}
          </StyledButton>
        </Wrapper>
      </Modal>
    </>
  );
};
