import { Table, TBody, Td, Th, THead, Tr } from '@kertPark/components/Atomic/Table';
import { SortKeys, useTeamPlayers } from '@kertPark/components/Game/Detail/useTeamPlayers';
import { CmshbGameDetailTeamPlayers } from '@kertPark/graphql/generated';
import React from 'react';

type Props = {
  players: Array<CmshbGameDetailTeamPlayers>;
};

export const TeamPlayers: React.FC<Props> = ({ players }) => {
  const { handleSortKeyChange, sortedData, sortState } = useTeamPlayers({ players });

  return (
    <Table>
      <THead>
        <Tr>
          <Th
            onClick={() => handleSortKeyChange(SortKeys.POST)}
            sortIsDesc={sortState.key === SortKeys.POST ? !sortState.isDesc : undefined}
            title="Post"
            width="2.5rem"
          >
            P
          </Th>
          <Th title="Číslo dresu" width="2.5rem">
            #
          </Th>
          <Th align="left">Hráč</Th>
          <Th
            onClick={() => handleSortKeyChange(SortKeys.POINTS)}
            sortIsDesc={sortState.key === SortKeys.POINTS ? sortState.isDesc : undefined}
            title="Kanadské body"
            width="3rem"
          >
            KB
          </Th>
          <Th
            onClick={() => handleSortKeyChange(SortKeys.GOALS)}
            sortIsDesc={sortState.key === SortKeys.GOALS ? sortState.isDesc : undefined}
            title="Góly"
            width="3rem"
          >
            G
          </Th>
          <Th
            onClick={() => handleSortKeyChange(SortKeys.ASSISTS)}
            sortIsDesc={sortState.key === SortKeys.ASSISTS ? sortState.isDesc : undefined}
            title="Asistence"
            width="3rem"
          >
            A
          </Th>
          <Th
            onClick={() => handleSortKeyChange(SortKeys.PENALTIES)}
            sortIsDesc={sortState.key === SortKeys.PENALTIES ? sortState.isDesc : undefined}
            title="Trestné minuty"
            width="3rem"
          >
            TM
          </Th>
        </Tr>
      </THead>
      <TBody>
        {sortedData.map((player) => (
          <Tr key={player.player.cmshbId}>
            <Td>{player.post}</Td>
            <Td>{player.number}</Td>
            <Td align="left">{player.player.name}</Td>
            <Td>{player.points || '-'}</Td>
            <Td>{player.goals || '-'}</Td>
            <Td>{player.assists || '-'}</Td>
            <Td>{player.penalties || '-'}</Td>
          </Tr>
        ))}
      </TBody>
    </Table>
  );
};
