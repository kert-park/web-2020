import { CmshbGameDetailTeamGoalkeepers } from '@kertPark/graphql/generated';
import { SortFunctions, sortNumber, sortNumberAsc, useSortData } from '@kertPark/hooks/useSortData';

export enum SortKeys {
  ASSISTS = 'assists',
  GOALS = 'goals',
  GOALS_AGAINST = 'goalsAgainst',
  PENALTIES = 'penalties',
  SAVES_PERCENTAGE = 'savesAverage',
  SHOTS_AGAINST = 'shotsAgainst',
  TIME_IN_SECONDS = 'timeInSeconds',
}

const sortFunctions: SortFunctions<CmshbGameDetailTeamGoalkeepers, SortKeys> = {
  [SortKeys.ASSISTS]: sortNumber('assists'),
  [SortKeys.GOALS]: sortNumber('goals'),
  [SortKeys.GOALS_AGAINST]: sortNumberAsc('goalsAgainst'),
  [SortKeys.PENALTIES]: sortNumber('penalties'),
  [SortKeys.SAVES_PERCENTAGE]: sortNumber('savesAverage'),
  [SortKeys.SHOTS_AGAINST]: sortNumber('shotsAgainst'),
  [SortKeys.TIME_IN_SECONDS]: sortNumber('timeInSeconds'),
};

type Props = {
  goalkeepers: Array<CmshbGameDetailTeamGoalkeepers>;
};

export const useTeamGoalkeepers = ({ goalkeepers }: Props) => {
  return useSortData<CmshbGameDetailTeamGoalkeepers, SortKeys>({
    data: goalkeepers,
    defaultSortKey: SortKeys.TIME_IN_SECONDS,
    sortFunctions,
  });
};
