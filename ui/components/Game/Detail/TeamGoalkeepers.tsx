import { Table, TBody, Td, Th, THead, Tr } from '@kertPark/components/Atomic/Table';
import { SortKeys, useTeamGoalkeepers } from '@kertPark/components/Game/Detail/useTeamGoalkeepers';
import { CmshbGameDetailTeamGoalkeepers } from '@kertPark/graphql/generated';
import { getReadableTimeFromTimeInSeconds } from '@kertPark/lib/date';
import React from 'react';

type Props = {
  className?: string;
  goalkeepers: Array<CmshbGameDetailTeamGoalkeepers>;
};

export const TeamGoalkeepers: React.FC<Props> = ({ className, goalkeepers }) => {
  const { handleSortKeyChange, sortedData, sortState } = useTeamGoalkeepers({ goalkeepers });

  return (
    <Table className={className}>
      <THead>
        <Tr>
          <Th title="Post" width="2.5rem">
            P
          </Th>
          <Th title="Číslo dresu" width="2.5rem">
            #
          </Th>
          <Th align="left">Brankář</Th>
          <Th
            onClick={() => handleSortKeyChange(SortKeys.TIME_IN_SECONDS)}
            sortIsDesc={sortState.key === SortKeys.TIME_IN_SECONDS ? sortState.isDesc : undefined}
            width="3rem"
          >
            Čas
          </Th>
          <Th
            onClick={() => handleSortKeyChange(SortKeys.SAVES_PERCENTAGE)}
            sortIsDesc={sortState.key === SortKeys.SAVES_PERCENTAGE ? sortState.isDesc : undefined}
            title="Úspěšnost zákroků"
            width="3rem"
          >
            %
          </Th>
          <Th
            onClick={() => handleSortKeyChange(SortKeys.GOALS_AGAINST)}
            sortIsDesc={sortState.key === SortKeys.GOALS_AGAINST ? !sortState.isDesc : undefined}
            title="Obdržených gólů"
            width="3rem"
          >
            GA
          </Th>
          <Th
            onClick={() => handleSortKeyChange(SortKeys.SHOTS_AGAINST)}
            sortIsDesc={sortState.key === SortKeys.SHOTS_AGAINST ? sortState.isDesc : undefined}
            title="Střel proti"
            width="3rem"
          >
            STŘ.
          </Th>
          <Th
            desktopOnly
            onClick={() => handleSortKeyChange(SortKeys.GOALS)}
            sortIsDesc={sortState.key === SortKeys.GOALS ? sortState.isDesc : undefined}
            title="Góly"
            width="3rem"
          >
            G
          </Th>
          <Th
            desktopOnly
            onClick={() => handleSortKeyChange(SortKeys.ASSISTS)}
            sortIsDesc={sortState.key === SortKeys.ASSISTS ? sortState.isDesc : undefined}
            title="Asistence"
            width="3rem"
          >
            A
          </Th>
          <Th
            desktopOnly
            onClick={() => handleSortKeyChange(SortKeys.PENALTIES)}
            sortIsDesc={sortState.key === SortKeys.PENALTIES ? sortState.isDesc : undefined}
            title="Trestné minuty"
            width="3rem"
          >
            TM
          </Th>
        </Tr>
      </THead>
      <TBody>
        {sortedData.map((player) => (
          <Tr key={player.player.cmshbId}>
            <Td>B</Td>
            <Td>{player.number}</Td>
            <Td align="left">{player.player.name}</Td>
            <Td>{player.timeInSeconds ? getReadableTimeFromTimeInSeconds(player.timeInSeconds) : '-'}</Td>
            <Td>{player.savesAverage || '-'}</Td>
            <Td>{player.goalsAgainst || '-'}</Td>
            <Td>{player.shotsAgainst || '-'}</Td>
            <Td desktopOnly>{player.goals || '-'}</Td>
            <Td desktopOnly>{player.assists || '-'}</Td>
            <Td desktopOnly>{player.penalties || '-'}</Td>
          </Tr>
        ))}
      </TBody>
    </Table>
  );
};
