import { Card } from '@kertPark/components/Atomic/Card';
import { RadioButtonList } from '@kertPark/components/Atomic/RadioButtonList';
import { Overview } from '@kertPark/components/Game/Detail/Overview';
import { Statistics } from '@kertPark/components/Game/Detail/Statistics';
import { Team } from '@kertPark/components/Game/Detail/Team';
import { theme, typography } from '@kertPark/config/theme';
import { GameWithCmshbGameDetailType } from '@kertPark/graphql/generated/helpers';
import { Box, Flex } from '@rebass/grid';
import React, { useState } from 'react';
import styled from 'styled-components';

const ShowMoreButton = styled.button`
  color: ${({ theme }) => theme.colors.textLight};
  font-family: ${theme.fonts.header};
  font-size: ${typography.pSmall.fontSize.xs};
  font-weight: bold;
  text-transform: uppercase;
  border: 0;
  cursor: pointer;
  text-decoration: none;
  background: transparent;
  &:hover {
    text-decoration: underline;
  }
  &:focus {
    color: ${({ theme }) => theme.colors.textLight};
    outline: none;
    border: 0;
  }
`;

enum DetailTabEnum {
  OVERVIEW = 'overview',
  STATS = 'stats',
  HOME = 'home',
  AWAY = 'away',
}

type Props = {
  defaultOpen?: boolean;
  game: GameWithCmshbGameDetailType;
};

export const Detail: React.FC<Props> = ({ defaultOpen = false, game }) => {
  const [isOpen, setIsOpen] = useState<boolean>(defaultOpen);
  const [tab, setTab] = useState<DetailTabEnum>(DetailTabEnum.OVERVIEW);

  if (!isOpen) {
    return (
      <Flex width={1} justifyContent="center">
        <ShowMoreButton onClick={() => setIsOpen(true)}>+ Zobrazit detail utkání</ShowMoreButton>
      </Flex>
    );
  }

  return (
    <Card>
      <Box mb={3}>
        <RadioButtonList<DetailTabEnum>
          options={[
            { text: 'Přehled', value: DetailTabEnum.OVERVIEW },
            { text: 'Statistiky', value: DetailTabEnum.STATS },
            { text: 'Domácí', value: DetailTabEnum.HOME },
            { text: 'Hosté', value: DetailTabEnum.AWAY },
          ]}
          onChange={(selected) => {
            setTab(selected);
          }}
          selected={tab}
        />
      </Box>
      {tab === DetailTabEnum.OVERVIEW && <Overview game={game} />}
      {tab === DetailTabEnum.STATS && <Statistics game={game} />}
      {tab === DetailTabEnum.HOME && (
        <Team goalkeepers={game.goalkeepers?.home || []} players={game.players?.home || []} />
      )}
      {tab === DetailTabEnum.AWAY && (
        <Team goalkeepers={game.goalkeepers?.away || []} players={game.players?.away || []} />
      )}
    </Card>
  );
};
