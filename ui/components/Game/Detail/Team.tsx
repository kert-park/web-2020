import { TeamGoalkeepers } from '@kertPark/components/Game/Detail/TeamGoalkeepers';
import { TeamPlayers } from '@kertPark/components/Game/Detail/TeamPlayers';
import { CmshbGameDetailTeamGoalkeepers, CmshbGameDetailTeamPlayers } from '@kertPark/graphql/generated';
import React from 'react';
import styled from 'styled-components';

const TeamGoalkeepersStyled = styled(TeamGoalkeepers)`
  margin-bottom: 1em;
`;

type Props = {
  goalkeepers: Array<CmshbGameDetailTeamGoalkeepers>;
  players: Array<CmshbGameDetailTeamPlayers>;
};

export const Team: React.FC<Props> = ({ goalkeepers, players }) => {
  return (
    <>
      <TeamGoalkeepersStyled goalkeepers={goalkeepers} />
      <TeamPlayers players={players} />
    </>
  );
};
