import { TwoSideVerticalBar } from '@kertPark/components/Chart/TwoSideVerticalBar';
import { GameWithCmshbGameDetailType } from '@kertPark/graphql/generated/helpers';
import { getGameStatistics } from '@kertPark/lib/gameDetail';
import React from 'react';
import styled from 'styled-components';

const TwoSideVerticalBarStyled = styled(TwoSideVerticalBar)`
  &:not(:last-child) {
    margin-bottom: 1rem;
  }
`;

type Props = {
  game: GameWithCmshbGameDetailType;
};

export const Statistics: React.FC<Props> = ({ game }) => {
  const {
    homeGoalsEn,
    awayGoalsEn,
    homeShots,
    awayShots,
    homeShotsPercentage,
    awayShotsPercentage,
    homeSaves,
    awaySaves,
    homeSavesPercentage,
    awaySavesPercentage,
    homePowerPlays,
    awayPowerPlays,
    homePowerPlayGoals,
    awayPowerPlayGoals,
    homePowerPlaysPercentage,
    awayPowerPlaysPercentage,
    homePenaltyKillsPercentage,
    awayPenaltyKillsPercentage,
  } = getGameStatistics(game);

  return (
    <>
      <TwoSideVerticalBarStyled label="Střely" home={{ value: homeShots }} away={{ value: awayShots }} />
      <TwoSideVerticalBarStyled
        label="Úspěšnost střel"
        home={{ value: homeShotsPercentage, label: `${homeShotsPercentage}%` }}
        away={{ value: awayShotsPercentage, label: `${awayShotsPercentage}%` }}
      />
      <TwoSideVerticalBarStyled label="Zákroky brankařů" home={{ value: homeSaves }} away={{ value: awaySaves }} />
      <TwoSideVerticalBarStyled
        label="Úspěšnost zákroků"
        home={{ value: homeSavesPercentage, label: `${homeSavesPercentage}%` }}
        away={{ value: awaySavesPercentage, label: `${awaySavesPercentage}%` }}
      />
      <TwoSideVerticalBarStyled
        label="Vyloučení"
        home={{ value: game.statistics?.home.penalties || 0 }}
        away={{ value: game.statistics?.away.penalties || 0 }}
      />
      <TwoSideVerticalBarStyled label="Přesilovky" home={{ value: homePowerPlays }} away={{ value: awayPowerPlays }} />
      <TwoSideVerticalBarStyled
        label="Góly v přesilovce"
        home={{ value: homePowerPlayGoals }}
        away={{ value: awayPowerPlayGoals }}
      />
      <TwoSideVerticalBarStyled
        label="Góly v oslabení"
        home={{ value: game.statistics?.home.penaltyKillGoals || 0 }}
        away={{ value: game.statistics?.away.penaltyKillGoals || 0 }}
      />
      <TwoSideVerticalBarStyled
        label="Využití přesilovek"
        home={{ value: homePowerPlaysPercentage, label: `${homePowerPlaysPercentage}%` }}
        away={{ value: awayPowerPlaysPercentage, label: `${awayPowerPlaysPercentage}%` }}
      />
      <TwoSideVerticalBarStyled
        label="Úspěšnost oslabení"
        home={{ value: homePenaltyKillsPercentage, label: `${homePenaltyKillsPercentage}%` }}
        away={{ value: awayPenaltyKillsPercentage, label: `${awayPenaltyKillsPercentage}%` }}
      />
      <TwoSideVerticalBarStyled label="Góly do prázdné" home={{ value: homeGoalsEn }} away={{ value: awayGoalsEn }} />
    </>
  );
};
