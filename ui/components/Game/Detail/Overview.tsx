import { P } from '@kertPark/components/Atomic/Typography';
import { GameWithCmshbGameDetailType } from '@kertPark/graphql/generated/helpers';
import { getGamePeriods, getGameProgress, getGoals } from '@kertPark/lib/gameDetail';
import React from 'react';
import styled from 'styled-components';

const Paragraph = styled(P)`
  text-align: left;
  margin: 0;
`;

type Props = {
  game: GameWithCmshbGameDetailType;
};

export const Overview: React.FC<Props> = ({ game }) => {
  return (
    <>
      <Paragraph>
        <strong>Branky:</strong> {getGoals(game)}
      </Paragraph>
      <Paragraph>
        <strong>Třetiny:</strong> {getGamePeriods(game)}
      </Paragraph>
      <Paragraph>
        <strong>Průběh utkání:</strong> {getGameProgress(game)}
      </Paragraph>
    </>
  );
};
