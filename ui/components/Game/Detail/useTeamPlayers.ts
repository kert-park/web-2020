import { CmshbGameDetailTeamPlayers } from '@kertPark/graphql/generated';
import { SortFunctions, sortNumber, sortStringAsc, useSortData } from '@kertPark/hooks/useSortData';

export enum SortKeys {
  ASSISTS = 'assists',
  GOALS = 'goals',
  PENALTIES = 'penalties',
  POINTS = 'points',
  POST = 'post',
}

const sortFunctions: SortFunctions<CmshbGameDetailTeamPlayers, SortKeys> = {
  [SortKeys.ASSISTS]: sortNumber('assists'),
  [SortKeys.GOALS]: sortNumber('goals'),
  [SortKeys.PENALTIES]: sortNumber('penalties'),
  [SortKeys.POINTS]: sortNumber('points'),
  [SortKeys.POST]: sortStringAsc('post'),
};

type Props = {
  players: Array<CmshbGameDetailTeamPlayers>;
};

export const useTeamPlayers = ({ players }: Props) => {
  return useSortData<CmshbGameDetailTeamPlayers, SortKeys>({
    data: players,
    defaultSortKey: SortKeys.POST,
    sortFunctions,
  });
};
