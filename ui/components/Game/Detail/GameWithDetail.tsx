import { CardDesktop, CardMobile } from '@kertPark/components/Atomic/Card';
import { Detail } from '@kertPark/components/Game/Detail/Detail';
import { GameOverview, GameOverviewLoader } from '@kertPark/components/Game/GameOverview';
import { SmallGameOverview, SmallGameOverviewLoader } from '@kertPark/components/Game/SmallGameOverview';
import { theme } from '@kertPark/config/theme';
import { GameWithCmshbGameDetailType } from '@kertPark/graphql/generated/helpers';
import { Box } from '@rebass/grid';
import React from 'react';

type Props = {
  defaultOpen?: boolean;
  game: GameWithCmshbGameDetailType;
};

export const GameWithDetail: React.FC<Props> = ({ defaultOpen = false, game }) => {
  return (
    <>
      <Box marginY={theme.spacing.paragraphs}>
        <CardDesktop>
          <GameOverview game={game} />
        </CardDesktop>
        <CardMobile>
          <SmallGameOverview game={game} />
        </CardMobile>
      </Box>
      {game.statistics && (
        <Box marginY={theme.spacing.paragraphs}>
          <Detail defaultOpen={defaultOpen || game.team.systemName === 'muzia'} game={game} />
        </Box>
      )}
    </>
  );
};

export const GameWithDetailLoader: React.FC = () => {
  return (
    <Box marginY={theme.spacing.paragraphs}>
      <CardDesktop alignItems="center">
        <GameOverviewLoader />
      </CardDesktop>
      <CardMobile alignItems="center">
        <SmallGameOverviewLoader />
      </CardMobile>
    </Box>
  );
};
