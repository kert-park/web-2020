import { StaticPlaceLogo } from '@kertPark/components/Atomic/StaticImage';
import { Table, TBody, Td, Th, THead, Tr } from '@kertPark/components/Atomic/Table';
import { theme } from '@kertPark/config/theme';
import { Tournament } from '@kertPark/graphql/generated';
import { getReadableDate } from '@kertPark/lib/date';
import React from 'react';
import styled from 'styled-components';

const Logo = styled(StaticPlaceLogo)`
  width: 2em;
  height: 2em;
`;

type Props = {
  tournaments: Array<Tournament>;
};

export const TournamentList: React.FC<Props> = ({ tournaments }) => {
  return (
    <Table>
      <THead>
        <Tr>
          <Th width="7rem">Datum</Th>
          <Th width="3rem" />
          <Th align="left">Místo</Th>
        </Tr>
      </THead>
      <TBody>
        {tournaments.map((tournament) => {
          const tdProps =
            tournament.place?.systemName === 'praha-luziny' ? { bold: true, color: theme.colors.kert.redPrimary } : {};
          return (
            <Tr key={tournament.id}>
              <Td {...tdProps}>{getReadableDate(tournament.date, false, true)}</Td>
              <Td {...tdProps} align="right">
                {tournament.place?.systemName && (
                  <Logo id={tournament.place.systemName} title={tournament.place.name} />
                )}
              </Td>
              <Td {...tdProps} align="left">
                {tournament.place?.name || 'Není známo'}
              </Td>
            </Tr>
          );
        })}
      </TBody>
    </Table>
  );
};
