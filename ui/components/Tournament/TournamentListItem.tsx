import { StaticPlaceLogo } from '@kertPark/components/Atomic/StaticImage';
import { ItemInfo, ItemInfoWrapper, itemLogoStyle, ItemName, ItemWrapper } from '@kertPark/components/ListItem/Styles';
import { TournamentType } from '@kertPark/graphql/generated/helpers';
import { getReadableDate } from '@kertPark/lib/date';
import React from 'react';
import styled from 'styled-components';

const PlaceLogo = styled(StaticPlaceLogo)`
  ${itemLogoStyle}
`;

type Props = {
  tournament: TournamentType;
};

export const TournamentListItem: React.FC<Props> = ({ tournament }) => {
  if (!tournament.place) {
    return null;
  }

  return (
    <ItemWrapper>
      <PlaceLogo id={tournament.place.systemName || ''} title={tournament.place.name} />
      <ItemInfoWrapper>
        <ItemInfo>{getReadableDate(tournament.date, false, true)}</ItemInfo>
        <ItemName>
          <strong>{tournament.place.name}</strong>
        </ItemName>
      </ItemInfoWrapper>
    </ItemWrapper>
  );
};
