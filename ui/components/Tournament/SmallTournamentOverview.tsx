import { StaticPlaceLogo } from '@kertPark/components/Atomic/StaticImage';
import { Overline } from '@kertPark/components/Atomic/Typography';
import { typography } from '@kertPark/config/theme';
import { Tournament } from '@kertPark/graphql/generated';
import { getReadableDate } from '@kertPark/lib/date';
import { Flex } from '@rebass/grid';
import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components';

const Logo = styled(StaticPlaceLogo)`
  width: 4.5em;
  height: 4.5em;
`;

const Name = styled(Overline)`
  color: ${({ theme }) => theme.colors.textPrimary};
  font-size: ${typography.overline.fontSize.normal};
  text-align: center;
`;

type Props = {
  tournament: Tournament;
};

export const SmallTournamentOverview: React.FC<Props> = ({ tournament }) => {
  const kertTheme = useContext(ThemeContext);
  return (
    <Flex flexDirection="column" justifyContent="center" alignItems="center">
      <Overline color={kertTheme.colors.secondary} textAlign="center">
        Turnaj
      </Overline>
      <Overline small textAlign="center">
        {getReadableDate(tournament.date, false, true)}
      </Overline>
      <Flex flexDirection="column" justifyContent="center" alignItems="center">
        {tournament.place?.systemName && <Logo id={tournament.place.systemName} title={tournament.place.name} />}
        <Name>{tournament.place?.name || 'Místo není známo'}</Name>
      </Flex>
    </Flex>
  );
};
