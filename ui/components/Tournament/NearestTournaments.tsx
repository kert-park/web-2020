import { Card } from '@kertPark/components/Atomic/Card';
import { SmallTournamentOverview } from '@kertPark/components/Tournament/SmallTournamentOverview';
import { useTournamentListQuery } from '@kertPark/graphql/generated';
import { getNearestTournaments } from '@kertPark/lib/tournament';
import { Box } from '@rebass/grid';
import React from 'react';

type Props = {
  limit: number;
  teamId: string;
};

export const NearestTournaments: React.FC<Props> = ({ limit, teamId }) => {
  const { data } = useTournamentListQuery({
    variables: {
      teamId,
    },
  });

  const allTournaments = data?.tournamentList || [];
  const tournaments = getNearestTournaments({ limit, tournaments: allTournaments });

  return (
    <>
      {tournaments.map((tournament) => (
        <Box key={tournament.id} width={1} p={2}>
          <Card>
            <SmallTournamentOverview tournament={tournament} />
          </Card>
        </Box>
      ))}
    </>
  );
};
