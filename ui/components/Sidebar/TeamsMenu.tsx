import { Card } from '@kertPark/components/Atomic/Card';
import { H2 } from '@kertPark/components/Atomic/Typography';
import { IconInverseLetter } from '@kertPark/components/Icon/IconInversedLetter';
import { SidebarMenuLink } from '@kertPark/components/Sidebar/SidebarMenuLink';
import { routes } from '@kertPark/config/routes';
import { activeTeams } from '@kertPark/config/teams';
import { spaces, theme } from '@kertPark/config/theme';
import { Box } from '@rebass/grid';
import React from 'react';

type Props = {
  marginTop?: string;
};

export const TeamsMenu: React.FC<Props> = ({ marginTop }) => {
  return (
    <Box width={1}>
      <H2
        marginBottom="0"
        marginLeft={`calc(${spaces[2]} + ${theme.spacing.card})`}
        {...(marginTop ? { marginTop } : {})}
      >
        Týmy
      </H2>
      <Box width={1} p={2}>
        <Card>
          {activeTeams.map((team) => (
            <SidebarMenuLink
              key={team.id}
              link={routes.team.index}
              linkAs={routes.team.indexAs(team.id)}
              icon={{
                component: IconInverseLetter,
                props: {
                  letter: team.initials,
                  gradient: true,
                },
              }}
              name={team.name}
            />
          ))}
        </Card>
      </Box>
    </Box>
  );
};
