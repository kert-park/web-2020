import { NearestGames } from '@kertPark/components/Game/NearestGames';
import { LastPosts } from '@kertPark/components/Sidebar/LastPosts';
import { TeamMenuWithHeader } from '@kertPark/components/Sidebar/TeamMenu';
import { NearestTournaments } from '@kertPark/components/Tournament/NearestTournaments';
import { Team } from '@kertPark/config/teams';
import { Box } from '@rebass/grid';
import React from 'react';

type Props = {
  team?: Team;
  postSlug?: string;
};

export const TeamSidebar = ({ postSlug, team }: Props) => {
  if (!team) {
    return null;
  }

  return (
    <Box width={1}>
      <TeamMenuWithHeader team={team} />
      {team.hasGames ? <NearestGames limit={1} teamId={team.id} /> : <NearestTournaments limit={1} teamId={team.id} />}
      <LastPosts postSlug={postSlug} team={team} />
    </Box>
  );
};
