import { ClubMenu } from '@kertPark/components/Sidebar/ClubMenu';
import { TeamsMenu } from '@kertPark/components/Sidebar/TeamsMenu';
import { Box } from '@rebass/grid';
import React from 'react';

export const ClubSidebar: React.FC = () => {
  return (
    <Box width={1}>
      <ClubMenu />
      <TeamsMenu />
    </Box>
  );
};
