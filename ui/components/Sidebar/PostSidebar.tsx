import { LastPosts } from '@kertPark/components/Sidebar/LastPosts';
import { TeamSidebar } from '@kertPark/components/Sidebar/TeamSidebar';
import { TeamsMenu } from '@kertPark/components/Sidebar/TeamsMenu';
import { spaces, theme } from '@kertPark/config/theme';
import { getTeamById } from '@kertPark/lib/team';
import { Box } from '@rebass/grid';
import React from 'react';

type Props = {
  categoryCode?: string;
  postSlug?: string;
};

export const PostSidebar = ({ categoryCode, postSlug }: Props) => {
  if (categoryCode) {
    const team = getTeamById({ teamId: categoryCode });
    if (team) {
      return <TeamSidebar postSlug={postSlug} team={team} />;
    }
  }
  return (
    <Box width={1}>
      <TeamsMenu marginTop={`calc(${spaces[2]} + ${theme.spacing.card})`} />
      <LastPosts limit={5} postSlug={postSlug} />
    </Box>
  );
};
