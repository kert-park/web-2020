import { Card } from '@kertPark/components/Atomic/Card';
import { H2 } from '@kertPark/components/Atomic/Typography';
import { IconInverseLetter } from '@kertPark/components/Icon/IconInversedLetter';
import { SidebarMenuLink, SidebarMenuLinkLoader } from '@kertPark/components/Sidebar/SidebarMenuLink';
import { MenuLinkType, routes } from '@kertPark/config/routes';
import { spaces, theme } from '@kertPark/config/theme';
import { useClubPagesQuery } from '@kertPark/graphql/generated';
import { Box } from '@rebass/grid';
import { useRouter } from 'next/router';
import React, { useMemo } from 'react';

export const ClubMenu: React.FC = () => {
  const { asPath } = useRouter();
  const { data, loading } = useClubPagesQuery();

  const clubPages = data?.clubPages?.data || [];

  const links = useMemo<Array<MenuLinkType>>(() => {
    return [
      {
        link: routes.clubAllGames,
        linkAs: routes.clubAllGamesAs,
        name: 'Multirozpis',
      },
      {
        link: routes.officialList,
        linkAs: routes.officialListAs,
        name: 'Realizační týmy',
      },
      ...clubPages.map((page) => {
        return {
          link: routes.club,
          linkAs: routes.clubAs(page.attributes?.slug || ''),
          name: page.attributes?.title || '',
        };
      }),
    ].sort((a, b) => a.name.localeCompare(b.name));
  }, [clubPages]);

  return (
    <Box width={1}>
      <H2
        margin="0"
        marginTop={`calc(${spaces[2]} + ${theme.spacing.card})`}
        marginLeft={`calc(${spaces[2]} + ${theme.spacing.card})`}
      >
        Rozcestník
      </H2>
      <Box width={1} p={2}>
        <Card>
          {loading ? (
            <>
              <SidebarMenuLinkLoader />
              <SidebarMenuLinkLoader />
              <SidebarMenuLinkLoader />
              <SidebarMenuLinkLoader />
              <SidebarMenuLinkLoader />
            </>
          ) : (
            links.map(({ link, linkAs, name }) => {
              return (
                <SidebarMenuLink
                  key={name}
                  active={asPath === linkAs}
                  link={link}
                  linkAs={linkAs}
                  icon={{
                    component: IconInverseLetter,
                    props: {
                      letter: name.slice(0, 2),
                    },
                  }}
                  name={name}
                />
              );
            })
          )}
        </Card>
      </Box>
    </Box>
  );
};
