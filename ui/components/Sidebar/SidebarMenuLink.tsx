import { ContentLoader, Rect } from '@kertPark/components/Atomic/ContentLoader';
import { LinkAnchor } from '@kertPark/components/Atomic/LinkAnchor';
import { H4 } from '@kertPark/components/Atomic/Typography';
import { MenuLinkType } from '@kertPark/config/routes';
import { spaces } from '@kertPark/config/theme';
import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components';

const IconWrapper = styled.div`
  margin-right: ${spaces[2]};
`;

const MenuLink = styled(LinkAnchor)<{ active?: boolean }>`
  color: ${({ active, theme }) => (active ? theme.colors.secondary : theme.colors.textPrimary)};
  display: flex;
  flex-direction: row;

  &:focus {
    outline: none;
    border: 0;
  }

  & + & {
    margin-top: ${spaces[2]};
  }
`;

const TitleH4 = styled(H4)<{ active?: boolean }>`
  color: ${({ active, theme }) => (active ? theme.colors.secondary : theme.colors.textPrimary)};
  ${MenuLink}:hover & {
    color: ${({ theme }) => theme.colors.secondary};
  }
`;

type Props = MenuLinkType & { active?: boolean };

export const SidebarMenuLink = ({ active, link, linkAs, icon, name }: Props) => {
  const kertTheme = useContext(ThemeContext);
  return (
    <MenuLink active={active} href={link || ''} hrefAs={linkAs}>
      {icon && (
        <IconWrapper>
          <icon.component {...icon.props} color={kertTheme.colors.textPrimary} size={'24px'} />
        </IconWrapper>
      )}
      <TitleH4 active={active} margin="0">
        {name}
      </TitleH4>
    </MenuLink>
  );
};

export const SidebarMenuLinkLoader: React.FC = () => {
  return (
    <ContentLoader viewBox="0 0 180 30" style={{ maxWidth: '180' }}>
      <Rect x="0" y="5" width="20" height="20" />
      <Rect x="30" y="6" width="150" height="18" />
    </ContentLoader>
  );
};
