import { Card } from '@kertPark/components/Atomic/Card';
import { H2 } from '@kertPark/components/Atomic/Typography';
import { SidebarMenuLink } from '@kertPark/components/Sidebar/SidebarMenuLink';
import { getTeamMenu } from '@kertPark/config/routes';
import { Team } from '@kertPark/config/teams';
import { spaces, theme } from '@kertPark/config/theme';
import { Box } from '@rebass/grid';
import { useRouter } from 'next/router';
import React from 'react';

type Props = {
  team: Team;
};

export const TeamMenu: React.FC<Props> = ({ team }) => {
  const { pathname } = useRouter();
  const teamMenu = getTeamMenu(team.id);
  return (
    <>
      {teamMenu.map((item, index) => (
        <SidebarMenuLink
          key={typeof item.linkAs === 'string' ? item.linkAs : item.linkAs?.pathname || `team_menu_${index}`}
          active={pathname === item.link}
          {...item}
        />
      ))}
    </>
  );
};

export const TeamMenuWithHeader: React.FC<Props> = ({ team }) => {
  return (
    <Box width={1}>
      <H2
        margin="0"
        marginTop={`calc(${spaces[2]} + ${theme.spacing.card})`}
        marginLeft={`calc(${spaces[2]} + ${theme.spacing.card})`}
      >
        {team.name}
      </H2>
      <Box width={1} p={2}>
        <Card>
          <TeamMenu team={team} />
        </Card>
      </Box>
    </Box>
  );
};
