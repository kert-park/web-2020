import { H2 } from '@kertPark/components/Atomic/Typography';
import { SmallPost, SmallPostLoader } from '@kertPark/components/PostList/SmallPost';
import { Team } from '@kertPark/config/teams';
import { spaces, theme } from '@kertPark/config/theme';
import { usePostsCompositeQuery } from '@kertPark/graphql/generated';
import { Box } from '@rebass/grid';
import React, { useMemo } from 'react';

type Props = {
  limit?: number;
  postSlug?: string;
  team?: Team;
};

export const LastPosts: React.FC<Props> = ({ limit = 3, postSlug, team }) => {
  const hasTeam = !!team?.id;

  const { data, loading } = usePostsCompositeQuery({
    variables: {
      ...(hasTeam ? { categoryCode: team.id } : {}),
      pagination: {
        page: 1,
        pageSize: limit + 1,
      },
    },
  });
  const allPosts = (data?.postsComposite?.data || [])
    .filter((post) => {
      if (post.__typename === 'PostEntity') {
        return post.attributes?.slug !== postSlug;
      }
      return true;
    })
    .slice(0, limit);

  const loadingPostsPlaceholders = useMemo(() => {
    const placeholders: Array<React.ReactNode> = [];
    for (let i = 0; i < limit; i++) {
      placeholders.push(<SmallPostLoader key={i} />);
    }
    return placeholders;
  }, []);

  return (
    <Box width={1}>
      <H2 marginBottom="0" marginLeft={`calc(${spaces[2]} + ${theme.spacing.card})`}>
        Nejnovější články
      </H2>
      {loading
        ? loadingPostsPlaceholders.map((placeholder, index) => (
            <Box width={1} p={2} key={index}>
              {placeholder}
            </Box>
          ))
        : allPosts.map((post) => (
            <Box width={1} p={2} key={post.id}>
              <SmallPost post={post} />
            </Box>
          ))}
    </Box>
  );
};
