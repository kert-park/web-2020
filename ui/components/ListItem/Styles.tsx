import { mediaBreakpoints, theme, typography } from '@kertPark/config/theme';
import styled, { css } from 'styled-components';

export const ItemWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-top: 0.5em;
  &:not(:last-child) {
    margin-bottom: 0.5em;
  }
`;

export const ItemInfoWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  flex: 1;
  margin: 0 0.5em;
`;

export const ItemInfo = styled.div`
  display: flex;
  flex-flow: row wrap-reverse;
  align-items: flex-end;
  font-family: ${theme.fonts.header};
  font-size: ${typography.overline.fontSize.small};
  font-weight: bold;
  color: ${({ theme }) => theme.colors.textPrimary};
  gap: 0.25em 0.5em;
`;

export const ItemName = styled.div`
  font-family: ${theme.fonts.header};
  font-size: 1rem;
  color: ${({ theme }) => theme.colors.textPrimary};
`;

export const itemLogoStyle = css`
  width: 2em;
  height: 2em;
  flex: 0 0 2em;
  margin-right: 0.25rem;

  @media (${mediaBreakpoints.lg}) {
    width: 3.125em;
    height: 3.125em;
    flex: 0 0 3.125em;
    margin-right: 0.375rem;
  }
`;
