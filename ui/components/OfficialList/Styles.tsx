import { NUMBER_OF_COLUMNS, NUMBER_OF_COLUMNS_MD } from '@kertPark/components/OfficialList/Official';
import { mediaBreakpoints, spaces } from '@kertPark/config/theme';
import styled from 'styled-components';

export const OfficialsContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(${NUMBER_OF_COLUMNS}, minmax(0, 1fr));
  grid-column-gap: ${spaces[3]};

  @media (${mediaBreakpoints.lg}) {
    grid-template-columns: repeat(${NUMBER_OF_COLUMNS_MD}, minmax(0, 1fr));
  }
`;
