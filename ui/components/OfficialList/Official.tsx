import { PersonImage, PersonImagePlaceholder } from '@kertPark/components/Atomic/PersonImage';
import { Overline } from '@kertPark/components/Atomic/Typography';
import { mediaBreakpoints, spaces, theme, typography } from '@kertPark/config/theme';
import { CommonOfficialFragment } from '@kertPark/graphql/generated';
import React from 'react';
import styled, { css } from 'styled-components';

export const NUMBER_OF_COLUMNS = 2;
export const NUMBER_OF_COLUMNS_MD = 4;
export const NUMBER_OF_COMPONENTS = 4;

const Container = styled.div`
  display: contents;
  justify-items: center;
  max-width: 100%;
`;

type CellProps = { componentIndex: number; itemIndex: number };

const cellCss = css<CellProps>`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  overflow-wrap: break-word;
  max-width: 100%;

  grid-row: ${({ componentIndex, itemIndex }) =>
    componentIndex + NUMBER_OF_COMPONENTS * Math.floor(itemIndex / NUMBER_OF_COLUMNS)};
  grid-column: ${({ itemIndex }) => 1 + (itemIndex % NUMBER_OF_COLUMNS)};

  @media (${mediaBreakpoints.lg}) {
    grid-row: ${({ componentIndex, itemIndex }) =>
      componentIndex + NUMBER_OF_COMPONENTS * Math.floor(itemIndex / NUMBER_OF_COLUMNS_MD)};
    grid-column: ${({ itemIndex }) => 1 + (itemIndex % NUMBER_OF_COLUMNS_MD)};
  }
`;

const Header = styled(Overline)`
  ${cellCss};
  display: flex;
  justify-content: flex-end;

  margin-top: ${({ itemIndex }) => (Math.floor(itemIndex / NUMBER_OF_COLUMNS) > 0 ? spaces[3] : '0')};

  @media (${mediaBreakpoints.lg}) {
    margin-top: ${({ itemIndex }) => (Math.floor(itemIndex / NUMBER_OF_COLUMNS_MD) > 0 ? spaces[3] : '0')};
  }
`;

const ImageWrapper = styled.div`
  ${cellCss};
`;

const PersonName = styled.div<CellProps>`
  ${cellCss};

  color: ${({ theme }) => theme.colors.textPrimary};
  font-family: ${theme.fonts.header};
  font-weight: bold;
  font-size: ${typography.h4.fontSize.xs};

  @media (${mediaBreakpoints.lg}) {
    font-size: ${typography.h4.fontSize.lg};
  }
`;

const PersonContact = styled.a`
  color: ${({ theme }) => theme.colors.textPrimary};
  font-family: ${theme.fonts.header};
  font-size: ${typography.pSmall.fontSize.xs};
  margin-bottom: 0.25rem;
  text-decoration: none;
  &:hover {
    text-decoration: underline;
  }

  @media (${mediaBreakpoints.lg}) {
    font-size: ${typography.pSmall.fontSize.lg};
  }
`;

const ContactWrapper = styled.div`
  ${cellCss};
`;

type Props = {
  itemIndex: number;
  official: CommonOfficialFragment;
  showContactInformation?: boolean;
};

export const Official = ({ itemIndex, official, showContactInformation = false }: Props) => {
  const imageSrc = official.person?.data?.attributes?.image?.data?.attributes?.formats?.thumbnail?.url;
  const person = official.person?.data?.attributes;

  return (
    <Container>
      <Header as="div" componentIndex={1} itemIndex={itemIndex}>
        {official.title}
      </Header>
      <ImageWrapper componentIndex={2} itemIndex={itemIndex}>
        {imageSrc ? <PersonImage src={imageSrc} /> : <PersonImagePlaceholder />}
      </ImageWrapper>
      <PersonName componentIndex={3} itemIndex={itemIndex}>
        {person?.name}
      </PersonName>
      {showContactInformation && (
        <ContactWrapper componentIndex={4} itemIndex={itemIndex}>
          {person?.email && <PersonContact href={`mailto:${person.email}`}>{person.email}</PersonContact>}
          {person?.phone && <PersonContact href={`tel:${person.phone}`}>{person.phone}</PersonContact>}
        </ContactWrapper>
      )}
    </Container>
  );
};
