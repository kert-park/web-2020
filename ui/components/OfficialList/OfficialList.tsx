import { H2 } from '@kertPark/components/Atomic/Typography';
import { Official } from '@kertPark/components/OfficialList/Official';
import { OfficialsContainer } from '@kertPark/components/OfficialList/Styles';
import { CommonOfficialListFragment } from '@kertPark/graphql/generated';
import React from 'react';

type Props = {
  item: CommonOfficialListFragment;
};

export const OfficialList = ({ item }: Props) => {
  return (
    <>
      <H2>{item.title}</H2>
      <OfficialsContainer>
        {item.officials.filter(Boolean).map((official, itemIndex) => {
          return (
            <Official
              key={official!.id}
              itemIndex={itemIndex}
              official={official!}
              showContactInformation={item.showContactInformation}
            />
          );
        })}
      </OfficialsContainer>
    </>
  );
};
