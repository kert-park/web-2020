import { Score } from '@kertPark/components/Atomic/Score';
import { StaticTeamLogo } from '@kertPark/components/Atomic/StaticImage';
import { Overline } from '@kertPark/components/Atomic/Typography';
import { mediaBreakpoints, spaces, theme, typography } from '@kertPark/config/theme';
import { PlayOffRoundType } from '@kertPark/graphql/generated/helpers';
import { getReadableDate, getReadableTime } from '@kertPark/lib/date';
import { GameOtherStatuses, GamePlayedStatuses, GameStatus, GameStatusName } from '@kertPark/lib/game';
import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const TeamsAndResultWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: stretch;
  justify-content: center;
  flex: 1;
  width: 100%;
  margin-top: ${spaces[2]};
`;

const TeamWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  flex: 1;
  width: 100%;
`;

const Logo = styled(StaticTeamLogo)`
  width: 4.5em;
  height: 4.5em;
  margin-bottom: ${spaces[2]};

  @media (${mediaBreakpoints.md}) {
    width: 3.25em;
    height: 3.25em;
  }

  @media (${mediaBreakpoints.lg}) {
    width: 4.5em;
    height: 4.5em;
  }
`;

const TeamName = styled(Overline)`
  color: ${({ theme }) => theme.colors.textPrimary};
  font-size: ${typography.overline.fontSize.normal};
  text-align: center;

  @media (${mediaBreakpoints.md}) {
    font-size: ${typography.overline.fontSize.small};
  }

  @media (${mediaBreakpoints.lg}) {
    font-size: ${typography.overline.fontSize.normal};
  }
`;

const ResultWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  flex: 0 0 auto;
`;

const StyledScore = styled.div`
  color: ${({ theme }) => theme.colors.black};
  font-family: ${theme.fonts.header};
  font-weight: bold;
  font-size: 1.625rem;

  @media (${mediaBreakpoints.md}) {
    font-size: 1.25rem;
  }

  @media (${mediaBreakpoints.lg}) {
    font-size: 1.625rem;
  }
`;

const GamesWrapper = styled(Container)`
  margin-top: ${spaces[4]};
  gap: ${spaces[3]};
  flex: 1;
  width: 100%;
`;

const GameWrapper = styled(Container)`
  flex: 1;
  width: 100%;
  gap: ${spaces[1]};

  @media (${mediaBreakpoints.xl}) {
    flex-direction: row;
    justify-content: center;
  }
`;

const GameTeamLogo = styled(StaticTeamLogo)`
  width: 1.875em;
  height: 1.875em;
`;

const GameScoreWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 6rem;
`;

const GameStyledScore = styled(Score)`
  font-size: 1.25rem;
`;

const GameStyledTime = styled(Overline)`
  font-size: 1rem;
`;

const GameTeamsAndResultWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  gap: ${spaces[1]};
`;

const GameDateAndTimeWrapper = styled(Overline)`
  @media (${mediaBreakpoints.xl}) {
    display: none;
  }
`;

const GameDateAndTimeXlWrapper = styled.div`
  display: none;

  @media (${mediaBreakpoints.xl}) {
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
    width: 10rem;
  }
`;

type Props = {
  playOffRound: PlayOffRoundType;
  showGames?: boolean;
};

export const PlayOffSmallOverview: React.FC<Props> = ({ playOffRound, showGames = false }) => {
  const kertTheme = useContext(ThemeContext);

  return (
    <Container>
      <Overline color={kertTheme.colors.secondary} textAlign="center">
        {playOffRound.name}
      </Overline>
      <TeamsAndResultWrapper>
        <TeamWrapper>
          <Logo id={playOffRound.kertTeam.cmshbId || ''} title={playOffRound.kertTeam.name} />
          <TeamName>{playOffRound.kertTeam.shortName}</TeamName>
        </TeamWrapper>
        <ResultWrapper>
          <Overline color={kertTheme.colors.textSecondary} textAlign="center">
            Stav série
          </Overline>
          <StyledScore>
            {playOffRound.score.kertTeam}:{playOffRound.score.opponentTeam}
          </StyledScore>
        </ResultWrapper>
        <TeamWrapper>
          <Logo id={playOffRound.opponentTeam.cmshbId || ''} title={playOffRound.opponentTeam.name} />
          <TeamName>{playOffRound.opponentTeam.shortName}</TeamName>
        </TeamWrapper>
      </TeamsAndResultWrapper>
      {showGames && (
        <GamesWrapper>
          {playOffRound.games.map((game, index) => {
            return (
              <GameWrapper key={game.id}>
                <GameDateAndTimeWrapper small textAlign="center">
                  {index + 1}.{playOffRound.shortcut} - {getReadableDate(game.date, false, true)}, {game.place}
                </GameDateAndTimeWrapper>
                <GameDateAndTimeXlWrapper>
                  <Overline small textAlign="center">
                    {index + 1}.{playOffRound.shortcut} - {getReadableDate(game.date, false, true)}
                  </Overline>
                  <Overline small textAlign="center">
                    {game.place}
                  </Overline>
                </GameDateAndTimeXlWrapper>
                <GameTeamsAndResultWrapper>
                  <GameTeamLogo id={game.home.cmshbId || ''} title={game.home.name} />
                  <GameScoreWrapper>
                    {GamePlayedStatuses.includes(game.status as GameStatus) ? (
                      <GameStyledScore game={game} />
                    ) : GameOtherStatuses.includes(game.status as GameStatus) ? (
                      <Overline small>{GameStatusName[game.status as GameStatus]}</Overline>
                    ) : (
                      <GameStyledTime>{getReadableTime(game.date)}</GameStyledTime>
                    )}
                  </GameScoreWrapper>
                  <GameTeamLogo id={game.away.cmshbId || ''} title={game.away.name} />
                </GameTeamsAndResultWrapper>
              </GameWrapper>
            );
          })}
        </GamesWrapper>
      )}
    </Container>
  );
};
