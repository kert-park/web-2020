import { StaticTeamLogo } from '@kertPark/components/Atomic/StaticImage';
import { Overline } from '@kertPark/components/Atomic/Typography';
import { spaces, theme } from '@kertPark/config/theme';
import { PlayOffRoundType } from '@kertPark/graphql/generated/helpers';
import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const TeamsAndResultWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: stretch;
  justify-content: center;
  flex: 1;
  width: 100%;
  margin-top: ${spaces[2]};
`;

const TeamWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  flex: 1;
  width: 100%;
`;

const Logo = styled(StaticTeamLogo)`
  width: 6.25em;
  height: 6.25em;
  margin-bottom: ${spaces[2]};
`;

const TeamName = styled.div`
  color: ${({ theme }) => theme.colors.textPrimary};
  font-family: ${theme.fonts.header};
  font-weight: bold;
  font-size: 1rem;
  text-align: center;
`;

const ResultWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  flex: 0 0 auto;
`;

const StyledScore = styled.div`
  color: ${({ theme }) => theme.colors.black};
  font-family: ${theme.fonts.header};
  font-weight: bold;
  font-size: 2.25rem;
`;

type Props = {
  playOffRound: PlayOffRoundType;
};

export const PlayOffOverview: React.FC<Props> = ({ playOffRound }) => {
  const kertTheme = useContext(ThemeContext);

  return (
    <Container>
      <Overline color={kertTheme.colors.secondary} textAlign="center">
        {playOffRound.name}
      </Overline>
      <TeamsAndResultWrapper>
        <TeamWrapper>
          <Logo id={playOffRound.kertTeam.cmshbId || ''} title={playOffRound.kertTeam.name} />
          <TeamName>{playOffRound.kertTeam.name}</TeamName>
        </TeamWrapper>
        <ResultWrapper>
          <Overline color={kertTheme.colors.textSecondary} textAlign="center">
            Stav série
          </Overline>
          <StyledScore>
            {playOffRound.score.kertTeam}:{playOffRound.score.opponentTeam}
          </StyledScore>
        </ResultWrapper>
        <TeamWrapper>
          <Logo id={playOffRound.opponentTeam.cmshbId || ''} title={playOffRound.opponentTeam.name} />
          <TeamName>{playOffRound.opponentTeam.name}</TeamName>
        </TeamWrapper>
      </TeamsAndResultWrapper>
    </Container>
  );
};
