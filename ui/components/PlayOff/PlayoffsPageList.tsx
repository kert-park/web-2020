import { Card, CardDesktop, CardMobile } from '@kertPark/components/Atomic/Card';
import { Overline } from '@kertPark/components/Atomic/Typography';
import { GameListItem } from '@kertPark/components/Game/GameListItem/GameListItem';
import { PlayOffOverview } from '@kertPark/components/PlayOff/PlayOffOverview';
import { PlayOffSmallOverview } from '@kertPark/components/PlayOff/PlayOffSmallOverview';
import { PlayOffListQuery } from '@kertPark/graphql/generated';
import { TeamProps } from '@kertPark/lib/team';
import { Box } from '@rebass/grid';
import React from 'react';

type Props = TeamProps & {
  playOffs: PlayOffListQuery['playOffList'];
};

export const PlayoffsPageList: React.FC<Props> = ({ playOffs, team }) => {
  return (
    <>
      {playOffs.map((playOff) => (
        <React.Fragment key={playOff.id}>
          {playOff.rounds.map((round) => (
            <React.Fragment key={round.id}>
              <Box width={1} p={2}>
                <CardDesktop>
                  <PlayOffOverview playOffRound={round} />
                </CardDesktop>
                <CardMobile>
                  <PlayOffSmallOverview playOffRound={round} />
                </CardMobile>
              </Box>
              <Box width={1} p={2}>
                <Card>
                  <Overline>{round.name}</Overline>
                  {round.games.map((game) => (
                    <GameListItem key={game.id} game={game} hasLargeMetadata={team?.id === 'muzia'} />
                  ))}
                </Card>
              </Box>
            </React.Fragment>
          ))}
        </React.Fragment>
      ))}
    </>
  );
};
