import { H1 } from '@kertPark/components/Atomic/Typography';
import { ClubContent } from '@kertPark/components/Club/ClubContent';
import { Layout } from '@kertPark/components/Layout/Layout';
import { PostHeaderLoader } from '@kertPark/components/Post/PostHeader';
import { PreviewLabel } from '@kertPark/components/Post/PreviewLabel';
import { ClubSidebar } from '@kertPark/components/Sidebar/ClubSidebar';
import { mediaBreakpoints, spaces, theme } from '@kertPark/config/theme';
import { ClubPageQuery } from '@kertPark/graphql/generated';
import { Flex } from '@rebass/grid';
import { NextPage } from 'next';
import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  width: 100%;
  @media (${mediaBreakpoints.xxl}) {
    max-width: ${theme.layout.textContentMaxWidth};
  }
`;

export type ClubPageProps = {
  page: NonNullable<ClubPageQuery['clubPages']>['data'][0] | undefined;
  preview: boolean;
};

export const ClubPage: NextPage<ClubPageProps> = ({ page, preview }) => {
  if (!page) {
    return (
      <Layout noRobots>
        <Flex alignItems="flex-start" flexWrap="wrap" m={-2}>
          <Flex alignItems="flex-start" flexWrap="wrap" width={[1, 2 / 3]}>
            <Flex width={1} p={2} alignItems="center" flexDirection="column">
              <PostHeaderLoader />
            </Flex>
          </Flex>
        </Flex>
      </Layout>
    );
  }

  return (
    <Layout
      title={[...(preview ? ['Preview'] : []), ...(page.attributes?.title ? [page.attributes.title] : []), 'Klub']}
      description={page.attributes?.title || ''}
      noRobots={preview}
    >
      <Flex alignItems="flex-start" flexWrap="wrap" m={-2}>
        <Flex alignItems="flex-start" flexWrap="wrap" width={[1, 2 / 3]}>
          <Flex width={1} p={2} flexDirection="column" alignItems="center">
            {preview && <PreviewLabel />}
            <Wrapper>
              <H1 marginTop={spaces[3]}>{page.attributes?.title}</H1>
              <ClubContent page={page} />
            </Wrapper>
          </Flex>
        </Flex>
        <Flex flexWrap="wrap" width={[1, 1 / 3]}>
          <ClubSidebar />
        </Flex>
      </Flex>
    </Layout>
  );
};
