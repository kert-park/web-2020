import { Markdown } from '@kertPark/components/Atomic/Markdown';
import { Gallery } from '@kertPark/components/Gallery/Gallery';
import { OfficialList } from '@kertPark/components/OfficialList/OfficialList';
import { PostVideo } from '@kertPark/components/Post/EmbedVideo/PostVideo';
import { ClubPageQueryClubPage } from '@kertPark/graphql/generated/helpers';
import React from 'react';

type Props = {
  page: ClubPageQueryClubPage;
};

export const ClubContent = ({ page }: Props) => {
  const content = page?.attributes?.content.map((item) => {
    switch (item?.__typename) {
      case 'ComponentCommonGallery':
        return item.gallery?.data ? (
          <Gallery key={`${item.__typename}_${item.id}`} gallery={item.gallery.data} text={item.text || undefined} />
        ) : undefined;
      case 'ComponentCommonRichText':
        return item.text ? <Markdown key={`${item.__typename}_${item.id}`} text={item.text} /> : undefined;
      case 'ComponentCommonOfficialList':
        return <OfficialList key={`${item.__typename}_${item.id}`} item={item} />;
      case 'ComponentCommonEmbedVideo':
        return <PostVideo key={`${item.__typename}_${item.id}`} type={item.type} url={item.url} />;
      default:
        return null;
    }
  });

  return <>{content}</>;
};
