import { useCookiesContext } from '@kertPark/components/Cookies/CookiesContext';
import { GoogleTagManagerScript } from '@kertPark/components/Scripts/GoogleAnalytics';
import React from 'react';

export const Scripts: React.FC = () => {
  const { settings } = useCookiesContext();

  return (
    <>
      {process.env.GOOGLE_ANALYTICS_ID && !!settings.analytics && (
        <GoogleTagManagerScript id={process.env.GOOGLE_ANALYTICS_ID} />
      )}
    </>
  );
};
