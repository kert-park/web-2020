import { useRouter } from 'next/router';
import Script from 'next/script';
import React, { useEffect } from 'react';

import { pageview } from './sdk';

type Props = { id: string };

export const GoogleTagManagerScript: React.FC<Props> = ({ id }) => {
  const router = useRouter();

  useEffect(() => {
    const handleTrackRouteChange = (url: string) => {
      pageview({ token: id, url });
    };

    router.events.on('routeChangeComplete', handleTrackRouteChange);

    return () => {
      router.events.off('routeChangeComplete', handleTrackRouteChange);
    };
  }, [router.events]);

  if (id) {
    return (
      <>
        <Script async src={`https://www.googletagmanager.com/gtag/js?id=${id}`} strategy="afterInteractive" />
        <Script id={`google-analytics-${id}`} strategy="afterInteractive">
          {`window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', '${id}', {
            page_path: window.location.pathname,
          });`}
        </Script>
      </>
    );
  }

  return null;
};
