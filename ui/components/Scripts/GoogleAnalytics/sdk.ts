export type GTagFn<Props> = (gtag: any) => (props: Props) => void;

export const wrapGTag = <Props>(fn: GTagFn<Props>) => {
  return (props: Props): void => {
    // @ts-ignore
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
    const gtag = window?.gtag || undefined;

    if (gtag) {
      return fn(gtag)(props);
    }
    return;
  };
};

type PageviewProps = { token: string; url: string };

const pageviewFn: GTagFn<PageviewProps> =
  (gtag) =>
  ({ token, url }) => {
    gtag('config', token, { page_path: url });
  };

export const pageview = wrapGTag<PageviewProps>(pageviewFn);
