import { Card } from '@kertPark/components/Atomic/Card';
import { AspectRatio, ImageAspectRatio } from '@kertPark/components/Atomic/ImageAspectRatio';
import { LinkAnchor } from '@kertPark/components/Atomic/LinkAnchor';
import { H1, P } from '@kertPark/components/Atomic/Typography';
import { routes } from '@kertPark/config/routes';
import { mediaBreakpoints, spaces, typography } from '@kertPark/config/theme';
import React from 'react';
import styled from 'styled-components';

const APP_PATH = process.env.APP_PATH;

const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: ${spaces[3]};
`;

const StyledCard = styled(Card)`
  background: url('${APP_PATH}/thumb-background.jpg');
  background-size: 100% 200%;
  box-shadow: 0 0.25em 1em 0 ${({ theme }) => theme.colors.boxShadow.normalRed};

  &:hover {
    box-shadow: 0 0.25em 1.5em 0 ${({ theme }) => theme.colors.boxShadow.hoverRed};
  }

  animation: gradient 5s ease infinite;

  @keyframes gradient {
    0% {
      background-position: 0% 0%;
    }
    50% {
      background-position: 100% 100%;
    }
    100% {
      background-position: 0% 0%;
    }
  }
`;

const StyledImageAspectRatio = styled(ImageAspectRatio)`
  flex: 0 0 auto;
  width: 12em;
  @media (${mediaBreakpoints.xl}) {
    width: 15em;
  }
`;

const Title = styled(H1)`
  color: ${({ theme }) => theme.colors.kert.yellowPrimary};
  text-transform: uppercase;
  text-align: center;

  font-size: ${typography.h1.fontSize.xs};

  @media (${mediaBreakpoints.xl}) {
    font-size: ${typography.h1.fontSize.lg};
  }

  margin: 0;
`;

const Description = styled(P)`
  color: ${({ theme }) => theme.colors.kert.yellowPrimary};
  margin: 0;
`;

export const RecruitmentBannerDesktop = () => {
  return (
    <LinkAnchor href={routes.club} hrefAs={routes.clubAs('nabor')} passHref>
      <StyledCard flexDirection="row" noPadding>
        <StyledImageAspectRatio
          ratio={AspectRatio.threeToOne}
          src={`${APP_PATH}/recruitment-players.jpg`}
          title="Nábor"
        />
        <Container>
          <Title>Pojď hrát hokejbal!</Title>
          <Description>Nábor mládeže každé úterý a čtvrtek od 16:45. Klikni pro více informací!</Description>
        </Container>
      </StyledCard>
    </LinkAnchor>
  );
};
