import { LinkAnchor } from '@kertPark/components/Atomic/LinkAnchor';
import { H3, H5 } from '@kertPark/components/Atomic/Typography';
import { IconChevron } from '@kertPark/components/Icon/IconChevron';
import { mainMenu, MenuItemType } from '@kertPark/config/routes';
import { spaces, theme } from '@kertPark/config/theme';
import React, { useContext, useState } from 'react';
import styled, { css, ThemeContext } from 'styled-components';

const WIDTH = '18rem';

const menuItemMarginCss = css`
  align-items: flex-start;
  padding: ${spaces[2]} ${spaces[3]};
`;

const Wrapper = styled.div<Pick<Props, 'isOpen'>>`
  position: fixed;
  right: 0;
  width: ${WIDTH};
  background: ${({ theme }) => theme.colors.backgroundSecondary};
  height: 100vh;
  z-index: 2;
  overflow-y: auto;
  transform: translateX(${({ isOpen }) => (isOpen ? 0 : `calc(1em + ${WIDTH})`)});
  transition: transform ${theme.transitionSpeedMenu};
  padding: 4rem 0rem 1rem;
  box-shadow: -1em 0 1em 0 ${({ theme }) => theme.colors.boxShadow.hover};
`;

const MenuAccordionContent = styled.div<{ isOpen: boolean }>`
  display: flex;
  flex-direction: column;
  overflow: hidden;
  max-height: ${({ isOpen }) => (isOpen ? '100vh' : 0)};
  transition: max-height ${theme.transitionSpeedMenu};
`;

const MenuLink = styled(LinkAnchor)`
  ${menuItemMarginCss};
  display: flex;
  flex-direction: row;
  align-items: center;

  &:hover {
    color: ${({ theme }) => theme.colors.secondary};
  }
  &:focus {
    outline: none;
    border: 0;
  }

  ${MenuAccordionContent} & {
    padding-left: ${spaces[4]};
    border-top: 1px solid ${({ theme }) => theme.colors.lightGrey};
    &:last-child {
      border-bottom: 1px solid ${({ theme }) => theme.colors.lightGrey};
    }
  }
`;

const MenuAccordionWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const MenuAccordionHeader = styled.div`
  ${menuItemMarginCss};
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const MenuPrimaryItem = styled(H3)<{ active?: boolean }>`
  padding-left: ${spaces[2]};

  ${({ active, theme }) => (active ? `color: ${theme.colors.secondary};` : '')}

  ${MenuLink}:hover & {
    color: ${({ theme }) => theme.colors.secondary};
  }
`;
const MenuSecondaryItem = styled(H5)`
  padding-left: ${spaces[2]};

  ${MenuLink}:hover & {
    color: ${({ theme }) => theme.colors.secondary};
  }
`;

const MenuAccordion: React.FC<
  AccordionStateProps & {
    menuItem: MenuItemType;
  }
> = ({ accordionOpen, toggleAccordion, menuItem: { items, name } }) => {
  const kertTheme = useContext(ThemeContext);
  const isOpen = accordionOpen === name;
  return (
    <MenuAccordionWrapper>
      <MenuAccordionHeader onClick={() => toggleAccordion(name)}>
        <IconChevron color={kertTheme.colors.textPrimary} rotate={isOpen ? 270 : 90} size={16} withAnimation />
        <MenuPrimaryItem active={isOpen} margin="0" textTransform="uppercase">
          {name}
        </MenuPrimaryItem>
      </MenuAccordionHeader>
      <MenuAccordionContent isOpen={isOpen}>
        {items?.map((item) => (
          <MenuLink key={item.name} href={item.link || ''} hrefAs={item.linkAs}>
            <IconChevron color={kertTheme.colors.textPrimary} size={10} />
            <MenuSecondaryItem margin="0">{item.name}</MenuSecondaryItem>
          </MenuLink>
        ))}
      </MenuAccordionContent>
    </MenuAccordionWrapper>
  );
};

type Props = {
  isOpen: boolean;
  onClose?: () => void;
};

type AccordionStateProps = {
  accordionOpen: string;
  toggleAccordion: (accordionName: string) => void;
};

export const MobileSidebarMenu: React.FC<Props> = ({ isOpen }) => {
  const kertTheme = useContext(ThemeContext);
  const [accordionOpen, setAccordionOpen] = useState<string>('');

  const toggleAccordion = (accordionName: string) => {
    setAccordionOpen(accordionName === accordionOpen ? '' : accordionName);
  };

  return (
    <Wrapper isOpen={isOpen}>
      {mainMenu.map((menuItem: MenuItemType) => {
        if (menuItem.link) {
          return (
            <MenuLink key={menuItem.name} href={menuItem.link} hrefAs={menuItem.linkAs}>
              <IconChevron color={kertTheme.colors.textPrimary} size={16} />
              <MenuPrimaryItem margin="0" textTransform="uppercase">
                {menuItem.name}
              </MenuPrimaryItem>
            </MenuLink>
          );
        }
        return (
          <MenuAccordion
            key={menuItem.name}
            accordionOpen={accordionOpen}
            menuItem={menuItem}
            toggleAccordion={toggleAccordion}
          />
        );
      })}
    </Wrapper>
  );
};
