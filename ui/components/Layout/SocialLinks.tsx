import { TextAnchor } from '@kertPark/components/Atomic/LinkAnchor';
import { IconFacebook } from '@kertPark/components/Icon/IconFacebook';
import { IconInstagram } from '@kertPark/components/Icon/IconInstagram';
import { social } from '@kertPark/config/social';
import { spaces } from '@kertPark/config/theme';
import { useKertTheme } from '@kertPark/hooks/useKertTheme';
import { Flex } from '@rebass/grid';
import React from 'react';
import styled from 'styled-components';

const SocialLink = styled(TextAnchor)`
  display: flex;
  align-items: center;
  margin: 0 ${spaces[2]} 0 0;

  color: ${({ theme }) => theme.colors.textSecondary};
  &:active,
  &:visited {
    color: ${({ theme }) => theme.colors.textSecondary};
  }
  &:hover {
    color: ${({ theme }) => theme.colors.textLight};
  }

  svg {
    margin-right: ${spaces[1]};
  }
`;

export const SocialLinks: React.FC = () => {
  const { colors } = useKertTheme();
  return (
    <Flex flexDirection="row" flexWrap="wrap" mb={4}>
      <SocialLink href={social.instagram} target="_blank">
        <IconInstagram color={colors.textPrimary} size={20} /> Instagram
      </SocialLink>
      <SocialLink href={social.facebook} target="_blank">
        <IconFacebook color={colors.textPrimary} size={20} /> Facebook
      </SocialLink>
    </Flex>
  );
};
