import { DesktopMenu } from '@kertPark/components/Layout/DesktopMenu';
import { routes } from '@kertPark/config/routes';
import { breakpoints, mediaBreakpoints, theme } from '@kertPark/config/theme';
import Link from 'next/link';
import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: none;

  @media (${mediaBreakpoints.md}) {
    width: ${breakpoints.md};
    height: 10em;
    z-index: 2;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    padding: 1em 1em 0;
  }

  @media (${mediaBreakpoints.lg}) {
    width: ${breakpoints.lg};
  }

  @media (${mediaBreakpoints.xl}) {
    width: ${breakpoints.xl};
  }
`;

const Main = styled.div`
  display: flex;
  flex-direction: row;
  color: ${({ theme }) => theme.colors.primary};
  font-family: ${theme.fonts.header};
  text-transform: uppercase;
`;

const Logo = styled.img`
  width: 6.25em;
  height: 6.25em;
  flex: 0 0 6.25em;
  cursor: pointer;
`;

const NameAndMenuWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  flex: 1;
  margin-left: 1.5em;
  padding: 0.5em 0;
`;

const Name = styled.div`
  font-weight: bold;
  font-style: italic;
  font-size: 3em;
`;

const AchievementWrapper = styled.div`
  display: none;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  flex: 0 0 auto;
  padding: 0.5em 0 0.5em 1em;

  @media (${mediaBreakpoints.lg}) {
    display: flex;
  }
`;

const AchievementText = styled.div`
  font-size: 0.75em;
  text-align: center;
`;

const TrophyWrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 0.25em;
`;

const Trophy = styled.img`
  height: 3.125em;
`;

export const DesktopHeader = () => {
  return (
    <Wrapper>
      <Main>
        <Link href={routes.index}>
          <Logo src="/logo.png" alt="HC Kert Park Praha" />
        </Link>
        <NameAndMenuWrapper>
          <Name>HC Kert Park Praha</Name>
          <DesktopMenu />
        </NameAndMenuWrapper>
        <AchievementWrapper>
          <TrophyWrapper>
            <Trophy src="/extraliga-pohar.png" alt="Mistr ČR 2017" />
            <Trophy src="/extraliga-pohar.png" alt="Mistr ČR 2018" />
            <Trophy src="/extraliga-pohar.png" alt="Mistr ČR 2019" />
            <Trophy src="/extraliga-pohar.png" alt="Mistr ČR 2021" />
            <Trophy src="/extraliga-pohar.png" alt="Mistr ČR 2022" />
            <Trophy src="/extraliga-pohar.png" alt="Mistr ČR 2023" />
            <Trophy src="/extraliga-pohar.png" alt="Mistr ČR 2024" />
          </TrophyWrapper>
          <AchievementText>
            Sedminásobný mistr ČR
            <br />
            2017, 2018, 2019, 2021, 2022, 2023, 2024
          </AchievementText>
        </AchievementWrapper>
        <AchievementWrapper>
          <TrophyWrapper>
            <Trophy src="/superpohar.png" alt="Československý Superpohár 2018" />
            <Trophy src="/superpohar.png" alt="Československý Superpohár 2022" />
            <Trophy src="/superpohar.png" alt="Československý Superpohár 2023" />
          </TrophyWrapper>
          <AchievementText>
            Vítěz ČS superpoháru
            <br />
            2018, 2022, 2023
          </AchievementText>
        </AchievementWrapper>
      </Main>
    </Wrapper>
  );
};
