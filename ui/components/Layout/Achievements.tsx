import { Achievement } from '@kertPark/components/Achievement/Achievement';
import { Card } from '@kertPark/components/Atomic/Card';
import { ContentLoader, Rect } from '@kertPark/components/Atomic/ContentLoader';
import { H4 } from '@kertPark/components/Atomic/Typography';
import { spaces } from '@kertPark/config/theme';
import { CommonAchievementFragment, useCategoriesQuery } from '@kertPark/graphql/generated';
import { Box, Flex } from '@rebass/grid';
import React, { PropsWithChildren, useMemo } from 'react';

const AchievementBox: React.FC<PropsWithChildren> = ({ children }) => {
  return (
    <Box width={[1 / 2, 1 / 3, 1 / 4, 1 / 4, 1 / 6]} p={2}>
      <Card>{children}</Card>
    </Box>
  );
};

const AchievementLoader = () => {
  return (
    <AchievementBox>
      <ContentLoader viewBox="0 0 160 160">
        <Rect x="45" y="0" width="70" height="120" />
        <Rect x="20" y="128" width="120" height="12" />
        <Rect x="10" y="148" width="140" height="8" />
      </ContentLoader>
    </AchievementBox>
  );
};

export const Achievements: React.FC = () => {
  const { data, loading } = useCategoriesQuery();

  const achievements = useMemo(() => {
    return (
      (data?.categories?.data.find((category) => category.attributes?.code === 'muzia')?.attributes?.achievements ||
        []) as Array<CommonAchievementFragment>
    ).filter((achievement) => achievement.isWinner);
  }, [data?.categories?.data]);

  if (achievements.length < 1) {
    return null;
  }

  return (
    <>
      <H4 marginLeft={spaces[3]} marginTop={spaces[4]} marginBottom={spaces[1]}>
        Úspěchy
      </H4>
      <Flex flexWrap="wrap" width={1}>
        {loading && (
          <>
            <AchievementLoader />
            <AchievementLoader />
            <AchievementLoader />
            <AchievementLoader />
          </>
        )}
        {!loading &&
          achievements.map((achievement, index) => {
            return (
              <AchievementBox key={index}>
                <Achievement achievement={achievement} />
              </AchievementBox>
            );
          })}
      </Flex>
    </>
  );
};
