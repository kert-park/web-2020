import { ContentLoader, Rect } from '@kertPark/components/Atomic/ContentLoader';
import { H4 } from '@kertPark/components/Atomic/Typography';
import { mediaBreakpoints, spaces, theme } from '@kertPark/config/theme';
import { usePartnersQuery } from '@kertPark/graphql/generated';
import { Flex } from '@rebass/grid';
import React from 'react';
import styled from 'styled-components';

const PartnersWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;

  @media (${mediaBreakpoints.md}) {
    justify-content: flex-start;
  }
`;

const Image = styled.img`
  flex: 0 0 auto;
  max-width: 280px;
  max-height: 100px;
  margin: ${spaces[3]};
  object-fit: contain;
`;

const StyledContentLoader = styled(ContentLoader)`
  flex: 0 0 auto;
  width: 150px;
  height: 100px;
  margin: ${spaces[3]};
  border-radius: ${theme.radius.card};
`;

const PartnerLoader = () => {
  return (
    <StyledContentLoader viewBox="0 0 150 100">
      <Rect x="0" y="0" width="150" height="100" />
    </StyledContentLoader>
  );
};

export const Partners: React.FC = () => {
  const { data, loading } = usePartnersQuery();

  if (loading) {
    return (
      <PartnersWrapper>
        <PartnerLoader />
        <PartnerLoader />
        <PartnerLoader />
      </PartnersWrapper>
    );
  }

  const partnerLists = data?.partner?.data?.attributes?.partnerLists || [];

  return (
    <Flex flexDirection="column">
      {partnerLists.map((partnerList, index) => {
        const partners = partnerList?.partners || [];
        return (
          <React.Fragment key={partnerList?.slug || `partner-list-${index}`}>
            <H4 marginLeft={spaces[3]} marginTop={spaces[4]} marginBottom={spaces[1]}>
              {partnerList?.name}
            </H4>
            <PartnersWrapper>
              {partners.reduce((acc, partner) => {
                const imageSrc =
                  partner?.image.data?.attributes?.formats?.small?.url || partner?.image.data?.attributes?.url;
                if (imageSrc) {
                  // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
                  return [...acc, <Image key={partner?.id} alt={partner?.name} src={imageSrc} />];
                }
                return acc;
              }, [])}
            </PartnersWrapper>
          </React.Fragment>
        );
      })}
    </Flex>
  );
};
