import { Card } from '@kertPark/components/Atomic/Card';
import { useStatusQuery } from '@kertPark/graphql/generated';
import { Flex } from '@rebass/grid';
import React, { PropsWithChildren } from 'react';
import styled from 'styled-components';

const StyledCard = styled(Card)`
  background-color: ${({ theme }) => theme.colors.warning.background};
  color: ${({ theme }) => theme.colors.warning.text};
  width: fit-content;
`;

const StatusMessage: React.FC<PropsWithChildren> = ({ children }) => {
  return (
    <Flex alignItems="center" justifyContent="center" width={1}>
      <StyledCard>⚠️ {children}</StyledCard>
    </Flex>
  );
};

export const Status: React.FC = () => {
  const { data } = useStatusQuery();
  const { showIncompleteGamesData = false, showMaintenance = false } = data?.status?.data?.attributes || {};

  if (showMaintenance) {
    return <StatusMessage>Probíhá údržba a aktualizace, omluvte prosím případné nefunkční části webu.</StatusMessage>;
  } else if (showIncompleteGamesData) {
    return (
      <StatusMessage>
        Některá statistická data nyní nejsou k dispozici, omluvte prosím nekompletní zápasy, tabulky nebo statistiky.
      </StatusMessage>
    );
  }

  return null;
};
