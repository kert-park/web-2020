import { Overline } from '@kertPark/components/Atomic/Typography';
import { useCookiesContext } from '@kertPark/components/Cookies/CookiesContext';
import { SocialLinks } from '@kertPark/components/Layout/SocialLinks';
import { spaces } from '@kertPark/config/theme';
import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: ${spaces[3]} ${spaces[3]} ${spaces[5]};
`;

const A = styled.a`
  color: ${({ theme }) => theme.colors.textSecondary};
  &:hover {
    color: ${({ theme }) => theme.colors.textLight};
  }
`;

const StyledOverline = styled(Overline)`
  color: ${({ theme }) => theme.colors.textSecondary};
`;

const currentYear = new Date().getFullYear();

export const Footer = () => {
  const { setOpen } = useCookiesContext();
  return (
    <Wrapper>
      <SocialLinks />
      <StyledOverline bold={false}>© {currentYear} HC Kert Park Praha</StyledOverline>
      <StyledOverline bold={false} textTransform="none">
        Vytvořil Michal Pěch - <A href="https://www.michalpech.net">michalpech.net</A>
      </StyledOverline>
      <StyledOverline bold={false} textTransform="none">
        <A href="#" onClick={() => setOpen(true)}>
          Nastavení cookies
        </A>
      </StyledOverline>
    </Wrapper>
  );
};
