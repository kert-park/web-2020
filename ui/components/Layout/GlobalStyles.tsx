import { theme } from '@kertPark/config/theme';
import { createGlobalStyle, css } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
  html {
    box-sizing: border-box;
    height: 100%;
  }
  
  *, *:before, *:after {
    box-sizing: inherit;

    ${
      process.env.GRAYSCALE === 'true' &&
      css`
        -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
        filter: grayscale(100%);
      `
    }
  }
  
  body {
    color: ${({ theme }: { theme: Record<string, any> }) => theme.colors.textPrimary};
    font-family: ${theme.fonts.text};
    font-size: 16px;
    height: 100%;
    background-color: ${({ theme }: { theme: Record<string, any> }) => theme.colors.backgroundPrimary};
  }
`;
