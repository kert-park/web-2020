import { IconMenuAnimated } from '@kertPark/components/Icon/IconMenuAnimated';
import { MobileSidebarMenu } from '@kertPark/components/Layout/MobileSidebarMenu';
import { routes } from '@kertPark/config/routes';
import { mediaBreakpoints, spaces, theme } from '@kertPark/config/theme';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  width: 100%;

  @media (${mediaBreakpoints.md}) {
    display: none;
  }
`;

const Container = styled.div`
  position: fixed;
  display: flex;
  width: 100%;
  height: 3em;
  z-index: 3;
  flex-direction: row;
  align-items: stretch;
  padding: 0 ${spaces[2]};
  background: ${theme.gradients.blueRed};
  box-shadow: 0 0.25em 0.5em ${({ theme }) => theme.colors.boxShadow.hover};
`;

const LogoNameWrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
`;

const Logo = styled.img`
  width: 1.5em;
  height: 1.5em;
  flex: 0 0 1.5em;
  align-self: center;
`;

const Name = styled.div`
  flex: 1;
  align-self: center;
  color: ${theme.colors.kert.yellowPrimary};
  font-family: ${theme.fonts.header};
  text-transform: uppercase;
  font-weight: bold;
  font-style: italic;
  font-size: 1.125em;
  padding: 0 ${spaces[2]};
`;

const MenuWrapper = styled.div`
  flex: 0 0 auto;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding-left: ${spaces[4]};
`;

const MenuText = styled.div`
  color: ${theme.colors.kert.yellowPrimary};
  font-family: ${theme.fonts.header};
  text-transform: uppercase;
  font-size: 0.75em;
  margin-left: ${spaces[1]};
`;

const AchievementWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  align-self: flex-end;
  flex: 1;
  padding: 4em 0.5em 0;
`;

const AchievementText = styled.div`
  color: ${theme.colors.primary};
  font-family: ${theme.fonts.header};
  text-transform: uppercase;
  font-size: 0.875em;
  text-align: center;
  text-wrap: wrap;
`;

const TrophyWrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin-right: 0.5em;
`;

const Trophy = styled.img`
  height: 1em;
`;

export const MobileHeader = () => {
  const { asPath } = useRouter();
  const [menuOpen, setMenuOpen] = useState<boolean>(false);

  useEffect(() => {
    setMenuOpen(false);
  }, [asPath]);

  return (
    <>
      <Wrapper>
        <Container>
          <Link href={routes.index}>
            <LogoNameWrapper>
              <Logo src="/logo.png" alt="HC Kert Park Praha" />
              <Name>HC Kert Park Praha</Name>
            </LogoNameWrapper>
          </Link>
          <MenuWrapper onClick={() => setMenuOpen(!menuOpen)}>
            <IconMenuAnimated checked={menuOpen} />
            <MenuText>Menu</MenuText>
          </MenuWrapper>
        </Container>
        <AchievementWrapper>
          <TrophyWrapper>
            <Trophy src="/extraliga-pohar.png" alt="Mistr ČR 2017" />
            <Trophy src="/extraliga-pohar.png" alt="Mistr ČR 2018" />
            <Trophy src="/extraliga-pohar.png" alt="Mistr ČR 2019" />
            <Trophy src="/extraliga-pohar.png" alt="Mistr ČR 2021" />
            <Trophy src="/extraliga-pohar.png" alt="Mistr ČR 2022" />
            <Trophy src="/extraliga-pohar.png" alt="Mistr ČR 2023" />
            <Trophy src="/extraliga-pohar.png" alt="Mistr ČR 2024" />
          </TrophyWrapper>
          <AchievementText>
            Mistr&nbsp;ČR 2017,&nbsp;2018,&nbsp;2019, 2021,&nbsp;2022,&nbsp;2023,&nbsp;2024
          </AchievementText>
        </AchievementWrapper>
      </Wrapper>
      <MobileSidebarMenu isOpen={menuOpen} />
    </>
  );
};
