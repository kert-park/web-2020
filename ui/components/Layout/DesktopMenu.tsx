import { LinkAnchor } from '@kertPark/components/Atomic/LinkAnchor';
import { mainMenu, MenuItemType, MenuLinkType } from '@kertPark/config/routes';
import { spaces } from '@kertPark/config/theme';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import styled, { css } from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  height: 1.75em;
`;

const menuItemMarginCss = css`
  &:not(:last-child) {
    margin-right: 1em;
  }
`;

const menuItemCss = css`
  font-size: 1.125em;
  font-weight: bold;
  cursor: pointer;
`;

const Caret = styled.span`
  display: inline-block;
  width: 0;
  height: 0;
  margin-left: 0.25em;
  vertical-align: middle;
  border-top: 0.25em solid;
  border-right: 0.25em solid transparent;
  border-left: 0.25em solid transparent;
`;

const MenuDropdownWrapper = styled.div`
  position: relative;
  ${menuItemMarginCss};
`;

const MenuLink = styled(LinkAnchor)`
  ${menuItemCss};
  ${menuItemMarginCss};
  color: ${({ theme }) => theme.colors.primary};
  text-decoration: none;

  &:hover {
    color: ${({ theme }) => theme.colors.secondary};
  }
  &:focus {
    color: ${({ theme }) => theme.colors.primary};
    outline: none;
    border: 0;
  }
`;

const MenuItem = styled.div<{ open: boolean }>`
  ${menuItemCss};
  z-index: 3;
  &:hover {
    color: ${({ theme }) => theme.colors.secondary};
  }

  ${MenuDropdownWrapper}:hover & {
    color: ${({ theme }) => theme.colors.secondary};
  }
`;

const DropdownWrapper = styled.div<{ open: boolean }>`
  display: ${({ open }) => (open ? 'flex' : 'none')};
  z-index: 2;
  position: absolute;
  top: 0.5em;
  left: -0.75em;
  background: transparent;
  padding-top: 1em;
`;

const Dropdown = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 9em;
  background-color: ${({ theme }) => theme.colors.backgroundSecondary};
  padding: 0.75em;
  border-radius: 0.25em;
  box-shadow: 0 0.375em 0.75em ${({ theme }) => theme.colors.boxShadow.hover};
`;

const DropdownLink = styled(LinkAnchor)`
  font-size: 1em;
  color: ${({ theme }) => theme.colors.primary};
  text-decoration: none;

  &:hover {
    color: ${({ theme }) => theme.colors.secondary};
  }
  &:focus {
    color: ${({ theme }) => theme.colors.primary};
    outline: none;
    border: 0;
  }

  & + & {
    margin-top: ${spaces[2]};
  }
`;

const MenuDropdown = ({ menuItem }: { menuItem: MenuItemType }) => {
  const { asPath } = useRouter();
  const [open, setOpen] = useState(false);

  useEffect(() => {
    setOpen(false);
  }, [asPath]);

  return (
    <MenuDropdownWrapper onMouseLeave={() => setOpen(false)}>
      <MenuItem key={menuItem.name} open={open} onClick={() => setOpen(!open)} onMouseEnter={() => setOpen(true)}>
        {menuItem.name}
        <Caret />
      </MenuItem>
      <DropdownWrapper open={open}>
        <Dropdown>
          {menuItem.items?.map((item: MenuLinkType) => (
            <DropdownLink key={item.name} href={item.link || ''} hrefAs={item.linkAs}>
              {item.name}
            </DropdownLink>
          ))}
        </Dropdown>
      </DropdownWrapper>
    </MenuDropdownWrapper>
  );
};

export const DesktopMenu = () => {
  return (
    <Wrapper>
      {mainMenu.map((menuItem: MenuItemType) => {
        if (menuItem.link) {
          return (
            <MenuLink key={menuItem.name} href={menuItem.link} hrefAs={menuItem.linkAs}>
              {menuItem.name}
            </MenuLink>
          );
        }
        return <MenuDropdown key={menuItem.name} menuItem={menuItem} />;
      })}
    </Wrapper>
  );
};
