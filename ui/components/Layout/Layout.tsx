import { Achievements } from '@kertPark/components/Layout/Achievements';
import { DesktopHeader } from '@kertPark/components/Layout/DesktopHeader';
import { Footer } from '@kertPark/components/Layout/Footer';
import { MobileHeader } from '@kertPark/components/Layout/MobileHeader';
import { Partners } from '@kertPark/components/Layout/Partners';
import { Status } from '@kertPark/components/Layout/Status';
import { getThumbImageUrl, ImageSize } from '@kertPark/config/images';
import { breakpoints, mediaBreakpoints, spaces } from '@kertPark/config/theme';
import Head from 'next/head';
import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Content = styled.div`
  z-index: 1;
  width: 100%;
  padding: ${spaces[2]};

  @media (${mediaBreakpoints.md}) {
    width: ${breakpoints.md};
    padding: ${spaces[3]};
  }

  @media (${mediaBreakpoints.lg}) {
    width: ${breakpoints.lg};
  }

  @media (${mediaBreakpoints.xl}) {
    width: ${breakpoints.xl};
  }

  @media (${mediaBreakpoints.xxl}) {
    width: ${breakpoints.xxl};
  }
`;

type Props = {
  description?: string;
  metadata?: Array<[key: string, value: string]>;
  noRobots?: boolean;
  title?: Array<string>;
  withPartners?: boolean;
};

export const Layout: ReactFCWithChildren<Props> = ({
  children,
  description,
  metadata = [],
  noRobots = false,
  title = [],
  withPartners = true,
}) => {
  const joinedTitle = [...title, 'HC Kert Park Praha'].join(' | ');
  const hasOgType = metadata.some(([key]) => key === 'og:type');
  const hasOgImage = metadata.some(([key]) => key === 'og:image');

  return (
    <>
      <Head>
        <title>{joinedTitle}</title>
        {noRobots && <meta name="robots" content="noindex" />}
        <meta property="og:title" content={joinedTitle} />
        <meta property="og:description" content={description || joinedTitle} />
        <meta name="description" content={description || joinedTitle} />
        {metadata.map(([key, value]) => (
          <meta key={key} property={key} content={value} />
        ))}
        {!hasOgType && <meta property="og:type" content="website" />}
        {!hasOgImage && <meta property="og:image" content={getThumbImageUrl({ size: ImageSize.medium })} />}
      </Head>
      <Wrapper>
        <DesktopHeader />
        <MobileHeader />
        <Content>
          <Status />
          {children}
          {withPartners && <Partners />}
          <Achievements />
        </Content>
        <Footer />
      </Wrapper>
    </>
  );
};
