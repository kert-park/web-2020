import { AspectRatio, ImageAspectRatio } from '@kertPark/components/Atomic/ImageAspectRatio';
import { Overline } from '@kertPark/components/Atomic/Typography';
import { CommonAchievementFragment } from '@kertPark/graphql/generated';
import { useKertTheme } from '@kertPark/hooks/useKertTheme';
import React from 'react';
import styled, { css } from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 0.5em;
`;

const Image = styled(ImageAspectRatio)<Pick<CommonAchievementFragment, 'isWinner'>>`
  width: 7.5em;
  height: 7.5em;

  ${({ isWinner }) =>
    !isWinner &&
    css`
      filter: grayscale(100%);
      opacity: 0.35;
    `}
`;

type Props = {
  achievement: CommonAchievementFragment;
};

export const Achievement = ({ achievement }: Props) => {
  const { colors } = useKertTheme();
  const imageSrc =
    achievement.image.data?.attributes?.formats?.thumbnail?.url || achievement.image.data?.attributes?.url;
  const count = achievement.years?.split(',').length || 1;

  return (
    <Container>
      <Image isWinner={achievement.isWinner} src={imageSrc} ratio={AspectRatio.oneToOne} title={achievement.title} />
      <Overline as="div" color={colors.textPrimary} textAlign="center">
        {`${count > 1 ? `${count}\u00d7\u00a0` : ''}${achievement.title}`}
      </Overline>
      <Overline as="div" small textAlign="center">
        {achievement.years}
      </Overline>
    </Container>
  );
};
