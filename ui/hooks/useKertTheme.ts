import { theme } from '@kertPark/config/theme';
import { useContext } from 'react';
import { ThemeContext } from 'styled-components';

export const useKertTheme = () => {
  const kertTheme = useContext<typeof theme | undefined>(ThemeContext);

  if (!kertTheme) {
    throw new Error('useKertTheme must be used within a theme provider');
  }

  return kertTheme;
};
