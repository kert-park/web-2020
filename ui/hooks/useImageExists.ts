import { useEffect, useState } from 'react';

type Props = { src?: string };
type Result = { exists: boolean };

export const useImageExists = ({ src }: Props): Result => {
  const [exists, setExists] = useState(false);

  useEffect(() => {
    const img = new Image();
    if (src) {
      img.src = src;
      img.onload = () => setExists(true);
    }
    return () => {
      img.onload = null;
    };
  }, [src]);

  return { exists };
};
