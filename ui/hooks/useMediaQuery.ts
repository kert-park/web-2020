import { useContext, useEffect, useState } from 'react';
import { ThemeContext } from 'styled-components';

export const useMediaQuery = (queryInput: string): boolean => {
  const theme = useContext(ThemeContext);
  const { ssrMatchMedia } = theme;

  let query = `(${queryInput})`;
  query = query.replace(/^@media( ?)/m, '');

  // Wait for jsdom to support the match media feature.
  // This defensive check is here for simplicity.
  // Most of the time, the match media logic isn't central to people tests.
  const supportMatchMedia = typeof window !== 'undefined' && typeof window.matchMedia !== 'undefined';
  const matchMedia = supportMatchMedia ? window.matchMedia : null;

  const [match, setMatch] = useState<boolean>(() => {
    if (supportMatchMedia) {
      return !!matchMedia?.(query).matches;
    }
    if (ssrMatchMedia) {
      return ssrMatchMedia(query).matches;
    }
    // Once the component is mounted, we rely on the
    // event listeners to return the correct matches value.
    return false;
  });

  useEffect(() => {
    let active = true;

    if (!supportMatchMedia) {
      return undefined;
    }

    const queryList = matchMedia?.(query);
    const updateMatch = () => {
      if (active) {
        setMatch(!!queryList?.matches);
      }
    };
    updateMatch();
    queryList?.addListener(updateMatch);
    return () => {
      active = false;
      queryList?.removeListener(updateMatch);
    };
  }, [query, matchMedia, supportMatchMedia]);

  return match;
};
