import { useMemo, useState } from 'react';

export type SortFunction<TData> = (isDesc: boolean) => (a: TData, b: TData) => number;
export type SortFunctions<TData, TSortKeys extends string> = Record<TSortKeys, SortFunction<TData>>;

type SortState<TSortKeys> = {
  key: TSortKeys;
  isDesc: boolean;
};

type Props<TData, TSortKeys extends string> = {
  data: Array<TData>;
  defaultSortKey: TSortKeys;
  sortFunctions: SortFunctions<TData, TSortKeys>;
};

export const useSortData = <TData, TSortKeys extends string>({
  data,
  defaultSortKey,
  sortFunctions,
}: Props<TData, TSortKeys>) => {
  const [sortState, setSortState] = useState<SortState<TSortKeys>>({
    key: defaultSortKey,
    isDesc: true,
  });

  const handleSortKeyChange = (key: TSortKeys) => {
    setSortState((prevSortState) => {
      if (prevSortState.key === key) {
        return { key, isDesc: !prevSortState.isDesc };
      } else {
        return { key, isDesc: true };
      }
    });
  };

  const sortedData = useMemo(() => {
    if (!sortState.key) {
      return data;
    }
    return [...data].sort(sortFunctions[sortState.key](sortState.isDesc));
  }, [data, sortState.key, sortState.isDesc, sortFunctions]);

  return {
    handleSortKeyChange,
    sortState,
    sortedData,
  };
};

export const sortNumber =
  <TData extends Record<any, any>>(key: KeysWithTypeNumber<TData>): SortFunction<TData> =>
  (isDesc) =>
  (a, b) => {
    const aVal = a[key] || 0;
    const bVal = b[key] || 0;
    if (isDesc) {
      return bVal - aVal;
    } else {
      return aVal - bVal;
    }
  };

export const sortNumberAsc =
  <TData extends Record<any, any>>(key: KeysWithTypeNumber<TData>): SortFunction<TData> =>
  (isAsc) =>
  (a, b) => {
    const aVal = a[key] || 0;
    const bVal = b[key] || 0;
    if (isAsc) {
      return aVal - bVal;
    } else {
      return bVal - aVal;
    }
  };

export const sortStringAsc =
  <TData extends Record<any, any>>(key: KeysWithTypeString<TData>): SortFunction<TData> =>
  (isAsc) =>
  (a, b) => {
    const aVal = typeof a[key] === 'string' ? (a[key] as string) : '';
    const bVal = typeof b[key] === 'string' ? (b[key] as string) : '';
    if (isAsc) {
      return aVal.localeCompare(bVal, 'cs-CZ');
    } else {
      return bVal.localeCompare(aVal, 'cs-CZ');
    }
  };

type KeysWithTypeNumber<T> = {
  [K in keyof T]: T[K] extends number | null | undefined ? K : never;
}[keyof T];

type KeysWithTypeString<T> = {
  [K in keyof T]: T[K] extends string | null | undefined ? K : never;
}[keyof T];
