import { useEffect, useLayoutEffect, useRef } from 'react';

/**
 * Reused https://usehooks.com/useclickaway
 */
export const useClickAway = <TElement extends Element>(callback: (event: Event) => void) => {
  const ref = useRef<TElement>(null);
  const refCb = useRef(callback);

  useLayoutEffect(() => {
    refCb.current = callback;
  });

  useEffect(() => {
    const handler = (event: Event) => {
      const element = ref.current;
      // @ts-ignore
      if (element && !element.contains(event.target)) {
        refCb.current(event);
      }
    };

    document.addEventListener('mousedown', handler);
    document.addEventListener('touchstart', handler);

    return () => {
      document.removeEventListener('mousedown', handler);
      document.removeEventListener('touchstart', handler);
    };
  }, []);

  return ref;
};
