import { PostsCompositeQueryPostType } from '@kertPark/graphql/generated/helpers';
import { debounce } from '@kertPark/lib/debounce';
import { useCallback, useLayoutEffect, useMemo, useRef, useState } from 'react';

export const REST_POST_ID = 'rest-posts';
const THUMB_POSTS_COUNT = 2;
const MIN_REST_POSTS_COUNT = 3;
const MOBILE_REST_POSTS_COUNT = 3;

type Props = {
  posts: Array<PostsCompositeQueryPostType>;
};

export const useHomePagePosts = ({ posts = [] }: Props) => {
  const [restPostsLimit, setRestPostsLimit] = useState<number | undefined>(undefined);
  const mainContentRef = useRef<HTMLDivElement>(null);
  const sidebarRef = useRef<HTMLDivElement>(null);

  const thumbPosts = useMemo(() => posts.slice(0, THUMB_POSTS_COUNT), [posts]);
  const mobileRestPosts = useMemo(
    () => posts.slice(THUMB_POSTS_COUNT, THUMB_POSTS_COUNT + MOBILE_REST_POSTS_COUNT),
    [posts],
  );
  const restPosts = useMemo(
    () =>
      posts.slice(
        THUMB_POSTS_COUNT,
        typeof restPostsLimit === 'number' ? restPostsLimit + THUMB_POSTS_COUNT : undefined,
      ),
    [posts, restPostsLimit],
  );

  const calculateRestPostsLimit = useCallback(() => {
    if (mainContentRef.current && sidebarRef.current) {
      const mainContentHeight = mainContentRef.current.clientHeight;
      const sidebarHeight = sidebarRef.current.clientHeight;
      const restPostsWrapper = mainContentRef.current.children.namedItem(REST_POST_ID);
      const restPostsHeight = restPostsWrapper?.clientHeight;

      if (mainContentHeight && sidebarHeight && restPostsHeight) {
        const mainContentHeightWithoutRestPosts = mainContentHeight - restPostsHeight;
        let postsToShow = 0;
        let totalMainContentHeight = mainContentHeightWithoutRestPosts;
        const restPostsElements = restPostsWrapper.children;

        for (let i = 0; i < restPostsElements.length; i++) {
          totalMainContentHeight += restPostsElements[i].clientHeight;
          postsToShow++;
          if (postsToShow >= MIN_REST_POSTS_COUNT && totalMainContentHeight > sidebarHeight) {
            setRestPostsLimit(postsToShow);
            break;
          }
        }
      } else {
        setRestPostsLimit(undefined);
      }
    } else {
      setRestPostsLimit(undefined);
    }
  }, []);

  useLayoutEffect(() => {
    calculateRestPostsLimit();
    const handleResize = debounce(calculateRestPostsLimit, 100);
    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, [posts]);

  return {
    mainContentRef,
    mobileRestPosts,
    restPosts,
    sidebarRef,
    thumbPosts,
  };
};
