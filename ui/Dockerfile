# Build
FROM node:gallium-alpine AS build

# Args
ARG APP_PATH
ARG DOMAIN
ARG GOOGLE_ANALYTICS_ID
ARG GRAPHQL_PATH
ARG GRAYSCALE
ARG IMAGE_DOMAINS
ARG OLD_WEB_PATH
ARG SEASON_YEAR
ARG STATIC_PATH
ARG TEAMS

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Copy files
COPY package.json package.json
COPY yarn.lock yarn.lock
COPY . /usr/src/app

# Set timezone
RUN apk add --no-cache tzdata
ENV TZ=Europe/Prague

# Install dependencies and build
RUN yarn install --frozen-lockfile --non-interactive
RUN yarn build
RUN npm prune --production

# Prod
FROM node:gallium-alpine AS prod

# Args
ARG APP_PATH
ARG DOMAIN
ARG GOOGLE_ANALYTICS_ID
ARG GRAPHQL_PATH
ARG GRAYSCALE
ARG IMAGE_DOMAINS
ARG OLD_WEB_PATH
ARG SEASON_YEAR
ARG STATIC_PATH
ARG TEAMS

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY --from=build /usr/src/app/package.json ./
COPY --from=build /usr/src/app/yarn.lock ./
COPY --from=build /usr/src/app/node_modules ./node_modules
COPY --from=build /usr/src/app/.next ./.next
COPY --from=build /usr/src/app/public ./public

EXPOSE 3000

CMD [ "yarn", "start:prod" ]
