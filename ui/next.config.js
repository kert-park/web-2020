require('dotenv').config();
const { redirects } = require('./lib/oldWebRedirectConfig');

const imageDomains = (process.env.IMAGE_DOMAINS || '').split(',');
const teams = (process.env.TEAMS || '').split(',');

const env = {
  APP_PATH: process.env.APP_PATH,
  DOMAIN: process.env.DOMAIN,
  GOOGLE_ANALYTICS_ID: process.env.GOOGLE_ANALYTICS_ID,
  GRAPHQL_PATH: process.env.GRAPHQL_PATH,
  GRAYSCALE: process.env.GRAYSCALE,
  OLD_WEB_PATH: process.env.OLD_WEB_PATH,
  SEASON_YEAR: process.env.SEASON_YEAR,
  STATIC_PATH: process.env.STATIC_PATH,
  TEAMS: teams,
};

console.log('Build time envs', env);
console.log('Redirects', redirects);

module.exports = {
  compiler: {
    styledComponents: true,
  },
  env,
  eslint: {
    // Warning: Dangerously allow production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
  images: {
    domains: imageDomains,
  },
  reactStrictMode: true,
  swcMinify: true,
  async redirects() {
    return redirects;
  },
};
