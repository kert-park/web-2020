import { teamsObject } from '@kertPark/config/teams';
import { GameFilterPlaceEnum } from '@kertPark/graphql/generated';
import { GameType, TournamentType } from '@kertPark/graphql/generated/helpers';
import { endOfDay, endOfWeek, startOfDay, startOfWeek } from 'date-fns';

export enum TextFilterPlace {
  ALL = 'vse',
  AWAY = 'venku',
  HOME = 'doma',
  HOME_GROUND = 'na-luzinach',
}

export enum TextFilterSchedule {
  ALL = 'vse',
  RESULT = 'odehrane',
  UPCOMING = 'nadchazejici',
  PLAY_OFF = 'play-off',
}

export enum UiScheduleEnum {
  PLAY_OFF = 'play-off',
}

export const getGameFilterPlace = (query: TextFilterPlace | string | undefined): GameFilterPlaceEnum => {
  switch (query) {
    case TextFilterPlace.AWAY:
      return GameFilterPlaceEnum.AWAY;
    case TextFilterPlace.HOME:
      return GameFilterPlaceEnum.HOME;
    case TextFilterPlace.HOME_GROUND:
      return GameFilterPlaceEnum.HOME_GROUND;
    case TextFilterPlace.ALL:
    default:
      return GameFilterPlaceEnum.ALL;
  }
};

export const getTextFilterPlace = (gameFilterPlaceEnum: GameFilterPlaceEnum): TextFilterPlace => {
  switch (gameFilterPlaceEnum) {
    case GameFilterPlaceEnum.AWAY:
      return TextFilterPlace.AWAY;
    case GameFilterPlaceEnum.HOME:
      return TextFilterPlace.HOME;
    case GameFilterPlaceEnum.HOME_GROUND:
      return TextFilterPlace.HOME_GROUND;
    case GameFilterPlaceEnum.ALL:
      return TextFilterPlace.ALL;
  }
};

export const filterPlaceOptions = [
  { text: 'vše', value: GameFilterPlaceEnum.ALL },
  { text: 'na lužinách', value: GameFilterPlaceEnum.HOME_GROUND },
  { text: 'domácí', value: GameFilterPlaceEnum.HOME },
  { text: 'hosté', value: GameFilterPlaceEnum.AWAY },
];

export const getDateRange = (date: Date) => {
  return {
    from: startOfDay(startOfWeek(date, { weekStartsOn: 1 })),
    to: endOfDay(endOfWeek(date, { weekStartsOn: 1 })),
  };
};

export const mergeGamesAndTournamentsAndSort = (
  games: Array<GameType>,
  tournaments: Array<TournamentType>,
): Array<XOR<GameType, TournamentType>> => {
  return [
    ...games,
    ...tournaments.map((tournament) => ({ ...tournament, date: endOfDay(new Date(tournament.date)) })),
  ].sort((a, b) => {
    const dateDiff = new Date(a.date).getTime() - new Date(b.date).getTime();

    if (dateDiff === 0) {
      return (teamsObject[a.team.systemName]?.order || 0) - (teamsObject[b.team.systemName]?.order || 0);
    }

    return dateDiff;
  });
};

export type GameAndTournamentGroupType = {
  label: string;
  items: Array<XOR<GameType, TournamentType>>;
};

export const groupGamesAndTournamentsByLabel = (
  gamesAndTournaments: Array<XOR<GameType, TournamentType>>,
  labelWithTeamName = false,
): Array<GameAndTournamentGroupType> => {
  return gamesAndTournaments.reduce<Array<GameAndTournamentGroupType>>((acc, item) => {
    const label = item.__typename === 'Tournament' ? 'Turnaj' : item.label || '';
    const itemLabel = [...(labelWithTeamName ? [item.team.name] : []), label].join(' - ');
    const isLastGroupLabelSame = acc.length > 0 && acc[acc.length - 1].label === itemLabel;
    if (isLastGroupLabelSame) {
      const last = acc.pop() as GameAndTournamentGroupType;
      last.items.push(item);
      return [...acc, last];
    }
    return [...acc, { label: itemLabel, items: [item] }];
  }, []);
};
