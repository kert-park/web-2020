import { getYouTubeVideoId } from '@kertPark/lib/url';

describe('getYouTubeVideoId', () => {
  it('returns the video ID from a standard YouTube link', () => {
    const link = 'https://www.youtube.com/watch?v=dQw4w9WgXcQ';
    const videoId = getYouTubeVideoId(link);
    expect(videoId).toEqual('dQw4w9WgXcQ');
  });

  it('returns the video ID from a short YouTube link', () => {
    const link = 'https://youtu.be/dQw4w9WgXcQ';
    const videoId = getYouTubeVideoId(link);
    expect(videoId).toEqual('dQw4w9WgXcQ');
  });

  it('returns the video ID from an embedded YouTube link', () => {
    const link = 'https://www.youtube.com/embed/dQw4w9WgXcQ';
    const videoId = getYouTubeVideoId(link);
    expect(videoId).toEqual('dQw4w9WgXcQ');
  });

  it('returns null for a non-YouTube link', () => {
    const link = 'https://example.com';
    const videoId = getYouTubeVideoId(link);
    expect(videoId).toBeUndefined();
  });

  it('returns null for an invalid YouTube link', () => {
    const link = 'https://www.youtube.com/watch?v=invalid';
    const videoId = getYouTubeVideoId(link);
    expect(videoId).toBeUndefined();
  });
});
