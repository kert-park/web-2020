import { activeTeams } from '@kertPark/config/teams';
import { CommonOfficialListFragment } from '@kertPark/graphql/generated';
import { CategoriesQueryCategoriesType } from '@kertPark/graphql/generated/helpers';

export const getOfficialListFromCategories = (categories: CategoriesQueryCategoriesType = []) => {
  return activeTeams.reduce<Array<CommonOfficialListFragment>>((acc, team) => {
    const category = categories.find((cat) => cat.attributes?.code === team.id);
    if (category) {
      acc.push({
        __typename: 'ComponentCommonOfficialList',
        id: team.id,
        officials: category.attributes?.officials || [],
        showContactInformation: true,
        title: team.name,
      });
    }
    return acc;
  }, []);
};
