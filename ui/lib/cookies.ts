import Cookies, { CookieAttributes } from 'js-cookie';

export type CookiesSettingsType = {
  functional: true;
  analytics: boolean;
  social: boolean;
};

export enum CookieKey {
  cookieSettings = 'kertpark-cookie',
}

export type SetCookieProps = {
  key: string;
  value: string;
  options: CookieAttributes;
};
export const setCookie = ({ key, value, options }: SetCookieProps) => {
  const opt = {
    sameSite: 'lax' as CookieAttributes['sameSite'],
    secure: true,
    ...options,
  };
  Cookies.set(key, value, opt);
};

export const getCookie = (key: string): string | undefined => Cookies.get(key);

export const initialCookieSettings: CookiesSettingsType = {
  functional: true,
  analytics: false,
  social: false,
};

const isCookieSettings = (value: unknown): value is CookiesSettingsType => {
  return (
    typeof value === 'object' &&
    value !== null &&
    'functional' in value &&
    'analytics' in value &&
    'social' in value &&
    value.functional === true &&
    typeof value.analytics === 'boolean' &&
    typeof value.social === 'boolean'
  );
};

export const getCookieSettings = (): CookiesSettingsType | undefined => {
  const value = getCookie(CookieKey.cookieSettings);

  if (!value) {
    return undefined;
  }
  try {
    const settings = JSON.parse(value);
    if (isCookieSettings(settings)) {
      return settings;
    }
    return undefined;
  } catch {
    return undefined;
  }
};
