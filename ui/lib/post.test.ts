import { getCategoriesNameJoined, getMostRelevantCategoryCode } from '@kertPark/lib/post';

const categoryDefintion = {
  muzia: {
    __typename: 'CategoryEntity',
    id: 'muzia',
    attributes: { __typename: 'Category', code: 'muzia', name: 'Muži A' },
  },
  dorost: {
    __typename: 'CategoryEntity',
    id: 'dorost',
    attributes: { __typename: 'Category', code: 'dorost', name: 'Dorost' },
  },
  juniori: {
    __typename: 'CategoryEntity',
    id: 'juniori',
    attributes: { __typename: 'Category', code: 'juniori', name: 'Junioři' },
  },
  starsiZaci: {
    __typename: 'CategoryEntity',
    id: 'starsi-zaci',
    attributes: { __typename: 'Category', code: 'starsi-zaci', name: 'Starší žáci' },
  },
  brankari: {
    __typename: 'CategoryEntity',
    id: 'brankari',
    attributes: { __typename: 'Category', code: 'brankari', name: 'Brankáři' },
  },
  fanshop: {
    __typename: 'CategoryEntity',
    id: 'fanshop',
    attributes: { __typename: 'Category', code: 'fanshop', name: 'Fanshop' },
  },
} as const;

describe('lib/post', () => {
  describe('get categories name joined', () => {
    it('undefined categories', () => {
      expect(getCategoriesNameJoined({ categories: undefined })).toBe('Klub');
    });

    it('one category', () => {
      expect(
        getCategoriesNameJoined({
          categories: [categoryDefintion.muzia],
        }),
      ).toBe('Muži A');
    });

    it('more categories', () => {
      expect(
        getCategoriesNameJoined({
          categories: [categoryDefintion.muzia, categoryDefintion.dorost, categoryDefintion.juniori],
        }),
      ).toBe('Muži A, Junioři, Dorost');
    });

    it('more categories', () => {
      expect(
        getCategoriesNameJoined({
          categories: [
            categoryDefintion.fanshop,
            categoryDefintion.brankari,
            categoryDefintion.muzia,
            categoryDefintion.dorost,
            categoryDefintion.juniori,
          ],
        }),
      ).toBe('Muži A, Junioři, Dorost, Fanshop, Brankáři');
    });
  });

  describe('get most relevant category code', () => {
    it('undefined category', () => {
      expect(getMostRelevantCategoryCode({ categories: undefined })).toBe('klub');
    });

    it('one category', () => {
      expect(
        getMostRelevantCategoryCode({
          categories: [categoryDefintion.muzia],
        }),
      ).toBe('muzia');
    });

    it('two or more team categories', () => {
      expect(
        getMostRelevantCategoryCode({
          categories: [categoryDefintion.muzia, categoryDefintion.dorost],
        }),
      ).toBe('muzia');
      expect(
        getMostRelevantCategoryCode({
          categories: [categoryDefintion.dorost, categoryDefintion.muzia],
        }),
      ).toBe('muzia');
      expect(
        getMostRelevantCategoryCode({
          categories: [categoryDefintion.dorost, categoryDefintion.muzia, categoryDefintion.starsiZaci],
        }),
      ).toBe('muzia');
    });

    it('one team and more other categories', () => {
      expect(
        getMostRelevantCategoryCode({
          categories: [categoryDefintion.muzia, categoryDefintion.brankari],
        }),
      ).toBe('muzia');
      expect(
        getMostRelevantCategoryCode({
          categories: [categoryDefintion.brankari, categoryDefintion.muzia, categoryDefintion.fanshop],
        }),
      ).toBe('muzia');
      expect(
        getMostRelevantCategoryCode({
          categories: [categoryDefintion.muzia, categoryDefintion.brankari, categoryDefintion.fanshop],
        }),
      ).toBe('muzia');
    });

    it('two or more other categories', () => {
      expect(
        getMostRelevantCategoryCode({
          categories: [categoryDefintion.brankari, categoryDefintion.fanshop],
        }),
      ).toBe('brankari');
      expect(
        getMostRelevantCategoryCode({
          categories: [categoryDefintion.fanshop, categoryDefintion.brankari],
        }),
      ).toBe('fanshop');
    });
  });
});
