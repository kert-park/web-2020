import { GameFilterPlaceEnum, GameFilterScheduleEnum } from '@kertPark/graphql/generated';
import { GameType } from '@kertPark/graphql/generated/helpers';
import { getDateObject } from '@kertPark/lib/date';
import { UiScheduleEnum } from '@kertPark/lib/gamePage';
import { compareAsc, compareDesc, differenceInMinutes, format } from 'date-fns';

const APP_PATH = process.env.APP_PATH;

export enum GameStatus {
  ok = 'ok',
  p = 'p',
  pp = 'pp',
  ss = 'ss',
  k = 'k',
  od = 'od',
}

export enum GameStatusName {
  k = 'kontumace',
  od = 'odloženo',
}

export const GamePlayedStatuses = [GameStatus.ok, GameStatus.p, GameStatus.pp, GameStatus.ss];
export const GameOtherStatuses = [GameStatus.k, GameStatus.od];

export const getGameResult = (game: GameType, swap = false): { score: string; hasWin: boolean } => {
  const { home, away } = game.score;
  const isHomeTeam = game.home.cmshbId === '3100003';
  const hasHomeTeamWin = (home.finalScore || 0) > (away.finalScore || 0);
  const hasKertParkWin = (isHomeTeam && hasHomeTeamWin) || (!isHomeTeam && !hasHomeTeamWin);
  return {
    score: `${swap ? away.finalScore : home.finalScore}:${swap ? home.finalScore : away.finalScore}${
      [GameStatus.p, GameStatus.pp, GameStatus.ss].includes(game.status as GameStatus) ? game.status : ''
    }`,
    hasWin: hasKertParkWin,
  };
};

export const groupGamesByDate = <TGame extends GameType = GameType>(
  games: Array<TGame>,
): { [key: string]: Array<TGame> } => {
  return games.reduce((acc, curr) => {
    const dateKey = format(getDateObject(curr.date), 'yyyy-MM-dd');
    const dateArray = acc[dateKey] || [];
    return { ...acc, [dateKey]: [...dateArray, curr] };
  }, {});
};

export type GameGroupType<TGame extends GameType = GameType> = { label: string; games: Array<TGame> };

export const groupGamesByLabel = <TGame extends GameType = GameType>(
  games: Array<TGame>,
  withTeamName = false,
): Array<GameGroupType<TGame>> => {
  return games.reduce<Array<GameGroupType<TGame>>>((acc, game) => {
    const gameLabel = [...(withTeamName ? [game.team.name] : []), game.label || ''].join(' - ');
    const isLastGroupLabelSame = acc.length > 0 && acc[acc.length - 1].label === gameLabel;
    if (isLastGroupLabelSame) {
      const last = acc.pop() as GameGroupType<TGame>;
      last.games.push(game);
      return [...acc, last];
    }
    return [...acc, { label: gameLabel, games: [game] }];
  }, []);
};

type FilterAndSortGamesProps<TGame extends GameType = GameType> = {
  filterPlace: GameFilterPlaceEnum;
  filterSchedule: GameFilterScheduleEnum | UiScheduleEnum;
  games: Array<TGame>;
};

export const filterAndSortGames = <TGame extends GameType = GameType>({
  filterPlace,
  filterSchedule,
  games,
}: FilterAndSortGamesProps<TGame>): Array<TGame> => {
  return games
    .map((game) => ({ ...game }))
    .reduce((acc, game) => {
      if (!pipeFilter([filterByPlace({ game, filterPlace }), filterBySchedule({ game, filterSchedule })])) {
        return acc;
      }
      return [...acc, game];
    }, [])
    .sort(sortGames({ filterSchedule }));
};

export const isGamePlayed = <TGame extends GameType = GameType>(game: TGame): boolean => {
  return !!(game.status && game.status !== 'od');
};

export const isNearestGamePlayed = <TGame extends GameType = GameType>(games: Array<TGame>): boolean => {
  if (games.length > 0) {
    const now = new Date();
    const sorted = games
      .map((game) => ({ ...game, gameDiff: Math.abs(differenceInMinutes(now, getDateObject(game.date))) }))
      .sort((a, b) => a.gameDiff - b.gameDiff);
    const game = sorted[0];
    return isGamePlayed(game);
  }

  return false;
};

const pipeFilter = (filters: Array<boolean>): boolean => {
  return filters.reduce<boolean>((acc, curr) => {
    if (!curr) {
      return curr;
    }
    return acc;
  }, true);
};

const filterByPlace = <TGame extends GameType = GameType>({
  game,
  filterPlace,
}: {
  game: TGame;
  filterPlace: GameFilterPlaceEnum | UiScheduleEnum;
}): boolean => {
  switch (filterPlace) {
    case GameFilterPlaceEnum.HOME:
      return game.home.cmshbId === '3100003';
    case GameFilterPlaceEnum.AWAY:
      return game.away.cmshbId === '3100003';
    case GameFilterPlaceEnum.HOME_GROUND:
      return ['luziny', 'lužiny'].some((place) => game.place?.toLowerCase().includes(place));
    case GameFilterPlaceEnum.ALL:
    default:
      return true;
  }
};

const filterBySchedule = <TGame extends GameType = GameType>({
  game,
  filterSchedule,
}: {
  game: TGame;
  filterSchedule: GameFilterScheduleEnum | UiScheduleEnum;
}): boolean => {
  switch (filterSchedule) {
    case GameFilterScheduleEnum.RESULT:
      return isGamePlayed(game);
    case GameFilterScheduleEnum.UPCOMING:
      return !isGamePlayed(game);
    case GameFilterScheduleEnum.ALL:
    default:
      return true;
  }
};

const sortGames =
  ({ filterSchedule }: { filterSchedule: GameFilterScheduleEnum | UiScheduleEnum }) =>
  <TGame extends GameType = GameType>(a: TGame, b: TGame): number => {
    switch (filterSchedule) {
      case GameFilterScheduleEnum.RESULT:
        return compareDesc(new Date(a.date), new Date(b.date));
      default:
        return compareAsc(new Date(a.date), new Date(b.date));
    }
  };

type GetGameImageProps = {
  gameId: string;
  teamId: string;
  year: string;
};

export const getGameImage = ({ gameId, teamId, year }: GetGameImageProps): string => {
  return `${APP_PATH}/api/og-images/web/game-result?id=${gameId}&teamId=${teamId}&year=${year}`;
};

export const sliceGamesGroupedByDate = <TGame extends GameType = GameType>(
  gamesByDate: Record<string, Array<TGame>>,
  maxCount = 4,
): Record<string, Array<TGame>> => {
  const { games } = Object.entries(gamesByDate)
    .sort(([aKey], [bKey]) => compareDesc(new Date(aKey), new Date(bKey)))
    .reduce<{ count; games: Record<string, Array<TGame>> }>(
      ({ count, games }, [key, value]) => {
        if (count < maxCount) {
          return {
            count: count + value.length,
            games: {
              [key]: value,
              ...games,
            },
          };
        }
        return { count, games };
      },
      { count: 0, games: {} },
    );

  return games;
};
