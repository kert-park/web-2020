export const getYouTubeVideoId = (url: string): string | undefined => {
  const pattern =
    /^(?:https?:\/\/)?(?:www\.)?(?:m\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=|embed\/)?([a-zA-Z0-9_-]{11})/;
  const match = url.match(pattern);
  return match && match[1] ? match[1] : undefined;
};
