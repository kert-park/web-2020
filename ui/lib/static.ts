const STATIC_PATH = process.env.STATIC_PATH;

export const getTeamLogoSrc = (id: string | number): string => `${STATIC_PATH}/teams/${id}.png`;
export const getPlaceSrc = (id: string): string => `${STATIC_PATH}/places/${id}.png`;
