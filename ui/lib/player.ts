import {
  EnumComponentplayerteamplayerPost,
  EnumComponentplayerteamplayerStick,
  Player,
  PlayerUnion,
} from '@kertPark/graphql/generated';
import { getDateObject, getReadableDate } from '@kertPark/lib/date';
import { differenceInYears } from 'date-fns';

export enum PlayerPostLabels {
  B = 'Brankáři',
  O = 'Obránci',
  U = 'Útočníci',
  P = 'Hráči v poli',
}

export enum PlayerPostLabel {
  B = 'Brankář',
  O = 'Obránce',
  U = 'Útočník',
}

enum PlayerPostKeys {
  B = 'B',
  O = 'O',
  U = 'U',
  P = 'P',
}

const PlayerPostMap = {
  [EnumComponentplayerteamplayerPost.Brankar]: PlayerPostKeys.B,
  [EnumComponentplayerteamplayerPost.Obrance]: PlayerPostKeys.O,
  [EnumComponentplayerteamplayerPost.Utocnik]: PlayerPostKeys.U,
};

export const PlayerPostLabelMap = {
  [EnumComponentplayerteamplayerPost.Brankar]: PlayerPostLabel.B,
  [EnumComponentplayerteamplayerPost.Obrance]: PlayerPostLabel.O,
  [EnumComponentplayerteamplayerPost.Utocnik]: PlayerPostLabel.U,
};

export const PlayerStickLabelMap = {
  [EnumComponentplayerteamplayerStick.Leva]: 'Levá',
  [EnumComponentplayerteamplayerStick.Prava]: 'Pravá',
};

export const groupPlayersByPost = (
  players: Array<Player>,
  goalkeeperOnly = false,
): { [key: string]: Array<Player> } => {
  const reduceAcc = goalkeeperOnly
    ? { [PlayerPostKeys.B]: [], [PlayerPostKeys.P]: [] }
    : { [PlayerPostKeys.B]: [], [PlayerPostKeys.O]: [], [PlayerPostKeys.U]: [] };

  // @ts-ignore
  return players.reduce((acc, curr) => {
    let postArrayKey = PlayerPostKeys.U;
    if (goalkeeperOnly) {
      postArrayKey = curr.post === EnumComponentplayerteamplayerPost.Brankar ? PlayerPostKeys.B : PlayerPostKeys.P;
    } else {
      postArrayKey = PlayerPostMap[curr.post || ''] || PlayerPostKeys.U;
    }
    const postArray = acc[postArrayKey] || [];
    return { ...acc, [postArrayKey]: [...postArray, curr] };
  }, reduceAcc);
};

export const getPlayerDate = ({ player }: { player: Player }): { birth?: string; year?: number } => {
  if (player.birth) {
    const birth = getDateObject(player.birth);
    const year = differenceInYears(new Date(), birth);
    return { birth: getReadableDate(birth), year };
  }
  return {};
};

export const isPlayer = (player: PlayerUnion): player is Player => {
  return player.__typename === 'Player';
};

export const sortPlayersByName = (players: Array<Player>): Array<Player> => {
  return [...players].sort((a: Player, b: Player): number => {
    return a.name.localeCompare(b.name, 'cs-CZ');
  });
};

export const getShortName = (name: string, firstNameFirst = false): string => {
  const [first, second] = name.split(' ');
  const surname = firstNameFirst ? second : first;
  const firstName = firstNameFirst ? first : second;
  return `${surname}${firstName ? `\u00a0${firstName.charAt(0)}.` : ''}`;
};
