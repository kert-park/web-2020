import { TypePolicies } from '@apollo/client';

export const typePolicies: TypePolicies = {
  CmshbPlayer: {
    keyFields: ['id', 'cmshbId'],
  },
  Season: {
    keyFields: ['seasonId'],
  },
  Team: {
    keyFields: ['systemName'],
  },
};
