import { ApolloClient, HttpLink, InMemoryCache, NormalizedCacheObject } from '@apollo/client';
import { typePolicies } from '@kertPark/lib/apollo/typePolicies';
import merge from 'deepmerge';
import isEqual from 'lodash/isEqual';
import { useMemo } from 'react';

const GRAPHQL_PATH = process.env.GRAPHQL_PATH;
export const APOLLO_STATE_PROP_NAME = '__APOLLO_STATE__';

let apolloClient: ApolloClient<NormalizedCacheObject>;

function createApolloClient(initialState?: NormalizedCacheObject): ApolloClient<NormalizedCacheObject> {
  return new ApolloClient({
    ssrMode: typeof window === 'undefined',
    link: new HttpLink({
      uri: GRAPHQL_PATH, // Server URL (must be absolute)
      credentials: 'same-origin', // Additional fetch() options like `credentials` or `headers`
    }),
    cache: new InMemoryCache({ typePolicies }).restore(initialState || {}),
  });
}

export function initializeApollo(initialState?: NormalizedCacheObject): ApolloClient<NormalizedCacheObject> {
  // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
  const _apolloClient = apolloClient ?? createApolloClient(initialState);

  // If your page has Next.js data fetching methods that use Apollo Client, the initial state
  // gets hydrated here
  if (initialState) {
    // Get existing cache, loaded during client side data fetching
    const existingCache = _apolloClient.extract();

    // Merge the existing cache into data passed from getStaticProps/getServerSideProps
    const data = merge(initialState, existingCache, {
      // combine arrays using object equality (like in sets)
      arrayMerge: (destinationArray, sourceArray) => [
        ...sourceArray,
        ...destinationArray.filter((d) => sourceArray.every((s) => !isEqual(d, s))),
      ],
    });

    // Restore the cache with the merged data
    _apolloClient.cache.restore(data);
  }
  // For SSG and SSR always create a new Apollo Client
  if (typeof window === 'undefined') {
    return _apolloClient;
  }
  // Create the Apollo Client once in the client
  // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
  if (!apolloClient) {
    apolloClient = _apolloClient;
  }

  return _apolloClient;
}

export type ApolloStateProp = { __APOLLO_STATE__: NormalizedCacheObject };

export const getApolloStateProp = (client: ApolloClient<NormalizedCacheObject>): ApolloStateProp => {
  return { [APOLLO_STATE_PROP_NAME]: client.cache.extract() };
};

export function useApollo(pageProps: Record<string, any>): ApolloClient<NormalizedCacheObject> {
  const state = pageProps[APOLLO_STATE_PROP_NAME];
  const store = useMemo(() => initializeApollo(state), [state]);
  return store;
}
