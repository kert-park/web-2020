import { CmshbGameDetailTeamGoalsFragment } from '@kertPark/graphql/generated';
import { GameWithCmshbGameDetailType } from '@kertPark/graphql/generated/helpers';
import { getReadableMinuteFromTimeInSeconds } from '@kertPark/lib/date';
import { roundAndClampPercentage } from '@kertPark/lib/math';
import { getShortName } from '@kertPark/lib/player';

export const getGoals = (game: GameWithCmshbGameDetailType) => {
  const { home, away } = game.goals!;
  const homeGoals = getTeamGoals(home);
  const awayGoals = getTeamGoals(away);

  return [...(homeGoals ? [homeGoals] : []), ...(awayGoals ? [awayGoals] : [])].join(' - ');
};

const getTeamGoals = (goals: Array<CmshbGameDetailTeamGoalsFragment>): string | undefined => {
  if (goals.length > 0) {
    return goals
      .map(({ timeInSeconds, goal, firstAssist, secondAssist, type }) => {
        const time = getReadableMinuteFromTimeInSeconds(timeInSeconds!);
        const assists = [...(firstAssist ? [firstAssist] : []), ...(secondAssist ? [secondAssist] : [])]
          .map<string>(({ name }) => getShortName(name))
          .join(', ');
        return `${time}\u00a0${getShortName(goal!.name)}${assists ? ` (${assists})` : ''}${
          type !== 'EQ' ? `\u00a0${type}` : ''
        }`;
      })
      .join(', ');
  }

  return undefined;
};

type GameProgressReduceType = {
  progress: Array<string>;
  homeGoals: number;
  awayGoals: number;
  lastTeamScored?: 'home' | 'away';
};

export const getGameProgress = (game: GameWithCmshbGameDetailType) => {
  const { home, away } = game.goals!;
  const goals: Array<{ timeInSeconds: number; side: 'home' | 'away' }> = [
    ...home.map((goal) => ({ timeInSeconds: goal.timeInSeconds || 0, side: 'home' as const })),
    ...away.map((goal) => ({ timeInSeconds: goal.timeInSeconds || 0, side: 'away' as const })),
  ].sort((a, b) => (a.timeInSeconds || 0) - (b.timeInSeconds || 0));
  const { progress, homeGoals, awayGoals } = goals.reduce<GameProgressReduceType>(
    (acc, goal) => {
      const homeGoals = acc.homeGoals + (goal.side === 'home' ? 1 : 0);
      const awayGoals = acc.awayGoals + (goal.side === 'away' ? 1 : 0);
      const lastTeamScored = goal.side;

      if (!!acc.lastTeamScored && acc.lastTeamScored !== lastTeamScored) {
        const newProgress = `${acc.homeGoals}:${acc.awayGoals}`;
        return {
          progress: [...acc.progress, newProgress],
          homeGoals,
          awayGoals,
          lastTeamScored,
        };
      }

      return { ...acc, homeGoals, awayGoals, lastTeamScored };
    },
    {
      progress: [],
      homeGoals: 0,
      awayGoals: 0,
      lastTeamScored: undefined,
    },
  );
  return [...progress, `${homeGoals}:${awayGoals}`].join(', ');
};

export const getFilteredGoalsNumber = (goals: Array<CmshbGameDetailTeamGoalsFragment>, types: Array<string>) => {
  return goals.filter((goal) => types.includes(goal.type || '')).length;
};

export const getGamePeriods = (game: GameWithCmshbGameDetailType) => {
  const { home, away } = game.score;

  const periods = `${home.firstPeriod}:${away.firstPeriod}, ${home.secondPeriod}:${away.secondPeriod}, ${home.thirdPeriod}:${away.thirdPeriod}`;
  const overtime = ['pp', 'ss'].includes(game.status || '') ? ` - ${home.overtime}:${away.overtime}` : '';

  return `${periods}${overtime}`;
};

export const getGameTitle = (game: GameWithCmshbGameDetailType): string => {
  const { home, away } = game.score;
  const isHomeTeam = game.home.cmshbId === '3100003';
  const opponent = isHomeTeam ? game.away.name : game.home.name;
  const hasHomeTeamWin = (home.finalScore || 0) > (away.finalScore || 0);
  const hasKertParkWin = (isHomeTeam && hasHomeTeamWin) || (!isHomeTeam && !hasHomeTeamWin);
  const score = `${home.finalScore}:${away.finalScore}`;
  const status = game.status === 'pp' ? ' po prodloužení' : game.status === 'ss' ? ' po samostatných střílení' : '';
  const hasSuffix = game.team.systemName !== 'dorost';

  if (hasKertParkWin) {
    return `${game.team.name} porazil${hasSuffix ? 'i' : ''} ${opponent} ${score}${status}`;
  } else {
    return `${game.team.name} nestačil${hasSuffix ? 'i' : ''} na ${opponent} a prohrál${
      hasSuffix ? 'i' : ''
    } ${score}${status}`;
  }
};

export const getGameStatistics = (game: GameWithCmshbGameDetailType) => {
  const { home, away } = game.statistics!;
  const homeGoals = game.score.home.finalScore || 0;
  const awayGoals = game.score.away.finalScore || 0;
  const homeGoalsSs = getFilteredGoalsNumber(game.goals?.home || [], ['SS']);
  const awayGoalsSs = getFilteredGoalsNumber(game.goals?.away || [], ['SS']);
  const homeGoalsEn = getFilteredGoalsNumber(game.goals?.home || [], ['EN']);
  const awayGoalsEn = getFilteredGoalsNumber(game.goals?.away || [], ['EN']);
  const homeShots = home.shots || 0;
  const awayShots = away.shots || 0;
  const homeShotsPercentage = roundAndClampPercentage((homeGoals - homeGoalsSs - homeGoalsEn) / homeShots);
  const awayShotsPercentage = roundAndClampPercentage((awayGoals - awayGoalsSs - awayGoalsEn) / awayShots);
  const homeSaves = awayShots - (awayGoals - awayGoalsSs - awayGoalsEn);
  const awaySaves = homeShots - (homeGoals - homeGoalsSs - homeGoalsEn);
  const homeSavesPercentage = roundAndClampPercentage(homeSaves / awayShots);
  const awaySavesPercentage = roundAndClampPercentage(awaySaves / homeShots);
  const homePowerPlays = game.statistics?.home.powerPlays || 0;
  const awayPowerPlays = game.statistics?.away.powerPlays || 0;
  const homePowerPlayGoals = game.statistics?.home.powerPlayGoals || 0;
  const awayPowerPlayGoals = game.statistics?.away.powerPlayGoals || 0;
  const homePowerPlaysPercentage = homePowerPlays ? roundAndClampPercentage(homePowerPlayGoals / homePowerPlays) : 0;
  const awayPowerPlaysPercentage = awayPowerPlays ? roundAndClampPercentage(awayPowerPlayGoals / awayPowerPlays) : 0;
  const homePenaltyKillsPercentage = roundAndClampPercentage((100 - awayPowerPlaysPercentage) / 100);
  const awayPenaltyKillsPercentage = roundAndClampPercentage((100 - homePowerPlaysPercentage) / 100);

  return {
    homeGoalsEn,
    awayGoalsEn,
    homeShots,
    awayShots,
    homeShotsPercentage,
    awayShotsPercentage,
    homeSaves,
    awaySaves,
    homeSavesPercentage,
    awaySavesPercentage,
    homePowerPlays,
    awayPowerPlays,
    homePowerPlayGoals,
    awayPowerPlayGoals,
    homePowerPlaysPercentage,
    awayPowerPlaysPercentage,
    homePenaltyKillsPercentage,
    awayPenaltyKillsPercentage,
  };
};
