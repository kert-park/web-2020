import { PlayOffListQuery } from '@kertPark/graphql/generated';
import { PlayOffRoundType } from '@kertPark/graphql/generated/helpers';

export const getCurrentPlayOffRound = (playOffList: PlayOffListQuery['playOffList']): PlayOffRoundType | undefined => {
  if (playOffList[0] && playOffList[0].rounds.length > 0) {
    const roundsLength = playOffList[0].rounds.length;
    return playOffList[0].rounds[roundsLength - 1];
  }
  return undefined;
};
