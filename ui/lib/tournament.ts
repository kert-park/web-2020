import { GameFilterPlaceEnum, Tournament } from '@kertPark/graphql/generated';
import { TournamentType } from '@kertPark/graphql/generated/helpers';
import { differenceInDays, startOfDay } from 'date-fns';

export const getNearestTournaments = ({
  limit,
  tournaments,
}: {
  limit?: number;
  tournaments: Array<Tournament>;
}): Array<Tournament> => {
  const today = startOfDay(new Date());
  const sorted = [...tournaments].sort((a, b) => {
    const aDiff = Math.abs(differenceInDays(today, startOfDay(new Date(a.date))));
    const bDiff = Math.abs(differenceInDays(today, startOfDay(new Date(b.date))));
    return aDiff - bDiff;
  });
  if (limit && sorted.length > limit) {
    return sorted.slice(0, limit);
  }
  return sorted;
};

const filterByPlace = ({
  tournament,
  filterPlace,
}: {
  tournament: TournamentType;
  filterPlace: GameFilterPlaceEnum;
}): boolean => {
  const isHomeTournament = ['luziny', 'lužiny'].some((place) => tournament.place?.name.toLowerCase().includes(place));
  switch (filterPlace) {
    case GameFilterPlaceEnum.HOME:
    case GameFilterPlaceEnum.HOME_GROUND:
      return isHomeTournament;
    case GameFilterPlaceEnum.AWAY:
      return !isHomeTournament;
    case GameFilterPlaceEnum.ALL:
    default:
      return true;
  }
};

export const filterTournaments = ({
  tournaments,
  filterPlace,
}: {
  tournaments: Array<TournamentType>;
  filterPlace: GameFilterPlaceEnum;
}): Array<TournamentType> => {
  return tournaments
    .map((tournament) => ({ ...tournament }))
    .reduce((acc, tournament) => {
      if (!filterByPlace({ tournament, filterPlace })) {
        return acc;
      }
      return [...acc, tournament];
    }, []);
};
