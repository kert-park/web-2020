import { getTeamById } from '@kertPark/lib/team';

describe('lib/team', () => {
  describe('get team by id', () => {
    it('existing team', () => {
      expect(getTeamById({ teamId: 'muzia' })?.id).toBe('muzia');
      expect(getTeamById({ teamId: 'starsi-zaci' })?.id).toBe('starsi-zaci');
      expect(getTeamById({ teamId: 'pripravka' })?.id).toBe('pripravka');
    });
    it('non existing team', () => {
      expect(getTeamById({ teamId: 'something' })?.id).toBeUndefined();
    });
    it('team has games', () => {
      expect(getTeamById({ hasGames: true, teamId: 'muzia' })?.id).toBe('muzia');
      expect(getTeamById({ hasGames: true, teamId: 'starsi-zaci' })?.id).toBe('starsi-zaci');
      expect(getTeamById({ hasGames: true, teamId: 'pripravka' })?.id).toBeUndefined();
    });
    it('team has no games', () => {
      expect(getTeamById({ hasGames: false, teamId: 'muzia' })?.id).toBeUndefined();
      expect(getTeamById({ hasGames: false, teamId: 'starsi-zaci' })?.id).toBeUndefined();
      expect(getTeamById({ hasGames: false, teamId: 'pripravka' })?.id).toBe('pripravka');
    });
  });
});
