import { sortObjectsArrayByProperties } from '@kertPark/lib/utils';

describe('lib/utils', () => {
  describe('sort objects array by properties', () => {
    const returnKeys = (array: Array<ObjectType>): Array<number> => {
      return array.map((item) => item.id);
    };

    type ObjectType = {
      id: number;
      first: number;
      second: number;
      third: number;
      name: string;
    };

    const data: Array<ObjectType> = [
      {
        id: 1,
        first: 2,
        second: 1,
        third: 3,
        name: 'Cde',
      },
      {
        id: 2,
        first: 1,
        second: 1,
        third: 1,
        name: 'Bcd',
      },
      {
        id: 3,
        first: 3,
        second: 2,
        third: 2,
        name: 'Abc',
      },
    ];

    it('sort one key', () => {
      expect(returnKeys(sortObjectsArrayByProperties<ObjectType>(data, ['first']))).toEqual([3, 1, 2]);

      expect(returnKeys(sortObjectsArrayByProperties<ObjectType>(data, ['third'], true))).toEqual([2, 3, 1]);

      expect(returnKeys(sortObjectsArrayByProperties<ObjectType>(data, ['second']))).toEqual([3, 1, 2]);

      expect(returnKeys(sortObjectsArrayByProperties<ObjectType>(data, ['name']))).toEqual([1, 2, 3]);

      expect(returnKeys(sortObjectsArrayByProperties<ObjectType>(data, ['name'], true))).toEqual([3, 2, 1]);
    });

    it('sort multiple keys', () => {
      expect(returnKeys(sortObjectsArrayByProperties<ObjectType>(data, ['second', 'first'], true))).toEqual([2, 1, 3]);

      expect(returnKeys(sortObjectsArrayByProperties<ObjectType>(data, ['second', 'name'], true))).toEqual([2, 1, 3]);

      expect(returnKeys(sortObjectsArrayByProperties<ObjectType>(data, ['second', 'name']))).toEqual([3, 1, 2]);
    });
  });
});
