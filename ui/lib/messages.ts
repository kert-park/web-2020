export const messages = {
  assists: (value: number): string => ([1, 2, 3, 4].includes(value) ? 'asistence' : 'asistencí'),
  games: (value: number): string => (value === 1 ? 'zápas' : [2, 3, 4].includes(value) ? 'zápasy' : 'zápasů'),
  goals: (value: number): string => (value === 1 ? 'gól' : [2, 3, 4].includes(value) ? 'góly' : 'gólů'),
  minutes: (value: number): string => (value === 1 ? 'minuta' : [2, 3, 4].includes(value) ? 'minuty' : 'minut'),
  penalties: (value: number): string =>
    value === 1 ? 'trestná minuta' : [2, 3, 4].includes(value) ? 'trestné minuty' : 'trestných minut',
  points: (value: number): string => (value === 1 ? 'bod' : [2, 3, 4].includes(value) ? 'body' : 'bodů'),
  shots: (value: number): string => (value === 1 ? 'střela' : [2, 3, 4].includes(value) ? 'střely' : 'střel'),
  shutouts: (value: number): string => (value === 1 ? 'nula' : [2, 3, 4].includes(value) ? 'nuly' : 'nul'),
};
