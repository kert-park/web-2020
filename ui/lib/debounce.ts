export const debounce = <T extends (...args: Array<any>) => any>(fn: T, delay: number): T => {
  let timeoutId: number | undefined;

  return function (this: any, ...args: Array<any>) {
    clearTimeout(timeoutId);
    timeoutId = window.setTimeout(() => fn.apply(this, args), delay);
  } as T;
};
