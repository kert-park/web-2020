import { Team, teamsArray, teamsObject } from '@kertPark/config/teams';
import { CategoryEntity, GameQueryVariables } from '@kertPark/graphql/generated';
import { PostQueryPostContentType, PostQueryPostType } from '@kertPark/graphql/generated/helpers';
import { getDateObject } from '@kertPark/lib/date';

export const defaultCategory: CategoryEntity = {
  __typename: 'CategoryEntity',
  id: 'klub',
  attributes: {
    __typename: 'Category',
    code: 'klub',
    name: 'Klub',
  },
};

export const getCategoriesNameJoined = ({
  categories = [defaultCategory],
}: {
  categories?: Array<CategoryEntity>;
}): string => {
  return (categories.length > 0 ? [...categories] : [defaultCategory])
    .sort((a, b) => {
      const aOrder = teamsObject[a.attributes?.code]?.order || 999;
      const bOrder = teamsObject[b.attributes?.code]?.order || 999;
      return aOrder - bOrder;
    })
    .map((category) => category.attributes?.name)
    .join(', ');
};

export const getMostRelevantCategoryCode = ({
  categories = [defaultCategory],
}: {
  categories?: Array<CategoryEntity>;
}): string => {
  if (categories.length === 1) {
    return categories[0].attributes?.code || '';
  }

  const categoriesCode = categories.map((category) => category.attributes?.code || '');
  const teamsMatch = teamsArray.filter((team) => categoriesCode.includes(team.id));

  if (teamsMatch.length > 0) {
    const team: Team = teamsMatch.reduce<Team>((result, current) => {
      return current.order < result.order ? current : result;
    }, teamsMatch[0]);
    return team.id;
  }

  if (categories.length > 0) {
    return categories[0].attributes?.code || '';
  }

  return '';
};

export const isPostApiGame = (game: unknown): game is GameQueryVariables => {
  return (
    !!game &&
    !!(game as GameQueryVariables).id &&
    !!(game as GameQueryVariables).teamId &&
    !!(game as GameQueryVariables).year
  );
};

export const getImageSrc = ({
  isXxlDesktop,
  post,
}: {
  isXxlDesktop: boolean;
  post: PostQueryPostType;
}): string | undefined => {
  const attributes = post?.attributes?.thumb?.data?.attributes;
  const formats = attributes?.formats;

  if (isXxlDesktop && (formats?.large?.url || attributes?.url)) {
    return formats?.large?.url || attributes?.url;
  }

  return formats?.medium?.url || attributes?.url || formats?.small?.url;
};

export const getAuthorName = (post: PostQueryPostType): string => {
  const postCreatedBy = post?.attributes?.createdBy
    ? `${post.attributes.createdBy.firstname} ${post.attributes.createdBy.lastname}`
    : undefined;
  return post?.attributes?.author || postCreatedBy || 'Redakce';
};

export const getOgTags = (post: PostQueryPostType) => {
  const image = getImageSrc({ isXxlDesktop: false, post });
  return [
    ['og:type', 'article'],
    ['og:article:author', getAuthorName(post)],
    ...(image ? [['og:image', image]] : []),
    ...(post?.attributes?.publishAt
      ? [['og:article:published_time', getDateObject(post.attributes.publishAt).toISOString()]]
      : []),
  ] as Array<[string, string]>;
};

export const getPostApiGameFromContent = (content: PostQueryPostContentType): GameQueryVariables | undefined => {
  if (content?.__typename === 'ComponentCommonApiGame') {
    try {
      const game = JSON.parse(content.game as string);
      if (isPostApiGame(game)) {
        return game;
      }
    } catch {}
  }
  return undefined;
};
