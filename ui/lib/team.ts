import { activeTeams, Team, teamsArray } from '@kertPark/config/teams';
import { GetStaticPathsResult } from 'next';

export type TeamProps = {
  team?: Team;
};

const TEAMS = process.env.TEAMS;

export const getTeamById = ({
  hasGames,
  teamId,
}: {
  hasGames?: boolean;
  teamId?: string | Array<string>;
}): Team | undefined => {
  if (teamId && typeof teamId === 'string' && TEAMS?.includes(teamId)) {
    return teamsArray.find((team) => {
      return team.id === teamId && (typeof hasGames === 'boolean' ? team.hasGames === hasGames : true);
    });
  }
  return undefined;
};

export const getTeamStaticPaths = ({ hasGames }: { hasGames?: boolean }): GetStaticPathsResult => {
  const { paths } = activeTeams.reduce<Pick<GetStaticPathsResult, 'paths'>>(
    (acc, team) => {
      if (typeof hasGames !== 'undefined' && team.hasGames !== hasGames) {
        return acc;
      }
      return { paths: [...acc.paths, { params: { teamId: team.id } }] };
    },
    { paths: [] },
  );

  return { paths, fallback: true };
};
