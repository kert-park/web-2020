export const roundAndClampPercentage = (value: number) => {
  const percentage = Math.round(value * 10000) / 100;
  return Math.min(Math.max(percentage, 0), 100);
};
