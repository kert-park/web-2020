// eslint-disable-next-line import/no-duplicates
import { format } from 'date-fns';
// eslint-disable-next-line import/no-duplicates
import { cs } from 'date-fns/locale';

export const getDateObject = (date: string | Date): Date => {
  if (typeof date === 'string') {
    return new Date(date);
  } else {
    return date as Date;
  }
};

export const getReadableDate = (date: string | Date, withTime = false, withDay = false): string => {
  const dateObject = getDateObject(date);
  return format(dateObject, `${withDay ? 'iiiiii ' : ''}dd.MM.yyyy${withTime ? ' H:mm' : ''}`, {
    locale: cs,
  }).toUpperCase();
};

export const getReadableTime = (date: string | Date): string => {
  const dateObject = getDateObject(date);
  return format(dateObject, 'H:mm');
};

export const getReadableMinuteFromTimeInSeconds = (timeInSeconds: number): string => {
  return `${Math.ceil(timeInSeconds / 60)}.`;
};

export const getReadableTimeFromTimeInSeconds = (timeInSeconds: number): string => {
  const seconds = Math.floor(timeInSeconds % 60);
  return `${Math.floor(timeInSeconds / 60)}:${seconds < 10 ? `0${seconds}` : seconds}`;
};

const getSeasonYears = (): { firstSeasonYear: number | null; secondSeasonYear: number | null } => {
  try {
    // @ts-ignore
    const [firstSeasonYear, secondSeasonYear] = process.env.SEASON_YEAR.split('-');
    return { firstSeasonYear: parseInt(firstSeasonYear), secondSeasonYear: parseInt(secondSeasonYear) };
  } catch (error) {
    return { firstSeasonYear: null, secondSeasonYear: null };
  }
};

export const { firstSeasonYear } = getSeasonYears();
