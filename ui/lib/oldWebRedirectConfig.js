const { teamsObject } = require('../config/teamsObject');

const teamCommonRoutes = [
  ['', ''],
  ['/soupiska', '/soupiska'],
];

const teamHasGamesRoutes = [
  ['/statistiky', '/statistiky'],
  ['/tabulka', '/tabulka'],
  ['/rozpis-zapasu', '/zapasy'],
];

const clubRoutes = [
  ['/nabor', '/klub/nabor'],
  ['/partneri', '/klub/partneri'],
];

const mapTeamRedirect =
  (id) =>
  ([oldRoute, newRoute]) => {
    const source = `/${id}${oldRoute}`;
    const destination = `/tym/${id}${newRoute}`;
    return {
      source,
      destination,
      permanent: false,
    };
  };

const getTeamConfigRedirects = () => {
  return Object.values(teamsObject).reduce((acc, { id, hasGames }) => {
    const commonRedirects = teamCommonRoutes.map(mapTeamRedirect(id));
    const hasGamesRedirects = hasGames ? teamHasGamesRoutes.map(mapTeamRedirect(id)) : [];
    return [...acc, ...commonRedirects, ...hasGamesRedirects];
  }, []);
};

const getClubConfigRedirects = () => {
  return clubRoutes.map(([oldRoute, newRoute]) => ({
    source: oldRoute,
    destination: newRoute,
    permanent: false,
  }));
};

module.exports = {
  redirects: [...getTeamConfigRedirects(), ...getClubConfigRedirects()],
};
