import { ApolloClient, NormalizedCacheObject } from '@apollo/client';
import {
  GameListDocument,
  GameListQuery,
  GameListQueryVariables,
  TournamentListDocument,
  TournamentListQuery,
  TournamentListQueryVariables,
} from '@kertPark/graphql/generated';
import { getDateObject } from '@kertPark/lib/date';
import { getDateRange } from '@kertPark/lib/gamePage';

type GetMultirozpisData = (client: ApolloClient<NormalizedCacheObject>) => Promise<{
  defaultDate: Date;
  games: GameListQuery['gameList'];
  tournaments: TournamentListQuery['tournamentList'];
}>;

export const getMultirozpisData: GetMultirozpisData = async (client) => {
  const { data: nearestGameData } = await client.query<GameListQuery, GameListQueryVariables>({
    query: GameListDocument,
    variables: {
      filter: {
        nearest: true,
        limit: 1,
      },
    },
  });

  const defaultDate = getDateObject(nearestGameData.gameList[0]?.date ?? new Date());
  const dateRange = getDateRange(defaultDate);

  const { data: gamesData, error: gamesError } = await client.query<GameListQuery, GameListQueryVariables>({
    query: GameListDocument,
    variables: {
      filter: {
        ...dateRange,
      },
    },
  });
  const games = gamesError ? [] : gamesData.gameList;

  const { data: tournamentsData, error: tournamentsError } = await client.query<
    TournamentListQuery,
    TournamentListQueryVariables
  >({
    query: TournamentListDocument,
    variables: {
      ...dateRange,
    },
  });
  const tournaments = tournamentsError ? [] : tournamentsData.tournamentList;

  return {
    defaultDate,
    games,
    tournaments,
  };
};
