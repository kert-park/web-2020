const OLD_WEB_PATH = process.env.OLD_WEB_PATH;

export const getExistingSlugOnOldWeb = async (slug: string): Promise<string | undefined> => {
  const url = `${OLD_WEB_PATH}/${slug}`;
  const response = await fetch(url);
  return response.ok ? url : undefined;
};
