export const sortObjectsArrayByProperties = <T>(array: Array<T>, keys: Array<keyof T>, asc = false): Array<T> => {
  return [...array].sort((a, b) => {
    return keys.reduce<number>((acc, key) => {
      if (acc === 0) {
        if (typeof a[key] === 'number' && typeof b[key] === 'number') {
          const result = (b[key] as unknown as number) - (a[key] as unknown as number);
          return asc ? -result : result;
        } else if (typeof a[key] === 'string' && typeof b[key] === 'string') {
          const result = (b[key] as unknown as string).localeCompare(a[key] as unknown as string);
          return asc ? -result : result;
        }
        return acc;
      }
      return acc;
    }, 0);
  });
};
