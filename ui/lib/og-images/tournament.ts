import { teamsObject } from '@kertPark/config/teams';
import { TournamentListDocument, TournamentListQuery, TournamentListQueryVariables } from '@kertPark/graphql/generated';
import { TournamentType } from '@kertPark/graphql/generated/helpers';
import { initializeApollo } from '@kertPark/lib/apollo/apolloClient';
import { GameError } from '@kertPark/lib/og-images/error';
import { addDays, startOfDay } from 'date-fns';
import { NextRequest } from 'next/server';

export const getTournaments = async (request: NextRequest): Promise<Array<TournamentType>> => {
  const teamIdsString = request.nextUrl.searchParams.get('teamIds');
  const year = request.nextUrl.searchParams.get('year');
  const fromString = request.nextUrl.searchParams.get('from');
  const toString = request.nextUrl.searchParams.get('to');

  if (
    typeof teamIdsString !== 'string' ||
    !teamIdsString ||
    typeof year !== 'string' ||
    !year ||
    typeof fromString !== 'string' ||
    !fromString ||
    typeof toString !== 'string' ||
    !toString
  ) {
    throw new GameError('Missing required parameters', 400);
  }

  const teamIds = teamIdsString.split(',');
  const from = startOfDay(new Date(fromString));
  const to = addDays(startOfDay(new Date(toString)), 1);

  const client = initializeApollo();

  const { data } = await client.query<TournamentListQuery, TournamentListQueryVariables>({
    query: TournamentListDocument,
    variables: { year, teamIds, from, to },
  });

  const tournaments = data.tournamentList;

  return [...tournaments].sort((a, b) => {
    const timeDiff = new Date(a.date).getTime() - new Date(b.date).getTime();

    if (timeDiff === 0) {
      return (teamsObject[a.team.systemName]?.order || 0) - (teamsObject[b.team.systemName]?.order || 0);
    }

    return timeDiff;
  });
};
