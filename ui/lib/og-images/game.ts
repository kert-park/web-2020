import { teamsObject } from '@kertPark/config/teams';
import {
  GameDocument,
  GameFilterScheduleEnum,
  GameListDocument,
  GameListQuery,
  GameListQueryVariables,
  GameQuery,
  GameQueryVariables,
} from '@kertPark/graphql/generated';
import { GameWithCmshbGameDetailType } from '@kertPark/graphql/generated/helpers';
import { initializeApollo } from '@kertPark/lib/apollo/apolloClient';
import { GameError } from '@kertPark/lib/og-images/error';
import { addDays, startOfDay } from 'date-fns';
import { NextRequest } from 'next/server';

export const getGame = async (request: NextRequest): Promise<GameWithCmshbGameDetailType> => {
  const teamId = request.nextUrl.searchParams.get('teamId');
  const year = request.nextUrl.searchParams.get('year');
  const id = request.nextUrl.searchParams.get('id');

  if (typeof id !== 'string' || !id || typeof teamId !== 'string' || !teamId || typeof year !== 'string' || !year) {
    throw new GameError('Missing required parameters', 400);
  }

  const client = initializeApollo();

  const { data } = await client.query<GameQuery, GameQueryVariables>({
    query: GameDocument,
    variables: { id, year, teamId },
  });

  const game = data.game;

  if (!game) {
    throw new GameError('Game not found', 404);
  }

  return game;
};

export const getGames = async (
  request: NextRequest,
  resultsOnly = false,
): Promise<Array<GameWithCmshbGameDetailType>> => {
  const teamIdsString = request.nextUrl.searchParams.get('teamIds');
  const year = request.nextUrl.searchParams.get('year');
  const fromString = request.nextUrl.searchParams.get('from');
  const toString = request.nextUrl.searchParams.get('to');
  const scheduleString = request.nextUrl.searchParams.get('schedule');

  if (
    typeof teamIdsString !== 'string' ||
    !teamIdsString ||
    typeof year !== 'string' ||
    !year ||
    typeof fromString !== 'string' ||
    !fromString ||
    typeof toString !== 'string' ||
    !toString
  ) {
    throw new GameError('Missing required parameters', 400);
  }

  const schedule = resultsOnly
    ? GameFilterScheduleEnum.RESULT
    : typeof scheduleString === 'string'
    ? (scheduleString as GameFilterScheduleEnum)
    : GameFilterScheduleEnum.ALL;
  const teamIds = teamIdsString.split(',');
  const from = startOfDay(new Date(fromString));
  const to = addDays(startOfDay(new Date(toString)), 1);

  const client = initializeApollo();

  const { data } = await client.query<GameListQuery, GameListQueryVariables>({
    query: GameListDocument,
    variables: { year, teamIds, filter: { from, to, schedule } },
  });

  const games = data.gameList;

  return [...games].sort((a, b) => {
    const teamDiff = (teamsObject[a.team.systemName]?.order || 0) - (teamsObject[b.team.systemName]?.order || 0);

    if (teamDiff === 0) {
      return new Date(a.date).getTime() - new Date(b.date).getTime();
    }

    return teamDiff;
  }) as unknown as Array<GameWithCmshbGameDetailType>;
};
