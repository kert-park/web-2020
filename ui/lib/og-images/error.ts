export class GameError extends Error {
  constructor(message, readonly status: number) {
    super(message);
  }
}
