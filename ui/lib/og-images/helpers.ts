import { teamsObject } from '@kertPark/config/teams';
import { GameWithCmshbGameDetailType, TournamentType } from '@kertPark/graphql/generated/helpers';
import { endOfDay } from 'date-fns';

export const mergeGamesAndTournamentsAndSort = (
  games: Array<GameWithCmshbGameDetailType>,
  tournaments: Array<TournamentType>,
): Array<XOR<GameWithCmshbGameDetailType, TournamentType>> => {
  return [
    ...games,
    ...tournaments.map((tournament) => ({ ...tournament, date: endOfDay(new Date(tournament.date)) })),
  ].sort((a, b) => {
    const teamDiff = (teamsObject[a.team.systemName]?.order || 0) - (teamsObject[b.team.systemName]?.order || 0);

    if (teamDiff === 0) {
      return new Date(a.date).getTime() - new Date(b.date).getTime();
    }

    return teamDiff;
  });
};
