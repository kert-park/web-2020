import { SatoriOptions } from 'satori';

const plusJakartaSansExtraBoldItalicPromise = fetch(
  new URL('../../public/fonts/PlusJakartaSans-ExtraBoldItalic.ttf', import.meta.url),
).then((res) => res.arrayBuffer());
const plusJakartaSansExtraBoldPromise = fetch(
  new URL('../../public/fonts/PlusJakartaSans-ExtraBold.ttf', import.meta.url),
).then((res) => res.arrayBuffer());

const getFontsImageResponse = async (): Promise<SatoriOptions['fonts']> => {
  return [
    {
      name: 'Plus Jakarta Sans',
      data: await plusJakartaSansExtraBoldPromise,
      style: 'normal',
      weight: 800,
    },
    {
      name: 'Plus Jakarta Sans',
      data: await plusJakartaSansExtraBoldItalicPromise,
      style: 'italic',
      weight: 800,
    },
  ];
};

export const fontsImageResponse = getFontsImageResponse();
