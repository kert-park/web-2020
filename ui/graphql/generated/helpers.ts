import {
  CategoriesQuery,
  ClubPageQuery,
  CmshbPlayerStatsList,
  ExternalPostsQuery,
  GameListQuery,
  GameQuery,
  ImageFragment,
  PartnersQuery,
  PlayOffListQuery,
  PostQuery,
  PostsCompositeQuery,
  TournamentListQuery,
} from '@kertPark/graphql/generated';

export type CategoriesQueryCategoriesType = NonNullable<CategoriesQuery['categories']>['data'] | undefined;
export type CategoriesQueryCategoryType =
  | NonNullable<NonNullable<CategoriesQuery['categories']>['data']>[0]
  | undefined;

export type ClubPageQueryClubPage = NonNullable<ClubPageQuery['clubPages']>['data'][0] | undefined;

export type CmshbPlayerStatsPlayerUnionType = CmshbPlayerStatsList['players'][0]['player'];

export type ExternalPostsQueryPostType = NonNullable<ExternalPostsQuery['externalPosts']>['data'][0] | undefined;

export type GameType = Omit<GameListQuery['gameList'][0], '__typename'>;

export type GameWithCmshbGameDetailType = NonNullable<GameQuery['game']>;

export type ImageType = {
  data?: ImageFragment | null;
};

export type PartnersQueryPartnersListType =
  | NonNullable<NonNullable<NonNullable<PartnersQuery['partner']>['data']>['attributes']>['partnerLists'][0]
  | undefined;
export type PartnersQueryPartnerType = NonNullable<PartnersQueryPartnersListType>['partners'][0];

export type PlayOffRoundType = NonNullable<PlayOffListQuery['playOffList'][0]>['rounds'][0];

export type PostQueryPostType = NonNullable<PostQuery['posts']>['data'][0] | undefined;
export type PostsCompositeQueryMetaType = NonNullable<PostsCompositeQuery['postsComposite']>['meta'] | undefined;
export type PostsCompositeQueryPostsType = NonNullable<PostsCompositeQuery['postsComposite']>['data'] | undefined;
export type PostsCompositeQueryPostType = NonNullable<NonNullable<PostsCompositeQuery['postsComposite']>['data'][0]>;
export type PostQueryPostContentType = NonNullable<NonNullable<PostQueryPostType>['attributes']>['content'][0];

export type TournamentType = TournamentListQuery['tournamentList'][0];
