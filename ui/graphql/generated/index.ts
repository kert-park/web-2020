import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
import { FieldPolicy, FieldReadFunction, TypePolicies, TypePolicy } from '@apollo/client/cache';

export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Date: Date;
  DateTime: Date;
  JSON: { [key: string]: any };
  Upload: any;
};

export type BooleanFilterInput = {
  and?: InputMaybe<Array<InputMaybe<Scalars['Boolean']>>>;
  between?: InputMaybe<Array<InputMaybe<Scalars['Boolean']>>>;
  contains?: InputMaybe<Scalars['Boolean']>;
  containsi?: InputMaybe<Scalars['Boolean']>;
  endsWith?: InputMaybe<Scalars['Boolean']>;
  eq?: InputMaybe<Scalars['Boolean']>;
  eqi?: InputMaybe<Scalars['Boolean']>;
  gt?: InputMaybe<Scalars['Boolean']>;
  gte?: InputMaybe<Scalars['Boolean']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['Boolean']>>>;
  lt?: InputMaybe<Scalars['Boolean']>;
  lte?: InputMaybe<Scalars['Boolean']>;
  ne?: InputMaybe<Scalars['Boolean']>;
  nei?: InputMaybe<Scalars['Boolean']>;
  not?: InputMaybe<BooleanFilterInput>;
  notContains?: InputMaybe<Scalars['Boolean']>;
  notContainsi?: InputMaybe<Scalars['Boolean']>;
  notIn?: InputMaybe<Array<InputMaybe<Scalars['Boolean']>>>;
  notNull?: InputMaybe<Scalars['Boolean']>;
  null?: InputMaybe<Scalars['Boolean']>;
  or?: InputMaybe<Array<InputMaybe<Scalars['Boolean']>>>;
  startsWith?: InputMaybe<Scalars['Boolean']>;
};

export enum CacheControlScope {
  PRIVATE = 'PRIVATE',
  PUBLIC = 'PUBLIC',
}

export type Category = {
  __typename: 'Category';
  achievements?: Maybe<Array<Maybe<ComponentCommonAchievement>>>;
  code: Scalars['String'];
  createdAt?: Maybe<Scalars['DateTime']>;
  galleries?: Maybe<GalleryRelationResponseCollection>;
  image?: Maybe<UploadFileEntityResponse>;
  name: Scalars['String'];
  officials?: Maybe<Array<Maybe<ComponentCommonOfficial>>>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type CategoryAchievementsArgs = {
  filters?: InputMaybe<ComponentCommonAchievementFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type CategoryGalleriesArgs = {
  filters?: InputMaybe<GalleryFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type CategoryOfficialsArgs = {
  filters?: InputMaybe<ComponentCommonOfficialFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type CategoryEntity = {
  __typename: 'CategoryEntity';
  attributes?: Maybe<Category>;
  id?: Maybe<Scalars['ID']>;
};

export type CategoryEntityResponse = {
  __typename: 'CategoryEntityResponse';
  data?: Maybe<CategoryEntity>;
};

export type CategoryEntityResponseCollection = {
  __typename: 'CategoryEntityResponseCollection';
  data: Array<CategoryEntity>;
  meta: ResponseCollectionMeta;
};

export type CategoryFiltersInput = {
  achievements?: InputMaybe<ComponentCommonAchievementFiltersInput>;
  and?: InputMaybe<Array<InputMaybe<CategoryFiltersInput>>>;
  code?: InputMaybe<StringFilterInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  galleries?: InputMaybe<GalleryFiltersInput>;
  id?: InputMaybe<IdFilterInput>;
  name?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<CategoryFiltersInput>;
  officials?: InputMaybe<ComponentCommonOfficialFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<CategoryFiltersInput>>>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
};

export type CategoryRelationResponseCollection = {
  __typename: 'CategoryRelationResponseCollection';
  data: Array<CategoryEntity>;
};

export type ClubPage = {
  __typename: 'ClubPage';
  content: Array<Maybe<ClubPageContentDynamicZone>>;
  createdAt?: Maybe<Scalars['DateTime']>;
  publishedAt?: Maybe<Scalars['DateTime']>;
  slug: Scalars['String'];
  title: Scalars['String'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type ClubPageContentDynamicZone =
  | ComponentCommonEmbedVideo
  | ComponentCommonGallery
  | ComponentCommonOfficialList
  | ComponentCommonRichText
  | Error;

export type ClubPageEntity = {
  __typename: 'ClubPageEntity';
  attributes?: Maybe<ClubPage>;
  id?: Maybe<Scalars['ID']>;
};

export type ClubPageEntityResponse = {
  __typename: 'ClubPageEntityResponse';
  data?: Maybe<ClubPageEntity>;
};

export type ClubPageEntityResponseCollection = {
  __typename: 'ClubPageEntityResponseCollection';
  data: Array<ClubPageEntity>;
  meta: ResponseCollectionMeta;
};

export type ClubPageFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<ClubPageFiltersInput>>>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  not?: InputMaybe<ClubPageFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<ClubPageFiltersInput>>>;
  publishedAt?: InputMaybe<DateTimeFilterInput>;
  slug?: InputMaybe<StringFilterInput>;
  title?: InputMaybe<StringFilterInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
};

export type CmshbGameDetailGoalkeepers = {
  __typename: 'CmshbGameDetailGoalkeepers';
  away: Array<CmshbGameDetailTeamGoalkeepers>;
  home: Array<CmshbGameDetailTeamGoalkeepers>;
};

export type CmshbGameDetailGoals = {
  __typename: 'CmshbGameDetailGoals';
  away: Array<CmshbGameDetailTeamGoals>;
  home: Array<CmshbGameDetailTeamGoals>;
};

export type CmshbGameDetailPenalties = {
  __typename: 'CmshbGameDetailPenalties';
  away: Array<CmshbGameDetailTeamPenalties>;
  home: Array<CmshbGameDetailTeamPenalties>;
};

export type CmshbGameDetailPlayers = {
  __typename: 'CmshbGameDetailPlayers';
  away: Array<CmshbGameDetailTeamPlayers>;
  home: Array<CmshbGameDetailTeamPlayers>;
};

export type CmshbGameDetailPost = {
  __typename: 'CmshbGameDetailPost';
  content?: Maybe<Scalars['String']>;
  perex: Scalars['String'];
  title: Scalars['String'];
};

export type CmshbGameDetailShootouts = {
  __typename: 'CmshbGameDetailShootouts';
  away: Array<CmshbGameDetailTeamShootouts>;
  home: Array<CmshbGameDetailTeamShootouts>;
};

export type CmshbGameDetailStats = {
  __typename: 'CmshbGameDetailStats';
  away: CmshbGameDetailTeamStats;
  home: CmshbGameDetailTeamStats;
};

export type CmshbGameDetailTeamGoalkeepers = {
  __typename: 'CmshbGameDetailTeamGoalkeepers';
  assists?: Maybe<Scalars['Int']>;
  goals?: Maybe<Scalars['Int']>;
  goalsAgainst?: Maybe<Scalars['Int']>;
  number?: Maybe<Scalars['Int']>;
  penalties?: Maybe<Scalars['Int']>;
  player: PlayerUnion;
  savesAverage?: Maybe<Scalars['Float']>;
  shotsAgainst?: Maybe<Scalars['Int']>;
  timeInSeconds?: Maybe<Scalars['Int']>;
};

export type CmshbGameDetailTeamGoals = {
  __typename: 'CmshbGameDetailTeamGoals';
  firstAssist?: Maybe<CmshbPlayer>;
  goal: CmshbPlayer;
  secondAssist?: Maybe<CmshbPlayer>;
  timeInSeconds: Scalars['Int'];
  type?: Maybe<Scalars['String']>;
};

export type CmshbGameDetailTeamPenalties = {
  __typename: 'CmshbGameDetailTeamPenalties';
  penalty?: Maybe<Scalars['String']>;
  penaltyMinutes?: Maybe<Scalars['Int']>;
  penaltyType: Scalars['String'];
  player: CmshbPlayer;
  timeInSeconds: Scalars['Int'];
};

export type CmshbGameDetailTeamPlayers = {
  __typename: 'CmshbGameDetailTeamPlayers';
  assists?: Maybe<Scalars['Int']>;
  goals?: Maybe<Scalars['Int']>;
  number?: Maybe<Scalars['Int']>;
  penalties?: Maybe<Scalars['Int']>;
  player: PlayerUnion;
  points?: Maybe<Scalars['Int']>;
  post?: Maybe<Scalars['String']>;
};

export type CmshbGameDetailTeamShootouts = {
  __typename: 'CmshbGameDetailTeamShootouts';
  isGoal: Scalars['Boolean'];
  player: CmshbPlayer;
};

export type CmshbGameDetailTeamStats = {
  __typename: 'CmshbGameDetailTeamStats';
  penalties?: Maybe<Scalars['Int']>;
  penaltyKillGoals?: Maybe<Scalars['Int']>;
  powerPlayGoals?: Maybe<Scalars['Int']>;
  powerPlays?: Maybe<Scalars['Int']>;
  shots?: Maybe<Scalars['Int']>;
};

export type CmshbGoalkeeperStats = {
  __typename: 'CmshbGoalkeeperStats';
  assists?: Maybe<Scalars['Int']>;
  games?: Maybe<Scalars['Int']>;
  goalsAgainst?: Maybe<Scalars['Int']>;
  goalsAgainstAverage?: Maybe<Scalars['Float']>;
  minutes?: Maybe<Scalars['Int']>;
  penalties?: Maybe<Scalars['Int']>;
  player: PlayerUnion;
  saves?: Maybe<Scalars['Int']>;
  savesAverage?: Maybe<Scalars['Float']>;
  shotsAgainst?: Maybe<Scalars['Int']>;
  shutouts?: Maybe<Scalars['Int']>;
};

/** order is used from TeamSeason */
export type CmshbLeague = {
  __typename: 'CmshbLeague';
  id: Scalars['ID'];
  leagueId: Scalars['Int'];
  name: Scalars['String'];
  order: Scalars['Int'];
};

/** consider to use CmshbLeagueGroupType enum */
export type CmshbLeagueGroup = {
  __typename: 'CmshbLeagueGroup';
  id: Scalars['ID'];
  label: Scalars['String'];
  league: CmshbLeague;
  metadata?: Maybe<Scalars['JSON']>;
  name: Scalars['String'];
  order: Scalars['Int'];
  skuId: Scalars['Int'];
  type: Scalars['String'];
};

export type CmshbPlayer = {
  __typename: 'CmshbPlayer';
  cmshbId?: Maybe<Scalars['ID']>;
  id: Scalars['ID'];
  name: Scalars['String'];
};

export type CmshbPlayerStats = {
  __typename: 'CmshbPlayerStats';
  assists?: Maybe<Scalars['Int']>;
  games?: Maybe<Scalars['Int']>;
  goals?: Maybe<Scalars['Int']>;
  penalties?: Maybe<Scalars['Int']>;
  penaltyKillGoals?: Maybe<Scalars['Int']>;
  player: PlayerUnion;
  points?: Maybe<Scalars['Int']>;
  powerPlayGoals?: Maybe<Scalars['Int']>;
};

export type CmshbPlayerStatsList = {
  __typename: 'CmshbPlayerStatsList';
  goalkeepers: Array<CmshbGoalkeeperStats>;
  leagueGroup: CmshbLeagueGroup;
  players: Array<CmshbPlayerStats>;
};

export type CmshbTeam = {
  __typename: 'CmshbTeam';
  cmshbId?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  shortName: Scalars['String'];
};

export type CmshbTeamTable = {
  __typename: 'CmshbTeamTable';
  leagueGroup: CmshbLeagueGroup;
  stats: Array<CmshbTeamTableStats>;
};

export type CmshbTeamTableStats = {
  __typename: 'CmshbTeamTableStats';
  games?: Maybe<Scalars['Int']>;
  goals?: Maybe<Scalars['Int']>;
  goalsAgainst?: Maybe<Scalars['Int']>;
  goalsAgainstAverage?: Maybe<Scalars['Float']>;
  goalsAverage?: Maybe<Scalars['Float']>;
  losses?: Maybe<Scalars['Int']>;
  overtimeLosses?: Maybe<Scalars['Int']>;
  overtimeWins?: Maybe<Scalars['Int']>;
  penalties?: Maybe<Scalars['Int']>;
  penaltiesAverage?: Maybe<Scalars['Int']>;
  penaltyKillGoals?: Maybe<Scalars['Int']>;
  penaltyKillGoalsAgainst?: Maybe<Scalars['Int']>;
  penaltyKills?: Maybe<Scalars['Int']>;
  penaltyKillsPercentage?: Maybe<Scalars['Float']>;
  points?: Maybe<Scalars['Int']>;
  powerPlayGoals?: Maybe<Scalars['Int']>;
  powerPlayGoalsAgainst?: Maybe<Scalars['Int']>;
  powerPlays?: Maybe<Scalars['Int']>;
  powerPlaysPercentage?: Maybe<Scalars['Float']>;
  standing?: Maybe<Scalars['Int']>;
  team: CmshbTeam;
  wins?: Maybe<Scalars['Int']>;
};

export type ComponentCommonAchievement = {
  __typename: 'ComponentCommonAchievement';
  id: Scalars['ID'];
  image: UploadFileEntityResponse;
  isWinner: Scalars['Boolean'];
  title: Scalars['String'];
  years?: Maybe<Scalars['String']>;
};

export type ComponentCommonAchievementFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<ComponentCommonAchievementFiltersInput>>>;
  isWinner?: InputMaybe<BooleanFilterInput>;
  not?: InputMaybe<ComponentCommonAchievementFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<ComponentCommonAchievementFiltersInput>>>;
  title?: InputMaybe<StringFilterInput>;
  years?: InputMaybe<StringFilterInput>;
};

export type ComponentCommonAchievementList = {
  __typename: 'ComponentCommonAchievementList';
  achievements?: Maybe<Array<Maybe<ComponentCommonAchievement>>>;
  id: Scalars['ID'];
  title: Scalars['String'];
};

export type ComponentCommonAchievementListAchievementsArgs = {
  filters?: InputMaybe<ComponentCommonAchievementFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type ComponentCommonApiGame = {
  __typename: 'ComponentCommonApiGame';
  game?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
};

export type ComponentCommonApiGameList = {
  __typename: 'ComponentCommonApiGameList';
  gameList?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
};

export type ComponentCommonEmbedVideo = {
  __typename: 'ComponentCommonEmbedVideo';
  id: Scalars['ID'];
  type: EnumComponentcommonembedvideoType;
  url: Scalars['String'];
};

export type ComponentCommonGallery = {
  __typename: 'ComponentCommonGallery';
  gallery?: Maybe<GalleryEntityResponse>;
  id: Scalars['ID'];
  text?: Maybe<Scalars['String']>;
};

export type ComponentCommonOfficial = {
  __typename: 'ComponentCommonOfficial';
  id: Scalars['ID'];
  person?: Maybe<PersonEntityResponse>;
  title: Scalars['String'];
};

export type ComponentCommonOfficialFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<ComponentCommonOfficialFiltersInput>>>;
  not?: InputMaybe<ComponentCommonOfficialFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<ComponentCommonOfficialFiltersInput>>>;
  person?: InputMaybe<PersonFiltersInput>;
  title?: InputMaybe<StringFilterInput>;
};

export type ComponentCommonOfficialList = {
  __typename: 'ComponentCommonOfficialList';
  id: Scalars['ID'];
  officials: Array<Maybe<ComponentCommonOfficial>>;
  showContactInformation: Scalars['Boolean'];
  title: Scalars['String'];
};

export type ComponentCommonOfficialListOfficialsArgs = {
  filters?: InputMaybe<ComponentCommonOfficialFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type ComponentCommonPartner = {
  __typename: 'ComponentCommonPartner';
  id: Scalars['ID'];
  image: UploadFileEntityResponse;
  name: Scalars['String'];
  url?: Maybe<Scalars['String']>;
};

export type ComponentCommonPartnerFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<ComponentCommonPartnerFiltersInput>>>;
  name?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<ComponentCommonPartnerFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<ComponentCommonPartnerFiltersInput>>>;
  url?: InputMaybe<StringFilterInput>;
};

export type ComponentCommonPartnerList = {
  __typename: 'ComponentCommonPartnerList';
  id: Scalars['ID'];
  name: Scalars['String'];
  partners: Array<Maybe<ComponentCommonPartner>>;
  slug: Scalars['String'];
};

export type ComponentCommonPartnerListPartnersArgs = {
  filters?: InputMaybe<ComponentCommonPartnerFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type ComponentCommonPartnerListFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<ComponentCommonPartnerListFiltersInput>>>;
  name?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<ComponentCommonPartnerListFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<ComponentCommonPartnerListFiltersInput>>>;
  partners?: InputMaybe<ComponentCommonPartnerFiltersInput>;
  slug?: InputMaybe<StringFilterInput>;
};

export type ComponentCommonRichText = {
  __typename: 'ComponentCommonRichText';
  id: Scalars['ID'];
  text?: Maybe<Scalars['String']>;
};

export type ComponentPlayerTeamPlayer = {
  __typename: 'ComponentPlayerTeamPlayer';
  id: Scalars['ID'];
  number?: Maybe<Scalars['Int']>;
  post?: Maybe<EnumComponentplayerteamplayerPost>;
  stick?: Maybe<EnumComponentplayerteamplayerStick>;
  team?: Maybe<CategoryEntityResponse>;
};

export type Creator = {
  __typename: 'Creator';
  firstname?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  lastname?: Maybe<Scalars['String']>;
};

export type DateRangeInput = {
  from: Scalars['DateTime'];
  to: Scalars['DateTime'];
};

export type DateTimeFilterInput = {
  and?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
  between?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
  contains?: InputMaybe<Scalars['DateTime']>;
  containsi?: InputMaybe<Scalars['DateTime']>;
  endsWith?: InputMaybe<Scalars['DateTime']>;
  eq?: InputMaybe<Scalars['DateTime']>;
  eqi?: InputMaybe<Scalars['DateTime']>;
  gt?: InputMaybe<Scalars['DateTime']>;
  gte?: InputMaybe<Scalars['DateTime']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
  lt?: InputMaybe<Scalars['DateTime']>;
  lte?: InputMaybe<Scalars['DateTime']>;
  ne?: InputMaybe<Scalars['DateTime']>;
  nei?: InputMaybe<Scalars['DateTime']>;
  not?: InputMaybe<DateTimeFilterInput>;
  notContains?: InputMaybe<Scalars['DateTime']>;
  notContainsi?: InputMaybe<Scalars['DateTime']>;
  notIn?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
  notNull?: InputMaybe<Scalars['Boolean']>;
  null?: InputMaybe<Scalars['Boolean']>;
  or?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
  startsWith?: InputMaybe<Scalars['DateTime']>;
};

export enum EnumComponentcommonembedvideoType {
  ceskatelevize = 'ceskatelevize',
  facebook = 'facebook',
  o2tvsport = 'o2tvsport',
  other = 'other',
  youtube = 'youtube',
}

export enum EnumComponentplayerteamplayerPost {
  Brankar = 'Brankar',
  Obrance = 'Obrance',
  Utocnik = 'Utocnik',
}

export enum EnumComponentplayerteamplayerStick {
  Leva = 'Leva',
  Prava = 'Prava',
}

export enum EnumExternalpostSource {
  hokejbal_cz = 'hokejbal_cz',
}

export type Error = {
  __typename: 'Error';
  code: Scalars['String'];
  message?: Maybe<Scalars['String']>;
};

export type ExternalPost = {
  __typename: 'ExternalPost';
  categories?: Maybe<CategoryRelationResponseCollection>;
  createdAt?: Maybe<Scalars['DateTime']>;
  pinToTop?: Maybe<Scalars['Boolean']>;
  publishAt: Scalars['DateTime'];
  source: EnumExternalpostSource;
  updatedAt?: Maybe<Scalars['DateTime']>;
  url: Scalars['String'];
};

export type ExternalPostCategoriesArgs = {
  filters?: InputMaybe<CategoryFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type ExternalPostEntity = {
  __typename: 'ExternalPostEntity';
  attributes?: Maybe<ExternalPost>;
  id?: Maybe<Scalars['ID']>;
};

export type ExternalPostEntityResponse = {
  __typename: 'ExternalPostEntityResponse';
  data?: Maybe<ExternalPostEntity>;
};

export type ExternalPostEntityResponseCollection = {
  __typename: 'ExternalPostEntityResponseCollection';
  data: Array<ExternalPostEntity>;
  meta: ResponseCollectionMeta;
};

export type ExternalPostFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<ExternalPostFiltersInput>>>;
  categories?: InputMaybe<CategoryFiltersInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  not?: InputMaybe<ExternalPostFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<ExternalPostFiltersInput>>>;
  pinToTop?: InputMaybe<BooleanFilterInput>;
  publishAt?: InputMaybe<DateTimeFilterInput>;
  source?: InputMaybe<StringFilterInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
  url?: InputMaybe<StringFilterInput>;
};

export type FileInfoInput = {
  alternativeText?: InputMaybe<Scalars['String']>;
  caption?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
};

export type FloatFilterInput = {
  and?: InputMaybe<Array<InputMaybe<Scalars['Float']>>>;
  between?: InputMaybe<Array<InputMaybe<Scalars['Float']>>>;
  contains?: InputMaybe<Scalars['Float']>;
  containsi?: InputMaybe<Scalars['Float']>;
  endsWith?: InputMaybe<Scalars['Float']>;
  eq?: InputMaybe<Scalars['Float']>;
  eqi?: InputMaybe<Scalars['Float']>;
  gt?: InputMaybe<Scalars['Float']>;
  gte?: InputMaybe<Scalars['Float']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['Float']>>>;
  lt?: InputMaybe<Scalars['Float']>;
  lte?: InputMaybe<Scalars['Float']>;
  ne?: InputMaybe<Scalars['Float']>;
  nei?: InputMaybe<Scalars['Float']>;
  not?: InputMaybe<FloatFilterInput>;
  notContains?: InputMaybe<Scalars['Float']>;
  notContainsi?: InputMaybe<Scalars['Float']>;
  notIn?: InputMaybe<Array<InputMaybe<Scalars['Float']>>>;
  notNull?: InputMaybe<Scalars['Boolean']>;
  null?: InputMaybe<Scalars['Boolean']>;
  or?: InputMaybe<Array<InputMaybe<Scalars['Float']>>>;
  startsWith?: InputMaybe<Scalars['Float']>;
};

export type Gallery = {
  __typename: 'Gallery';
  category?: Maybe<CategoryEntityResponse>;
  createdAt?: Maybe<Scalars['DateTime']>;
  images: UploadFileRelationResponseCollection;
  title: Scalars['String'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type GalleryImagesArgs = {
  filters?: InputMaybe<UploadFileFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type GalleryEntity = {
  __typename: 'GalleryEntity';
  attributes?: Maybe<Gallery>;
  id?: Maybe<Scalars['ID']>;
};

export type GalleryEntityResponse = {
  __typename: 'GalleryEntityResponse';
  data?: Maybe<GalleryEntity>;
};

export type GalleryEntityResponseCollection = {
  __typename: 'GalleryEntityResponseCollection';
  data: Array<GalleryEntity>;
  meta: ResponseCollectionMeta;
};

export type GalleryFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<GalleryFiltersInput>>>;
  category?: InputMaybe<CategoryFiltersInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  not?: InputMaybe<GalleryFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<GalleryFiltersInput>>>;
  title?: InputMaybe<StringFilterInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
};

export type GalleryRelationResponseCollection = {
  __typename: 'GalleryRelationResponseCollection';
  data: Array<GalleryEntity>;
};

export type Game = {
  __typename: 'Game';
  away: CmshbTeam;
  cmshbId?: Maybe<Scalars['String']>;
  date: Scalars['Date'];
  home: CmshbTeam;
  id: Scalars['ID'];
  kertId?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
  metadata?: Maybe<GameMetadata>;
  place?: Maybe<Scalars['String']>;
  score: GameScore;
  status?: Maybe<Scalars['String']>;
  team: Team;
};

export type GameFilter = {
  from?: InputMaybe<Scalars['Date']>;
  limit?: InputMaybe<Scalars['Int']>;
  nearest?: InputMaybe<Scalars['Boolean']>;
  place?: InputMaybe<GameFilterPlaceEnum>;
  schedule?: InputMaybe<GameFilterScheduleEnum>;
  to?: InputMaybe<Scalars['Date']>;
};

export enum GameFilterPlaceEnum {
  ALL = 'ALL',
  AWAY = 'AWAY',
  HOME = 'HOME',
  HOME_GROUND = 'HOME_GROUND',
}

export enum GameFilterScheduleEnum {
  ALL = 'ALL',
  RESULT = 'RESULT',
  UPCOMING = 'UPCOMING',
}

export type GameMetadata = {
  __typename: 'GameMetadata';
  hasAiReport?: Maybe<Scalars['Boolean']>;
  highlightsVideo?: Maybe<GameMetadataVideo>;
  liveVideo?: Maybe<GameMetadataVideo>;
  onlajnyCz?: Maybe<Scalars['String']>;
  reportPostSlug?: Maybe<Scalars['String']>;
};

export type GameMetadataVideo = {
  __typename: 'GameMetadataVideo';
  type: Scalars['String'];
  url: Scalars['String'];
};

export type GameScore = {
  __typename: 'GameScore';
  away: GameTeamScore;
  home: GameTeamScore;
};

export type GameTeamScore = {
  __typename: 'GameTeamScore';
  finalScore?: Maybe<Scalars['Int']>;
  firstPeriod?: Maybe<Scalars['Int']>;
  overtime?: Maybe<Scalars['Int']>;
  secondPeriod?: Maybe<Scalars['Int']>;
  shootout?: Maybe<Scalars['Int']>;
  thirdPeriod?: Maybe<Scalars['Int']>;
};

export type GameWithCmshbGameDetail = {
  __typename: 'GameWithCmshbGameDetail';
  away: CmshbTeam;
  cmshbId?: Maybe<Scalars['String']>;
  date: Scalars['Date'];
  goalkeepers?: Maybe<CmshbGameDetailGoalkeepers>;
  goals?: Maybe<CmshbGameDetailGoals>;
  home: CmshbTeam;
  id: Scalars['ID'];
  kertId?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
  metadata?: Maybe<GameMetadata>;
  penalties?: Maybe<CmshbGameDetailPenalties>;
  place?: Maybe<Scalars['String']>;
  players?: Maybe<CmshbGameDetailPlayers>;
  post?: Maybe<CmshbGameDetailPost>;
  score: GameScore;
  shootouts?: Maybe<CmshbGameDetailShootouts>;
  statistics?: Maybe<CmshbGameDetailStats>;
  status?: Maybe<Scalars['String']>;
  team: Team;
};

export type GenericMorph =
  | Category
  | ClubPage
  | ComponentCommonAchievement
  | ComponentCommonAchievementList
  | ComponentCommonApiGame
  | ComponentCommonApiGameList
  | ComponentCommonEmbedVideo
  | ComponentCommonGallery
  | ComponentCommonOfficial
  | ComponentCommonOfficialList
  | ComponentCommonPartner
  | ComponentCommonPartnerList
  | ComponentCommonRichText
  | ComponentPlayerTeamPlayer
  | ExternalPost
  | Gallery
  | I18NLocale
  | Partner
  | Person
  | Post
  | Status
  | UploadFile
  | UploadFolder
  | UsersPermissionsPermission
  | UsersPermissionsRole
  | UsersPermissionsUser;

export type HokejbalCzArticle = {
  __typename: 'HokejbalCzArticle';
  date: Scalars['Date'];
  id: Scalars['ID'];
  imageUrl?: Maybe<Scalars['String']>;
  perex: Scalars['String'];
  title: Scalars['String'];
};

export type I18NLocale = {
  __typename: 'I18NLocale';
  code?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  name?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type I18NLocaleEntity = {
  __typename: 'I18NLocaleEntity';
  attributes?: Maybe<I18NLocale>;
  id?: Maybe<Scalars['ID']>;
};

export type I18NLocaleEntityResponse = {
  __typename: 'I18NLocaleEntityResponse';
  data?: Maybe<I18NLocaleEntity>;
};

export type I18NLocaleEntityResponseCollection = {
  __typename: 'I18NLocaleEntityResponseCollection';
  data: Array<I18NLocaleEntity>;
  meta: ResponseCollectionMeta;
};

export type I18NLocaleFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<I18NLocaleFiltersInput>>>;
  code?: InputMaybe<StringFilterInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  name?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<I18NLocaleFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<I18NLocaleFiltersInput>>>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
};

export type IdFilterInput = {
  and?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  between?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  contains?: InputMaybe<Scalars['ID']>;
  containsi?: InputMaybe<Scalars['ID']>;
  endsWith?: InputMaybe<Scalars['ID']>;
  eq?: InputMaybe<Scalars['ID']>;
  eqi?: InputMaybe<Scalars['ID']>;
  gt?: InputMaybe<Scalars['ID']>;
  gte?: InputMaybe<Scalars['ID']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  lt?: InputMaybe<Scalars['ID']>;
  lte?: InputMaybe<Scalars['ID']>;
  ne?: InputMaybe<Scalars['ID']>;
  nei?: InputMaybe<Scalars['ID']>;
  not?: InputMaybe<IdFilterInput>;
  notContains?: InputMaybe<Scalars['ID']>;
  notContainsi?: InputMaybe<Scalars['ID']>;
  notIn?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  notNull?: InputMaybe<Scalars['Boolean']>;
  null?: InputMaybe<Scalars['Boolean']>;
  or?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  startsWith?: InputMaybe<Scalars['ID']>;
};

export type IntFilterInput = {
  and?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
  between?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
  contains?: InputMaybe<Scalars['Int']>;
  containsi?: InputMaybe<Scalars['Int']>;
  endsWith?: InputMaybe<Scalars['Int']>;
  eq?: InputMaybe<Scalars['Int']>;
  eqi?: InputMaybe<Scalars['Int']>;
  gt?: InputMaybe<Scalars['Int']>;
  gte?: InputMaybe<Scalars['Int']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
  lt?: InputMaybe<Scalars['Int']>;
  lte?: InputMaybe<Scalars['Int']>;
  ne?: InputMaybe<Scalars['Int']>;
  nei?: InputMaybe<Scalars['Int']>;
  not?: InputMaybe<IntFilterInput>;
  notContains?: InputMaybe<Scalars['Int']>;
  notContainsi?: InputMaybe<Scalars['Int']>;
  notIn?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
  notNull?: InputMaybe<Scalars['Boolean']>;
  null?: InputMaybe<Scalars['Boolean']>;
  or?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
  startsWith?: InputMaybe<Scalars['Int']>;
};

export type JsonFilterInput = {
  and?: InputMaybe<Array<InputMaybe<Scalars['JSON']>>>;
  between?: InputMaybe<Array<InputMaybe<Scalars['JSON']>>>;
  contains?: InputMaybe<Scalars['JSON']>;
  containsi?: InputMaybe<Scalars['JSON']>;
  endsWith?: InputMaybe<Scalars['JSON']>;
  eq?: InputMaybe<Scalars['JSON']>;
  eqi?: InputMaybe<Scalars['JSON']>;
  gt?: InputMaybe<Scalars['JSON']>;
  gte?: InputMaybe<Scalars['JSON']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['JSON']>>>;
  lt?: InputMaybe<Scalars['JSON']>;
  lte?: InputMaybe<Scalars['JSON']>;
  ne?: InputMaybe<Scalars['JSON']>;
  nei?: InputMaybe<Scalars['JSON']>;
  not?: InputMaybe<JsonFilterInput>;
  notContains?: InputMaybe<Scalars['JSON']>;
  notContainsi?: InputMaybe<Scalars['JSON']>;
  notIn?: InputMaybe<Array<InputMaybe<Scalars['JSON']>>>;
  notNull?: InputMaybe<Scalars['Boolean']>;
  null?: InputMaybe<Scalars['Boolean']>;
  or?: InputMaybe<Array<InputMaybe<Scalars['JSON']>>>;
  startsWith?: InputMaybe<Scalars['JSON']>;
};

export type Mutation = {
  __typename: 'Mutation';
  /** Change user password. Confirm with the current password. */
  changePassword?: Maybe<UsersPermissionsLoginPayload>;
  createUploadFile?: Maybe<UploadFileEntityResponse>;
  createUploadFolder?: Maybe<UploadFolderEntityResponse>;
  /** Create a new role */
  createUsersPermissionsRole?: Maybe<UsersPermissionsCreateRolePayload>;
  /** Create a new user */
  createUsersPermissionsUser: UsersPermissionsUserEntityResponse;
  deleteUploadFile?: Maybe<UploadFileEntityResponse>;
  deleteUploadFolder?: Maybe<UploadFolderEntityResponse>;
  /** Delete an existing role */
  deleteUsersPermissionsRole?: Maybe<UsersPermissionsDeleteRolePayload>;
  /** Delete an existing user */
  deleteUsersPermissionsUser: UsersPermissionsUserEntityResponse;
  /** Confirm an email users email address */
  emailConfirmation?: Maybe<UsersPermissionsLoginPayload>;
  /** Request a reset password token */
  forgotPassword?: Maybe<UsersPermissionsPasswordPayload>;
  login: UsersPermissionsLoginPayload;
  multipleUpload: Array<Maybe<UploadFileEntityResponse>>;
  /** Register a user */
  register: UsersPermissionsLoginPayload;
  removeFile?: Maybe<UploadFileEntityResponse>;
  /** Reset user password. Confirm with a code (resetToken from forgotPassword) */
  resetPassword?: Maybe<UsersPermissionsLoginPayload>;
  updateFileInfo: UploadFileEntityResponse;
  updateUploadFile?: Maybe<UploadFileEntityResponse>;
  updateUploadFolder?: Maybe<UploadFolderEntityResponse>;
  /** Update an existing role */
  updateUsersPermissionsRole?: Maybe<UsersPermissionsUpdateRolePayload>;
  /** Update an existing user */
  updateUsersPermissionsUser: UsersPermissionsUserEntityResponse;
  upload: UploadFileEntityResponse;
};

export type MutationChangePasswordArgs = {
  currentPassword: Scalars['String'];
  password: Scalars['String'];
  passwordConfirmation: Scalars['String'];
};

export type MutationCreateUploadFileArgs = {
  data: UploadFileInput;
};

export type MutationCreateUploadFolderArgs = {
  data: UploadFolderInput;
};

export type MutationCreateUsersPermissionsRoleArgs = {
  data: UsersPermissionsRoleInput;
};

export type MutationCreateUsersPermissionsUserArgs = {
  data: UsersPermissionsUserInput;
};

export type MutationDeleteUploadFileArgs = {
  id: Scalars['ID'];
};

export type MutationDeleteUploadFolderArgs = {
  id: Scalars['ID'];
};

export type MutationDeleteUsersPermissionsRoleArgs = {
  id: Scalars['ID'];
};

export type MutationDeleteUsersPermissionsUserArgs = {
  id: Scalars['ID'];
};

export type MutationEmailConfirmationArgs = {
  confirmation: Scalars['String'];
};

export type MutationForgotPasswordArgs = {
  email: Scalars['String'];
};

export type MutationLoginArgs = {
  input: UsersPermissionsLoginInput;
};

export type MutationMultipleUploadArgs = {
  field?: InputMaybe<Scalars['String']>;
  files: Array<InputMaybe<Scalars['Upload']>>;
  ref?: InputMaybe<Scalars['String']>;
  refId?: InputMaybe<Scalars['ID']>;
};

export type MutationRegisterArgs = {
  input: UsersPermissionsRegisterInput;
};

export type MutationRemoveFileArgs = {
  id: Scalars['ID'];
};

export type MutationResetPasswordArgs = {
  code: Scalars['String'];
  password: Scalars['String'];
  passwordConfirmation: Scalars['String'];
};

export type MutationUpdateFileInfoArgs = {
  id: Scalars['ID'];
  info?: InputMaybe<FileInfoInput>;
};

export type MutationUpdateUploadFileArgs = {
  data: UploadFileInput;
  id: Scalars['ID'];
};

export type MutationUpdateUploadFolderArgs = {
  data: UploadFolderInput;
  id: Scalars['ID'];
};

export type MutationUpdateUsersPermissionsRoleArgs = {
  data: UsersPermissionsRoleInput;
  id: Scalars['ID'];
};

export type MutationUpdateUsersPermissionsUserArgs = {
  data: UsersPermissionsUserInput;
  id: Scalars['ID'];
};

export type MutationUploadArgs = {
  field?: InputMaybe<Scalars['String']>;
  file: Scalars['Upload'];
  info?: InputMaybe<FileInfoInput>;
  ref?: InputMaybe<Scalars['String']>;
  refId?: InputMaybe<Scalars['ID']>;
};

export type Pagination = {
  __typename: 'Pagination';
  page: Scalars['Int'];
  pageCount: Scalars['Int'];
  pageSize: Scalars['Int'];
  total: Scalars['Int'];
};

export type PaginationArg = {
  limit?: InputMaybe<Scalars['Int']>;
  page?: InputMaybe<Scalars['Int']>;
  pageSize?: InputMaybe<Scalars['Int']>;
  start?: InputMaybe<Scalars['Int']>;
};

export type Partner = {
  __typename: 'Partner';
  createdAt?: Maybe<Scalars['DateTime']>;
  partnerLists: Array<Maybe<ComponentCommonPartnerList>>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type PartnerPartnerListsArgs = {
  filters?: InputMaybe<ComponentCommonPartnerListFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type PartnerEntity = {
  __typename: 'PartnerEntity';
  attributes?: Maybe<Partner>;
  id?: Maybe<Scalars['ID']>;
};

export type PartnerEntityResponse = {
  __typename: 'PartnerEntityResponse';
  data?: Maybe<PartnerEntity>;
};

export type Person = {
  __typename: 'Person';
  createdAt?: Maybe<Scalars['DateTime']>;
  email?: Maybe<Scalars['String']>;
  image?: Maybe<UploadFileEntityResponse>;
  name: Scalars['String'];
  phone?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type PersonEntity = {
  __typename: 'PersonEntity';
  attributes?: Maybe<Person>;
  id?: Maybe<Scalars['ID']>;
};

export type PersonEntityResponse = {
  __typename: 'PersonEntityResponse';
  data?: Maybe<PersonEntity>;
};

export type PersonEntityResponseCollection = {
  __typename: 'PersonEntityResponseCollection';
  data: Array<PersonEntity>;
  meta: ResponseCollectionMeta;
};

export type PersonFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<PersonFiltersInput>>>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  email?: InputMaybe<StringFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  name?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<PersonFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<PersonFiltersInput>>>;
  phone?: InputMaybe<StringFilterInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
};

export type Place = {
  __typename: 'Place';
  id: Scalars['ID'];
  name: Scalars['String'];
  systemName: Scalars['String'];
};

export type PlayOff = {
  __typename: 'PlayOff';
  id: Scalars['ID'];
  label?: Maybe<Scalars['String']>;
  rounds: Array<PlayOffRound>;
};

export type PlayOffRound = {
  __typename: 'PlayOffRound';
  games: Array<Game>;
  id: Scalars['ID'];
  kertTeam: CmshbTeam;
  name: Scalars['String'];
  opponentTeam: CmshbTeam;
  score: PlayOffRoundScore;
  shortName: Scalars['String'];
  shortcut: Scalars['String'];
};

export type PlayOffRoundScore = {
  __typename: 'PlayOffRoundScore';
  kertTeam: Scalars['Int'];
  opponentTeam: Scalars['Int'];
};

export type Player = {
  __typename: 'Player';
  achievements?: Maybe<Array<Maybe<ComponentCommonAchievementList>>>;
  birth?: Maybe<Scalars['Date']>;
  cmshbId?: Maybe<Scalars['ID']>;
  height?: Maybe<Scalars['Int']>;
  id: Scalars['ID'];
  largeImage?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  number?: Maybe<Scalars['Int']>;
  post?: Maybe<EnumComponentplayerteamplayerPost>;
  smallImage?: Maybe<Scalars['String']>;
  stick?: Maybe<EnumComponentplayerteamplayerStick>;
  weight?: Maybe<Scalars['Int']>;
};

export type PlayerUnion = CmshbPlayer | Player;

export type Post = {
  __typename: 'Post';
  author?: Maybe<Scalars['String']>;
  categories?: Maybe<CategoryRelationResponseCollection>;
  content: Array<Maybe<PostContentDynamicZone>>;
  createdAt?: Maybe<Scalars['DateTime']>;
  createdBy?: Maybe<Creator>;
  game?: Maybe<Scalars['String']>;
  perex: Scalars['String'];
  pinToTop?: Maybe<Scalars['Boolean']>;
  publishAt: Scalars['DateTime'];
  publishedAt?: Maybe<Scalars['DateTime']>;
  slug: Scalars['String'];
  thumb?: Maybe<UploadFileEntityResponse>;
  thumbAuthor?: Maybe<Scalars['String']>;
  title: Scalars['String'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type PostCategoriesArgs = {
  filters?: InputMaybe<CategoryFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type PostCompositeResponseCollection = {
  __typename: 'PostCompositeResponseCollection';
  data: Array<PostCompositeUnion>;
  meta: PostCompositeResponseMeta;
};

export type PostCompositeResponseMeta = {
  __typename: 'PostCompositeResponseMeta';
  pagination: Pagination;
};

export type PostCompositeUnion = PostEntity | PostExternalComposite;

export type PostContentDynamicZone =
  | ComponentCommonApiGame
  | ComponentCommonApiGameList
  | ComponentCommonEmbedVideo
  | ComponentCommonGallery
  | ComponentCommonRichText
  | Error;

export type PostEntity = {
  __typename: 'PostEntity';
  attributes?: Maybe<Post>;
  id?: Maybe<Scalars['ID']>;
};

export type PostEntityResponse = {
  __typename: 'PostEntityResponse';
  data?: Maybe<PostEntity>;
};

export type PostEntityResponseCollection = {
  __typename: 'PostEntityResponseCollection';
  data: Array<PostEntity>;
  meta: ResponseCollectionMeta;
};

export type PostExternalComposite = {
  __typename: 'PostExternalComposite';
  categories: Array<CategoryEntity>;
  id?: Maybe<Scalars['ID']>;
  imageUrl?: Maybe<Scalars['String']>;
  perex: Scalars['String'];
  publishAt: Scalars['DateTime'];
  title: Scalars['String'];
  url: Scalars['String'];
};

export type PostFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<PostFiltersInput>>>;
  author?: InputMaybe<StringFilterInput>;
  categories?: InputMaybe<CategoryFiltersInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  not?: InputMaybe<PostFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<PostFiltersInput>>>;
  perex?: InputMaybe<StringFilterInput>;
  pinToTop?: InputMaybe<BooleanFilterInput>;
  publishAt?: InputMaybe<DateTimeFilterInput>;
  publishedAt?: InputMaybe<DateTimeFilterInput>;
  slug?: InputMaybe<StringFilterInput>;
  thumbAuthor?: InputMaybe<StringFilterInput>;
  title?: InputMaybe<StringFilterInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
};

export enum PublicationState {
  LIVE = 'LIVE',
  PREVIEW = 'PREVIEW',
}

export type Query = {
  __typename: 'Query';
  categories?: Maybe<CategoryEntityResponseCollection>;
  category?: Maybe<CategoryEntityResponse>;
  clubPage?: Maybe<ClubPageEntityResponse>;
  clubPages?: Maybe<ClubPageEntityResponseCollection>;
  cmshbPlayerStatsList: Array<CmshbPlayerStatsList>;
  cmshbTeamStats?: Maybe<CmshbTeamTableStats>;
  cmshbTeamTable: Array<CmshbTeamTable>;
  externalPost?: Maybe<ExternalPostEntityResponse>;
  externalPosts?: Maybe<ExternalPostEntityResponseCollection>;
  galleries?: Maybe<GalleryEntityResponseCollection>;
  gallery?: Maybe<GalleryEntityResponse>;
  game?: Maybe<GameWithCmshbGameDetail>;
  gameList: Array<Game>;
  hokejbalCzArticle?: Maybe<HokejbalCzArticle>;
  i18NLocale?: Maybe<I18NLocaleEntityResponse>;
  i18NLocales?: Maybe<I18NLocaleEntityResponseCollection>;
  me?: Maybe<UsersPermissionsMe>;
  partner?: Maybe<PartnerEntityResponse>;
  people?: Maybe<PersonEntityResponseCollection>;
  person?: Maybe<PersonEntityResponse>;
  playOffList: Array<PlayOff>;
  player?: Maybe<Player>;
  players: Array<Player>;
  post?: Maybe<PostEntityResponse>;
  postByApiGame?: Maybe<PostEntityResponse>;
  postBySlug?: Maybe<PostEntityResponse>;
  posts?: Maybe<PostEntityResponseCollection>;
  postsByApiGames?: Maybe<PostEntityResponseCollection>;
  postsComposite?: Maybe<PostCompositeResponseCollection>;
  seasonList: Array<Season>;
  status?: Maybe<StatusEntityResponse>;
  teamList: Array<Team>;
  tournamentList: Array<Tournament>;
  uploadFile?: Maybe<UploadFileEntityResponse>;
  uploadFiles?: Maybe<UploadFileEntityResponseCollection>;
  uploadFolder?: Maybe<UploadFolderEntityResponse>;
  uploadFolders?: Maybe<UploadFolderEntityResponseCollection>;
  usersPermissionsRole?: Maybe<UsersPermissionsRoleEntityResponse>;
  usersPermissionsRoles?: Maybe<UsersPermissionsRoleEntityResponseCollection>;
  usersPermissionsUser?: Maybe<UsersPermissionsUserEntityResponse>;
  usersPermissionsUsers?: Maybe<UsersPermissionsUserEntityResponseCollection>;
};

export type QueryCategoriesArgs = {
  filters?: InputMaybe<CategoryFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryCategoryArgs = {
  id?: InputMaybe<Scalars['ID']>;
};

export type QueryClubPageArgs = {
  id?: InputMaybe<Scalars['ID']>;
};

export type QueryClubPagesArgs = {
  filters?: InputMaybe<ClubPageFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  publicationState?: InputMaybe<PublicationState>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryCmshbPlayerStatsListArgs = {
  teamId: Scalars['String'];
  year: Scalars['String'];
};

export type QueryCmshbTeamStatsArgs = {
  teamId: Scalars['String'];
  year: Scalars['String'];
};

export type QueryCmshbTeamTableArgs = {
  teamId: Scalars['String'];
  year: Scalars['String'];
};

export type QueryExternalPostArgs = {
  id?: InputMaybe<Scalars['ID']>;
};

export type QueryExternalPostsArgs = {
  filters?: InputMaybe<ExternalPostFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryGalleriesArgs = {
  filters?: InputMaybe<GalleryFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryGalleryArgs = {
  id?: InputMaybe<Scalars['ID']>;
};

export type QueryGameArgs = {
  id: Scalars['String'];
  teamId: Scalars['String'];
  year: Scalars['String'];
};

export type QueryGameListArgs = {
  filter?: InputMaybe<GameFilter>;
  teamId?: InputMaybe<Scalars['String']>;
  teamIds?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  year: Scalars['String'];
};

export type QueryHokejbalCzArticleArgs = {
  url: Scalars['String'];
};

export type QueryI18NLocaleArgs = {
  id?: InputMaybe<Scalars['ID']>;
};

export type QueryI18NLocalesArgs = {
  filters?: InputMaybe<I18NLocaleFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryPeopleArgs = {
  filters?: InputMaybe<PersonFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryPersonArgs = {
  id?: InputMaybe<Scalars['ID']>;
};

export type QueryPlayOffListArgs = {
  teamId: Scalars['String'];
  year: Scalars['String'];
};

export type QueryPlayerArgs = {
  id: Scalars['ID'];
  teamId: Scalars['ID'];
};

export type QueryPlayersArgs = {
  teamId: Scalars['ID'];
};

export type QueryPostArgs = {
  id?: InputMaybe<Scalars['ID']>;
};

export type QueryPostByApiGameArgs = {
  id?: InputMaybe<Scalars['String']>;
  teamId?: InputMaybe<Scalars['String']>;
  year?: InputMaybe<Scalars['String']>;
};

export type QueryPostBySlugArgs = {
  slug?: InputMaybe<Scalars['String']>;
};

export type QueryPostsArgs = {
  filters?: InputMaybe<PostFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  publicationState?: InputMaybe<PublicationState>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryPostsByApiGamesArgs = {
  teamId?: InputMaybe<Scalars['String']>;
  year?: InputMaybe<Scalars['String']>;
};

export type QueryPostsCompositeArgs = {
  categoryCode?: InputMaybe<Scalars['String']>;
  dateRange?: InputMaybe<DateRangeInput>;
  pagination?: InputMaybe<PaginationArg>;
};

export type QueryTournamentListArgs = {
  from?: InputMaybe<Scalars['Date']>;
  teamId?: InputMaybe<Scalars['String']>;
  teamIds?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  to?: InputMaybe<Scalars['Date']>;
  year: Scalars['String'];
};

export type QueryUploadFileArgs = {
  id?: InputMaybe<Scalars['ID']>;
};

export type QueryUploadFilesArgs = {
  filters?: InputMaybe<UploadFileFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryUploadFolderArgs = {
  id?: InputMaybe<Scalars['ID']>;
};

export type QueryUploadFoldersArgs = {
  filters?: InputMaybe<UploadFolderFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryUsersPermissionsRoleArgs = {
  id?: InputMaybe<Scalars['ID']>;
};

export type QueryUsersPermissionsRolesArgs = {
  filters?: InputMaybe<UsersPermissionsRoleFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryUsersPermissionsUserArgs = {
  id?: InputMaybe<Scalars['ID']>;
};

export type QueryUsersPermissionsUsersArgs = {
  filters?: InputMaybe<UsersPermissionsUserFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type ResponseCollectionMeta = {
  __typename: 'ResponseCollectionMeta';
  pagination: Pagination;
};

export type Season = {
  __typename: 'Season';
  seasonId: Scalars['ID'];
  year: Scalars['String'];
};

export type Status = {
  __typename: 'Status';
  createdAt?: Maybe<Scalars['DateTime']>;
  showIncompleteGamesData?: Maybe<Scalars['Boolean']>;
  showMaintenance?: Maybe<Scalars['Boolean']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type StatusEntity = {
  __typename: 'StatusEntity';
  attributes?: Maybe<Status>;
  id?: Maybe<Scalars['ID']>;
};

export type StatusEntityResponse = {
  __typename: 'StatusEntityResponse';
  data?: Maybe<StatusEntity>;
};

export type StringFilterInput = {
  and?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  between?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  contains?: InputMaybe<Scalars['String']>;
  containsi?: InputMaybe<Scalars['String']>;
  endsWith?: InputMaybe<Scalars['String']>;
  eq?: InputMaybe<Scalars['String']>;
  eqi?: InputMaybe<Scalars['String']>;
  gt?: InputMaybe<Scalars['String']>;
  gte?: InputMaybe<Scalars['String']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  lt?: InputMaybe<Scalars['String']>;
  lte?: InputMaybe<Scalars['String']>;
  ne?: InputMaybe<Scalars['String']>;
  nei?: InputMaybe<Scalars['String']>;
  not?: InputMaybe<StringFilterInput>;
  notContains?: InputMaybe<Scalars['String']>;
  notContainsi?: InputMaybe<Scalars['String']>;
  notIn?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  notNull?: InputMaybe<Scalars['Boolean']>;
  null?: InputMaybe<Scalars['Boolean']>;
  or?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  startsWith?: InputMaybe<Scalars['String']>;
};

export type Team = {
  __typename: 'Team';
  name: Scalars['String'];
  systemName: Scalars['ID'];
};

export type Tournament = {
  __typename: 'Tournament';
  date: Scalars['Date'];
  id: Scalars['ID'];
  place?: Maybe<Place>;
  team: Team;
};

export type UploadFile = {
  __typename: 'UploadFile';
  alternativeText?: Maybe<Scalars['String']>;
  caption?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  ext?: Maybe<Scalars['String']>;
  formats?: Maybe<Scalars['JSON']>;
  hash: Scalars['String'];
  height?: Maybe<Scalars['Int']>;
  mime: Scalars['String'];
  name: Scalars['String'];
  previewUrl?: Maybe<Scalars['String']>;
  provider: Scalars['String'];
  provider_metadata?: Maybe<Scalars['JSON']>;
  related?: Maybe<Array<Maybe<GenericMorph>>>;
  size: Scalars['Float'];
  updatedAt?: Maybe<Scalars['DateTime']>;
  url: Scalars['String'];
  width?: Maybe<Scalars['Int']>;
};

export type UploadFileEntity = {
  __typename: 'UploadFileEntity';
  attributes?: Maybe<UploadFile>;
  id?: Maybe<Scalars['ID']>;
};

export type UploadFileEntityResponse = {
  __typename: 'UploadFileEntityResponse';
  data?: Maybe<UploadFileEntity>;
};

export type UploadFileEntityResponseCollection = {
  __typename: 'UploadFileEntityResponseCollection';
  data: Array<UploadFileEntity>;
  meta: ResponseCollectionMeta;
};

export type UploadFileFiltersInput = {
  alternativeText?: InputMaybe<StringFilterInput>;
  and?: InputMaybe<Array<InputMaybe<UploadFileFiltersInput>>>;
  caption?: InputMaybe<StringFilterInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  ext?: InputMaybe<StringFilterInput>;
  folder?: InputMaybe<UploadFolderFiltersInput>;
  folderPath?: InputMaybe<StringFilterInput>;
  formats?: InputMaybe<JsonFilterInput>;
  hash?: InputMaybe<StringFilterInput>;
  height?: InputMaybe<IntFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  mime?: InputMaybe<StringFilterInput>;
  name?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<UploadFileFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<UploadFileFiltersInput>>>;
  previewUrl?: InputMaybe<StringFilterInput>;
  provider?: InputMaybe<StringFilterInput>;
  provider_metadata?: InputMaybe<JsonFilterInput>;
  size?: InputMaybe<FloatFilterInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
  url?: InputMaybe<StringFilterInput>;
  width?: InputMaybe<IntFilterInput>;
};

export type UploadFileInput = {
  alternativeText?: InputMaybe<Scalars['String']>;
  caption?: InputMaybe<Scalars['String']>;
  ext?: InputMaybe<Scalars['String']>;
  folder?: InputMaybe<Scalars['ID']>;
  folderPath?: InputMaybe<Scalars['String']>;
  formats?: InputMaybe<Scalars['JSON']>;
  hash?: InputMaybe<Scalars['String']>;
  height?: InputMaybe<Scalars['Int']>;
  mime?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  previewUrl?: InputMaybe<Scalars['String']>;
  provider?: InputMaybe<Scalars['String']>;
  provider_metadata?: InputMaybe<Scalars['JSON']>;
  size?: InputMaybe<Scalars['Float']>;
  url?: InputMaybe<Scalars['String']>;
  width?: InputMaybe<Scalars['Int']>;
};

export type UploadFileRelationResponseCollection = {
  __typename: 'UploadFileRelationResponseCollection';
  data: Array<UploadFileEntity>;
};

export type UploadFolder = {
  __typename: 'UploadFolder';
  children?: Maybe<UploadFolderRelationResponseCollection>;
  createdAt?: Maybe<Scalars['DateTime']>;
  files?: Maybe<UploadFileRelationResponseCollection>;
  name: Scalars['String'];
  parent?: Maybe<UploadFolderEntityResponse>;
  path: Scalars['String'];
  pathId: Scalars['Int'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type UploadFolderChildrenArgs = {
  filters?: InputMaybe<UploadFolderFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type UploadFolderFilesArgs = {
  filters?: InputMaybe<UploadFileFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type UploadFolderEntity = {
  __typename: 'UploadFolderEntity';
  attributes?: Maybe<UploadFolder>;
  id?: Maybe<Scalars['ID']>;
};

export type UploadFolderEntityResponse = {
  __typename: 'UploadFolderEntityResponse';
  data?: Maybe<UploadFolderEntity>;
};

export type UploadFolderEntityResponseCollection = {
  __typename: 'UploadFolderEntityResponseCollection';
  data: Array<UploadFolderEntity>;
  meta: ResponseCollectionMeta;
};

export type UploadFolderFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<UploadFolderFiltersInput>>>;
  children?: InputMaybe<UploadFolderFiltersInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  files?: InputMaybe<UploadFileFiltersInput>;
  id?: InputMaybe<IdFilterInput>;
  name?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<UploadFolderFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<UploadFolderFiltersInput>>>;
  parent?: InputMaybe<UploadFolderFiltersInput>;
  path?: InputMaybe<StringFilterInput>;
  pathId?: InputMaybe<IntFilterInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
};

export type UploadFolderInput = {
  children?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  files?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  name?: InputMaybe<Scalars['String']>;
  parent?: InputMaybe<Scalars['ID']>;
  path?: InputMaybe<Scalars['String']>;
  pathId?: InputMaybe<Scalars['Int']>;
};

export type UploadFolderRelationResponseCollection = {
  __typename: 'UploadFolderRelationResponseCollection';
  data: Array<UploadFolderEntity>;
};

export type UsersPermissionsCreateRolePayload = {
  __typename: 'UsersPermissionsCreateRolePayload';
  ok: Scalars['Boolean'];
};

export type UsersPermissionsDeleteRolePayload = {
  __typename: 'UsersPermissionsDeleteRolePayload';
  ok: Scalars['Boolean'];
};

export type UsersPermissionsLoginInput = {
  identifier: Scalars['String'];
  password: Scalars['String'];
  provider?: Scalars['String'];
};

export type UsersPermissionsLoginPayload = {
  __typename: 'UsersPermissionsLoginPayload';
  jwt?: Maybe<Scalars['String']>;
  user: UsersPermissionsMe;
};

export type UsersPermissionsMe = {
  __typename: 'UsersPermissionsMe';
  blocked?: Maybe<Scalars['Boolean']>;
  confirmed?: Maybe<Scalars['Boolean']>;
  email?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  role?: Maybe<UsersPermissionsMeRole>;
  username: Scalars['String'];
};

export type UsersPermissionsMeRole = {
  __typename: 'UsersPermissionsMeRole';
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  type?: Maybe<Scalars['String']>;
};

export type UsersPermissionsPasswordPayload = {
  __typename: 'UsersPermissionsPasswordPayload';
  ok: Scalars['Boolean'];
};

export type UsersPermissionsPermission = {
  __typename: 'UsersPermissionsPermission';
  action: Scalars['String'];
  createdAt?: Maybe<Scalars['DateTime']>;
  role?: Maybe<UsersPermissionsRoleEntityResponse>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type UsersPermissionsPermissionEntity = {
  __typename: 'UsersPermissionsPermissionEntity';
  attributes?: Maybe<UsersPermissionsPermission>;
  id?: Maybe<Scalars['ID']>;
};

export type UsersPermissionsPermissionFiltersInput = {
  action?: InputMaybe<StringFilterInput>;
  and?: InputMaybe<Array<InputMaybe<UsersPermissionsPermissionFiltersInput>>>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  not?: InputMaybe<UsersPermissionsPermissionFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<UsersPermissionsPermissionFiltersInput>>>;
  role?: InputMaybe<UsersPermissionsRoleFiltersInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
};

export type UsersPermissionsPermissionRelationResponseCollection = {
  __typename: 'UsersPermissionsPermissionRelationResponseCollection';
  data: Array<UsersPermissionsPermissionEntity>;
};

export type UsersPermissionsRegisterInput = {
  email: Scalars['String'];
  password: Scalars['String'];
  username: Scalars['String'];
};

export type UsersPermissionsRole = {
  __typename: 'UsersPermissionsRole';
  createdAt?: Maybe<Scalars['DateTime']>;
  description?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  permissions?: Maybe<UsersPermissionsPermissionRelationResponseCollection>;
  type?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  users?: Maybe<UsersPermissionsUserRelationResponseCollection>;
};

export type UsersPermissionsRolePermissionsArgs = {
  filters?: InputMaybe<UsersPermissionsPermissionFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type UsersPermissionsRoleUsersArgs = {
  filters?: InputMaybe<UsersPermissionsUserFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type UsersPermissionsRoleEntity = {
  __typename: 'UsersPermissionsRoleEntity';
  attributes?: Maybe<UsersPermissionsRole>;
  id?: Maybe<Scalars['ID']>;
};

export type UsersPermissionsRoleEntityResponse = {
  __typename: 'UsersPermissionsRoleEntityResponse';
  data?: Maybe<UsersPermissionsRoleEntity>;
};

export type UsersPermissionsRoleEntityResponseCollection = {
  __typename: 'UsersPermissionsRoleEntityResponseCollection';
  data: Array<UsersPermissionsRoleEntity>;
  meta: ResponseCollectionMeta;
};

export type UsersPermissionsRoleFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<UsersPermissionsRoleFiltersInput>>>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  description?: InputMaybe<StringFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  name?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<UsersPermissionsRoleFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<UsersPermissionsRoleFiltersInput>>>;
  permissions?: InputMaybe<UsersPermissionsPermissionFiltersInput>;
  type?: InputMaybe<StringFilterInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
  users?: InputMaybe<UsersPermissionsUserFiltersInput>;
};

export type UsersPermissionsRoleInput = {
  description?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  permissions?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  type?: InputMaybe<Scalars['String']>;
  users?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
};

export type UsersPermissionsUpdateRolePayload = {
  __typename: 'UsersPermissionsUpdateRolePayload';
  ok: Scalars['Boolean'];
};

export type UsersPermissionsUser = {
  __typename: 'UsersPermissionsUser';
  blocked?: Maybe<Scalars['Boolean']>;
  confirmed?: Maybe<Scalars['Boolean']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  email: Scalars['String'];
  provider?: Maybe<Scalars['String']>;
  role?: Maybe<UsersPermissionsRoleEntityResponse>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  username: Scalars['String'];
};

export type UsersPermissionsUserEntity = {
  __typename: 'UsersPermissionsUserEntity';
  attributes?: Maybe<UsersPermissionsUser>;
  id?: Maybe<Scalars['ID']>;
};

export type UsersPermissionsUserEntityResponse = {
  __typename: 'UsersPermissionsUserEntityResponse';
  data?: Maybe<UsersPermissionsUserEntity>;
};

export type UsersPermissionsUserEntityResponseCollection = {
  __typename: 'UsersPermissionsUserEntityResponseCollection';
  data: Array<UsersPermissionsUserEntity>;
  meta: ResponseCollectionMeta;
};

export type UsersPermissionsUserFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<UsersPermissionsUserFiltersInput>>>;
  blocked?: InputMaybe<BooleanFilterInput>;
  confirmationToken?: InputMaybe<StringFilterInput>;
  confirmed?: InputMaybe<BooleanFilterInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  email?: InputMaybe<StringFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  not?: InputMaybe<UsersPermissionsUserFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<UsersPermissionsUserFiltersInput>>>;
  password?: InputMaybe<StringFilterInput>;
  provider?: InputMaybe<StringFilterInput>;
  resetPasswordToken?: InputMaybe<StringFilterInput>;
  role?: InputMaybe<UsersPermissionsRoleFiltersInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
  username?: InputMaybe<StringFilterInput>;
};

export type UsersPermissionsUserInput = {
  blocked?: InputMaybe<Scalars['Boolean']>;
  confirmationToken?: InputMaybe<Scalars['String']>;
  confirmed?: InputMaybe<Scalars['Boolean']>;
  email?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
  provider?: InputMaybe<Scalars['String']>;
  resetPasswordToken?: InputMaybe<Scalars['String']>;
  role?: InputMaybe<Scalars['ID']>;
  username?: InputMaybe<Scalars['String']>;
};

export type UsersPermissionsUserRelationResponseCollection = {
  __typename: 'UsersPermissionsUserRelationResponseCollection';
  data: Array<UsersPermissionsUserEntity>;
};

export type CategoriesQueryVariables = Exact<{ [key: string]: never }>;

export type CategoriesQuery = {
  __typename: 'Query';
  categories?: {
    __typename: 'CategoryEntityResponseCollection';
    data: Array<{
      __typename: 'CategoryEntity';
      attributes?: {
        __typename: 'Category';
        name: string;
        code: string;
        image?: {
          __typename: 'UploadFileEntityResponse';
          data?: {
            __typename: 'UploadFileEntity';
            id?: string | null;
            attributes?: {
              __typename: 'UploadFile';
              formats?: { [key: string]: any } | null;
              name: string;
              url: string;
            } | null;
          } | null;
        } | null;
        officials?: Array<{
          __typename: 'ComponentCommonOfficial';
          id: string;
          title: string;
          person?: {
            __typename: 'PersonEntityResponse';
            data?: {
              __typename: 'PersonEntity';
              id?: string | null;
              attributes?: {
                __typename: 'Person';
                name: string;
                email?: string | null;
                phone?: string | null;
                image?: {
                  __typename: 'UploadFileEntityResponse';
                  data?: {
                    __typename: 'UploadFileEntity';
                    id?: string | null;
                    attributes?: {
                      __typename: 'UploadFile';
                      formats?: { [key: string]: any } | null;
                      name: string;
                      url: string;
                    } | null;
                  } | null;
                } | null;
              } | null;
            } | null;
          } | null;
        } | null> | null;
        achievements?: Array<{
          __typename: 'ComponentCommonAchievement';
          id: string;
          title: string;
          years?: string | null;
          isWinner: boolean;
          image: {
            __typename: 'UploadFileEntityResponse';
            data?: {
              __typename: 'UploadFileEntity';
              id?: string | null;
              attributes?: {
                __typename: 'UploadFile';
                formats?: { [key: string]: any } | null;
                name: string;
                url: string;
              } | null;
            } | null;
          };
        } | null> | null;
      } | null;
    }>;
  } | null;
};

export type ClubPageQueryVariables = Exact<{
  slug: Scalars['String'];
  publicationState?: InputMaybe<PublicationState>;
}>;

export type ClubPageQuery = {
  __typename: 'Query';
  clubPages?: {
    __typename: 'ClubPageEntityResponseCollection';
    data: Array<{
      __typename: 'ClubPageEntity';
      id?: string | null;
      attributes?: {
        __typename: 'ClubPage';
        title: string;
        slug: string;
        content: Array<
          | {
              __typename: 'ComponentCommonEmbedVideo';
              id: string;
              type: EnumComponentcommonembedvideoType;
              url: string;
            }
          | {
              __typename: 'ComponentCommonGallery';
              id: string;
              text?: string | null;
              gallery?: {
                __typename: 'GalleryEntityResponse';
                data?: {
                  __typename: 'GalleryEntity';
                  id?: string | null;
                  attributes?: {
                    __typename: 'Gallery';
                    title: string;
                    images: {
                      __typename: 'UploadFileRelationResponseCollection';
                      data: Array<{
                        __typename: 'UploadFileEntity';
                        id?: string | null;
                        attributes?: {
                          __typename: 'UploadFile';
                          formats?: { [key: string]: any } | null;
                          name: string;
                          url: string;
                        } | null;
                      }>;
                    };
                  } | null;
                } | null;
              } | null;
            }
          | {
              __typename: 'ComponentCommonOfficialList';
              id: string;
              title: string;
              showContactInformation: boolean;
              officials: Array<{
                __typename: 'ComponentCommonOfficial';
                id: string;
                title: string;
                person?: {
                  __typename: 'PersonEntityResponse';
                  data?: {
                    __typename: 'PersonEntity';
                    id?: string | null;
                    attributes?: {
                      __typename: 'Person';
                      name: string;
                      email?: string | null;
                      phone?: string | null;
                      image?: {
                        __typename: 'UploadFileEntityResponse';
                        data?: {
                          __typename: 'UploadFileEntity';
                          id?: string | null;
                          attributes?: {
                            __typename: 'UploadFile';
                            formats?: { [key: string]: any } | null;
                            name: string;
                            url: string;
                          } | null;
                        } | null;
                      } | null;
                    } | null;
                  } | null;
                } | null;
              } | null>;
            }
          | { __typename: 'ComponentCommonRichText'; id: string; text?: string | null }
          | { __typename: 'Error' }
          | null
        >;
      } | null;
    }>;
  } | null;
};

export type ClubPagesQueryVariables = Exact<{ [key: string]: never }>;

export type ClubPagesQuery = {
  __typename: 'Query';
  clubPages?: {
    __typename: 'ClubPageEntityResponseCollection';
    data: Array<{
      __typename: 'ClubPageEntity';
      id?: string | null;
      attributes?: { __typename: 'ClubPage'; title: string; slug: string } | null;
    }>;
  } | null;
};

export type CmshbGameDetailTeamGoalkeepersFragment = {
  __typename: 'CmshbGameDetailTeamGoalkeepers';
  number?: number | null;
  timeInSeconds?: number | null;
  goalsAgainst?: number | null;
  shotsAgainst?: number | null;
  savesAverage?: number | null;
  penalties?: number | null;
  goals?: number | null;
  assists?: number | null;
  player:
    | { __typename: 'CmshbPlayer'; id: string; cmshbId?: string | null; name: string }
    | { __typename: 'Player'; id: string; cmshbId?: string | null; name: string; smallImage?: string | null };
};

export type CmshbGameDetailTeamGoalsFragment = {
  __typename: 'CmshbGameDetailTeamGoals';
  timeInSeconds: number;
  type?: string | null;
  goal: { __typename: 'CmshbPlayer'; id: string; cmshbId?: string | null; name: string };
  firstAssist?: { __typename: 'CmshbPlayer'; id: string; cmshbId?: string | null; name: string } | null;
  secondAssist?: { __typename: 'CmshbPlayer'; id: string; cmshbId?: string | null; name: string } | null;
};

export type CmshbGameDetailTeamPlayersFragment = {
  __typename: 'CmshbGameDetailTeamPlayers';
  post?: string | null;
  number?: number | null;
  points?: number | null;
  goals?: number | null;
  assists?: number | null;
  penalties?: number | null;
  player:
    | { __typename: 'CmshbPlayer'; id: string; cmshbId?: string | null; name: string }
    | { __typename: 'Player'; id: string; cmshbId?: string | null; name: string; smallImage?: string | null };
};

export type CmshbGameDetailTeamStatsFragment = {
  __typename: 'CmshbGameDetailTeamStats';
  shots?: number | null;
  penalties?: number | null;
  powerPlays?: number | null;
  powerPlayGoals?: number | null;
  penaltyKillGoals?: number | null;
};

export type CmshbLeagueGroupFragment = {
  __typename: 'CmshbLeagueGroup';
  id: string;
  skuId: number;
  label: string;
  name: string;
  type: string;
  order: number;
  metadata?: { [key: string]: any } | null;
  league: { __typename: 'CmshbLeague'; id: string; leagueId: number; name: string; order: number };
};

export type CmshbPlayerFragment = { __typename: 'CmshbPlayer'; id: string; cmshbId?: string | null; name: string };

export type CmshbPlayerStatsListQueryVariables = Exact<{
  teamId: Scalars['String'];
  year?: InputMaybe<Scalars['String']>;
}>;

export type CmshbPlayerStatsListQuery = {
  __typename: 'Query';
  cmshbPlayerStatsList: Array<{
    __typename: 'CmshbPlayerStatsList';
    leagueGroup: {
      __typename: 'CmshbLeagueGroup';
      id: string;
      skuId: number;
      label: string;
      name: string;
      type: string;
      order: number;
      metadata?: { [key: string]: any } | null;
      league: { __typename: 'CmshbLeague'; id: string; leagueId: number; name: string; order: number };
    };
    goalkeepers: Array<{
      __typename: 'CmshbGoalkeeperStats';
      games?: number | null;
      minutes?: number | null;
      savesAverage?: number | null;
      goalsAgainst?: number | null;
      goalsAgainstAverage?: number | null;
      shotsAgainst?: number | null;
      saves?: number | null;
      penalties?: number | null;
      assists?: number | null;
      shutouts?: number | null;
      player:
        | { __typename: 'CmshbPlayer'; id: string; cmshbId?: string | null; name: string }
        | {
            __typename: 'Player';
            id: string;
            cmshbId?: string | null;
            name: string;
            number?: number | null;
            post?: EnumComponentplayerteamplayerPost | null;
            smallImage?: string | null;
          };
    }>;
    players: Array<{
      __typename: 'CmshbPlayerStats';
      games?: number | null;
      points?: number | null;
      goals?: number | null;
      assists?: number | null;
      powerPlayGoals?: number | null;
      penaltyKillGoals?: number | null;
      penalties?: number | null;
      player:
        | { __typename: 'CmshbPlayer'; id: string; cmshbId?: string | null; name: string }
        | {
            __typename: 'Player';
            id: string;
            cmshbId?: string | null;
            name: string;
            number?: number | null;
            post?: EnumComponentplayerteamplayerPost | null;
            smallImage?: string | null;
          };
    }>;
  }>;
};

export type CmshbTeamFragment = {
  __typename: 'CmshbTeam';
  id: string;
  cmshbId?: string | null;
  name: string;
  shortName: string;
};

export type CmshbTeamTableQueryVariables = Exact<{
  teamId: Scalars['String'];
  year?: InputMaybe<Scalars['String']>;
}>;

export type CmshbTeamTableQuery = {
  __typename: 'Query';
  cmshbTeamTable: Array<{
    __typename: 'CmshbTeamTable';
    leagueGroup: {
      __typename: 'CmshbLeagueGroup';
      id: string;
      skuId: number;
      label: string;
      name: string;
      type: string;
      order: number;
      metadata?: { [key: string]: any } | null;
      league: { __typename: 'CmshbLeague'; id: string; leagueId: number; name: string; order: number };
    };
    stats: Array<{
      __typename: 'CmshbTeamTableStats';
      standing?: number | null;
      games?: number | null;
      wins?: number | null;
      overtimeWins?: number | null;
      overtimeLosses?: number | null;
      losses?: number | null;
      goals?: number | null;
      goalsAgainst?: number | null;
      points?: number | null;
      goalsAverage?: number | null;
      goalsAgainstAverage?: number | null;
      powerPlayGoals?: number | null;
      powerPlayGoalsAgainst?: number | null;
      powerPlays?: number | null;
      powerPlaysPercentage?: number | null;
      penaltyKillGoals?: number | null;
      penaltyKillGoalsAgainst?: number | null;
      penaltyKills?: number | null;
      penaltyKillsPercentage?: number | null;
      penalties?: number | null;
      penaltiesAverage?: number | null;
      team: { __typename: 'CmshbTeam'; id: string; cmshbId?: string | null; name: string; shortName: string };
    }>;
  }>;
};

export type CommonAchievementFragment = {
  __typename: 'ComponentCommonAchievement';
  id: string;
  title: string;
  years?: string | null;
  isWinner: boolean;
  image: {
    __typename: 'UploadFileEntityResponse';
    data?: {
      __typename: 'UploadFileEntity';
      id?: string | null;
      attributes?: {
        __typename: 'UploadFile';
        formats?: { [key: string]: any } | null;
        name: string;
        url: string;
      } | null;
    } | null;
  };
};

export type CommonOfficialFragment = {
  __typename: 'ComponentCommonOfficial';
  id: string;
  title: string;
  person?: {
    __typename: 'PersonEntityResponse';
    data?: {
      __typename: 'PersonEntity';
      id?: string | null;
      attributes?: {
        __typename: 'Person';
        name: string;
        email?: string | null;
        phone?: string | null;
        image?: {
          __typename: 'UploadFileEntityResponse';
          data?: {
            __typename: 'UploadFileEntity';
            id?: string | null;
            attributes?: {
              __typename: 'UploadFile';
              formats?: { [key: string]: any } | null;
              name: string;
              url: string;
            } | null;
          } | null;
        } | null;
      } | null;
    } | null;
  } | null;
};

export type CommonOfficialListFragment = {
  __typename: 'ComponentCommonOfficialList';
  id: string;
  title: string;
  showContactInformation: boolean;
  officials: Array<{
    __typename: 'ComponentCommonOfficial';
    id: string;
    title: string;
    person?: {
      __typename: 'PersonEntityResponse';
      data?: {
        __typename: 'PersonEntity';
        id?: string | null;
        attributes?: {
          __typename: 'Person';
          name: string;
          email?: string | null;
          phone?: string | null;
          image?: {
            __typename: 'UploadFileEntityResponse';
            data?: {
              __typename: 'UploadFileEntity';
              id?: string | null;
              attributes?: {
                __typename: 'UploadFile';
                formats?: { [key: string]: any } | null;
                name: string;
                url: string;
              } | null;
            } | null;
          } | null;
        } | null;
      } | null;
    } | null;
  } | null>;
};

export type ExternalPostsQueryVariables = Exact<{
  filters?: InputMaybe<ExternalPostFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
}>;

export type ExternalPostsQuery = {
  __typename: 'Query';
  externalPosts?: {
    __typename: 'ExternalPostEntityResponseCollection';
    data: Array<{
      __typename: 'ExternalPostEntity';
      id?: string | null;
      attributes?: {
        __typename: 'ExternalPost';
        source: EnumExternalpostSource;
        url: string;
        pinToTop?: boolean | null;
        publishAt: Date;
        categories?: {
          __typename: 'CategoryRelationResponseCollection';
          data: Array<{
            __typename: 'CategoryEntity';
            id?: string | null;
            attributes?: { __typename: 'Category'; name: string; code: string } | null;
          }>;
        } | null;
      } | null;
    }>;
  } | null;
};

export type GalleryFragment = {
  __typename: 'GalleryEntity';
  id?: string | null;
  attributes?: {
    __typename: 'Gallery';
    title: string;
    images: {
      __typename: 'UploadFileRelationResponseCollection';
      data: Array<{
        __typename: 'UploadFileEntity';
        id?: string | null;
        attributes?: {
          __typename: 'UploadFile';
          formats?: { [key: string]: any } | null;
          name: string;
          url: string;
        } | null;
      }>;
    };
  } | null;
};

export type GameQueryVariables = Exact<{
  id: Scalars['String'];
  teamId: Scalars['String'];
  year: Scalars['String'];
}>;

export type GameQuery = {
  __typename: 'Query';
  game?: {
    __typename: 'GameWithCmshbGameDetail';
    id: string;
    cmshbId?: string | null;
    kertId?: string | null;
    date: Date;
    place?: string | null;
    label?: string | null;
    status?: string | null;
    home: { __typename: 'CmshbTeam'; id: string; cmshbId?: string | null; name: string; shortName: string };
    away: { __typename: 'CmshbTeam'; id: string; cmshbId?: string | null; name: string; shortName: string };
    score: {
      __typename: 'GameScore';
      home: {
        __typename: 'GameTeamScore';
        finalScore?: number | null;
        firstPeriod?: number | null;
        secondPeriod?: number | null;
        thirdPeriod?: number | null;
        overtime?: number | null;
        shootout?: number | null;
      };
      away: {
        __typename: 'GameTeamScore';
        finalScore?: number | null;
        firstPeriod?: number | null;
        secondPeriod?: number | null;
        thirdPeriod?: number | null;
        overtime?: number | null;
        shootout?: number | null;
      };
    };
    statistics?: {
      __typename: 'CmshbGameDetailStats';
      home: {
        __typename: 'CmshbGameDetailTeamStats';
        shots?: number | null;
        penalties?: number | null;
        powerPlays?: number | null;
        powerPlayGoals?: number | null;
        penaltyKillGoals?: number | null;
      };
      away: {
        __typename: 'CmshbGameDetailTeamStats';
        shots?: number | null;
        penalties?: number | null;
        powerPlays?: number | null;
        powerPlayGoals?: number | null;
        penaltyKillGoals?: number | null;
      };
    } | null;
    goals?: {
      __typename: 'CmshbGameDetailGoals';
      home: Array<{
        __typename: 'CmshbGameDetailTeamGoals';
        timeInSeconds: number;
        type?: string | null;
        goal: { __typename: 'CmshbPlayer'; id: string; cmshbId?: string | null; name: string };
        firstAssist?: { __typename: 'CmshbPlayer'; id: string; cmshbId?: string | null; name: string } | null;
        secondAssist?: { __typename: 'CmshbPlayer'; id: string; cmshbId?: string | null; name: string } | null;
      }>;
      away: Array<{
        __typename: 'CmshbGameDetailTeamGoals';
        timeInSeconds: number;
        type?: string | null;
        goal: { __typename: 'CmshbPlayer'; id: string; cmshbId?: string | null; name: string };
        firstAssist?: { __typename: 'CmshbPlayer'; id: string; cmshbId?: string | null; name: string } | null;
        secondAssist?: { __typename: 'CmshbPlayer'; id: string; cmshbId?: string | null; name: string } | null;
      }>;
    } | null;
    players?: {
      __typename: 'CmshbGameDetailPlayers';
      home: Array<{
        __typename: 'CmshbGameDetailTeamPlayers';
        post?: string | null;
        number?: number | null;
        points?: number | null;
        goals?: number | null;
        assists?: number | null;
        penalties?: number | null;
        player:
          | { __typename: 'CmshbPlayer'; id: string; cmshbId?: string | null; name: string }
          | { __typename: 'Player'; id: string; cmshbId?: string | null; name: string; smallImage?: string | null };
      }>;
      away: Array<{
        __typename: 'CmshbGameDetailTeamPlayers';
        post?: string | null;
        number?: number | null;
        points?: number | null;
        goals?: number | null;
        assists?: number | null;
        penalties?: number | null;
        player:
          | { __typename: 'CmshbPlayer'; id: string; cmshbId?: string | null; name: string }
          | { __typename: 'Player'; id: string; cmshbId?: string | null; name: string; smallImage?: string | null };
      }>;
    } | null;
    goalkeepers?: {
      __typename: 'CmshbGameDetailGoalkeepers';
      home: Array<{
        __typename: 'CmshbGameDetailTeamGoalkeepers';
        number?: number | null;
        timeInSeconds?: number | null;
        goalsAgainst?: number | null;
        shotsAgainst?: number | null;
        savesAverage?: number | null;
        penalties?: number | null;
        goals?: number | null;
        assists?: number | null;
        player:
          | { __typename: 'CmshbPlayer'; id: string; cmshbId?: string | null; name: string }
          | { __typename: 'Player'; id: string; cmshbId?: string | null; name: string; smallImage?: string | null };
      }>;
      away: Array<{
        __typename: 'CmshbGameDetailTeamGoalkeepers';
        number?: number | null;
        timeInSeconds?: number | null;
        goalsAgainst?: number | null;
        shotsAgainst?: number | null;
        savesAverage?: number | null;
        penalties?: number | null;
        goals?: number | null;
        assists?: number | null;
        player:
          | { __typename: 'CmshbPlayer'; id: string; cmshbId?: string | null; name: string }
          | { __typename: 'Player'; id: string; cmshbId?: string | null; name: string; smallImage?: string | null };
      }>;
    } | null;
    team: { __typename: 'Team'; systemName: string; name: string };
    metadata?: {
      __typename: 'GameMetadata';
      reportPostSlug?: string | null;
      hasAiReport?: boolean | null;
      onlajnyCz?: string | null;
      liveVideo?: { __typename: 'GameMetadataVideo'; type: string; url: string } | null;
      highlightsVideo?: { __typename: 'GameMetadataVideo'; type: string; url: string } | null;
    } | null;
    post?: { __typename: 'CmshbGameDetailPost'; title: string; perex: string; content?: string | null } | null;
  } | null;
};

export type GameListQueryVariables = Exact<{
  teamId?: InputMaybe<Scalars['String']>;
  teamIds?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
  year?: InputMaybe<Scalars['String']>;
  filter?: InputMaybe<GameFilter>;
}>;

export type GameListQuery = {
  __typename: 'Query';
  gameList: Array<{
    __typename: 'Game';
    id: string;
    cmshbId?: string | null;
    kertId?: string | null;
    date: Date;
    place?: string | null;
    label?: string | null;
    status?: string | null;
    home: { __typename: 'CmshbTeam'; id: string; cmshbId?: string | null; name: string; shortName: string };
    away: { __typename: 'CmshbTeam'; id: string; cmshbId?: string | null; name: string; shortName: string };
    score: {
      __typename: 'GameScore';
      home: {
        __typename: 'GameTeamScore';
        finalScore?: number | null;
        firstPeriod?: number | null;
        secondPeriod?: number | null;
        thirdPeriod?: number | null;
        overtime?: number | null;
        shootout?: number | null;
      };
      away: {
        __typename: 'GameTeamScore';
        finalScore?: number | null;
        firstPeriod?: number | null;
        secondPeriod?: number | null;
        thirdPeriod?: number | null;
        overtime?: number | null;
        shootout?: number | null;
      };
    };
    metadata?: {
      __typename: 'GameMetadata';
      reportPostSlug?: string | null;
      hasAiReport?: boolean | null;
      onlajnyCz?: string | null;
      liveVideo?: { __typename: 'GameMetadataVideo'; type: string; url: string } | null;
      highlightsVideo?: { __typename: 'GameMetadataVideo'; type: string; url: string } | null;
    } | null;
    team: { __typename: 'Team'; systemName: string; name: string };
  }>;
};

export type GameListFragment = {
  __typename: 'Game';
  id: string;
  cmshbId?: string | null;
  kertId?: string | null;
  date: Date;
  place?: string | null;
  label?: string | null;
  status?: string | null;
  home: { __typename: 'CmshbTeam'; id: string; cmshbId?: string | null; name: string; shortName: string };
  away: { __typename: 'CmshbTeam'; id: string; cmshbId?: string | null; name: string; shortName: string };
  score: {
    __typename: 'GameScore';
    home: {
      __typename: 'GameTeamScore';
      finalScore?: number | null;
      firstPeriod?: number | null;
      secondPeriod?: number | null;
      thirdPeriod?: number | null;
      overtime?: number | null;
      shootout?: number | null;
    };
    away: {
      __typename: 'GameTeamScore';
      finalScore?: number | null;
      firstPeriod?: number | null;
      secondPeriod?: number | null;
      thirdPeriod?: number | null;
      overtime?: number | null;
      shootout?: number | null;
    };
  };
  metadata?: {
    __typename: 'GameMetadata';
    reportPostSlug?: string | null;
    hasAiReport?: boolean | null;
    onlajnyCz?: string | null;
    liveVideo?: { __typename: 'GameMetadataVideo'; type: string; url: string } | null;
    highlightsVideo?: { __typename: 'GameMetadataVideo'; type: string; url: string } | null;
  } | null;
  team: { __typename: 'Team'; systemName: string; name: string };
};

export type GameMetadataFragment = {
  __typename: 'GameMetadata';
  reportPostSlug?: string | null;
  hasAiReport?: boolean | null;
  onlajnyCz?: string | null;
  liveVideo?: { __typename: 'GameMetadataVideo'; type: string; url: string } | null;
  highlightsVideo?: { __typename: 'GameMetadataVideo'; type: string; url: string } | null;
};

export type GameTeamScoreFragment = {
  __typename: 'GameTeamScore';
  finalScore?: number | null;
  firstPeriod?: number | null;
  secondPeriod?: number | null;
  thirdPeriod?: number | null;
  overtime?: number | null;
  shootout?: number | null;
};

export type HokejbalCzArticleQueryVariables = Exact<{
  url: Scalars['String'];
}>;

export type HokejbalCzArticleQuery = {
  __typename: 'Query';
  hokejbalCzArticle?: {
    __typename: 'HokejbalCzArticle';
    id: string;
    title: string;
    date: Date;
    imageUrl?: string | null;
    perex: string;
  } | null;
};

export type ImageFragment = {
  __typename: 'UploadFileEntity';
  id?: string | null;
  attributes?: { __typename: 'UploadFile'; formats?: { [key: string]: any } | null; name: string; url: string } | null;
};

export type PartnersQueryVariables = Exact<{ [key: string]: never }>;

export type PartnersQuery = {
  __typename: 'Query';
  partner?: {
    __typename: 'PartnerEntityResponse';
    data?: {
      __typename: 'PartnerEntity';
      id?: string | null;
      attributes?: {
        __typename: 'Partner';
        partnerLists: Array<{
          __typename: 'ComponentCommonPartnerList';
          name: string;
          slug: string;
          partners: Array<{
            __typename: 'ComponentCommonPartner';
            id: string;
            name: string;
            url?: string | null;
            image: {
              __typename: 'UploadFileEntityResponse';
              data?: {
                __typename: 'UploadFileEntity';
                id?: string | null;
                attributes?: {
                  __typename: 'UploadFile';
                  formats?: { [key: string]: any } | null;
                  name: string;
                  url: string;
                } | null;
              } | null;
            };
          } | null>;
        } | null>;
      } | null;
    } | null;
  } | null;
};

export type PlayOffListQueryVariables = Exact<{
  teamId: Scalars['String'];
  year?: InputMaybe<Scalars['String']>;
}>;

export type PlayOffListQuery = {
  __typename: 'Query';
  playOffList: Array<{
    __typename: 'PlayOff';
    id: string;
    label?: string | null;
    rounds: Array<{
      __typename: 'PlayOffRound';
      id: string;
      name: string;
      shortName: string;
      shortcut: string;
      kertTeam: { __typename: 'CmshbTeam'; id: string; cmshbId?: string | null; name: string; shortName: string };
      opponentTeam: { __typename: 'CmshbTeam'; id: string; cmshbId?: string | null; name: string; shortName: string };
      score: { __typename: 'PlayOffRoundScore'; kertTeam: number; opponentTeam: number };
      games: Array<{
        __typename: 'Game';
        id: string;
        cmshbId?: string | null;
        kertId?: string | null;
        date: Date;
        place?: string | null;
        label?: string | null;
        status?: string | null;
        home: { __typename: 'CmshbTeam'; id: string; cmshbId?: string | null; name: string; shortName: string };
        away: { __typename: 'CmshbTeam'; id: string; cmshbId?: string | null; name: string; shortName: string };
        score: {
          __typename: 'GameScore';
          home: {
            __typename: 'GameTeamScore';
            finalScore?: number | null;
            firstPeriod?: number | null;
            secondPeriod?: number | null;
            thirdPeriod?: number | null;
            overtime?: number | null;
            shootout?: number | null;
          };
          away: {
            __typename: 'GameTeamScore';
            finalScore?: number | null;
            firstPeriod?: number | null;
            secondPeriod?: number | null;
            thirdPeriod?: number | null;
            overtime?: number | null;
            shootout?: number | null;
          };
        };
        metadata?: {
          __typename: 'GameMetadata';
          reportPostSlug?: string | null;
          hasAiReport?: boolean | null;
          onlajnyCz?: string | null;
          liveVideo?: { __typename: 'GameMetadataVideo'; type: string; url: string } | null;
          highlightsVideo?: { __typename: 'GameMetadataVideo'; type: string; url: string } | null;
        } | null;
        team: { __typename: 'Team'; systemName: string; name: string };
      }>;
    }>;
  }>;
};

export type PlayerQueryVariables = Exact<{
  teamId: Scalars['ID'];
  id: Scalars['ID'];
}>;

export type PlayerQuery = {
  __typename: 'Query';
  player?: {
    __typename: 'Player';
    id: string;
    cmshbId?: string | null;
    name: string;
    number?: number | null;
    post?: EnumComponentplayerteamplayerPost | null;
    birth?: Date | null;
    height?: number | null;
    weight?: number | null;
    stick?: EnumComponentplayerteamplayerStick | null;
    smallImage?: string | null;
    largeImage?: string | null;
  } | null;
};

export type PlayerFragment = {
  __typename: 'Player';
  id: string;
  cmshbId?: string | null;
  name: string;
  number?: number | null;
  post?: EnumComponentplayerteamplayerPost | null;
  birth?: Date | null;
  height?: number | null;
  weight?: number | null;
  stick?: EnumComponentplayerteamplayerStick | null;
  smallImage?: string | null;
  largeImage?: string | null;
};

export type PlayersQueryVariables = Exact<{
  teamId: Scalars['ID'];
}>;

export type PlayersQuery = {
  __typename: 'Query';
  players: Array<{
    __typename: 'Player';
    id: string;
    cmshbId?: string | null;
    name: string;
    number?: number | null;
    post?: EnumComponentplayerteamplayerPost | null;
    birth?: Date | null;
    height?: number | null;
    weight?: number | null;
    stick?: EnumComponentplayerteamplayerStick | null;
    smallImage?: string | null;
    largeImage?: string | null;
  }>;
};

export type PostQueryVariables = Exact<{
  slug: Scalars['String'];
  publicationState?: InputMaybe<PublicationState>;
}>;

export type PostQuery = {
  __typename: 'Query';
  posts?: {
    __typename: 'PostEntityResponseCollection';
    data: Array<{
      __typename: 'PostEntity';
      id?: string | null;
      attributes?: {
        __typename: 'Post';
        title: string;
        slug: string;
        perex: string;
        publishAt: Date;
        author?: string | null;
        thumbAuthor?: string | null;
        createdBy?: { __typename: 'Creator'; firstname?: string | null; lastname?: string | null } | null;
        categories?: {
          __typename: 'CategoryRelationResponseCollection';
          data: Array<{
            __typename: 'CategoryEntity';
            attributes?: { __typename: 'Category'; name: string; code: string } | null;
          }>;
        } | null;
        thumb?: {
          __typename: 'UploadFileEntityResponse';
          data?: {
            __typename: 'UploadFileEntity';
            id?: string | null;
            attributes?: {
              __typename: 'UploadFile';
              formats?: { [key: string]: any } | null;
              name: string;
              url: string;
            } | null;
          } | null;
        } | null;
        content: Array<
          | { __typename: 'ComponentCommonApiGame'; id: string; game?: string | null }
          | { __typename: 'ComponentCommonApiGameList'; id: string; gameList?: string | null }
          | {
              __typename: 'ComponentCommonEmbedVideo';
              id: string;
              type: EnumComponentcommonembedvideoType;
              url: string;
            }
          | {
              __typename: 'ComponentCommonGallery';
              id: string;
              text?: string | null;
              gallery?: {
                __typename: 'GalleryEntityResponse';
                data?: {
                  __typename: 'GalleryEntity';
                  id?: string | null;
                  attributes?: {
                    __typename: 'Gallery';
                    title: string;
                    images: {
                      __typename: 'UploadFileRelationResponseCollection';
                      data: Array<{
                        __typename: 'UploadFileEntity';
                        id?: string | null;
                        attributes?: {
                          __typename: 'UploadFile';
                          formats?: { [key: string]: any } | null;
                          name: string;
                          url: string;
                        } | null;
                      }>;
                    };
                  } | null;
                } | null;
              } | null;
            }
          | { __typename: 'ComponentCommonRichText'; id: string; text?: string | null }
          | { __typename: 'Error' }
          | null
        >;
      } | null;
    }>;
  } | null;
};

export type PostsCompositeQueryVariables = Exact<{
  categoryCode?: InputMaybe<Scalars['String']>;
  dateRange?: InputMaybe<DateRangeInput>;
  pagination?: InputMaybe<PaginationArg>;
}>;

export type PostsCompositeQuery = {
  __typename: 'Query';
  postsComposite?: {
    __typename: 'PostCompositeResponseCollection';
    data: Array<
      | {
          __typename: 'PostEntity';
          id?: string | null;
          attributes?: {
            __typename: 'Post';
            title: string;
            slug: string;
            perex: string;
            pinToTop?: boolean | null;
            publishAt: Date;
            author?: string | null;
            thumbAuthor?: string | null;
            categories?: {
              __typename: 'CategoryRelationResponseCollection';
              data: Array<{
                __typename: 'CategoryEntity';
                id?: string | null;
                attributes?: { __typename: 'Category'; name: string; code: string } | null;
              }>;
            } | null;
            thumb?: {
              __typename: 'UploadFileEntityResponse';
              data?: {
                __typename: 'UploadFileEntity';
                id?: string | null;
                attributes?: {
                  __typename: 'UploadFile';
                  formats?: { [key: string]: any } | null;
                  name: string;
                  url: string;
                } | null;
              } | null;
            } | null;
          } | null;
        }
      | {
          __typename: 'PostExternalComposite';
          id?: string | null;
          title: string;
          url: string;
          perex: string;
          publishAt: Date;
          imageUrl?: string | null;
          categories: Array<{
            __typename: 'CategoryEntity';
            id?: string | null;
            attributes?: { __typename: 'Category'; name: string; code: string } | null;
          }>;
        }
    >;
    meta: {
      __typename: 'PostCompositeResponseMeta';
      pagination: { __typename: 'Pagination'; page: number; pageCount: number; pageSize: number; total: number };
    };
  } | null;
};

export type StatusQueryVariables = Exact<{ [key: string]: never }>;

export type StatusQuery = {
  __typename: 'Query';
  status?: {
    __typename: 'StatusEntityResponse';
    data?: {
      __typename: 'StatusEntity';
      attributes?: {
        __typename: 'Status';
        showIncompleteGamesData?: boolean | null;
        showMaintenance?: boolean | null;
      } | null;
    } | null;
  } | null;
};

export type TournamentListQueryVariables = Exact<{
  teamId?: InputMaybe<Scalars['String']>;
  teamIds?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
  from?: InputMaybe<Scalars['Date']>;
  to?: InputMaybe<Scalars['Date']>;
  year?: InputMaybe<Scalars['String']>;
}>;

export type TournamentListQuery = {
  __typename: 'Query';
  tournamentList: Array<{
    __typename: 'Tournament';
    id: string;
    date: Date;
    place?: { __typename: 'Place'; id: string; systemName: string; name: string } | null;
    team: { __typename: 'Team'; systemName: string; name: string };
  }>;
};

export const CmshbPlayerFragmentDoc = gql`
  fragment CmshbPlayer on CmshbPlayer {
    id
    cmshbId
    name
  }
`;
export const CmshbGameDetailTeamGoalkeepersFragmentDoc = gql`
  fragment CmshbGameDetailTeamGoalkeepers on CmshbGameDetailTeamGoalkeepers {
    player {
      ...CmshbPlayer
      ... on Player {
        id
        cmshbId
        name
        smallImage
      }
    }
    number
    timeInSeconds
    goalsAgainst
    shotsAgainst
    savesAverage
    penalties
    goals
    assists
  }
  ${CmshbPlayerFragmentDoc}
`;
export const CmshbGameDetailTeamGoalsFragmentDoc = gql`
  fragment CmshbGameDetailTeamGoals on CmshbGameDetailTeamGoals {
    timeInSeconds
    type
    goal {
      ...CmshbPlayer
    }
    firstAssist {
      ...CmshbPlayer
    }
    secondAssist {
      ...CmshbPlayer
    }
  }
  ${CmshbPlayerFragmentDoc}
`;
export const CmshbGameDetailTeamPlayersFragmentDoc = gql`
  fragment CmshbGameDetailTeamPlayers on CmshbGameDetailTeamPlayers {
    player {
      ...CmshbPlayer
      ... on Player {
        id
        cmshbId
        name
        smallImage
      }
    }
    post
    number
    points
    goals
    assists
    penalties
  }
  ${CmshbPlayerFragmentDoc}
`;
export const CmshbGameDetailTeamStatsFragmentDoc = gql`
  fragment CmshbGameDetailTeamStats on CmshbGameDetailTeamStats {
    shots
    penalties
    powerPlays
    powerPlayGoals
    penaltyKillGoals
  }
`;
export const CmshbLeagueGroupFragmentDoc = gql`
  fragment CmshbLeagueGroup on CmshbLeagueGroup {
    id
    skuId
    label
    name
    type
    order
    metadata
    league {
      id
      leagueId
      name
      order
    }
  }
`;
export const ImageFragmentDoc = gql`
  fragment Image on UploadFileEntity {
    id
    attributes {
      formats
      name
      url
    }
  }
`;
export const CommonAchievementFragmentDoc = gql`
  fragment CommonAchievement on ComponentCommonAchievement {
    id
    title
    years
    isWinner
    image {
      data {
        ...Image
      }
    }
  }
  ${ImageFragmentDoc}
`;
export const CommonOfficialFragmentDoc = gql`
  fragment CommonOfficial on ComponentCommonOfficial {
    id
    title
    person {
      data {
        id
        attributes {
          name
          email
          phone
          image {
            data {
              ...Image
            }
          }
        }
      }
    }
  }
  ${ImageFragmentDoc}
`;
export const CommonOfficialListFragmentDoc = gql`
  fragment CommonOfficialList on ComponentCommonOfficialList {
    id
    title
    showContactInformation
    officials {
      id
      title
      person {
        data {
          id
          attributes {
            name
            email
            phone
            image {
              data {
                ...Image
              }
            }
          }
        }
      }
    }
  }
  ${ImageFragmentDoc}
`;
export const GalleryFragmentDoc = gql`
  fragment Gallery on GalleryEntity {
    id
    attributes {
      title
      images(pagination: { limit: 1000 }, sort: "name:asc") {
        data {
          ...Image
        }
      }
    }
  }
  ${ImageFragmentDoc}
`;
export const CmshbTeamFragmentDoc = gql`
  fragment CmshbTeam on CmshbTeam {
    id
    cmshbId
    name
    shortName
  }
`;
export const GameTeamScoreFragmentDoc = gql`
  fragment GameTeamScore on GameTeamScore {
    finalScore
    firstPeriod
    secondPeriod
    thirdPeriod
    overtime
    shootout
  }
`;
export const GameMetadataFragmentDoc = gql`
  fragment GameMetadata on GameMetadata {
    liveVideo {
      type
      url
    }
    highlightsVideo {
      type
      url
    }
    reportPostSlug
    hasAiReport
    onlajnyCz
  }
`;
export const GameListFragmentDoc = gql`
  fragment GameList on Game {
    id
    cmshbId
    kertId
    date
    place
    label
    home {
      ...CmshbTeam
    }
    away {
      ...CmshbTeam
    }
    status
    score {
      home {
        ...GameTeamScore
      }
      away {
        ...GameTeamScore
      }
    }
    metadata {
      ...GameMetadata
    }
    team {
      systemName
      name
    }
  }
  ${CmshbTeamFragmentDoc}
  ${GameTeamScoreFragmentDoc}
  ${GameMetadataFragmentDoc}
`;
export const PlayerFragmentDoc = gql`
  fragment Player on Player {
    id
    cmshbId
    name
    number
    post
    birth
    height
    weight
    stick
    smallImage
    largeImage
  }
`;
export const CategoriesDocument = gql`
  query categories {
    categories(sort: "name:asc") {
      data {
        attributes {
          name
          code
          image {
            data {
              ...Image
            }
          }
          officials {
            ...CommonOfficial
          }
          achievements {
            ...CommonAchievement
          }
        }
      }
    }
  }
  ${ImageFragmentDoc}
  ${CommonOfficialFragmentDoc}
  ${CommonAchievementFragmentDoc}
`;

/**
 * __useCategoriesQuery__
 *
 * To run a query within a React component, call `useCategoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useCategoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCategoriesQuery({
 *   variables: {
 *   },
 * });
 */
export function useCategoriesQuery(baseOptions?: Apollo.QueryHookOptions<CategoriesQuery, CategoriesQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<CategoriesQuery, CategoriesQueryVariables>(CategoriesDocument, options);
}
export function useCategoriesLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<CategoriesQuery, CategoriesQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<CategoriesQuery, CategoriesQueryVariables>(CategoriesDocument, options);
}
export type CategoriesQueryHookResult = ReturnType<typeof useCategoriesQuery>;
export type CategoriesLazyQueryHookResult = ReturnType<typeof useCategoriesLazyQuery>;
export type CategoriesQueryResult = Apollo.QueryResult<CategoriesQuery, CategoriesQueryVariables>;
export const ClubPageDocument = gql`
  query clubPage($slug: String!, $publicationState: PublicationState = LIVE) {
    clubPages(filters: { slug: { eq: $slug } }, publicationState: $publicationState) {
      data {
        id
        attributes {
          title
          slug
          content {
            __typename
            ... on ComponentCommonRichText {
              __typename
              id
              text
            }
            ... on ComponentCommonOfficialList {
              ...CommonOfficialList
            }
            ... on ComponentCommonGallery {
              __typename
              id
              text
              gallery {
                data {
                  ...Gallery
                }
              }
            }
            ... on ComponentCommonEmbedVideo {
              id
              type
              url
            }
          }
        }
      }
    }
  }
  ${CommonOfficialListFragmentDoc}
  ${GalleryFragmentDoc}
`;

/**
 * __useClubPageQuery__
 *
 * To run a query within a React component, call `useClubPageQuery` and pass it any options that fit your needs.
 * When your component renders, `useClubPageQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useClubPageQuery({
 *   variables: {
 *      slug: // value for 'slug'
 *      publicationState: // value for 'publicationState'
 *   },
 * });
 */
export function useClubPageQuery(baseOptions: Apollo.QueryHookOptions<ClubPageQuery, ClubPageQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<ClubPageQuery, ClubPageQueryVariables>(ClubPageDocument, options);
}
export function useClubPageLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ClubPageQuery, ClubPageQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<ClubPageQuery, ClubPageQueryVariables>(ClubPageDocument, options);
}
export type ClubPageQueryHookResult = ReturnType<typeof useClubPageQuery>;
export type ClubPageLazyQueryHookResult = ReturnType<typeof useClubPageLazyQuery>;
export type ClubPageQueryResult = Apollo.QueryResult<ClubPageQuery, ClubPageQueryVariables>;
export const ClubPagesDocument = gql`
  query clubPages {
    clubPages(publicationState: LIVE, sort: "title:asc") {
      data {
        id
        attributes {
          title
          slug
        }
      }
    }
  }
`;

/**
 * __useClubPagesQuery__
 *
 * To run a query within a React component, call `useClubPagesQuery` and pass it any options that fit your needs.
 * When your component renders, `useClubPagesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useClubPagesQuery({
 *   variables: {
 *   },
 * });
 */
export function useClubPagesQuery(baseOptions?: Apollo.QueryHookOptions<ClubPagesQuery, ClubPagesQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<ClubPagesQuery, ClubPagesQueryVariables>(ClubPagesDocument, options);
}
export function useClubPagesLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<ClubPagesQuery, ClubPagesQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<ClubPagesQuery, ClubPagesQueryVariables>(ClubPagesDocument, options);
}
export type ClubPagesQueryHookResult = ReturnType<typeof useClubPagesQuery>;
export type ClubPagesLazyQueryHookResult = ReturnType<typeof useClubPagesLazyQuery>;
export type ClubPagesQueryResult = Apollo.QueryResult<ClubPagesQuery, ClubPagesQueryVariables>;
export const CmshbPlayerStatsListDocument = gql`
    query cmshbPlayerStatsList($teamId: String!, $year: String = "${process.env.SEASON_YEAR}") {
  cmshbPlayerStatsList(teamId: $teamId, year: $year) {
    leagueGroup {
      ...CmshbLeagueGroup
    }
    goalkeepers {
      player {
        __typename
        ... on CmshbPlayer {
          id
          cmshbId
          name
        }
        ... on Player {
          id
          cmshbId
          name
          number
          post
          smallImage
        }
      }
      games
      minutes
      savesAverage
      goalsAgainst
      goalsAgainstAverage
      shotsAgainst
      saves
      penalties
      assists
      shutouts
    }
    players {
      player {
        __typename
        ... on CmshbPlayer {
          id
          cmshbId
          name
        }
        ... on Player {
          id
          cmshbId
          name
          number
          post
          smallImage
        }
      }
      games
      points
      goals
      assists
      powerPlayGoals
      penaltyKillGoals
      penalties
    }
  }
}
    ${CmshbLeagueGroupFragmentDoc}`;

/**
 * __useCmshbPlayerStatsListQuery__
 *
 * To run a query within a React component, call `useCmshbPlayerStatsListQuery` and pass it any options that fit your needs.
 * When your component renders, `useCmshbPlayerStatsListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCmshbPlayerStatsListQuery({
 *   variables: {
 *      teamId: // value for 'teamId'
 *      year: // value for 'year'
 *   },
 * });
 */
export function useCmshbPlayerStatsListQuery(
  baseOptions: Apollo.QueryHookOptions<CmshbPlayerStatsListQuery, CmshbPlayerStatsListQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<CmshbPlayerStatsListQuery, CmshbPlayerStatsListQueryVariables>(
    CmshbPlayerStatsListDocument,
    options,
  );
}
export function useCmshbPlayerStatsListLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<CmshbPlayerStatsListQuery, CmshbPlayerStatsListQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<CmshbPlayerStatsListQuery, CmshbPlayerStatsListQueryVariables>(
    CmshbPlayerStatsListDocument,
    options,
  );
}
export type CmshbPlayerStatsListQueryHookResult = ReturnType<typeof useCmshbPlayerStatsListQuery>;
export type CmshbPlayerStatsListLazyQueryHookResult = ReturnType<typeof useCmshbPlayerStatsListLazyQuery>;
export type CmshbPlayerStatsListQueryResult = Apollo.QueryResult<
  CmshbPlayerStatsListQuery,
  CmshbPlayerStatsListQueryVariables
>;
export const CmshbTeamTableDocument = gql`
    query cmshbTeamTable($teamId: String!, $year: String = "${process.env.SEASON_YEAR}") {
  cmshbTeamTable(teamId: $teamId, year: $year) {
    leagueGroup {
      ...CmshbLeagueGroup
    }
    stats {
      standing
      team {
        ...CmshbTeam
      }
      games
      wins
      overtimeWins
      overtimeLosses
      losses
      goals
      goalsAgainst
      points
      goalsAverage
      goalsAgainstAverage
      powerPlayGoals
      powerPlayGoalsAgainst
      powerPlays
      powerPlaysPercentage
      penaltyKillGoals
      penaltyKillGoalsAgainst
      penaltyKills
      penaltyKillsPercentage
      penalties
      penaltiesAverage
    }
  }
}
    ${CmshbLeagueGroupFragmentDoc}
${CmshbTeamFragmentDoc}`;

/**
 * __useCmshbTeamTableQuery__
 *
 * To run a query within a React component, call `useCmshbTeamTableQuery` and pass it any options that fit your needs.
 * When your component renders, `useCmshbTeamTableQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCmshbTeamTableQuery({
 *   variables: {
 *      teamId: // value for 'teamId'
 *      year: // value for 'year'
 *   },
 * });
 */
export function useCmshbTeamTableQuery(
  baseOptions: Apollo.QueryHookOptions<CmshbTeamTableQuery, CmshbTeamTableQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<CmshbTeamTableQuery, CmshbTeamTableQueryVariables>(CmshbTeamTableDocument, options);
}
export function useCmshbTeamTableLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<CmshbTeamTableQuery, CmshbTeamTableQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<CmshbTeamTableQuery, CmshbTeamTableQueryVariables>(CmshbTeamTableDocument, options);
}
export type CmshbTeamTableQueryHookResult = ReturnType<typeof useCmshbTeamTableQuery>;
export type CmshbTeamTableLazyQueryHookResult = ReturnType<typeof useCmshbTeamTableLazyQuery>;
export type CmshbTeamTableQueryResult = Apollo.QueryResult<CmshbTeamTableQuery, CmshbTeamTableQueryVariables>;
export const ExternalPostsDocument = gql`
  query externalPosts(
    $filters: ExternalPostFiltersInput
    $pagination: PaginationArg
    $sort: [String] = ["pinToTop:desc", "publishAt:desc"]
  ) {
    externalPosts(filters: $filters, pagination: $pagination, sort: $sort) {
      data {
        id
        attributes {
          source
          url
          pinToTop
          publishAt
          categories {
            data {
              id
              attributes {
                name
                code
              }
            }
          }
        }
      }
    }
  }
`;

/**
 * __useExternalPostsQuery__
 *
 * To run a query within a React component, call `useExternalPostsQuery` and pass it any options that fit your needs.
 * When your component renders, `useExternalPostsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useExternalPostsQuery({
 *   variables: {
 *      filters: // value for 'filters'
 *      pagination: // value for 'pagination'
 *      sort: // value for 'sort'
 *   },
 * });
 */
export function useExternalPostsQuery(
  baseOptions?: Apollo.QueryHookOptions<ExternalPostsQuery, ExternalPostsQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<ExternalPostsQuery, ExternalPostsQueryVariables>(ExternalPostsDocument, options);
}
export function useExternalPostsLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<ExternalPostsQuery, ExternalPostsQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<ExternalPostsQuery, ExternalPostsQueryVariables>(ExternalPostsDocument, options);
}
export type ExternalPostsQueryHookResult = ReturnType<typeof useExternalPostsQuery>;
export type ExternalPostsLazyQueryHookResult = ReturnType<typeof useExternalPostsLazyQuery>;
export type ExternalPostsQueryResult = Apollo.QueryResult<ExternalPostsQuery, ExternalPostsQueryVariables>;
export const GameDocument = gql`
  query game($id: String!, $teamId: String!, $year: String!) {
    game(id: $id, teamId: $teamId, year: $year) {
      id
      cmshbId
      kertId
      date
      place
      label
      home {
        ...CmshbTeam
      }
      away {
        ...CmshbTeam
      }
      status
      score {
        home {
          ...GameTeamScore
        }
        away {
          ...GameTeamScore
        }
      }
      statistics {
        home {
          ...CmshbGameDetailTeamStats
        }
        away {
          ...CmshbGameDetailTeamStats
        }
      }
      goals {
        home {
          ...CmshbGameDetailTeamGoals
        }
        away {
          ...CmshbGameDetailTeamGoals
        }
      }
      players {
        home {
          ...CmshbGameDetailTeamPlayers
        }
        away {
          ...CmshbGameDetailTeamPlayers
        }
      }
      goalkeepers {
        home {
          ...CmshbGameDetailTeamGoalkeepers
        }
        away {
          ...CmshbGameDetailTeamGoalkeepers
        }
      }
      team {
        systemName
        name
      }
      metadata {
        ...GameMetadata
      }
      post {
        title
        perex
        content
      }
    }
  }
  ${CmshbTeamFragmentDoc}
  ${GameTeamScoreFragmentDoc}
  ${CmshbGameDetailTeamStatsFragmentDoc}
  ${CmshbGameDetailTeamGoalsFragmentDoc}
  ${CmshbGameDetailTeamPlayersFragmentDoc}
  ${CmshbGameDetailTeamGoalkeepersFragmentDoc}
  ${GameMetadataFragmentDoc}
`;

/**
 * __useGameQuery__
 *
 * To run a query within a React component, call `useGameQuery` and pass it any options that fit your needs.
 * When your component renders, `useGameQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGameQuery({
 *   variables: {
 *      id: // value for 'id'
 *      teamId: // value for 'teamId'
 *      year: // value for 'year'
 *   },
 * });
 */
export function useGameQuery(baseOptions: Apollo.QueryHookOptions<GameQuery, GameQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GameQuery, GameQueryVariables>(GameDocument, options);
}
export function useGameLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GameQuery, GameQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GameQuery, GameQueryVariables>(GameDocument, options);
}
export type GameQueryHookResult = ReturnType<typeof useGameQuery>;
export type GameLazyQueryHookResult = ReturnType<typeof useGameLazyQuery>;
export type GameQueryResult = Apollo.QueryResult<GameQuery, GameQueryVariables>;
export const GameListDocument = gql`
    query gameList($teamId: String, $teamIds: [String], $year: String = "${process.env.SEASON_YEAR}", $filter: GameFilter) {
  gameList(teamId: $teamId, teamIds: $teamIds, year: $year, filter: $filter) {
    ...GameList
  }
}
    ${GameListFragmentDoc}`;

/**
 * __useGameListQuery__
 *
 * To run a query within a React component, call `useGameListQuery` and pass it any options that fit your needs.
 * When your component renders, `useGameListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGameListQuery({
 *   variables: {
 *      teamId: // value for 'teamId'
 *      teamIds: // value for 'teamIds'
 *      year: // value for 'year'
 *      filter: // value for 'filter'
 *   },
 * });
 */
export function useGameListQuery(baseOptions?: Apollo.QueryHookOptions<GameListQuery, GameListQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GameListQuery, GameListQueryVariables>(GameListDocument, options);
}
export function useGameListLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GameListQuery, GameListQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GameListQuery, GameListQueryVariables>(GameListDocument, options);
}
export type GameListQueryHookResult = ReturnType<typeof useGameListQuery>;
export type GameListLazyQueryHookResult = ReturnType<typeof useGameListLazyQuery>;
export type GameListQueryResult = Apollo.QueryResult<GameListQuery, GameListQueryVariables>;
export const HokejbalCzArticleDocument = gql`
  query hokejbalCzArticle($url: String!) {
    hokejbalCzArticle(url: $url) {
      id
      title
      date
      imageUrl
      perex
    }
  }
`;

/**
 * __useHokejbalCzArticleQuery__
 *
 * To run a query within a React component, call `useHokejbalCzArticleQuery` and pass it any options that fit your needs.
 * When your component renders, `useHokejbalCzArticleQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useHokejbalCzArticleQuery({
 *   variables: {
 *      url: // value for 'url'
 *   },
 * });
 */
export function useHokejbalCzArticleQuery(
  baseOptions: Apollo.QueryHookOptions<HokejbalCzArticleQuery, HokejbalCzArticleQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<HokejbalCzArticleQuery, HokejbalCzArticleQueryVariables>(HokejbalCzArticleDocument, options);
}
export function useHokejbalCzArticleLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<HokejbalCzArticleQuery, HokejbalCzArticleQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<HokejbalCzArticleQuery, HokejbalCzArticleQueryVariables>(
    HokejbalCzArticleDocument,
    options,
  );
}
export type HokejbalCzArticleQueryHookResult = ReturnType<typeof useHokejbalCzArticleQuery>;
export type HokejbalCzArticleLazyQueryHookResult = ReturnType<typeof useHokejbalCzArticleLazyQuery>;
export type HokejbalCzArticleQueryResult = Apollo.QueryResult<HokejbalCzArticleQuery, HokejbalCzArticleQueryVariables>;
export const PartnersDocument = gql`
  query partners {
    partner {
      data {
        id
        attributes {
          partnerLists(pagination: { limit: 1000 }) {
            name
            slug
            partners(pagination: { limit: 1000 }) {
              id
              name
              url
              image {
                data {
                  ...Image
                }
              }
            }
          }
        }
      }
    }
  }
  ${ImageFragmentDoc}
`;

/**
 * __usePartnersQuery__
 *
 * To run a query within a React component, call `usePartnersQuery` and pass it any options that fit your needs.
 * When your component renders, `usePartnersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePartnersQuery({
 *   variables: {
 *   },
 * });
 */
export function usePartnersQuery(baseOptions?: Apollo.QueryHookOptions<PartnersQuery, PartnersQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<PartnersQuery, PartnersQueryVariables>(PartnersDocument, options);
}
export function usePartnersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<PartnersQuery, PartnersQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<PartnersQuery, PartnersQueryVariables>(PartnersDocument, options);
}
export type PartnersQueryHookResult = ReturnType<typeof usePartnersQuery>;
export type PartnersLazyQueryHookResult = ReturnType<typeof usePartnersLazyQuery>;
export type PartnersQueryResult = Apollo.QueryResult<PartnersQuery, PartnersQueryVariables>;
export const PlayOffListDocument = gql`
    query playOffList($teamId: String!, $year: String = "${process.env.SEASON_YEAR}") {
  playOffList(teamId: $teamId, year: $year) {
    id
    label
    rounds {
      id
      name
      shortName
      shortcut
      kertTeam {
        ...CmshbTeam
      }
      opponentTeam {
        ...CmshbTeam
      }
      score {
        kertTeam
        opponentTeam
      }
      games {
        ...GameList
      }
    }
  }
}
    ${CmshbTeamFragmentDoc}
${GameListFragmentDoc}`;

/**
 * __usePlayOffListQuery__
 *
 * To run a query within a React component, call `usePlayOffListQuery` and pass it any options that fit your needs.
 * When your component renders, `usePlayOffListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePlayOffListQuery({
 *   variables: {
 *      teamId: // value for 'teamId'
 *      year: // value for 'year'
 *   },
 * });
 */
export function usePlayOffListQuery(baseOptions: Apollo.QueryHookOptions<PlayOffListQuery, PlayOffListQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<PlayOffListQuery, PlayOffListQueryVariables>(PlayOffListDocument, options);
}
export function usePlayOffListLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<PlayOffListQuery, PlayOffListQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<PlayOffListQuery, PlayOffListQueryVariables>(PlayOffListDocument, options);
}
export type PlayOffListQueryHookResult = ReturnType<typeof usePlayOffListQuery>;
export type PlayOffListLazyQueryHookResult = ReturnType<typeof usePlayOffListLazyQuery>;
export type PlayOffListQueryResult = Apollo.QueryResult<PlayOffListQuery, PlayOffListQueryVariables>;
export const PlayerDocument = gql`
  query player($teamId: ID!, $id: ID!) {
    player(teamId: $teamId, id: $id) {
      ...Player
    }
  }
  ${PlayerFragmentDoc}
`;

/**
 * __usePlayerQuery__
 *
 * To run a query within a React component, call `usePlayerQuery` and pass it any options that fit your needs.
 * When your component renders, `usePlayerQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePlayerQuery({
 *   variables: {
 *      teamId: // value for 'teamId'
 *      id: // value for 'id'
 *   },
 * });
 */
export function usePlayerQuery(baseOptions: Apollo.QueryHookOptions<PlayerQuery, PlayerQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<PlayerQuery, PlayerQueryVariables>(PlayerDocument, options);
}
export function usePlayerLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<PlayerQuery, PlayerQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<PlayerQuery, PlayerQueryVariables>(PlayerDocument, options);
}
export type PlayerQueryHookResult = ReturnType<typeof usePlayerQuery>;
export type PlayerLazyQueryHookResult = ReturnType<typeof usePlayerLazyQuery>;
export type PlayerQueryResult = Apollo.QueryResult<PlayerQuery, PlayerQueryVariables>;
export const PlayersDocument = gql`
  query players($teamId: ID!) {
    players(teamId: $teamId) {
      ...Player
    }
  }
  ${PlayerFragmentDoc}
`;

/**
 * __usePlayersQuery__
 *
 * To run a query within a React component, call `usePlayersQuery` and pass it any options that fit your needs.
 * When your component renders, `usePlayersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePlayersQuery({
 *   variables: {
 *      teamId: // value for 'teamId'
 *   },
 * });
 */
export function usePlayersQuery(baseOptions: Apollo.QueryHookOptions<PlayersQuery, PlayersQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<PlayersQuery, PlayersQueryVariables>(PlayersDocument, options);
}
export function usePlayersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<PlayersQuery, PlayersQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<PlayersQuery, PlayersQueryVariables>(PlayersDocument, options);
}
export type PlayersQueryHookResult = ReturnType<typeof usePlayersQuery>;
export type PlayersLazyQueryHookResult = ReturnType<typeof usePlayersLazyQuery>;
export type PlayersQueryResult = Apollo.QueryResult<PlayersQuery, PlayersQueryVariables>;
export const PostDocument = gql`
  query post($slug: String!, $publicationState: PublicationState = LIVE) {
    posts(filters: { slug: { eq: $slug } }, publicationState: $publicationState) {
      data {
        id
        attributes {
          title
          slug
          perex
          publishAt
          author
          createdBy {
            firstname
            lastname
          }
          categories {
            data {
              attributes {
                name
                code
              }
            }
          }
          thumbAuthor
          thumb {
            data {
              ...Image
            }
          }
          content {
            __typename
            ... on ComponentCommonRichText {
              id
              text
            }
            ... on ComponentCommonApiGame {
              id
              game
            }
            ... on ComponentCommonApiGameList {
              id
              gameList
            }
            ... on ComponentCommonGallery {
              id
              text
              gallery {
                data {
                  ...Gallery
                }
              }
            }
            ... on ComponentCommonEmbedVideo {
              id
              type
              url
            }
          }
        }
      }
    }
  }
  ${ImageFragmentDoc}
  ${GalleryFragmentDoc}
`;

/**
 * __usePostQuery__
 *
 * To run a query within a React component, call `usePostQuery` and pass it any options that fit your needs.
 * When your component renders, `usePostQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePostQuery({
 *   variables: {
 *      slug: // value for 'slug'
 *      publicationState: // value for 'publicationState'
 *   },
 * });
 */
export function usePostQuery(baseOptions: Apollo.QueryHookOptions<PostQuery, PostQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<PostQuery, PostQueryVariables>(PostDocument, options);
}
export function usePostLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<PostQuery, PostQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<PostQuery, PostQueryVariables>(PostDocument, options);
}
export type PostQueryHookResult = ReturnType<typeof usePostQuery>;
export type PostLazyQueryHookResult = ReturnType<typeof usePostLazyQuery>;
export type PostQueryResult = Apollo.QueryResult<PostQuery, PostQueryVariables>;
export const PostsCompositeDocument = gql`
  query postsComposite($categoryCode: String, $dateRange: DateRangeInput, $pagination: PaginationArg) {
    postsComposite(categoryCode: $categoryCode, dateRange: $dateRange, pagination: $pagination) {
      data {
        __typename
        ... on PostEntity {
          id
          attributes {
            title
            slug
            perex
            pinToTop
            publishAt
            author
            categories {
              data {
                id
                attributes {
                  name
                  code
                }
              }
            }
            thumbAuthor
            thumb {
              data {
                ...Image
              }
            }
          }
        }
        ... on PostExternalComposite {
          id
          title
          url
          perex
          publishAt
          imageUrl
          categories {
            id
            attributes {
              name
              code
            }
          }
        }
      }
      meta {
        pagination {
          page
          pageCount
          pageSize
          total
        }
      }
    }
  }
  ${ImageFragmentDoc}
`;

/**
 * __usePostsCompositeQuery__
 *
 * To run a query within a React component, call `usePostsCompositeQuery` and pass it any options that fit your needs.
 * When your component renders, `usePostsCompositeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePostsCompositeQuery({
 *   variables: {
 *      categoryCode: // value for 'categoryCode'
 *      dateRange: // value for 'dateRange'
 *      pagination: // value for 'pagination'
 *   },
 * });
 */
export function usePostsCompositeQuery(
  baseOptions?: Apollo.QueryHookOptions<PostsCompositeQuery, PostsCompositeQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<PostsCompositeQuery, PostsCompositeQueryVariables>(PostsCompositeDocument, options);
}
export function usePostsCompositeLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<PostsCompositeQuery, PostsCompositeQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<PostsCompositeQuery, PostsCompositeQueryVariables>(PostsCompositeDocument, options);
}
export type PostsCompositeQueryHookResult = ReturnType<typeof usePostsCompositeQuery>;
export type PostsCompositeLazyQueryHookResult = ReturnType<typeof usePostsCompositeLazyQuery>;
export type PostsCompositeQueryResult = Apollo.QueryResult<PostsCompositeQuery, PostsCompositeQueryVariables>;
export const StatusDocument = gql`
  query status {
    status {
      data {
        attributes {
          showIncompleteGamesData
          showMaintenance
        }
      }
    }
  }
`;

/**
 * __useStatusQuery__
 *
 * To run a query within a React component, call `useStatusQuery` and pass it any options that fit your needs.
 * When your component renders, `useStatusQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useStatusQuery({
 *   variables: {
 *   },
 * });
 */
export function useStatusQuery(baseOptions?: Apollo.QueryHookOptions<StatusQuery, StatusQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<StatusQuery, StatusQueryVariables>(StatusDocument, options);
}
export function useStatusLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<StatusQuery, StatusQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<StatusQuery, StatusQueryVariables>(StatusDocument, options);
}
export type StatusQueryHookResult = ReturnType<typeof useStatusQuery>;
export type StatusLazyQueryHookResult = ReturnType<typeof useStatusLazyQuery>;
export type StatusQueryResult = Apollo.QueryResult<StatusQuery, StatusQueryVariables>;
export const TournamentListDocument = gql`
    query tournamentList($teamId: String, $teamIds: [String], $from: Date, $to: Date, $year: String = "${process.env.SEASON_YEAR}") {
  tournamentList(
    teamId: $teamId
    teamIds: $teamIds
    year: $year
    from: $from
    to: $to
  ) {
    id
    date
    place {
      id
      systemName
      name
    }
    team {
      systemName
      name
    }
  }
}
    `;

/**
 * __useTournamentListQuery__
 *
 * To run a query within a React component, call `useTournamentListQuery` and pass it any options that fit your needs.
 * When your component renders, `useTournamentListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTournamentListQuery({
 *   variables: {
 *      teamId: // value for 'teamId'
 *      teamIds: // value for 'teamIds'
 *      from: // value for 'from'
 *      to: // value for 'to'
 *      year: // value for 'year'
 *   },
 * });
 */
export function useTournamentListQuery(
  baseOptions?: Apollo.QueryHookOptions<TournamentListQuery, TournamentListQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<TournamentListQuery, TournamentListQueryVariables>(TournamentListDocument, options);
}
export function useTournamentListLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<TournamentListQuery, TournamentListQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<TournamentListQuery, TournamentListQueryVariables>(TournamentListDocument, options);
}
export type TournamentListQueryHookResult = ReturnType<typeof useTournamentListQuery>;
export type TournamentListLazyQueryHookResult = ReturnType<typeof useTournamentListLazyQuery>;
export type TournamentListQueryResult = Apollo.QueryResult<TournamentListQuery, TournamentListQueryVariables>;
export type CategoryKeySpecifier = Array<
  | 'achievements'
  | 'code'
  | 'createdAt'
  | 'galleries'
  | 'image'
  | 'name'
  | 'officials'
  | 'updatedAt'
  | CategoryKeySpecifier
>;
export type CategoryFieldPolicy = {
  achievements?: FieldPolicy<any> | FieldReadFunction<any>;
  code?: FieldPolicy<any> | FieldReadFunction<any>;
  createdAt?: FieldPolicy<any> | FieldReadFunction<any>;
  galleries?: FieldPolicy<any> | FieldReadFunction<any>;
  image?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  officials?: FieldPolicy<any> | FieldReadFunction<any>;
  updatedAt?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CategoryEntityKeySpecifier = Array<'attributes' | 'id' | CategoryEntityKeySpecifier>;
export type CategoryEntityFieldPolicy = {
  attributes?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CategoryEntityResponseKeySpecifier = Array<'data' | CategoryEntityResponseKeySpecifier>;
export type CategoryEntityResponseFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CategoryEntityResponseCollectionKeySpecifier = Array<
  'data' | 'meta' | CategoryEntityResponseCollectionKeySpecifier
>;
export type CategoryEntityResponseCollectionFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
  meta?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CategoryRelationResponseCollectionKeySpecifier = Array<
  'data' | CategoryRelationResponseCollectionKeySpecifier
>;
export type CategoryRelationResponseCollectionFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ClubPageKeySpecifier = Array<
  'content' | 'createdAt' | 'publishedAt' | 'slug' | 'title' | 'updatedAt' | ClubPageKeySpecifier
>;
export type ClubPageFieldPolicy = {
  content?: FieldPolicy<any> | FieldReadFunction<any>;
  createdAt?: FieldPolicy<any> | FieldReadFunction<any>;
  publishedAt?: FieldPolicy<any> | FieldReadFunction<any>;
  slug?: FieldPolicy<any> | FieldReadFunction<any>;
  title?: FieldPolicy<any> | FieldReadFunction<any>;
  updatedAt?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ClubPageEntityKeySpecifier = Array<'attributes' | 'id' | ClubPageEntityKeySpecifier>;
export type ClubPageEntityFieldPolicy = {
  attributes?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ClubPageEntityResponseKeySpecifier = Array<'data' | ClubPageEntityResponseKeySpecifier>;
export type ClubPageEntityResponseFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ClubPageEntityResponseCollectionKeySpecifier = Array<
  'data' | 'meta' | ClubPageEntityResponseCollectionKeySpecifier
>;
export type ClubPageEntityResponseCollectionFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
  meta?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbGameDetailGoalkeepersKeySpecifier = Array<'away' | 'home' | CmshbGameDetailGoalkeepersKeySpecifier>;
export type CmshbGameDetailGoalkeepersFieldPolicy = {
  away?: FieldPolicy<any> | FieldReadFunction<any>;
  home?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbGameDetailGoalsKeySpecifier = Array<'away' | 'home' | CmshbGameDetailGoalsKeySpecifier>;
export type CmshbGameDetailGoalsFieldPolicy = {
  away?: FieldPolicy<any> | FieldReadFunction<any>;
  home?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbGameDetailPenaltiesKeySpecifier = Array<'away' | 'home' | CmshbGameDetailPenaltiesKeySpecifier>;
export type CmshbGameDetailPenaltiesFieldPolicy = {
  away?: FieldPolicy<any> | FieldReadFunction<any>;
  home?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbGameDetailPlayersKeySpecifier = Array<'away' | 'home' | CmshbGameDetailPlayersKeySpecifier>;
export type CmshbGameDetailPlayersFieldPolicy = {
  away?: FieldPolicy<any> | FieldReadFunction<any>;
  home?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbGameDetailPostKeySpecifier = Array<'content' | 'perex' | 'title' | CmshbGameDetailPostKeySpecifier>;
export type CmshbGameDetailPostFieldPolicy = {
  content?: FieldPolicy<any> | FieldReadFunction<any>;
  perex?: FieldPolicy<any> | FieldReadFunction<any>;
  title?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbGameDetailShootoutsKeySpecifier = Array<'away' | 'home' | CmshbGameDetailShootoutsKeySpecifier>;
export type CmshbGameDetailShootoutsFieldPolicy = {
  away?: FieldPolicy<any> | FieldReadFunction<any>;
  home?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbGameDetailStatsKeySpecifier = Array<'away' | 'home' | CmshbGameDetailStatsKeySpecifier>;
export type CmshbGameDetailStatsFieldPolicy = {
  away?: FieldPolicy<any> | FieldReadFunction<any>;
  home?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbGameDetailTeamGoalkeepersKeySpecifier = Array<
  | 'assists'
  | 'goals'
  | 'goalsAgainst'
  | 'number'
  | 'penalties'
  | 'player'
  | 'savesAverage'
  | 'shotsAgainst'
  | 'timeInSeconds'
  | CmshbGameDetailTeamGoalkeepersKeySpecifier
>;
export type CmshbGameDetailTeamGoalkeepersFieldPolicy = {
  assists?: FieldPolicy<any> | FieldReadFunction<any>;
  goals?: FieldPolicy<any> | FieldReadFunction<any>;
  goalsAgainst?: FieldPolicy<any> | FieldReadFunction<any>;
  number?: FieldPolicy<any> | FieldReadFunction<any>;
  penalties?: FieldPolicy<any> | FieldReadFunction<any>;
  player?: FieldPolicy<any> | FieldReadFunction<any>;
  savesAverage?: FieldPolicy<any> | FieldReadFunction<any>;
  shotsAgainst?: FieldPolicy<any> | FieldReadFunction<any>;
  timeInSeconds?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbGameDetailTeamGoalsKeySpecifier = Array<
  'firstAssist' | 'goal' | 'secondAssist' | 'timeInSeconds' | 'type' | CmshbGameDetailTeamGoalsKeySpecifier
>;
export type CmshbGameDetailTeamGoalsFieldPolicy = {
  firstAssist?: FieldPolicy<any> | FieldReadFunction<any>;
  goal?: FieldPolicy<any> | FieldReadFunction<any>;
  secondAssist?: FieldPolicy<any> | FieldReadFunction<any>;
  timeInSeconds?: FieldPolicy<any> | FieldReadFunction<any>;
  type?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbGameDetailTeamPenaltiesKeySpecifier = Array<
  'penalty' | 'penaltyMinutes' | 'penaltyType' | 'player' | 'timeInSeconds' | CmshbGameDetailTeamPenaltiesKeySpecifier
>;
export type CmshbGameDetailTeamPenaltiesFieldPolicy = {
  penalty?: FieldPolicy<any> | FieldReadFunction<any>;
  penaltyMinutes?: FieldPolicy<any> | FieldReadFunction<any>;
  penaltyType?: FieldPolicy<any> | FieldReadFunction<any>;
  player?: FieldPolicy<any> | FieldReadFunction<any>;
  timeInSeconds?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbGameDetailTeamPlayersKeySpecifier = Array<
  'assists' | 'goals' | 'number' | 'penalties' | 'player' | 'points' | 'post' | CmshbGameDetailTeamPlayersKeySpecifier
>;
export type CmshbGameDetailTeamPlayersFieldPolicy = {
  assists?: FieldPolicy<any> | FieldReadFunction<any>;
  goals?: FieldPolicy<any> | FieldReadFunction<any>;
  number?: FieldPolicy<any> | FieldReadFunction<any>;
  penalties?: FieldPolicy<any> | FieldReadFunction<any>;
  player?: FieldPolicy<any> | FieldReadFunction<any>;
  points?: FieldPolicy<any> | FieldReadFunction<any>;
  post?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbGameDetailTeamShootoutsKeySpecifier = Array<
  'isGoal' | 'player' | CmshbGameDetailTeamShootoutsKeySpecifier
>;
export type CmshbGameDetailTeamShootoutsFieldPolicy = {
  isGoal?: FieldPolicy<any> | FieldReadFunction<any>;
  player?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbGameDetailTeamStatsKeySpecifier = Array<
  'penalties' | 'penaltyKillGoals' | 'powerPlayGoals' | 'powerPlays' | 'shots' | CmshbGameDetailTeamStatsKeySpecifier
>;
export type CmshbGameDetailTeamStatsFieldPolicy = {
  penalties?: FieldPolicy<any> | FieldReadFunction<any>;
  penaltyKillGoals?: FieldPolicy<any> | FieldReadFunction<any>;
  powerPlayGoals?: FieldPolicy<any> | FieldReadFunction<any>;
  powerPlays?: FieldPolicy<any> | FieldReadFunction<any>;
  shots?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbGoalkeeperStatsKeySpecifier = Array<
  | 'assists'
  | 'games'
  | 'goalsAgainst'
  | 'goalsAgainstAverage'
  | 'minutes'
  | 'penalties'
  | 'player'
  | 'saves'
  | 'savesAverage'
  | 'shotsAgainst'
  | 'shutouts'
  | CmshbGoalkeeperStatsKeySpecifier
>;
export type CmshbGoalkeeperStatsFieldPolicy = {
  assists?: FieldPolicy<any> | FieldReadFunction<any>;
  games?: FieldPolicy<any> | FieldReadFunction<any>;
  goalsAgainst?: FieldPolicy<any> | FieldReadFunction<any>;
  goalsAgainstAverage?: FieldPolicy<any> | FieldReadFunction<any>;
  minutes?: FieldPolicy<any> | FieldReadFunction<any>;
  penalties?: FieldPolicy<any> | FieldReadFunction<any>;
  player?: FieldPolicy<any> | FieldReadFunction<any>;
  saves?: FieldPolicy<any> | FieldReadFunction<any>;
  savesAverage?: FieldPolicy<any> | FieldReadFunction<any>;
  shotsAgainst?: FieldPolicy<any> | FieldReadFunction<any>;
  shutouts?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbLeagueKeySpecifier = Array<'id' | 'leagueId' | 'name' | 'order' | CmshbLeagueKeySpecifier>;
export type CmshbLeagueFieldPolicy = {
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  leagueId?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  order?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbLeagueGroupKeySpecifier = Array<
  'id' | 'label' | 'league' | 'metadata' | 'name' | 'order' | 'skuId' | 'type' | CmshbLeagueGroupKeySpecifier
>;
export type CmshbLeagueGroupFieldPolicy = {
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  label?: FieldPolicy<any> | FieldReadFunction<any>;
  league?: FieldPolicy<any> | FieldReadFunction<any>;
  metadata?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  order?: FieldPolicy<any> | FieldReadFunction<any>;
  skuId?: FieldPolicy<any> | FieldReadFunction<any>;
  type?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbPlayerKeySpecifier = Array<'cmshbId' | 'id' | 'name' | CmshbPlayerKeySpecifier>;
export type CmshbPlayerFieldPolicy = {
  cmshbId?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbPlayerStatsKeySpecifier = Array<
  | 'assists'
  | 'games'
  | 'goals'
  | 'penalties'
  | 'penaltyKillGoals'
  | 'player'
  | 'points'
  | 'powerPlayGoals'
  | CmshbPlayerStatsKeySpecifier
>;
export type CmshbPlayerStatsFieldPolicy = {
  assists?: FieldPolicy<any> | FieldReadFunction<any>;
  games?: FieldPolicy<any> | FieldReadFunction<any>;
  goals?: FieldPolicy<any> | FieldReadFunction<any>;
  penalties?: FieldPolicy<any> | FieldReadFunction<any>;
  penaltyKillGoals?: FieldPolicy<any> | FieldReadFunction<any>;
  player?: FieldPolicy<any> | FieldReadFunction<any>;
  points?: FieldPolicy<any> | FieldReadFunction<any>;
  powerPlayGoals?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbPlayerStatsListKeySpecifier = Array<
  'goalkeepers' | 'leagueGroup' | 'players' | CmshbPlayerStatsListKeySpecifier
>;
export type CmshbPlayerStatsListFieldPolicy = {
  goalkeepers?: FieldPolicy<any> | FieldReadFunction<any>;
  leagueGroup?: FieldPolicy<any> | FieldReadFunction<any>;
  players?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbTeamKeySpecifier = Array<'cmshbId' | 'id' | 'name' | 'shortName' | CmshbTeamKeySpecifier>;
export type CmshbTeamFieldPolicy = {
  cmshbId?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  shortName?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbTeamTableKeySpecifier = Array<'leagueGroup' | 'stats' | CmshbTeamTableKeySpecifier>;
export type CmshbTeamTableFieldPolicy = {
  leagueGroup?: FieldPolicy<any> | FieldReadFunction<any>;
  stats?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CmshbTeamTableStatsKeySpecifier = Array<
  | 'games'
  | 'goals'
  | 'goalsAgainst'
  | 'goalsAgainstAverage'
  | 'goalsAverage'
  | 'losses'
  | 'overtimeLosses'
  | 'overtimeWins'
  | 'penalties'
  | 'penaltiesAverage'
  | 'penaltyKillGoals'
  | 'penaltyKillGoalsAgainst'
  | 'penaltyKills'
  | 'penaltyKillsPercentage'
  | 'points'
  | 'powerPlayGoals'
  | 'powerPlayGoalsAgainst'
  | 'powerPlays'
  | 'powerPlaysPercentage'
  | 'standing'
  | 'team'
  | 'wins'
  | CmshbTeamTableStatsKeySpecifier
>;
export type CmshbTeamTableStatsFieldPolicy = {
  games?: FieldPolicy<any> | FieldReadFunction<any>;
  goals?: FieldPolicy<any> | FieldReadFunction<any>;
  goalsAgainst?: FieldPolicy<any> | FieldReadFunction<any>;
  goalsAgainstAverage?: FieldPolicy<any> | FieldReadFunction<any>;
  goalsAverage?: FieldPolicy<any> | FieldReadFunction<any>;
  losses?: FieldPolicy<any> | FieldReadFunction<any>;
  overtimeLosses?: FieldPolicy<any> | FieldReadFunction<any>;
  overtimeWins?: FieldPolicy<any> | FieldReadFunction<any>;
  penalties?: FieldPolicy<any> | FieldReadFunction<any>;
  penaltiesAverage?: FieldPolicy<any> | FieldReadFunction<any>;
  penaltyKillGoals?: FieldPolicy<any> | FieldReadFunction<any>;
  penaltyKillGoalsAgainst?: FieldPolicy<any> | FieldReadFunction<any>;
  penaltyKills?: FieldPolicy<any> | FieldReadFunction<any>;
  penaltyKillsPercentage?: FieldPolicy<any> | FieldReadFunction<any>;
  points?: FieldPolicy<any> | FieldReadFunction<any>;
  powerPlayGoals?: FieldPolicy<any> | FieldReadFunction<any>;
  powerPlayGoalsAgainst?: FieldPolicy<any> | FieldReadFunction<any>;
  powerPlays?: FieldPolicy<any> | FieldReadFunction<any>;
  powerPlaysPercentage?: FieldPolicy<any> | FieldReadFunction<any>;
  standing?: FieldPolicy<any> | FieldReadFunction<any>;
  team?: FieldPolicy<any> | FieldReadFunction<any>;
  wins?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ComponentCommonAchievementKeySpecifier = Array<
  'id' | 'image' | 'isWinner' | 'title' | 'years' | ComponentCommonAchievementKeySpecifier
>;
export type ComponentCommonAchievementFieldPolicy = {
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  image?: FieldPolicy<any> | FieldReadFunction<any>;
  isWinner?: FieldPolicy<any> | FieldReadFunction<any>;
  title?: FieldPolicy<any> | FieldReadFunction<any>;
  years?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ComponentCommonAchievementListKeySpecifier = Array<
  'achievements' | 'id' | 'title' | ComponentCommonAchievementListKeySpecifier
>;
export type ComponentCommonAchievementListFieldPolicy = {
  achievements?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  title?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ComponentCommonApiGameKeySpecifier = Array<'game' | 'id' | ComponentCommonApiGameKeySpecifier>;
export type ComponentCommonApiGameFieldPolicy = {
  game?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ComponentCommonApiGameListKeySpecifier = Array<'gameList' | 'id' | ComponentCommonApiGameListKeySpecifier>;
export type ComponentCommonApiGameListFieldPolicy = {
  gameList?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ComponentCommonEmbedVideoKeySpecifier = Array<
  'id' | 'type' | 'url' | ComponentCommonEmbedVideoKeySpecifier
>;
export type ComponentCommonEmbedVideoFieldPolicy = {
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  type?: FieldPolicy<any> | FieldReadFunction<any>;
  url?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ComponentCommonGalleryKeySpecifier = Array<'gallery' | 'id' | 'text' | ComponentCommonGalleryKeySpecifier>;
export type ComponentCommonGalleryFieldPolicy = {
  gallery?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  text?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ComponentCommonOfficialKeySpecifier = Array<
  'id' | 'person' | 'title' | ComponentCommonOfficialKeySpecifier
>;
export type ComponentCommonOfficialFieldPolicy = {
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  person?: FieldPolicy<any> | FieldReadFunction<any>;
  title?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ComponentCommonOfficialListKeySpecifier = Array<
  'id' | 'officials' | 'showContactInformation' | 'title' | ComponentCommonOfficialListKeySpecifier
>;
export type ComponentCommonOfficialListFieldPolicy = {
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  officials?: FieldPolicy<any> | FieldReadFunction<any>;
  showContactInformation?: FieldPolicy<any> | FieldReadFunction<any>;
  title?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ComponentCommonPartnerKeySpecifier = Array<
  'id' | 'image' | 'name' | 'url' | ComponentCommonPartnerKeySpecifier
>;
export type ComponentCommonPartnerFieldPolicy = {
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  image?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  url?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ComponentCommonPartnerListKeySpecifier = Array<
  'id' | 'name' | 'partners' | 'slug' | ComponentCommonPartnerListKeySpecifier
>;
export type ComponentCommonPartnerListFieldPolicy = {
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  partners?: FieldPolicy<any> | FieldReadFunction<any>;
  slug?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ComponentCommonRichTextKeySpecifier = Array<'id' | 'text' | ComponentCommonRichTextKeySpecifier>;
export type ComponentCommonRichTextFieldPolicy = {
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  text?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ComponentPlayerTeamPlayerKeySpecifier = Array<
  'id' | 'number' | 'post' | 'stick' | 'team' | ComponentPlayerTeamPlayerKeySpecifier
>;
export type ComponentPlayerTeamPlayerFieldPolicy = {
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  number?: FieldPolicy<any> | FieldReadFunction<any>;
  post?: FieldPolicy<any> | FieldReadFunction<any>;
  stick?: FieldPolicy<any> | FieldReadFunction<any>;
  team?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CreatorKeySpecifier = Array<'firstname' | 'id' | 'lastname' | CreatorKeySpecifier>;
export type CreatorFieldPolicy = {
  firstname?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  lastname?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ErrorKeySpecifier = Array<'code' | 'message' | ErrorKeySpecifier>;
export type ErrorFieldPolicy = {
  code?: FieldPolicy<any> | FieldReadFunction<any>;
  message?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ExternalPostKeySpecifier = Array<
  'categories' | 'createdAt' | 'pinToTop' | 'publishAt' | 'source' | 'updatedAt' | 'url' | ExternalPostKeySpecifier
>;
export type ExternalPostFieldPolicy = {
  categories?: FieldPolicy<any> | FieldReadFunction<any>;
  createdAt?: FieldPolicy<any> | FieldReadFunction<any>;
  pinToTop?: FieldPolicy<any> | FieldReadFunction<any>;
  publishAt?: FieldPolicy<any> | FieldReadFunction<any>;
  source?: FieldPolicy<any> | FieldReadFunction<any>;
  updatedAt?: FieldPolicy<any> | FieldReadFunction<any>;
  url?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ExternalPostEntityKeySpecifier = Array<'attributes' | 'id' | ExternalPostEntityKeySpecifier>;
export type ExternalPostEntityFieldPolicy = {
  attributes?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ExternalPostEntityResponseKeySpecifier = Array<'data' | ExternalPostEntityResponseKeySpecifier>;
export type ExternalPostEntityResponseFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ExternalPostEntityResponseCollectionKeySpecifier = Array<
  'data' | 'meta' | ExternalPostEntityResponseCollectionKeySpecifier
>;
export type ExternalPostEntityResponseCollectionFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
  meta?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type GalleryKeySpecifier = Array<
  'category' | 'createdAt' | 'images' | 'title' | 'updatedAt' | GalleryKeySpecifier
>;
export type GalleryFieldPolicy = {
  category?: FieldPolicy<any> | FieldReadFunction<any>;
  createdAt?: FieldPolicy<any> | FieldReadFunction<any>;
  images?: FieldPolicy<any> | FieldReadFunction<any>;
  title?: FieldPolicy<any> | FieldReadFunction<any>;
  updatedAt?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type GalleryEntityKeySpecifier = Array<'attributes' | 'id' | GalleryEntityKeySpecifier>;
export type GalleryEntityFieldPolicy = {
  attributes?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type GalleryEntityResponseKeySpecifier = Array<'data' | GalleryEntityResponseKeySpecifier>;
export type GalleryEntityResponseFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type GalleryEntityResponseCollectionKeySpecifier = Array<
  'data' | 'meta' | GalleryEntityResponseCollectionKeySpecifier
>;
export type GalleryEntityResponseCollectionFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
  meta?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type GalleryRelationResponseCollectionKeySpecifier = Array<
  'data' | GalleryRelationResponseCollectionKeySpecifier
>;
export type GalleryRelationResponseCollectionFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type GameKeySpecifier = Array<
  | 'away'
  | 'cmshbId'
  | 'date'
  | 'home'
  | 'id'
  | 'kertId'
  | 'label'
  | 'metadata'
  | 'place'
  | 'score'
  | 'status'
  | 'team'
  | GameKeySpecifier
>;
export type GameFieldPolicy = {
  away?: FieldPolicy<any> | FieldReadFunction<any>;
  cmshbId?: FieldPolicy<any> | FieldReadFunction<any>;
  date?: FieldPolicy<any> | FieldReadFunction<any>;
  home?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  kertId?: FieldPolicy<any> | FieldReadFunction<any>;
  label?: FieldPolicy<any> | FieldReadFunction<any>;
  metadata?: FieldPolicy<any> | FieldReadFunction<any>;
  place?: FieldPolicy<any> | FieldReadFunction<any>;
  score?: FieldPolicy<any> | FieldReadFunction<any>;
  status?: FieldPolicy<any> | FieldReadFunction<any>;
  team?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type GameMetadataKeySpecifier = Array<
  'hasAiReport' | 'highlightsVideo' | 'liveVideo' | 'onlajnyCz' | 'reportPostSlug' | GameMetadataKeySpecifier
>;
export type GameMetadataFieldPolicy = {
  hasAiReport?: FieldPolicy<any> | FieldReadFunction<any>;
  highlightsVideo?: FieldPolicy<any> | FieldReadFunction<any>;
  liveVideo?: FieldPolicy<any> | FieldReadFunction<any>;
  onlajnyCz?: FieldPolicy<any> | FieldReadFunction<any>;
  reportPostSlug?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type GameMetadataVideoKeySpecifier = Array<'type' | 'url' | GameMetadataVideoKeySpecifier>;
export type GameMetadataVideoFieldPolicy = {
  type?: FieldPolicy<any> | FieldReadFunction<any>;
  url?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type GameScoreKeySpecifier = Array<'away' | 'home' | GameScoreKeySpecifier>;
export type GameScoreFieldPolicy = {
  away?: FieldPolicy<any> | FieldReadFunction<any>;
  home?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type GameTeamScoreKeySpecifier = Array<
  'finalScore' | 'firstPeriod' | 'overtime' | 'secondPeriod' | 'shootout' | 'thirdPeriod' | GameTeamScoreKeySpecifier
>;
export type GameTeamScoreFieldPolicy = {
  finalScore?: FieldPolicy<any> | FieldReadFunction<any>;
  firstPeriod?: FieldPolicy<any> | FieldReadFunction<any>;
  overtime?: FieldPolicy<any> | FieldReadFunction<any>;
  secondPeriod?: FieldPolicy<any> | FieldReadFunction<any>;
  shootout?: FieldPolicy<any> | FieldReadFunction<any>;
  thirdPeriod?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type GameWithCmshbGameDetailKeySpecifier = Array<
  | 'away'
  | 'cmshbId'
  | 'date'
  | 'goalkeepers'
  | 'goals'
  | 'home'
  | 'id'
  | 'kertId'
  | 'label'
  | 'metadata'
  | 'penalties'
  | 'place'
  | 'players'
  | 'post'
  | 'score'
  | 'shootouts'
  | 'statistics'
  | 'status'
  | 'team'
  | GameWithCmshbGameDetailKeySpecifier
>;
export type GameWithCmshbGameDetailFieldPolicy = {
  away?: FieldPolicy<any> | FieldReadFunction<any>;
  cmshbId?: FieldPolicy<any> | FieldReadFunction<any>;
  date?: FieldPolicy<any> | FieldReadFunction<any>;
  goalkeepers?: FieldPolicy<any> | FieldReadFunction<any>;
  goals?: FieldPolicy<any> | FieldReadFunction<any>;
  home?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  kertId?: FieldPolicy<any> | FieldReadFunction<any>;
  label?: FieldPolicy<any> | FieldReadFunction<any>;
  metadata?: FieldPolicy<any> | FieldReadFunction<any>;
  penalties?: FieldPolicy<any> | FieldReadFunction<any>;
  place?: FieldPolicy<any> | FieldReadFunction<any>;
  players?: FieldPolicy<any> | FieldReadFunction<any>;
  post?: FieldPolicy<any> | FieldReadFunction<any>;
  score?: FieldPolicy<any> | FieldReadFunction<any>;
  shootouts?: FieldPolicy<any> | FieldReadFunction<any>;
  statistics?: FieldPolicy<any> | FieldReadFunction<any>;
  status?: FieldPolicy<any> | FieldReadFunction<any>;
  team?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type HokejbalCzArticleKeySpecifier = Array<
  'date' | 'id' | 'imageUrl' | 'perex' | 'title' | HokejbalCzArticleKeySpecifier
>;
export type HokejbalCzArticleFieldPolicy = {
  date?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  imageUrl?: FieldPolicy<any> | FieldReadFunction<any>;
  perex?: FieldPolicy<any> | FieldReadFunction<any>;
  title?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type I18NLocaleKeySpecifier = Array<'code' | 'createdAt' | 'name' | 'updatedAt' | I18NLocaleKeySpecifier>;
export type I18NLocaleFieldPolicy = {
  code?: FieldPolicy<any> | FieldReadFunction<any>;
  createdAt?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  updatedAt?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type I18NLocaleEntityKeySpecifier = Array<'attributes' | 'id' | I18NLocaleEntityKeySpecifier>;
export type I18NLocaleEntityFieldPolicy = {
  attributes?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type I18NLocaleEntityResponseKeySpecifier = Array<'data' | I18NLocaleEntityResponseKeySpecifier>;
export type I18NLocaleEntityResponseFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type I18NLocaleEntityResponseCollectionKeySpecifier = Array<
  'data' | 'meta' | I18NLocaleEntityResponseCollectionKeySpecifier
>;
export type I18NLocaleEntityResponseCollectionFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
  meta?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type MutationKeySpecifier = Array<
  | 'changePassword'
  | 'createUploadFile'
  | 'createUploadFolder'
  | 'createUsersPermissionsRole'
  | 'createUsersPermissionsUser'
  | 'deleteUploadFile'
  | 'deleteUploadFolder'
  | 'deleteUsersPermissionsRole'
  | 'deleteUsersPermissionsUser'
  | 'emailConfirmation'
  | 'forgotPassword'
  | 'login'
  | 'multipleUpload'
  | 'register'
  | 'removeFile'
  | 'resetPassword'
  | 'updateFileInfo'
  | 'updateUploadFile'
  | 'updateUploadFolder'
  | 'updateUsersPermissionsRole'
  | 'updateUsersPermissionsUser'
  | 'upload'
  | MutationKeySpecifier
>;
export type MutationFieldPolicy = {
  changePassword?: FieldPolicy<any> | FieldReadFunction<any>;
  createUploadFile?: FieldPolicy<any> | FieldReadFunction<any>;
  createUploadFolder?: FieldPolicy<any> | FieldReadFunction<any>;
  createUsersPermissionsRole?: FieldPolicy<any> | FieldReadFunction<any>;
  createUsersPermissionsUser?: FieldPolicy<any> | FieldReadFunction<any>;
  deleteUploadFile?: FieldPolicy<any> | FieldReadFunction<any>;
  deleteUploadFolder?: FieldPolicy<any> | FieldReadFunction<any>;
  deleteUsersPermissionsRole?: FieldPolicy<any> | FieldReadFunction<any>;
  deleteUsersPermissionsUser?: FieldPolicy<any> | FieldReadFunction<any>;
  emailConfirmation?: FieldPolicy<any> | FieldReadFunction<any>;
  forgotPassword?: FieldPolicy<any> | FieldReadFunction<any>;
  login?: FieldPolicy<any> | FieldReadFunction<any>;
  multipleUpload?: FieldPolicy<any> | FieldReadFunction<any>;
  register?: FieldPolicy<any> | FieldReadFunction<any>;
  removeFile?: FieldPolicy<any> | FieldReadFunction<any>;
  resetPassword?: FieldPolicy<any> | FieldReadFunction<any>;
  updateFileInfo?: FieldPolicy<any> | FieldReadFunction<any>;
  updateUploadFile?: FieldPolicy<any> | FieldReadFunction<any>;
  updateUploadFolder?: FieldPolicy<any> | FieldReadFunction<any>;
  updateUsersPermissionsRole?: FieldPolicy<any> | FieldReadFunction<any>;
  updateUsersPermissionsUser?: FieldPolicy<any> | FieldReadFunction<any>;
  upload?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PaginationKeySpecifier = Array<'page' | 'pageCount' | 'pageSize' | 'total' | PaginationKeySpecifier>;
export type PaginationFieldPolicy = {
  page?: FieldPolicy<any> | FieldReadFunction<any>;
  pageCount?: FieldPolicy<any> | FieldReadFunction<any>;
  pageSize?: FieldPolicy<any> | FieldReadFunction<any>;
  total?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PartnerKeySpecifier = Array<'createdAt' | 'partnerLists' | 'updatedAt' | PartnerKeySpecifier>;
export type PartnerFieldPolicy = {
  createdAt?: FieldPolicy<any> | FieldReadFunction<any>;
  partnerLists?: FieldPolicy<any> | FieldReadFunction<any>;
  updatedAt?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PartnerEntityKeySpecifier = Array<'attributes' | 'id' | PartnerEntityKeySpecifier>;
export type PartnerEntityFieldPolicy = {
  attributes?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PartnerEntityResponseKeySpecifier = Array<'data' | PartnerEntityResponseKeySpecifier>;
export type PartnerEntityResponseFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PersonKeySpecifier = Array<
  'createdAt' | 'email' | 'image' | 'name' | 'phone' | 'updatedAt' | PersonKeySpecifier
>;
export type PersonFieldPolicy = {
  createdAt?: FieldPolicy<any> | FieldReadFunction<any>;
  email?: FieldPolicy<any> | FieldReadFunction<any>;
  image?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  phone?: FieldPolicy<any> | FieldReadFunction<any>;
  updatedAt?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PersonEntityKeySpecifier = Array<'attributes' | 'id' | PersonEntityKeySpecifier>;
export type PersonEntityFieldPolicy = {
  attributes?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PersonEntityResponseKeySpecifier = Array<'data' | PersonEntityResponseKeySpecifier>;
export type PersonEntityResponseFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PersonEntityResponseCollectionKeySpecifier = Array<
  'data' | 'meta' | PersonEntityResponseCollectionKeySpecifier
>;
export type PersonEntityResponseCollectionFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
  meta?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PlaceKeySpecifier = Array<'id' | 'name' | 'systemName' | PlaceKeySpecifier>;
export type PlaceFieldPolicy = {
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  systemName?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PlayOffKeySpecifier = Array<'id' | 'label' | 'rounds' | PlayOffKeySpecifier>;
export type PlayOffFieldPolicy = {
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  label?: FieldPolicy<any> | FieldReadFunction<any>;
  rounds?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PlayOffRoundKeySpecifier = Array<
  'games' | 'id' | 'kertTeam' | 'name' | 'opponentTeam' | 'score' | 'shortName' | 'shortcut' | PlayOffRoundKeySpecifier
>;
export type PlayOffRoundFieldPolicy = {
  games?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  kertTeam?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  opponentTeam?: FieldPolicy<any> | FieldReadFunction<any>;
  score?: FieldPolicy<any> | FieldReadFunction<any>;
  shortName?: FieldPolicy<any> | FieldReadFunction<any>;
  shortcut?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PlayOffRoundScoreKeySpecifier = Array<'kertTeam' | 'opponentTeam' | PlayOffRoundScoreKeySpecifier>;
export type PlayOffRoundScoreFieldPolicy = {
  kertTeam?: FieldPolicy<any> | FieldReadFunction<any>;
  opponentTeam?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PlayerKeySpecifier = Array<
  | 'achievements'
  | 'birth'
  | 'cmshbId'
  | 'height'
  | 'id'
  | 'largeImage'
  | 'name'
  | 'number'
  | 'post'
  | 'smallImage'
  | 'stick'
  | 'weight'
  | PlayerKeySpecifier
>;
export type PlayerFieldPolicy = {
  achievements?: FieldPolicy<any> | FieldReadFunction<any>;
  birth?: FieldPolicy<any> | FieldReadFunction<any>;
  cmshbId?: FieldPolicy<any> | FieldReadFunction<any>;
  height?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  largeImage?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  number?: FieldPolicy<any> | FieldReadFunction<any>;
  post?: FieldPolicy<any> | FieldReadFunction<any>;
  smallImage?: FieldPolicy<any> | FieldReadFunction<any>;
  stick?: FieldPolicy<any> | FieldReadFunction<any>;
  weight?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PostKeySpecifier = Array<
  | 'author'
  | 'categories'
  | 'content'
  | 'createdAt'
  | 'createdBy'
  | 'game'
  | 'perex'
  | 'pinToTop'
  | 'publishAt'
  | 'publishedAt'
  | 'slug'
  | 'thumb'
  | 'thumbAuthor'
  | 'title'
  | 'updatedAt'
  | PostKeySpecifier
>;
export type PostFieldPolicy = {
  author?: FieldPolicy<any> | FieldReadFunction<any>;
  categories?: FieldPolicy<any> | FieldReadFunction<any>;
  content?: FieldPolicy<any> | FieldReadFunction<any>;
  createdAt?: FieldPolicy<any> | FieldReadFunction<any>;
  createdBy?: FieldPolicy<any> | FieldReadFunction<any>;
  game?: FieldPolicy<any> | FieldReadFunction<any>;
  perex?: FieldPolicy<any> | FieldReadFunction<any>;
  pinToTop?: FieldPolicy<any> | FieldReadFunction<any>;
  publishAt?: FieldPolicy<any> | FieldReadFunction<any>;
  publishedAt?: FieldPolicy<any> | FieldReadFunction<any>;
  slug?: FieldPolicy<any> | FieldReadFunction<any>;
  thumb?: FieldPolicy<any> | FieldReadFunction<any>;
  thumbAuthor?: FieldPolicy<any> | FieldReadFunction<any>;
  title?: FieldPolicy<any> | FieldReadFunction<any>;
  updatedAt?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PostCompositeResponseCollectionKeySpecifier = Array<
  'data' | 'meta' | PostCompositeResponseCollectionKeySpecifier
>;
export type PostCompositeResponseCollectionFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
  meta?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PostCompositeResponseMetaKeySpecifier = Array<'pagination' | PostCompositeResponseMetaKeySpecifier>;
export type PostCompositeResponseMetaFieldPolicy = {
  pagination?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PostEntityKeySpecifier = Array<'attributes' | 'id' | PostEntityKeySpecifier>;
export type PostEntityFieldPolicy = {
  attributes?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PostEntityResponseKeySpecifier = Array<'data' | PostEntityResponseKeySpecifier>;
export type PostEntityResponseFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PostEntityResponseCollectionKeySpecifier = Array<
  'data' | 'meta' | PostEntityResponseCollectionKeySpecifier
>;
export type PostEntityResponseCollectionFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
  meta?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type PostExternalCompositeKeySpecifier = Array<
  'categories' | 'id' | 'imageUrl' | 'perex' | 'publishAt' | 'title' | 'url' | PostExternalCompositeKeySpecifier
>;
export type PostExternalCompositeFieldPolicy = {
  categories?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  imageUrl?: FieldPolicy<any> | FieldReadFunction<any>;
  perex?: FieldPolicy<any> | FieldReadFunction<any>;
  publishAt?: FieldPolicy<any> | FieldReadFunction<any>;
  title?: FieldPolicy<any> | FieldReadFunction<any>;
  url?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type QueryKeySpecifier = Array<
  | 'categories'
  | 'category'
  | 'clubPage'
  | 'clubPages'
  | 'cmshbPlayerStatsList'
  | 'cmshbTeamStats'
  | 'cmshbTeamTable'
  | 'externalPost'
  | 'externalPosts'
  | 'galleries'
  | 'gallery'
  | 'game'
  | 'gameList'
  | 'hokejbalCzArticle'
  | 'i18NLocale'
  | 'i18NLocales'
  | 'me'
  | 'partner'
  | 'people'
  | 'person'
  | 'playOffList'
  | 'player'
  | 'players'
  | 'post'
  | 'postByApiGame'
  | 'postBySlug'
  | 'posts'
  | 'postsByApiGames'
  | 'postsComposite'
  | 'seasonList'
  | 'status'
  | 'teamList'
  | 'tournamentList'
  | 'uploadFile'
  | 'uploadFiles'
  | 'uploadFolder'
  | 'uploadFolders'
  | 'usersPermissionsRole'
  | 'usersPermissionsRoles'
  | 'usersPermissionsUser'
  | 'usersPermissionsUsers'
  | QueryKeySpecifier
>;
export type QueryFieldPolicy = {
  categories?: FieldPolicy<any> | FieldReadFunction<any>;
  category?: FieldPolicy<any> | FieldReadFunction<any>;
  clubPage?: FieldPolicy<any> | FieldReadFunction<any>;
  clubPages?: FieldPolicy<any> | FieldReadFunction<any>;
  cmshbPlayerStatsList?: FieldPolicy<any> | FieldReadFunction<any>;
  cmshbTeamStats?: FieldPolicy<any> | FieldReadFunction<any>;
  cmshbTeamTable?: FieldPolicy<any> | FieldReadFunction<any>;
  externalPost?: FieldPolicy<any> | FieldReadFunction<any>;
  externalPosts?: FieldPolicy<any> | FieldReadFunction<any>;
  galleries?: FieldPolicy<any> | FieldReadFunction<any>;
  gallery?: FieldPolicy<any> | FieldReadFunction<any>;
  game?: FieldPolicy<any> | FieldReadFunction<any>;
  gameList?: FieldPolicy<any> | FieldReadFunction<any>;
  hokejbalCzArticle?: FieldPolicy<any> | FieldReadFunction<any>;
  i18NLocale?: FieldPolicy<any> | FieldReadFunction<any>;
  i18NLocales?: FieldPolicy<any> | FieldReadFunction<any>;
  me?: FieldPolicy<any> | FieldReadFunction<any>;
  partner?: FieldPolicy<any> | FieldReadFunction<any>;
  people?: FieldPolicy<any> | FieldReadFunction<any>;
  person?: FieldPolicy<any> | FieldReadFunction<any>;
  playOffList?: FieldPolicy<any> | FieldReadFunction<any>;
  player?: FieldPolicy<any> | FieldReadFunction<any>;
  players?: FieldPolicy<any> | FieldReadFunction<any>;
  post?: FieldPolicy<any> | FieldReadFunction<any>;
  postByApiGame?: FieldPolicy<any> | FieldReadFunction<any>;
  postBySlug?: FieldPolicy<any> | FieldReadFunction<any>;
  posts?: FieldPolicy<any> | FieldReadFunction<any>;
  postsByApiGames?: FieldPolicy<any> | FieldReadFunction<any>;
  postsComposite?: FieldPolicy<any> | FieldReadFunction<any>;
  seasonList?: FieldPolicy<any> | FieldReadFunction<any>;
  status?: FieldPolicy<any> | FieldReadFunction<any>;
  teamList?: FieldPolicy<any> | FieldReadFunction<any>;
  tournamentList?: FieldPolicy<any> | FieldReadFunction<any>;
  uploadFile?: FieldPolicy<any> | FieldReadFunction<any>;
  uploadFiles?: FieldPolicy<any> | FieldReadFunction<any>;
  uploadFolder?: FieldPolicy<any> | FieldReadFunction<any>;
  uploadFolders?: FieldPolicy<any> | FieldReadFunction<any>;
  usersPermissionsRole?: FieldPolicy<any> | FieldReadFunction<any>;
  usersPermissionsRoles?: FieldPolicy<any> | FieldReadFunction<any>;
  usersPermissionsUser?: FieldPolicy<any> | FieldReadFunction<any>;
  usersPermissionsUsers?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ResponseCollectionMetaKeySpecifier = Array<'pagination' | ResponseCollectionMetaKeySpecifier>;
export type ResponseCollectionMetaFieldPolicy = {
  pagination?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type SeasonKeySpecifier = Array<'seasonId' | 'year' | SeasonKeySpecifier>;
export type SeasonFieldPolicy = {
  seasonId?: FieldPolicy<any> | FieldReadFunction<any>;
  year?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type StatusKeySpecifier = Array<
  'createdAt' | 'showIncompleteGamesData' | 'showMaintenance' | 'updatedAt' | StatusKeySpecifier
>;
export type StatusFieldPolicy = {
  createdAt?: FieldPolicy<any> | FieldReadFunction<any>;
  showIncompleteGamesData?: FieldPolicy<any> | FieldReadFunction<any>;
  showMaintenance?: FieldPolicy<any> | FieldReadFunction<any>;
  updatedAt?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type StatusEntityKeySpecifier = Array<'attributes' | 'id' | StatusEntityKeySpecifier>;
export type StatusEntityFieldPolicy = {
  attributes?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type StatusEntityResponseKeySpecifier = Array<'data' | StatusEntityResponseKeySpecifier>;
export type StatusEntityResponseFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type TeamKeySpecifier = Array<'name' | 'systemName' | TeamKeySpecifier>;
export type TeamFieldPolicy = {
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  systemName?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type TournamentKeySpecifier = Array<'date' | 'id' | 'place' | 'team' | TournamentKeySpecifier>;
export type TournamentFieldPolicy = {
  date?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  place?: FieldPolicy<any> | FieldReadFunction<any>;
  team?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UploadFileKeySpecifier = Array<
  | 'alternativeText'
  | 'caption'
  | 'createdAt'
  | 'ext'
  | 'formats'
  | 'hash'
  | 'height'
  | 'mime'
  | 'name'
  | 'previewUrl'
  | 'provider'
  | 'provider_metadata'
  | 'related'
  | 'size'
  | 'updatedAt'
  | 'url'
  | 'width'
  | UploadFileKeySpecifier
>;
export type UploadFileFieldPolicy = {
  alternativeText?: FieldPolicy<any> | FieldReadFunction<any>;
  caption?: FieldPolicy<any> | FieldReadFunction<any>;
  createdAt?: FieldPolicy<any> | FieldReadFunction<any>;
  ext?: FieldPolicy<any> | FieldReadFunction<any>;
  formats?: FieldPolicy<any> | FieldReadFunction<any>;
  hash?: FieldPolicy<any> | FieldReadFunction<any>;
  height?: FieldPolicy<any> | FieldReadFunction<any>;
  mime?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  previewUrl?: FieldPolicy<any> | FieldReadFunction<any>;
  provider?: FieldPolicy<any> | FieldReadFunction<any>;
  provider_metadata?: FieldPolicy<any> | FieldReadFunction<any>;
  related?: FieldPolicy<any> | FieldReadFunction<any>;
  size?: FieldPolicy<any> | FieldReadFunction<any>;
  updatedAt?: FieldPolicy<any> | FieldReadFunction<any>;
  url?: FieldPolicy<any> | FieldReadFunction<any>;
  width?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UploadFileEntityKeySpecifier = Array<'attributes' | 'id' | UploadFileEntityKeySpecifier>;
export type UploadFileEntityFieldPolicy = {
  attributes?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UploadFileEntityResponseKeySpecifier = Array<'data' | UploadFileEntityResponseKeySpecifier>;
export type UploadFileEntityResponseFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UploadFileEntityResponseCollectionKeySpecifier = Array<
  'data' | 'meta' | UploadFileEntityResponseCollectionKeySpecifier
>;
export type UploadFileEntityResponseCollectionFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
  meta?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UploadFileRelationResponseCollectionKeySpecifier = Array<
  'data' | UploadFileRelationResponseCollectionKeySpecifier
>;
export type UploadFileRelationResponseCollectionFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UploadFolderKeySpecifier = Array<
  'children' | 'createdAt' | 'files' | 'name' | 'parent' | 'path' | 'pathId' | 'updatedAt' | UploadFolderKeySpecifier
>;
export type UploadFolderFieldPolicy = {
  children?: FieldPolicy<any> | FieldReadFunction<any>;
  createdAt?: FieldPolicy<any> | FieldReadFunction<any>;
  files?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  parent?: FieldPolicy<any> | FieldReadFunction<any>;
  path?: FieldPolicy<any> | FieldReadFunction<any>;
  pathId?: FieldPolicy<any> | FieldReadFunction<any>;
  updatedAt?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UploadFolderEntityKeySpecifier = Array<'attributes' | 'id' | UploadFolderEntityKeySpecifier>;
export type UploadFolderEntityFieldPolicy = {
  attributes?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UploadFolderEntityResponseKeySpecifier = Array<'data' | UploadFolderEntityResponseKeySpecifier>;
export type UploadFolderEntityResponseFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UploadFolderEntityResponseCollectionKeySpecifier = Array<
  'data' | 'meta' | UploadFolderEntityResponseCollectionKeySpecifier
>;
export type UploadFolderEntityResponseCollectionFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
  meta?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UploadFolderRelationResponseCollectionKeySpecifier = Array<
  'data' | UploadFolderRelationResponseCollectionKeySpecifier
>;
export type UploadFolderRelationResponseCollectionFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsCreateRolePayloadKeySpecifier = Array<'ok' | UsersPermissionsCreateRolePayloadKeySpecifier>;
export type UsersPermissionsCreateRolePayloadFieldPolicy = {
  ok?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsDeleteRolePayloadKeySpecifier = Array<'ok' | UsersPermissionsDeleteRolePayloadKeySpecifier>;
export type UsersPermissionsDeleteRolePayloadFieldPolicy = {
  ok?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsLoginPayloadKeySpecifier = Array<'jwt' | 'user' | UsersPermissionsLoginPayloadKeySpecifier>;
export type UsersPermissionsLoginPayloadFieldPolicy = {
  jwt?: FieldPolicy<any> | FieldReadFunction<any>;
  user?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsMeKeySpecifier = Array<
  'blocked' | 'confirmed' | 'email' | 'id' | 'role' | 'username' | UsersPermissionsMeKeySpecifier
>;
export type UsersPermissionsMeFieldPolicy = {
  blocked?: FieldPolicy<any> | FieldReadFunction<any>;
  confirmed?: FieldPolicy<any> | FieldReadFunction<any>;
  email?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  role?: FieldPolicy<any> | FieldReadFunction<any>;
  username?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsMeRoleKeySpecifier = Array<
  'description' | 'id' | 'name' | 'type' | UsersPermissionsMeRoleKeySpecifier
>;
export type UsersPermissionsMeRoleFieldPolicy = {
  description?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  type?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsPasswordPayloadKeySpecifier = Array<'ok' | UsersPermissionsPasswordPayloadKeySpecifier>;
export type UsersPermissionsPasswordPayloadFieldPolicy = {
  ok?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsPermissionKeySpecifier = Array<
  'action' | 'createdAt' | 'role' | 'updatedAt' | UsersPermissionsPermissionKeySpecifier
>;
export type UsersPermissionsPermissionFieldPolicy = {
  action?: FieldPolicy<any> | FieldReadFunction<any>;
  createdAt?: FieldPolicy<any> | FieldReadFunction<any>;
  role?: FieldPolicy<any> | FieldReadFunction<any>;
  updatedAt?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsPermissionEntityKeySpecifier = Array<
  'attributes' | 'id' | UsersPermissionsPermissionEntityKeySpecifier
>;
export type UsersPermissionsPermissionEntityFieldPolicy = {
  attributes?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsPermissionRelationResponseCollectionKeySpecifier = Array<
  'data' | UsersPermissionsPermissionRelationResponseCollectionKeySpecifier
>;
export type UsersPermissionsPermissionRelationResponseCollectionFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsRoleKeySpecifier = Array<
  | 'createdAt'
  | 'description'
  | 'name'
  | 'permissions'
  | 'type'
  | 'updatedAt'
  | 'users'
  | UsersPermissionsRoleKeySpecifier
>;
export type UsersPermissionsRoleFieldPolicy = {
  createdAt?: FieldPolicy<any> | FieldReadFunction<any>;
  description?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  permissions?: FieldPolicy<any> | FieldReadFunction<any>;
  type?: FieldPolicy<any> | FieldReadFunction<any>;
  updatedAt?: FieldPolicy<any> | FieldReadFunction<any>;
  users?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsRoleEntityKeySpecifier = Array<
  'attributes' | 'id' | UsersPermissionsRoleEntityKeySpecifier
>;
export type UsersPermissionsRoleEntityFieldPolicy = {
  attributes?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsRoleEntityResponseKeySpecifier = Array<
  'data' | UsersPermissionsRoleEntityResponseKeySpecifier
>;
export type UsersPermissionsRoleEntityResponseFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsRoleEntityResponseCollectionKeySpecifier = Array<
  'data' | 'meta' | UsersPermissionsRoleEntityResponseCollectionKeySpecifier
>;
export type UsersPermissionsRoleEntityResponseCollectionFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
  meta?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsUpdateRolePayloadKeySpecifier = Array<'ok' | UsersPermissionsUpdateRolePayloadKeySpecifier>;
export type UsersPermissionsUpdateRolePayloadFieldPolicy = {
  ok?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsUserKeySpecifier = Array<
  | 'blocked'
  | 'confirmed'
  | 'createdAt'
  | 'email'
  | 'provider'
  | 'role'
  | 'updatedAt'
  | 'username'
  | UsersPermissionsUserKeySpecifier
>;
export type UsersPermissionsUserFieldPolicy = {
  blocked?: FieldPolicy<any> | FieldReadFunction<any>;
  confirmed?: FieldPolicy<any> | FieldReadFunction<any>;
  createdAt?: FieldPolicy<any> | FieldReadFunction<any>;
  email?: FieldPolicy<any> | FieldReadFunction<any>;
  provider?: FieldPolicy<any> | FieldReadFunction<any>;
  role?: FieldPolicy<any> | FieldReadFunction<any>;
  updatedAt?: FieldPolicy<any> | FieldReadFunction<any>;
  username?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsUserEntityKeySpecifier = Array<
  'attributes' | 'id' | UsersPermissionsUserEntityKeySpecifier
>;
export type UsersPermissionsUserEntityFieldPolicy = {
  attributes?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsUserEntityResponseKeySpecifier = Array<
  'data' | UsersPermissionsUserEntityResponseKeySpecifier
>;
export type UsersPermissionsUserEntityResponseFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsUserEntityResponseCollectionKeySpecifier = Array<
  'data' | 'meta' | UsersPermissionsUserEntityResponseCollectionKeySpecifier
>;
export type UsersPermissionsUserEntityResponseCollectionFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
  meta?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type UsersPermissionsUserRelationResponseCollectionKeySpecifier = Array<
  'data' | UsersPermissionsUserRelationResponseCollectionKeySpecifier
>;
export type UsersPermissionsUserRelationResponseCollectionFieldPolicy = {
  data?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type StrictTypedTypePolicies = {
  Category?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CategoryKeySpecifier | (() => undefined | CategoryKeySpecifier);
    fields?: CategoryFieldPolicy;
  };
  CategoryEntity?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CategoryEntityKeySpecifier | (() => undefined | CategoryEntityKeySpecifier);
    fields?: CategoryEntityFieldPolicy;
  };
  CategoryEntityResponse?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CategoryEntityResponseKeySpecifier | (() => undefined | CategoryEntityResponseKeySpecifier);
    fields?: CategoryEntityResponseFieldPolicy;
  };
  CategoryEntityResponseCollection?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | CategoryEntityResponseCollectionKeySpecifier
      | (() => undefined | CategoryEntityResponseCollectionKeySpecifier);
    fields?: CategoryEntityResponseCollectionFieldPolicy;
  };
  CategoryRelationResponseCollection?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | CategoryRelationResponseCollectionKeySpecifier
      | (() => undefined | CategoryRelationResponseCollectionKeySpecifier);
    fields?: CategoryRelationResponseCollectionFieldPolicy;
  };
  ClubPage?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | ClubPageKeySpecifier | (() => undefined | ClubPageKeySpecifier);
    fields?: ClubPageFieldPolicy;
  };
  ClubPageEntity?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | ClubPageEntityKeySpecifier | (() => undefined | ClubPageEntityKeySpecifier);
    fields?: ClubPageEntityFieldPolicy;
  };
  ClubPageEntityResponse?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | ClubPageEntityResponseKeySpecifier | (() => undefined | ClubPageEntityResponseKeySpecifier);
    fields?: ClubPageEntityResponseFieldPolicy;
  };
  ClubPageEntityResponseCollection?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | ClubPageEntityResponseCollectionKeySpecifier
      | (() => undefined | ClubPageEntityResponseCollectionKeySpecifier);
    fields?: ClubPageEntityResponseCollectionFieldPolicy;
  };
  CmshbGameDetailGoalkeepers?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | CmshbGameDetailGoalkeepersKeySpecifier
      | (() => undefined | CmshbGameDetailGoalkeepersKeySpecifier);
    fields?: CmshbGameDetailGoalkeepersFieldPolicy;
  };
  CmshbGameDetailGoals?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CmshbGameDetailGoalsKeySpecifier | (() => undefined | CmshbGameDetailGoalsKeySpecifier);
    fields?: CmshbGameDetailGoalsFieldPolicy;
  };
  CmshbGameDetailPenalties?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CmshbGameDetailPenaltiesKeySpecifier | (() => undefined | CmshbGameDetailPenaltiesKeySpecifier);
    fields?: CmshbGameDetailPenaltiesFieldPolicy;
  };
  CmshbGameDetailPlayers?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CmshbGameDetailPlayersKeySpecifier | (() => undefined | CmshbGameDetailPlayersKeySpecifier);
    fields?: CmshbGameDetailPlayersFieldPolicy;
  };
  CmshbGameDetailPost?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CmshbGameDetailPostKeySpecifier | (() => undefined | CmshbGameDetailPostKeySpecifier);
    fields?: CmshbGameDetailPostFieldPolicy;
  };
  CmshbGameDetailShootouts?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CmshbGameDetailShootoutsKeySpecifier | (() => undefined | CmshbGameDetailShootoutsKeySpecifier);
    fields?: CmshbGameDetailShootoutsFieldPolicy;
  };
  CmshbGameDetailStats?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CmshbGameDetailStatsKeySpecifier | (() => undefined | CmshbGameDetailStatsKeySpecifier);
    fields?: CmshbGameDetailStatsFieldPolicy;
  };
  CmshbGameDetailTeamGoalkeepers?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | CmshbGameDetailTeamGoalkeepersKeySpecifier
      | (() => undefined | CmshbGameDetailTeamGoalkeepersKeySpecifier);
    fields?: CmshbGameDetailTeamGoalkeepersFieldPolicy;
  };
  CmshbGameDetailTeamGoals?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CmshbGameDetailTeamGoalsKeySpecifier | (() => undefined | CmshbGameDetailTeamGoalsKeySpecifier);
    fields?: CmshbGameDetailTeamGoalsFieldPolicy;
  };
  CmshbGameDetailTeamPenalties?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | CmshbGameDetailTeamPenaltiesKeySpecifier
      | (() => undefined | CmshbGameDetailTeamPenaltiesKeySpecifier);
    fields?: CmshbGameDetailTeamPenaltiesFieldPolicy;
  };
  CmshbGameDetailTeamPlayers?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | CmshbGameDetailTeamPlayersKeySpecifier
      | (() => undefined | CmshbGameDetailTeamPlayersKeySpecifier);
    fields?: CmshbGameDetailTeamPlayersFieldPolicy;
  };
  CmshbGameDetailTeamShootouts?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | CmshbGameDetailTeamShootoutsKeySpecifier
      | (() => undefined | CmshbGameDetailTeamShootoutsKeySpecifier);
    fields?: CmshbGameDetailTeamShootoutsFieldPolicy;
  };
  CmshbGameDetailTeamStats?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CmshbGameDetailTeamStatsKeySpecifier | (() => undefined | CmshbGameDetailTeamStatsKeySpecifier);
    fields?: CmshbGameDetailTeamStatsFieldPolicy;
  };
  CmshbGoalkeeperStats?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CmshbGoalkeeperStatsKeySpecifier | (() => undefined | CmshbGoalkeeperStatsKeySpecifier);
    fields?: CmshbGoalkeeperStatsFieldPolicy;
  };
  CmshbLeague?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CmshbLeagueKeySpecifier | (() => undefined | CmshbLeagueKeySpecifier);
    fields?: CmshbLeagueFieldPolicy;
  };
  CmshbLeagueGroup?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CmshbLeagueGroupKeySpecifier | (() => undefined | CmshbLeagueGroupKeySpecifier);
    fields?: CmshbLeagueGroupFieldPolicy;
  };
  CmshbPlayer?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CmshbPlayerKeySpecifier | (() => undefined | CmshbPlayerKeySpecifier);
    fields?: CmshbPlayerFieldPolicy;
  };
  CmshbPlayerStats?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CmshbPlayerStatsKeySpecifier | (() => undefined | CmshbPlayerStatsKeySpecifier);
    fields?: CmshbPlayerStatsFieldPolicy;
  };
  CmshbPlayerStatsList?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CmshbPlayerStatsListKeySpecifier | (() => undefined | CmshbPlayerStatsListKeySpecifier);
    fields?: CmshbPlayerStatsListFieldPolicy;
  };
  CmshbTeam?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CmshbTeamKeySpecifier | (() => undefined | CmshbTeamKeySpecifier);
    fields?: CmshbTeamFieldPolicy;
  };
  CmshbTeamTable?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CmshbTeamTableKeySpecifier | (() => undefined | CmshbTeamTableKeySpecifier);
    fields?: CmshbTeamTableFieldPolicy;
  };
  CmshbTeamTableStats?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CmshbTeamTableStatsKeySpecifier | (() => undefined | CmshbTeamTableStatsKeySpecifier);
    fields?: CmshbTeamTableStatsFieldPolicy;
  };
  ComponentCommonAchievement?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | ComponentCommonAchievementKeySpecifier
      | (() => undefined | ComponentCommonAchievementKeySpecifier);
    fields?: ComponentCommonAchievementFieldPolicy;
  };
  ComponentCommonAchievementList?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | ComponentCommonAchievementListKeySpecifier
      | (() => undefined | ComponentCommonAchievementListKeySpecifier);
    fields?: ComponentCommonAchievementListFieldPolicy;
  };
  ComponentCommonApiGame?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | ComponentCommonApiGameKeySpecifier | (() => undefined | ComponentCommonApiGameKeySpecifier);
    fields?: ComponentCommonApiGameFieldPolicy;
  };
  ComponentCommonApiGameList?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | ComponentCommonApiGameListKeySpecifier
      | (() => undefined | ComponentCommonApiGameListKeySpecifier);
    fields?: ComponentCommonApiGameListFieldPolicy;
  };
  ComponentCommonEmbedVideo?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | ComponentCommonEmbedVideoKeySpecifier
      | (() => undefined | ComponentCommonEmbedVideoKeySpecifier);
    fields?: ComponentCommonEmbedVideoFieldPolicy;
  };
  ComponentCommonGallery?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | ComponentCommonGalleryKeySpecifier | (() => undefined | ComponentCommonGalleryKeySpecifier);
    fields?: ComponentCommonGalleryFieldPolicy;
  };
  ComponentCommonOfficial?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | ComponentCommonOfficialKeySpecifier | (() => undefined | ComponentCommonOfficialKeySpecifier);
    fields?: ComponentCommonOfficialFieldPolicy;
  };
  ComponentCommonOfficialList?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | ComponentCommonOfficialListKeySpecifier
      | (() => undefined | ComponentCommonOfficialListKeySpecifier);
    fields?: ComponentCommonOfficialListFieldPolicy;
  };
  ComponentCommonPartner?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | ComponentCommonPartnerKeySpecifier | (() => undefined | ComponentCommonPartnerKeySpecifier);
    fields?: ComponentCommonPartnerFieldPolicy;
  };
  ComponentCommonPartnerList?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | ComponentCommonPartnerListKeySpecifier
      | (() => undefined | ComponentCommonPartnerListKeySpecifier);
    fields?: ComponentCommonPartnerListFieldPolicy;
  };
  ComponentCommonRichText?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | ComponentCommonRichTextKeySpecifier | (() => undefined | ComponentCommonRichTextKeySpecifier);
    fields?: ComponentCommonRichTextFieldPolicy;
  };
  ComponentPlayerTeamPlayer?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | ComponentPlayerTeamPlayerKeySpecifier
      | (() => undefined | ComponentPlayerTeamPlayerKeySpecifier);
    fields?: ComponentPlayerTeamPlayerFieldPolicy;
  };
  Creator?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CreatorKeySpecifier | (() => undefined | CreatorKeySpecifier);
    fields?: CreatorFieldPolicy;
  };
  Error?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | ErrorKeySpecifier | (() => undefined | ErrorKeySpecifier);
    fields?: ErrorFieldPolicy;
  };
  ExternalPost?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | ExternalPostKeySpecifier | (() => undefined | ExternalPostKeySpecifier);
    fields?: ExternalPostFieldPolicy;
  };
  ExternalPostEntity?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | ExternalPostEntityKeySpecifier | (() => undefined | ExternalPostEntityKeySpecifier);
    fields?: ExternalPostEntityFieldPolicy;
  };
  ExternalPostEntityResponse?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | ExternalPostEntityResponseKeySpecifier
      | (() => undefined | ExternalPostEntityResponseKeySpecifier);
    fields?: ExternalPostEntityResponseFieldPolicy;
  };
  ExternalPostEntityResponseCollection?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | ExternalPostEntityResponseCollectionKeySpecifier
      | (() => undefined | ExternalPostEntityResponseCollectionKeySpecifier);
    fields?: ExternalPostEntityResponseCollectionFieldPolicy;
  };
  Gallery?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | GalleryKeySpecifier | (() => undefined | GalleryKeySpecifier);
    fields?: GalleryFieldPolicy;
  };
  GalleryEntity?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | GalleryEntityKeySpecifier | (() => undefined | GalleryEntityKeySpecifier);
    fields?: GalleryEntityFieldPolicy;
  };
  GalleryEntityResponse?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | GalleryEntityResponseKeySpecifier | (() => undefined | GalleryEntityResponseKeySpecifier);
    fields?: GalleryEntityResponseFieldPolicy;
  };
  GalleryEntityResponseCollection?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | GalleryEntityResponseCollectionKeySpecifier
      | (() => undefined | GalleryEntityResponseCollectionKeySpecifier);
    fields?: GalleryEntityResponseCollectionFieldPolicy;
  };
  GalleryRelationResponseCollection?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | GalleryRelationResponseCollectionKeySpecifier
      | (() => undefined | GalleryRelationResponseCollectionKeySpecifier);
    fields?: GalleryRelationResponseCollectionFieldPolicy;
  };
  Game?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | GameKeySpecifier | (() => undefined | GameKeySpecifier);
    fields?: GameFieldPolicy;
  };
  GameMetadata?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | GameMetadataKeySpecifier | (() => undefined | GameMetadataKeySpecifier);
    fields?: GameMetadataFieldPolicy;
  };
  GameMetadataVideo?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | GameMetadataVideoKeySpecifier | (() => undefined | GameMetadataVideoKeySpecifier);
    fields?: GameMetadataVideoFieldPolicy;
  };
  GameScore?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | GameScoreKeySpecifier | (() => undefined | GameScoreKeySpecifier);
    fields?: GameScoreFieldPolicy;
  };
  GameTeamScore?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | GameTeamScoreKeySpecifier | (() => undefined | GameTeamScoreKeySpecifier);
    fields?: GameTeamScoreFieldPolicy;
  };
  GameWithCmshbGameDetail?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | GameWithCmshbGameDetailKeySpecifier | (() => undefined | GameWithCmshbGameDetailKeySpecifier);
    fields?: GameWithCmshbGameDetailFieldPolicy;
  };
  HokejbalCzArticle?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | HokejbalCzArticleKeySpecifier | (() => undefined | HokejbalCzArticleKeySpecifier);
    fields?: HokejbalCzArticleFieldPolicy;
  };
  I18NLocale?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | I18NLocaleKeySpecifier | (() => undefined | I18NLocaleKeySpecifier);
    fields?: I18NLocaleFieldPolicy;
  };
  I18NLocaleEntity?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | I18NLocaleEntityKeySpecifier | (() => undefined | I18NLocaleEntityKeySpecifier);
    fields?: I18NLocaleEntityFieldPolicy;
  };
  I18NLocaleEntityResponse?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | I18NLocaleEntityResponseKeySpecifier | (() => undefined | I18NLocaleEntityResponseKeySpecifier);
    fields?: I18NLocaleEntityResponseFieldPolicy;
  };
  I18NLocaleEntityResponseCollection?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | I18NLocaleEntityResponseCollectionKeySpecifier
      | (() => undefined | I18NLocaleEntityResponseCollectionKeySpecifier);
    fields?: I18NLocaleEntityResponseCollectionFieldPolicy;
  };
  Mutation?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | MutationKeySpecifier | (() => undefined | MutationKeySpecifier);
    fields?: MutationFieldPolicy;
  };
  Pagination?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | PaginationKeySpecifier | (() => undefined | PaginationKeySpecifier);
    fields?: PaginationFieldPolicy;
  };
  Partner?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | PartnerKeySpecifier | (() => undefined | PartnerKeySpecifier);
    fields?: PartnerFieldPolicy;
  };
  PartnerEntity?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | PartnerEntityKeySpecifier | (() => undefined | PartnerEntityKeySpecifier);
    fields?: PartnerEntityFieldPolicy;
  };
  PartnerEntityResponse?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | PartnerEntityResponseKeySpecifier | (() => undefined | PartnerEntityResponseKeySpecifier);
    fields?: PartnerEntityResponseFieldPolicy;
  };
  Person?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | PersonKeySpecifier | (() => undefined | PersonKeySpecifier);
    fields?: PersonFieldPolicy;
  };
  PersonEntity?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | PersonEntityKeySpecifier | (() => undefined | PersonEntityKeySpecifier);
    fields?: PersonEntityFieldPolicy;
  };
  PersonEntityResponse?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | PersonEntityResponseKeySpecifier | (() => undefined | PersonEntityResponseKeySpecifier);
    fields?: PersonEntityResponseFieldPolicy;
  };
  PersonEntityResponseCollection?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | PersonEntityResponseCollectionKeySpecifier
      | (() => undefined | PersonEntityResponseCollectionKeySpecifier);
    fields?: PersonEntityResponseCollectionFieldPolicy;
  };
  Place?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | PlaceKeySpecifier | (() => undefined | PlaceKeySpecifier);
    fields?: PlaceFieldPolicy;
  };
  PlayOff?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | PlayOffKeySpecifier | (() => undefined | PlayOffKeySpecifier);
    fields?: PlayOffFieldPolicy;
  };
  PlayOffRound?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | PlayOffRoundKeySpecifier | (() => undefined | PlayOffRoundKeySpecifier);
    fields?: PlayOffRoundFieldPolicy;
  };
  PlayOffRoundScore?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | PlayOffRoundScoreKeySpecifier | (() => undefined | PlayOffRoundScoreKeySpecifier);
    fields?: PlayOffRoundScoreFieldPolicy;
  };
  Player?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | PlayerKeySpecifier | (() => undefined | PlayerKeySpecifier);
    fields?: PlayerFieldPolicy;
  };
  Post?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | PostKeySpecifier | (() => undefined | PostKeySpecifier);
    fields?: PostFieldPolicy;
  };
  PostCompositeResponseCollection?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | PostCompositeResponseCollectionKeySpecifier
      | (() => undefined | PostCompositeResponseCollectionKeySpecifier);
    fields?: PostCompositeResponseCollectionFieldPolicy;
  };
  PostCompositeResponseMeta?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | PostCompositeResponseMetaKeySpecifier
      | (() => undefined | PostCompositeResponseMetaKeySpecifier);
    fields?: PostCompositeResponseMetaFieldPolicy;
  };
  PostEntity?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | PostEntityKeySpecifier | (() => undefined | PostEntityKeySpecifier);
    fields?: PostEntityFieldPolicy;
  };
  PostEntityResponse?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | PostEntityResponseKeySpecifier | (() => undefined | PostEntityResponseKeySpecifier);
    fields?: PostEntityResponseFieldPolicy;
  };
  PostEntityResponseCollection?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | PostEntityResponseCollectionKeySpecifier
      | (() => undefined | PostEntityResponseCollectionKeySpecifier);
    fields?: PostEntityResponseCollectionFieldPolicy;
  };
  PostExternalComposite?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | PostExternalCompositeKeySpecifier | (() => undefined | PostExternalCompositeKeySpecifier);
    fields?: PostExternalCompositeFieldPolicy;
  };
  Query?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | QueryKeySpecifier | (() => undefined | QueryKeySpecifier);
    fields?: QueryFieldPolicy;
  };
  ResponseCollectionMeta?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | ResponseCollectionMetaKeySpecifier | (() => undefined | ResponseCollectionMetaKeySpecifier);
    fields?: ResponseCollectionMetaFieldPolicy;
  };
  Season?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | SeasonKeySpecifier | (() => undefined | SeasonKeySpecifier);
    fields?: SeasonFieldPolicy;
  };
  Status?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | StatusKeySpecifier | (() => undefined | StatusKeySpecifier);
    fields?: StatusFieldPolicy;
  };
  StatusEntity?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | StatusEntityKeySpecifier | (() => undefined | StatusEntityKeySpecifier);
    fields?: StatusEntityFieldPolicy;
  };
  StatusEntityResponse?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | StatusEntityResponseKeySpecifier | (() => undefined | StatusEntityResponseKeySpecifier);
    fields?: StatusEntityResponseFieldPolicy;
  };
  Team?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | TeamKeySpecifier | (() => undefined | TeamKeySpecifier);
    fields?: TeamFieldPolicy;
  };
  Tournament?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | TournamentKeySpecifier | (() => undefined | TournamentKeySpecifier);
    fields?: TournamentFieldPolicy;
  };
  UploadFile?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | UploadFileKeySpecifier | (() => undefined | UploadFileKeySpecifier);
    fields?: UploadFileFieldPolicy;
  };
  UploadFileEntity?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | UploadFileEntityKeySpecifier | (() => undefined | UploadFileEntityKeySpecifier);
    fields?: UploadFileEntityFieldPolicy;
  };
  UploadFileEntityResponse?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | UploadFileEntityResponseKeySpecifier | (() => undefined | UploadFileEntityResponseKeySpecifier);
    fields?: UploadFileEntityResponseFieldPolicy;
  };
  UploadFileEntityResponseCollection?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UploadFileEntityResponseCollectionKeySpecifier
      | (() => undefined | UploadFileEntityResponseCollectionKeySpecifier);
    fields?: UploadFileEntityResponseCollectionFieldPolicy;
  };
  UploadFileRelationResponseCollection?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UploadFileRelationResponseCollectionKeySpecifier
      | (() => undefined | UploadFileRelationResponseCollectionKeySpecifier);
    fields?: UploadFileRelationResponseCollectionFieldPolicy;
  };
  UploadFolder?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | UploadFolderKeySpecifier | (() => undefined | UploadFolderKeySpecifier);
    fields?: UploadFolderFieldPolicy;
  };
  UploadFolderEntity?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | UploadFolderEntityKeySpecifier | (() => undefined | UploadFolderEntityKeySpecifier);
    fields?: UploadFolderEntityFieldPolicy;
  };
  UploadFolderEntityResponse?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UploadFolderEntityResponseKeySpecifier
      | (() => undefined | UploadFolderEntityResponseKeySpecifier);
    fields?: UploadFolderEntityResponseFieldPolicy;
  };
  UploadFolderEntityResponseCollection?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UploadFolderEntityResponseCollectionKeySpecifier
      | (() => undefined | UploadFolderEntityResponseCollectionKeySpecifier);
    fields?: UploadFolderEntityResponseCollectionFieldPolicy;
  };
  UploadFolderRelationResponseCollection?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UploadFolderRelationResponseCollectionKeySpecifier
      | (() => undefined | UploadFolderRelationResponseCollectionKeySpecifier);
    fields?: UploadFolderRelationResponseCollectionFieldPolicy;
  };
  UsersPermissionsCreateRolePayload?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UsersPermissionsCreateRolePayloadKeySpecifier
      | (() => undefined | UsersPermissionsCreateRolePayloadKeySpecifier);
    fields?: UsersPermissionsCreateRolePayloadFieldPolicy;
  };
  UsersPermissionsDeleteRolePayload?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UsersPermissionsDeleteRolePayloadKeySpecifier
      | (() => undefined | UsersPermissionsDeleteRolePayloadKeySpecifier);
    fields?: UsersPermissionsDeleteRolePayloadFieldPolicy;
  };
  UsersPermissionsLoginPayload?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UsersPermissionsLoginPayloadKeySpecifier
      | (() => undefined | UsersPermissionsLoginPayloadKeySpecifier);
    fields?: UsersPermissionsLoginPayloadFieldPolicy;
  };
  UsersPermissionsMe?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | UsersPermissionsMeKeySpecifier | (() => undefined | UsersPermissionsMeKeySpecifier);
    fields?: UsersPermissionsMeFieldPolicy;
  };
  UsersPermissionsMeRole?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | UsersPermissionsMeRoleKeySpecifier | (() => undefined | UsersPermissionsMeRoleKeySpecifier);
    fields?: UsersPermissionsMeRoleFieldPolicy;
  };
  UsersPermissionsPasswordPayload?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UsersPermissionsPasswordPayloadKeySpecifier
      | (() => undefined | UsersPermissionsPasswordPayloadKeySpecifier);
    fields?: UsersPermissionsPasswordPayloadFieldPolicy;
  };
  UsersPermissionsPermission?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UsersPermissionsPermissionKeySpecifier
      | (() => undefined | UsersPermissionsPermissionKeySpecifier);
    fields?: UsersPermissionsPermissionFieldPolicy;
  };
  UsersPermissionsPermissionEntity?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UsersPermissionsPermissionEntityKeySpecifier
      | (() => undefined | UsersPermissionsPermissionEntityKeySpecifier);
    fields?: UsersPermissionsPermissionEntityFieldPolicy;
  };
  UsersPermissionsPermissionRelationResponseCollection?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UsersPermissionsPermissionRelationResponseCollectionKeySpecifier
      | (() => undefined | UsersPermissionsPermissionRelationResponseCollectionKeySpecifier);
    fields?: UsersPermissionsPermissionRelationResponseCollectionFieldPolicy;
  };
  UsersPermissionsRole?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | UsersPermissionsRoleKeySpecifier | (() => undefined | UsersPermissionsRoleKeySpecifier);
    fields?: UsersPermissionsRoleFieldPolicy;
  };
  UsersPermissionsRoleEntity?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UsersPermissionsRoleEntityKeySpecifier
      | (() => undefined | UsersPermissionsRoleEntityKeySpecifier);
    fields?: UsersPermissionsRoleEntityFieldPolicy;
  };
  UsersPermissionsRoleEntityResponse?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UsersPermissionsRoleEntityResponseKeySpecifier
      | (() => undefined | UsersPermissionsRoleEntityResponseKeySpecifier);
    fields?: UsersPermissionsRoleEntityResponseFieldPolicy;
  };
  UsersPermissionsRoleEntityResponseCollection?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UsersPermissionsRoleEntityResponseCollectionKeySpecifier
      | (() => undefined | UsersPermissionsRoleEntityResponseCollectionKeySpecifier);
    fields?: UsersPermissionsRoleEntityResponseCollectionFieldPolicy;
  };
  UsersPermissionsUpdateRolePayload?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UsersPermissionsUpdateRolePayloadKeySpecifier
      | (() => undefined | UsersPermissionsUpdateRolePayloadKeySpecifier);
    fields?: UsersPermissionsUpdateRolePayloadFieldPolicy;
  };
  UsersPermissionsUser?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | UsersPermissionsUserKeySpecifier | (() => undefined | UsersPermissionsUserKeySpecifier);
    fields?: UsersPermissionsUserFieldPolicy;
  };
  UsersPermissionsUserEntity?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UsersPermissionsUserEntityKeySpecifier
      | (() => undefined | UsersPermissionsUserEntityKeySpecifier);
    fields?: UsersPermissionsUserEntityFieldPolicy;
  };
  UsersPermissionsUserEntityResponse?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UsersPermissionsUserEntityResponseKeySpecifier
      | (() => undefined | UsersPermissionsUserEntityResponseKeySpecifier);
    fields?: UsersPermissionsUserEntityResponseFieldPolicy;
  };
  UsersPermissionsUserEntityResponseCollection?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UsersPermissionsUserEntityResponseCollectionKeySpecifier
      | (() => undefined | UsersPermissionsUserEntityResponseCollectionKeySpecifier);
    fields?: UsersPermissionsUserEntityResponseCollectionFieldPolicy;
  };
  UsersPermissionsUserRelationResponseCollection?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | UsersPermissionsUserRelationResponseCollectionKeySpecifier
      | (() => undefined | UsersPermissionsUserRelationResponseCollectionKeySpecifier);
    fields?: UsersPermissionsUserRelationResponseCollectionFieldPolicy;
  };
};
export type TypedTypePolicies = StrictTypedTypePolicies & TypePolicies;
